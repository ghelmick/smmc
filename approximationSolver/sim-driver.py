# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 23:35:37 2019

@author: greg
"""

import os
import shlex
import subprocess

def main(nprocesses=1, nprocs=12, exe_name='cuSMMC', 
         config_file='appConfig.conf', init_config=None, sim_type=0, 
         pressure=1, dt=20, tmin=300, tmax=600, anneal=True):
    for temp in range(tmin, tmax, dt):    
        cmd = 'mpirun -n ' + str(nprocesses) + ' ./' + exe_name + ' -n ' + \
              str(nprocs) + ' -t ' + str(temp) + ' -p ' + str(pressure) + \
              ' -s ' + str(sim_type) + ' -c ' + config_file
        if anneal is True:
            if init_config is not None:
                cmd = cmd + ' -f ' + init_config
            
            init_config = 'after_' + str(temp) + '.bin'
        
        print(cmd)
        
        # format command for use with the subprocess command
        cmd_array = shlex.split(cmd)
        process = subprocess.Popen(cmd_array, stdout=subprocess.PIPE, 
                                   universal_newlines=True)
        # display the output 
        fout = open('stdout-' + str(temp) + 'K.txt', 'wt')                               
        while True:
            output = process.stdout.readline()
            print(output.strip())
            fout.write(output.strip())
            fout.write('\n')
            fout.flush()
            # Do something like plot the acceptance rates
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                # process has finished, the rest of the output
                for output in process.stdout.readlines():
                    print(output.strip())
                    fout.write(output.strip())
                    fout.write('\n')
                    fout.flush()
                break
            
        fout.flush()
        fout.close()
            
if __name__ == '__main__':
    nprocesses = 1
    sim_type = 1
    pressure = 1.0
    nprocs = 12
    config_file = 'appConfig.conf'
    exe_name = 'cuSMMC'
    init_config = None
    anneal = True
    
    dt = 20          # change in temperature
    tmin = 200       # initial temperature
    tmax = 600 + dt  # ending temperature

    main(nprocesses, nprocs, exe_name, config_file, init_config, sim_type, 
         pressure, dt, tmin, tmax, anneal) 
