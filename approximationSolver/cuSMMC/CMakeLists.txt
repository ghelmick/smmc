CMAKE_MINIMUM_REQUIRED(VERSION 3.10)

# this really sucks, but for some reason CMAKE does not pass pthreads and fopenmp correctly on linux
# so as a major hack, they are split
if (WIN32)
	PROJECT(cuSMMC_hybrid LANGUAGES CXX CUDA)

	#add_definitions( -D__USE_GPU__)
	add_definitions( -D__MINIMIZE_OUTPUT__) # only uncomment this if you speed is a concern, will not save reset points and movies
	#add_definitions( -D__USE_FLOAT__) # experimental switches data type to float instead of double
	#add_definitions( -D__USE_SIMPLE_POLYNOMIAL__) #defaults to using a precomputed hermite spline
	#add_definitions( -D__USE_INLINE_HERMITE_SPLINE__) #calculates hermite spline coefficents on the fly, reduces memory footprint
        #add_definitions( -D__USE_INLINE_SPLINE__) ##calculates spline coefficents on the fly, reduces memory footprint 
	add_definitions( -D__MAXIMUM_SPEED__) #polynomial evaluations are performed inline, no function call overhead

        #add_definitions( -D__WEIGHTED_VOLUME_ACC_RATE__ )
        add_definitions( -D__INDIVIDUAL_PARTICLE_VOLUME_ACC_RATE__ )
        #add_definitions( -D__COMBINED_PARTICLE_ACC_RATE__ )

	FILE(GLOB SOURCES "*.cu" "*.cuh" "*.cpp" "*.c" "*.h")
	ADD_EXECUTABLE(cuSMMC_hybrid ${SOURCES})

	if(NOT DEFINED CMAKE_CUDA_STANDARD)
		set(CMAKE_CUDA_STANDARD 11)
		set(CMAKE_CUDA_STANDARD_REQUIRED ON)
	endif()

	find_package(OpenMP)
	if(OpenMP_CXX_FOUND)
		target_link_libraries(cuSMMC_hybrid PUBLIC OpenMP::OpenMP_CXX)
	endif()

	find_package(MPI REQUIRED)
	target_link_libraries(cuSMMC_hybrid PUBLIC MPI::MPI_CXX)

	set(CUDA_NVCC_FLAGS
	   ${CUDA_NVCC_FLAGS};
		 -std=c++11 -O3  --default-stream per-thread)

	LIST(APPEND CUDA_NVCC_FLAGS --compiler-options -fno-strict-aliasing -lineinfo -Xptxas -dlcm=cg --default-stream per-thread)
	LIST(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_60,code=sm_60 )

	set_target_properties(cuSMMC_hybrid PROPERTIES CUDA_SEPERABLE_COMPILATION ON)

	INCLUDE_DIRECTORIES(${CUDA_INCLUDE_DIRS})

else()
	PROJECT(cuSMMC_hybrid)

	FIND_PACKAGE(CUDA REQUIRED)
	FIND_PACKAGE(MPI REQUIRED)

	add_definitions( -D__USE_GPU__)
	add_definitions( -D__MINIMIZE_OUTPUT__) # only uncomment this if you speed is a concern, will not save reset points and movies
	#add_definitions( -D__USE_FLOAT__) # experimental switches data type to float instead of double
	#add_definitions( -D__USE_SIMPLE_POLYNOMIAL__) #defaults to using a precomputed hermite spline
	#add_definitions( -D__USE_INLINE_HERMITE_SPLINE__) #calculates hermite spline coefficents on the fly, reduces memory footprint
        #add_definitions( -D__USE_INLINE_SPLINE__) ##calculates spline coefficents on the fly, reduces memory footprint 
	add_definitions( -D__MAXIMUM_SPEED__) #polynomial evaluations are performed inline, no function call overhead
 
	#add_definitions( -D__WEIGHTED_VOLUME_ACC_RATE__ )
        add_definitions( -D__INDIVIDUAL_PARTICLE_VOLUME_ACC_RATE__ )
        #add_definitions( -D__COMBINED_PARTICLE_ACC_RATE__ )

	INCLUDE(FindCUDA)

	INCLUDE_DIRECTORIES(/usr/local/cuda/include ${MPI_INCLUDE_PATH})

	set(CMAKE_CXX_FLAGS "-std=c++11 -O3")

	find_package(OpenMP)
	if (OPENMP_FOUND)
		set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
		set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
		set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
	endif()

	set(CUDA_PROPAGATE_HOST_FLAGS OFF)

	set(CUDA_NVCC_FLAGS
	   ${CUDA_NVCC_FLAGS};
		 -std=c++11 -O3  --default-stream per-thread)

	INCLUDE_DIRECTORIES(/usr/local/cuda/include ${MPI_INCLUDE_PATH})

	FILE(GLOB SOURCES "*.cu"  "*.cuh" "*.cpp" "*.c" "*.h")
	CUDA_ADD_EXECUTABLE(cuSMMC_hybrid ${SOURCES})

	LIST(APPEND CUDA_NVCC_FLAGS --compiler-options -fno-strict-aliasing -lineinfo -Xptxas -dlcm=cg --default-stream per-thread)
	LIST(APPEND CUDA_NVCC_FLAGS -gencode arch=compute_60,code=sm_60 )

	TARGET_LINK_LIBRARIES(cuSMMC_hybrid ${MPI_LIBRARIES})

endif()
