﻿#include "GPUWorkUnit.cuh"
#include "cuUtil.cuh"
#include "gpuHelper.cuh"

GPUWorkUnit::GPUWorkUnit(int nodeId_, int coprocessor_id_, int num_monomer_monomer_streams_, int num_monomer_dopant_streams_ , int num_dopant_dopant_streams_, int num_dopant_monomer_streams_)
{
    nThreadsMonomer = NUMBER_GPU_THREADS_MON;
    nThreadsDopant = NUMBER_GPU_THREADS_DOP;

    nodeId = nodeId_;
    coprocessor_id = coprocessor_id_;

    upypy = 0.0;
    upydop = 0.0;
    udopdop = 0.0;

    num_monomer_monomer_streams = num_monomer_monomer_streams_;
    num_monomer_dopant_streams = num_monomer_dopant_streams_;
    num_dopant_dopant_streams = num_dopant_dopant_streams_;
    num_dopant_monomer_streams = num_dopant_monomer_streams_;

    setDevice(coprocessor_id);

    monomer_monomer_stream = new cudaStream_t[num_monomer_monomer_streams];
    monomer_dopant_stream  = new cudaStream_t[num_monomer_dopant_streams];
    dopant_dopant_stream   = new cudaStream_t[num_dopant_dopant_streams];
    dopant_monomer_stream  = new cudaStream_t[num_dopant_monomer_streams];

    cudaError_t retVal = cudaSuccess;

    for (int i = 0; i < num_monomer_dopant_streams; i++)
    {
        retVal = cudaStreamCreateWithFlags(&monomer_dopant_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (monomer_dopant_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

    for (int i = 0; i < num_dopant_dopant_streams; i++)
    {
        retVal = cudaStreamCreateWithFlags(&dopant_dopant_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (dopant_dopant_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

    for (int i = 0; i < num_dopant_monomer_streams; i++)
    {
        retVal = cudaStreamCreateWithFlags(&dopant_monomer_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (dopant_monomer_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

    for (int i = 0; i < num_monomer_monomer_streams; i++)
    {
        // create the streams
        retVal = cudaStreamCreateWithFlags(&monomer_monomer_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (monomer_monomer_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

}

GPUWorkUnit::GPUWorkUnit(int mlb_, int mub_, int dlb_, int dub_, int nodeId_, int coprocessor_id_, int num_monomer_monomer_streams_, int num_monomer_dopant_streams_, int num_dopant_dopant_streams_, int num_dopant_monomer_streams_)
{
    nThreadsMonomer = NUMBER_GPU_THREADS_MON;
    nThreadsDopant = NUMBER_GPU_THREADS_DOP;

    nodeId = nodeId_;
    coprocessor_id = coprocessor_id_;
    mlb = mlb_;
    mub = mub_;
    dlb = dlb_;
    dub = dub_;

    upypy = 0.0;
    upydop = 0.0;
    udopdop = 0.0;

    num_monomer_monomer_streams = num_monomer_monomer_streams_;
    num_monomer_dopant_streams = num_monomer_dopant_streams_;
    num_dopant_dopant_streams = num_dopant_dopant_streams_;
    num_dopant_monomer_streams = num_dopant_monomer_streams_;

    setDevice(coprocessor_id);

    monomer_monomer_stream = new cudaStream_t[num_monomer_monomer_streams];
    monomer_dopant_stream  = new cudaStream_t[num_monomer_dopant_streams];
    dopant_dopant_stream   = new cudaStream_t[num_dopant_dopant_streams];
    dopant_monomer_stream  = new cudaStream_t[num_dopant_monomer_streams];

    cudaError_t retVal = cudaSuccess;

    for (int i = 0; i < num_monomer_dopant_streams; i++)
    {
        retVal = cudaStreamCreateWithFlags(&monomer_dopant_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (monomer_dopant_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

    for (int i = 0; i < num_dopant_dopant_streams; i++)
    {
        retVal = cudaStreamCreateWithFlags(&dopant_dopant_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (dopant_dopant_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

    for (int i = 0; i < num_dopant_monomer_streams; i++)
    {
        retVal = cudaStreamCreateWithFlags(&dopant_monomer_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (dopant_monomer_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

    for (int i = 0; i < num_monomer_monomer_streams; i++)
    {
        // create the streams
        retVal = cudaStreamCreateWithFlags(&monomer_monomer_stream[i], cudaStreamNonBlocking);
        if (retVal != cudaSuccess)
        {
            fprintf(stderr, "Failed to create stream (monomer_monomer_stream): %s\n", cudaGetErrorString(retVal));
        }
    }

}

GPUWorkUnit::~GPUWorkUnit()
{
    cleanup();
}

void GPUWorkUnit::initGPUDataStructures(PARTICLE_SYSTEM& ps, DATA_TYPE* interpolationParameters, 
    int nInterpolationRows_, int nInterpolationCols_, DATA_TYPE interpolation_delta_, int interpolation_offset_,
    int interpolationOrder_, int coefficient_offset_, int number_gpu_threads_mon, int number_gpu_threads_dop)
{
    setDevice(coprocessor_id);

    interpolation_delta = interpolation_delta_;
    interpolation_offset = interpolation_offset_;
    nInterpolationRows = nInterpolationRows_;
    nInterpolationCols = nInterpolationCols_;
    interpolationOrder = interpolationOrder_;
    interpolationCoeffOffset = coefficient_offset_;

    nThreadsMonomer = number_gpu_threads_mon;
    nThreadsDopant = number_gpu_threads_dop;

    nBlocksDopants = 
        ((dub - dlb) + nThreadsDopant - 1)
        / nThreadsDopant;
    nBlocksMonomers = 
        ((mub - mlb) + nThreadsMonomer - 1)
        / nThreadsMonomer;

    cudaError_t retVal = cudaSuccess;

    if (ps.nDopants)
    {
        // setup and copy data to the gpu
        // first allocate memory on the host to store the results
        retVal = cudaMallocHost(
            (void**)&dop_results,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" <<  endl;

        // allocate energies in shared memory
        retVal = cudaMalloc(
            (void**)&d_dop_results,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        retVal = cudaMallocHost(
            (void**)&t_dop_results,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMalloc(
            (void**)&t_d_dop_results,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMallocHost(
            (void**)&pydop_mon_mov,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMallocHost(
            (void**)&t_pydop_mon_mov,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMallocHost(
            (void**)&tr_pydop_mon_mov,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMalloc(
            (void**)&d_pydop_mon_mov,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMalloc(
            (void**)&t_d_pydop_mon_mov,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMalloc(
            (void**)&tr_d_pydop_mon_mov,
            nBlocksDopants * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMallocHost(
            (void**)&pydop_dop_mov,
            nBlocksMonomers * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        // allocate energies in shared memory
        retVal = cudaMalloc(
            (void**)&d_pydop_dop_mov,
            nBlocksMonomers * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        retVal = cudaMallocHost(
            (void**)&t_pydop_dop_mov,
            nBlocksMonomers * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

        cudaStreamSynchronize(monomer_dopant_stream[0]);
        // allocate energies in shared memory
        retVal = cudaMalloc(
            (void**)&t_d_pydop_dop_mov,
            nBlocksMonomers * sizeof(DATA_TYPE));
        if (retVal != cudaSuccess)
            cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;
    }

    // allocate energies in shared memory
    retVal = cudaMallocHost(
        (void**)&mon_results,
        nBlocksMonomers * sizeof(DATA_TYPE));
    if (retVal != cudaSuccess)
        cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

    // allocate energies in shared memory
    retVal = cudaMalloc(
        (void**)&d_mon_results,
        nBlocksMonomers * sizeof(DATA_TYPE));
    if (retVal != cudaSuccess)
        cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;
    ////
        // allocate energies in shared memory
    retVal = cudaMallocHost(
        (void**)&t_mon_results,
        nBlocksMonomers * sizeof(DATA_TYPE));
    if (retVal != cudaSuccess)
        cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

    // allocate energies in shared memory
    retVal = cudaMalloc(
        (void**)&d_t_mon_results,
        nBlocksMonomers * sizeof(DATA_TYPE));
    if (retVal != cudaSuccess)
        cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;
    ///
        // allocate energies in shared memory
    retVal = cudaMallocHost(
        (void**)&tr_mon_results,
        nBlocksMonomers * sizeof(DATA_TYPE));
    if (retVal != cudaSuccess)
        cout << "Unable to allocate block energy on host [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

    // allocate energies in shared memory
    retVal = cudaMalloc(
        (void**)&d_tr_mon_results,
        nBlocksMonomers * sizeof(DATA_TYPE));
    if (retVal != cudaSuccess)
        cout << "Unable to allocate monomer energies on GPU [" << __FILE__ << ':' << __LINE__ << ':' << __FUNCTION__ << " ]" << endl;

    // allocate memory
    allocateMemoryGPU(ps, *this);
    // copy data to the GPU
    copyMemoryToGPU(ps, *this);
    // update/set the interpolation parameters
    updateInterpolationparemeters(ps, interpolationParameters, nInterpolationRows);
}

void GPUWorkUnit::updateInterpolationparemeters(PARTICLE_SYSTEM& ps, DATA_TYPE* interpolationParameters, int nInterpolationRows_)
{
    nInterpolationRows = nInterpolationRows_;
    updateDeviceInterpolationparemeters(ps, interpolationParameters, *this);
}

void GPUWorkUnit::cleanup()
{
    setDevice(coprocessor_id);

    // free the device memory
    cudaFree(d_cm_x);
    cudaFree(d_cm_y);
    cudaFree(d_cm_z);
    cudaFree(d_dpm_x);
    cudaFree(d_dpm_y);
    cudaFree(d_dpm_z);
    cudaFree(d_mon_results);
    cudaFree(mon_results);
    cudaFree(d_t_mon_results);
    cudaFree(t_mon_results);
    cudaFree(d_tr_mon_results);
    cudaFree(tr_mon_results);
    cudaFree(t_dop_results);
    cudaFree(t_d_dop_results);

    cudaFree(pydop_mon_mov);
    cudaFree(t_pydop_mon_mov);
    cudaFree(tr_pydop_mon_mov);
    cudaFree(pydop_dop_mov);
    cudaFree(t_pydop_dop_mov);

    cudaFree(d_dop_x);
    cudaFree(d_dop_y);
    cudaFree(d_dop_z);
    cudaFree(d_dop_results);
    cudaFree(dop_results);
    cudaFree(d_pydop_mon_mov);
    cudaFree(t_d_pydop_mon_mov);
    cudaFree(tr_d_pydop_mon_mov);
    cudaFree(d_pydop_dop_mov);
    cudaFree(t_d_pydop_dop_mov);

    cudaFree(d_interpolationParameters);


    // destroy the streams
    for (int i = 0; i < num_dopant_monomer_streams; i++)
    {
        cudaStreamDestroy(monomer_monomer_stream[i]);
    }

    for (int i = 0; i < num_monomer_dopant_streams; i++)
    {
        cudaStreamDestroy(monomer_dopant_stream[i]);
    }

    for (int i = 0; i < num_dopant_dopant_streams; i++)
    {
        cudaStreamDestroy(dopant_dopant_stream[i]);
    }

    for (int i = 0; i < num_dopant_monomer_streams; i++)
    {
        cudaStreamDestroy(dopant_monomer_stream[i]);
    }
}
