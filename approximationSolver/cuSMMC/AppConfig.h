#if !defined (__APP_CONFIG_H__)
#define       __APP_CONFIG_H__

#include <string>
#include <fstream>
#include <map>
#include <iostream>
#include <vector>
#include <cmath>
#include "util.h"

using std::string;
using std::ifstream;
using std::map;
using std::vector;
using std::cout;
using std::endl;

typedef struct AppConfig
{
	int	    max_itr;
	int	    x_rep;
	int	    y_rep;
	int	    z_rep;
	int	    stat_sample_rate;
	int     movie_sample_rate;
	int	    itrs_to_skip;
	int	    monomers_per_chain;
	int	    atoms_per_monomer;
	int	    dopants_per_chain;
	int	    chains_per_unit_cell;
	int     recenter_interval;
	int     vol_chg_freq;
	int     number_gpu_threads_dop;
	int     number_gpu_threads_mon;
	int     gpu_id;
	bool    multiple_gpu;
	int     volume_const_change_itrs;
	int     volume_relaxation_itrs;
	int     volume_relaxation_period;
	int     volume_max_failed_attempts;
	bool    use_pcb;
	bool    scale_x;
	bool    scale_y;
	bool    scale_z;
	bool self_interaction;
	int simple_polynomial_order;
	DATA_TYPE interpolation_delta;

	DATA_TYPE monomer_step_size;
	DATA_TYPE monomer_init_step_size;
	DATA_TYPE monomer_min_step_size;
	DATA_TYPE monomer_max_step_size;

	DATA_TYPE	monomer_rotation_rate;

	DATA_TYPE dopant_step_size;
	DATA_TYPE dopant_init_step_size;
	DATA_TYPE dopant_min_step_size;
	DATA_TYPE dopant_max_step_size;

	DATA_TYPE volume_change_step_size;
	DATA_TYPE volume_init_step_size;
	DATA_TYPE volume_change_min_step_size;
	DATA_TYPE volume_change_max_step_size;

	DATA_TYPE g_of_r_dr;

	DATA_TYPE atmc_dt_min;
	DATA_TYPE atmc_dt_max;
	DATA_TYPE atmc_temp_min;
	DATA_TYPE atmc_temp_max;
	DATA_TYPE atmc_xsig;

	DATA_TYPE	x_off;
	DATA_TYPE	y_off;
	DATA_TYPE	z_off;
	DATA_TYPE	d_off;
	DATA_TYPE  r_cut;
	DATA_TYPE  dp_mag;
	DATA_TYPE  angle_limit;
	DATA_TYPE  oligomer_mass;
	DATA_TYPE  dopant_mass;

	bool single_chain;

	map<string, DATA_TYPE> constants;

	char COMMENT = '#';

	// Default-constructor is private, to prevent client code from creating new
	// instances of this class. The only instance shall be retrieved through the
	// get_instance() function.
	AppConfig(const string & fname)
	{
		//string fname = "appConfig.conf";  // default configuration file, this file must be present or defaults will be used

		// set default values for these attributes
		max_itr = 5000;
		x_rep = 1;
		y_rep = 1;
		z_rep = 1;
		monomers_per_chain = 12;
		atoms_per_monomer = 96;
		dopants_per_chain = 4;
		chains_per_unit_cell = 4;
		stat_sample_rate = 1000;
		movie_sample_rate = 1000;
		itrs_to_skip = 1000;
		recenter_interval = 100;
		vol_chg_freq = 1;
		number_gpu_threads_dop = 64;
		number_gpu_threads_mon = 256;
		gpu_id = -1;
		multiple_gpu = false;

		monomer_step_size = 0.975;
		monomer_min_step_size = 0.0001;
		monomer_max_step_size = 1.0;
		monomer_init_step_size = monomer_min_step_size;

		monomer_rotation_rate = 0.0174533;

		dopant_step_size = 0.975;
		dopant_min_step_size = 0.0001;
		dopant_max_step_size = 1.0;
		dopant_init_step_size = dopant_min_step_size;

		volume_change_step_size = 0.0008;
		volume_change_min_step_size = 0.0001;
		volume_change_max_step_size = 0.01;
		volume_init_step_size = volume_change_min_step_size;
		volume_const_change_itrs = 0;
		volume_relaxation_itrs = 0;
		volume_relaxation_period = 0;
		volume_max_failed_attempts = 10;
		simple_polynomial_order = 6;
		interpolation_delta = 0.001;

		g_of_r_dr = 0.2;

		x_off = 22.085;
		y_off = 5.7;
		z_off = 5.7;
		d_off = 2.97984;
		r_cut = floor(z_off  * 0.6);
		dp_mag = 1.0;
		angle_limit = 20.0;
		single_chain = false;
		dopant_mass = 35.453;
		oligomer_mass = 782.334238;

		atmc_dt_min = 0.0;
		atmc_dt_max = 1000.0;
		atmc_temp_min = 0.0;
		atmc_temp_max = 1000.0;
		atmc_xsig = 1.0;
		use_pcb = true;
		scale_x = true;
		scale_y = true;
		scale_z = true;
		self_interaction = true;

		ifstream in(fname.c_str());

		if (!in.is_open())
		{
			cout << "Load Default Values" << endl;
			// Bonding Constants
			constants.insert(std::pair<string, DATA_TYPE>("a0", 0.52917720859));
			constants.insert(std::pair<string, DATA_TYPE>("De", 2.42846));
			constants.insert(std::pair<string, DATA_TYPE>("r0", 3.80883));
			constants.insert(std::pair<string, DATA_TYPE>("alpha_", 0.854622));

			// Bending Constants
			constants.insert(std::pair<string, DATA_TYPE>("k_theta", 26.116962135533));
			constants.insert(std::pair<string, DATA_TYPE>("theta_0", 141.576));

			// Torsion Constants
			constants.insert(std::pair<string, DATA_TYPE>("k1", 0.0547971));
			constants.insert(std::pair<string, DATA_TYPE>("k2", 0.3795003731335262));
			constants.insert(std::pair<string, DATA_TYPE>("gamma_0", 180.0));

			// Anti - Coiling Constants
			constants.insert(std::pair<string, DATA_TYPE>("epsilon_intra", 0.00690624952569));
			constants.insert(std::pair<string, DATA_TYPE>("sigma_intra", 9.460153875684289));

			// Rotation Constants
			constants.insert(std::pair<string, DATA_TYPE>("k4", 48.0));

			// Restoration Constants
			constants.insert(std::pair<string, DATA_TYPE>("k5", 0.002));
			constants.insert(std::pair<string, DATA_TYPE>("k5_end", 0.004));

			// intra coulomb 
			constants.insert(std::pair<string, int>("intra_coul_offset", 1));
			constants.insert(std::pair<string, DATA_TYPE>("intra_kappa", 0.085)); 


			// InterPotential Constants
			// UCoulomb Py - Dopant constants
			constants.insert(std::pair<string, DATA_TYPE>("q", 0.33614566666666666666666666666667));
			constants.insert(std::pair<string, DATA_TYPE>("Q", -1.008437));
			constants.insert(std::pair<string, DATA_TYPE>("QSqrtd", 1.016945182969));
			constants.insert(std::pair<string, DATA_TYPE>("kappa", 0.085));
			constants.insert(std::pair<string, DATA_TYPE>("C", 14.230382435523));

			// UDisp Py - Dopant
			constants.insert(std::pair<string, DATA_TYPE>("A", 0.003963497682));
			constants.insert(std::pair<string, DATA_TYPE>("B", 0.019720955317));
			constants.insert(std::pair<string, DATA_TYPE>("sigmaPyDopant", 9.267237389052));

			// UExcluded Py - Py
			constants.insert(std::pair<string, DATA_TYPE>("epsilon_inter", 0.001715869540));
			constants.insert(std::pair<string, DATA_TYPE>("sigma_inter", 10.813540083815));
			constants.insert(std::pair<string, DATA_TYPE>("epsilon_inter_end", 0.01715869540));
			constants.insert(std::pair<string, DATA_TYPE>("sigma_inter_end", 11.981990359176));
			// inter coulomb end-end 
			constants.insert(std::pair<string, DATA_TYPE>("kappa_inter_end", 0.15));

			// UCD Py - Dopant
			constants.insert(std::pair<string, DATA_TYPE>("C", 0.742622783));

			// UDopant - Dopant
			constants.insert(std::pair<string, DATA_TYPE>("epsilon_dopant", 0.1095499286));
			constants.insert(std::pair<string, DATA_TYPE>("sigma_dopant", 4.5));
			constants.insert(std::pair<string, DATA_TYPE>("sigma_dopant_9", 756680.642578125));
		}
		else
		{
			// now load the default configuration file
			loadApplicationConfiguration(in, ":");
		}
	}

	void loadApplicationConfiguration(ifstream & in, const string & delimiter = ":")
	{
		if (in.is_open())
		{
			string line;
			while (std::getline(in, line))
			{
				if (line.size() < 1)
					continue;
				if (line.at(0) == COMMENT)
					continue;

				vector<string> tokens;
				Tokenizer(line, tokens, delimiter);
				if (tokens.size() < 2)
				{
					cout << "Parameter: " << tokens[0] << " empty, using default value" << endl;
				}
				else
				{
					if (tokens[0] == "max_itr")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						max_itr = value;
					}
					else if (tokens[0] == "x_rep")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						x_rep = value;
					}
					else if (tokens[0] == "y_rep")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						y_rep = value;
					}
					else if (tokens[0] == "z_rep")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						z_rep = value;
					}
					else if (tokens[0] == "monomers_per_chain")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						monomers_per_chain = value;
					}
					else if (tokens[0] == "atoms_per_monomer")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						atoms_per_monomer = value;
					}
					else if (tokens[0] == "dopants_per_chain")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						dopants_per_chain = value;
					}
					else if (tokens[0] == "chains_per_unit_cell")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						chains_per_unit_cell = value;
					}
					else if (tokens[0] == "stat_sample_rate")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						stat_sample_rate = value;
					}
					else if (tokens[0] == "itrs_to_skip")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						itrs_to_skip = value;
					}
					else if (tokens[0] == "monomer_init_step_size")
					{
						DATA_TYPE value = std::strtod(tokens[1].c_str(), NULL);
						monomer_init_step_size = value;
					}
					else if (tokens[0] == "monomer_step_size")
					{
						DATA_TYPE value = std::strtod(tokens[1].c_str(), NULL);
						monomer_step_size = value;
					}
					else if (tokens[0] == "monomer_rotation_rate")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						monomer_rotation_rate = value;
					}
					else if (tokens[0] == "dopant_init_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						dopant_init_step_size = value;
					}
					else if (tokens[0] == "dopant_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						dopant_step_size = value;
					}
					else if (tokens[0] == "x_off")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						x_off = value;
					}
					else if (tokens[0] == "y_off")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						y_off = value;
					}
					else if (tokens[0] == "z_off")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						z_off = value;
					}
					else if (tokens[0] == "d_off")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						d_off = value;
					}
					else if (tokens[0] == "r_cut")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						r_cut = value;
					}
					else if (tokens[0] == "dp_mag")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						dp_mag = value;
					}
					else if (tokens[0] == "angle_limit")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						angle_limit = value;
					}
					else if (tokens[0] == "single_chain")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						if (value == 1)
							single_chain = true;
						else
							single_chain = false;
					}
					else if (tokens[0] == "self_interaction")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						if (value == 1)
							self_interaction = true;
						else
							self_interaction = false;
					}
					else if (tokens[0] == "dopant_mass")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						dopant_mass = value;
					}
					else if (tokens[0] == "oligomer_mass")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						oligomer_mass = value;
					}
					else if (tokens[0] == "recenter_interval")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						recenter_interval = value;
					}
					else if (tokens[0] == "vol_chg_freq")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						vol_chg_freq = value;
					}							
					else if (tokens[0] == "monomer_min_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						monomer_min_step_size = value;
					}
					else if (tokens[0] == "monomer_max_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						monomer_max_step_size = value;
					}
					else if (tokens[0] == "dopant_min_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						dopant_min_step_size = value;
					}
					else if (tokens[0] == "dopant_max_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						dopant_max_step_size = value;
					}
					else if (tokens[0] == "volume_change_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						volume_change_step_size = value;
					}
					else if (tokens[0] == "volume_init_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						volume_init_step_size = value;
					}
					else if (tokens[0] == "volume_change_min_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						volume_change_min_step_size = value;
					}
					else if (tokens[0] == "volume_change_max_step_size")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						volume_change_max_step_size = value;
					}
					else if (tokens[0] == "volume_max_failed_attempts")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						volume_max_failed_attempts = value;
					}
					else if (tokens[0] == "atmc_dt_min")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						atmc_dt_min = value;
					}
					else if (tokens[0] == "atmc_dt_max")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						atmc_dt_max = value;
					}
					else if (tokens[0] == "atmc_temp_min")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						atmc_temp_min = value;
					}
					else if (tokens[0] == "atmc_temp_max")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						atmc_temp_max = value;
					}
					else if (tokens[0] == "atmc_xsig")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						atmc_xsig = value;
					}
					else if (tokens[0] == "number_gpu_threads_dop")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						number_gpu_threads_dop = value;
					}
					else if (tokens[0] == "number_gpu_threads_mon")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						number_gpu_threads_mon = value;
					}
					else if (tokens[0] == "gpu_id")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						gpu_id = value;
					}
					else if (tokens[0] == "g_of_r_dr")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						g_of_r_dr = value;
					}
					else if (tokens[0] == "use_pcb")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						if (!value)
							use_pcb = false;
						else
							use_pcb = true;
					}
					else if (tokens[0] == "scale_x")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						if (!value)
							scale_x = false;
						else
							scale_x = true;
					}
					else if (tokens[0] == "scale_y")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						if (!value)
							scale_y = false;
						else
							scale_y = true;
					}
					else if (tokens[0] == "scale_z")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						if (!value)
							scale_z = false;
						else
							scale_z = true;
					}
					else if (tokens[0] == "movie_sample_rate")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						movie_sample_rate = value;
					}	
					else if (tokens[0] == "volume_const_change_itrs")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						volume_const_change_itrs = value;
					}
					else if (tokens[0] == "volume_relaxation_itrs")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						volume_relaxation_itrs = value;
					}
					else if (tokens[0] == "volume_relaxation_period")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						volume_relaxation_period = value;
					}
					else if (tokens[0] == "multiple_gpu")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						if (!value)
							multiple_gpu = false;
						else
							multiple_gpu = true;
					}
					else if (tokens[0] == "simple_polynomial_order")
					{
						std::string::size_type sz;     // alias of size_t
						int value = std::stoi(tokens[1], &sz);
						simple_polynomial_order = value;
					}
					else if (tokens[0] == "interpolation_delta")
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						interpolation_delta = value;
					}
					else
					{
						std::string::size_type sz;     // alias of size_t
						DATA_TYPE value = std::stod(tokens[1], &sz);
						string label = tokens[0];
						constants.insert(std::pair<string, DATA_TYPE>(label, value));
					}
				}
			}
		}
	}
} APP_CONFIG;
#endif
