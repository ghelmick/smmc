#if !defined (__RANDOM_H__)
#define       __RANDOM_H__

#include <string>
#include <fstream>
#include <random>
#include "MyRandom.h"
#include <sstream>

using std::string;
using std::ofstream;
using std::ifstream;
using std::istringstream;
using std::getline;

#if defined USE_TEST_DATA
class TestData
{
public:
	TestData() {}
	TestData(const string & testData)
	{
		inData = ifstream(testData.c_str());
		readData();
		rowId = 0;
	}

	DATA_TYPE getValue()
	{
		return data[rowId++];
	}

	int getCount()
	{
		return data.size();
	}

private:
	void readData()
	{
		if (inData.is_open())
		{
			istringstream iss;
			std::string line;
			while (getline(inData, line))
			{
				iss = istringstream(line);
				vector<DATA_TYPE> row;
				DATA_TYPE v = 0.0;
				for (int i = 0; i < 1; i++)
				{
					iss >> v;
					row.push_back(v);
				}
				data.push_back(row[0]);
			}
		}
	}

	int rowId;
	vector<DATA_TYPE> data;
	ifstream inData;
};

//TestData tData;

#endif

class Random
{
public:
	static Random & getInstance()
	{
		static Random instance;
		return instance;
	}

	// The copy constructor is deleted, to prevent client code from creating new
	// instances of this class by copying the instance returned by get_instance()
	Random(Random const&) = delete;

	// The move constructor is deleted, to prevent client code from moving from
	// the object returned by get_instance(), which could result in other clients
	// retrieving a reference to an object with unspecified state.
	Random(Random&&) = delete;

	~Random();

	DATA_TYPE randomDouble();

private:
	Random();

	std::mt19937 generator;
	std::uniform_real_distribution<DATA_TYPE> dis;

	MyRandom myRandom;

#if defined USE_TEST_DATA
	TestData tData;
#endif
};

#endif

