#if !defined __COPROCESSOR_WORKUNIT_H__
#define      __COPROCESSOR_WORKUNIT_H__

#include "util.h"
#include "polymerUtil.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

typedef struct GPUWorkUnit
{
    int nodeId;
    int coprocessor_id;
    int mlb;
    int mub;
    int dlb;
    int dub;
    
    DATA_TYPE upypy = 0.0;
    DATA_TYPE upydop = 0.0;
    DATA_TYPE udopdop = 0.0;

    int num_monomer_monomer_streams = 4;
    int num_monomer_dopant_streams = 4;
    int num_dopant_dopant_streams = 4;
    int num_dopant_monomer_streams = 4;

    int nBlocksMonomers = 1;
    int nBlocksDopants = 1;
    int nThreadsMonomer = 1;
    int nThreadsDopant = 1;

    cudaStream_t * monomer_monomer_stream;
    cudaStream_t * monomer_dopant_stream;
    cudaStream_t * dopant_dopant_stream;
    cudaStream_t * dopant_monomer_stream;

    DATA_TYPE* d_cm_x = NULL;
    DATA_TYPE* d_cm_y = NULL;
    DATA_TYPE* d_cm_z = NULL;
    DATA_TYPE* d_dpm_x = NULL;
    DATA_TYPE* d_dpm_y = NULL;
    DATA_TYPE* d_dpm_z = NULL;

    DATA_TYPE* d_mon_results = NULL;
    DATA_TYPE* mon_results = NULL;
    DATA_TYPE* d_t_mon_results = NULL;
    DATA_TYPE* t_mon_results = NULL;
    DATA_TYPE* d_tr_mon_results = NULL;
    DATA_TYPE* tr_mon_results = NULL;


    DATA_TYPE* pydop_mon_mov = NULL;
    DATA_TYPE* t_pydop_mon_mov = NULL;
    DATA_TYPE* tr_pydop_mon_mov = NULL;
    DATA_TYPE* pydop_dop_mov = NULL;
    DATA_TYPE* t_pydop_dop_mov = NULL;

    DATA_TYPE* d_olig_cm_x = NULL;
    DATA_TYPE* d_olig_cm_y = NULL;
    DATA_TYPE* d_olig_cm_z = NULL;

    DATA_TYPE* d_dop_x = NULL;
    DATA_TYPE* d_dop_y = NULL;
    DATA_TYPE* d_dop_z = NULL;
    DATA_TYPE* d_dop_results = NULL;
    DATA_TYPE* dop_results = NULL;
    DATA_TYPE* t_d_dop_results = NULL;
    DATA_TYPE* t_dop_results = NULL;
    DATA_TYPE* d_pydop_mon_mov = NULL;
    DATA_TYPE* t_d_pydop_mon_mov = NULL;
    DATA_TYPE* tr_d_pydop_mon_mov = NULL;
    DATA_TYPE* d_pydop_dop_mov = NULL;
    DATA_TYPE* t_d_pydop_dop_mov = NULL;

    // interpolation constants
    DATA_TYPE interpolation_delta;
    int interpolation_offset;
    int nInterpolationRows;
    int nInterpolationCols;
    int interpolationOrder;
    int interpolationCoeffOffset;
    DATA_TYPE* d_interpolationParameters;

    GPUWorkUnit() {} // default constructor, just create an empty struct
    GPUWorkUnit(int nodeId_, int coprocessor_id_, int num_monomer_monomer_streams_ = 4, int num_monomer_dopant_streams_ = 4, int num_dopant_dopant_streams_ = 4, int num_dopant_monomer_streams_ = 4);
    GPUWorkUnit(int mlb_, int mub_, int dlb_, int dub_, int nodeId_ = 0, int coprocessor_id_ = 0, int num_monomer_monomer_streams_ = 4, int num_monomer_dopant_streams_ = 4, int num_dopant_dopant_streams_ = 4, int num_dopant_monomer_streams_ = 4);
    ~GPUWorkUnit();

    void initGPUDataStructures(PARTICLE_SYSTEM& ps, DATA_TYPE* interpolationParameters, int nInterpolationRows_, int nInterpolationCols_,  
        DATA_TYPE interpolation_delta_, int interpolation_offset_, int interpolationOrder_, int coefficient_offset_,
        int number_gpu_threads_mon = 256, int number_gpu_threads_dop = 64);
    void updateInterpolationparemeters(PARTICLE_SYSTEM& ps, DATA_TYPE* interpolationParameters, int nInterpolationRows_);
    void cleanup();
} COPROCESSOR_WORKUNIT;

#endif
