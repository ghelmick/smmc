#if defined WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#endif
#include <ios>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <omp.h>
#include <mpi.h>
#include "getopt_pp.h"
#include "AppConfig.h"
#include "polymerUtil.h"
#include "ModelPotential.h"
#include "Random.h"
#include "util.h"
#include "gpuHelper.cuh"
#include "cuUtil.cuh"
#include "GPUWorkUnit.cuh"


using namespace std;
using namespace GetOpt;

//#define MONOMER_TRANSLATE

ofstream systemStatsFile;
ofstream logFile;

unsigned int* gofrMonomerCM = NULL;
unsigned int* gofrMonomer = NULL;
unsigned int* gofrDopant = NULL;
unsigned int* gofrAll = NULL;

SIMULATION_RESULTS SMMC_NVT(PARTICLE_SYSTEM & ps, ModelPotential & mp, const AppConfig & appConfig, DATA_TYPE temperature, int maxItr, int nprc = -1, int myid = -1);
SIMULATION_RESULTS SMMC_NPT(PARTICLE_SYSTEM & ps, ModelPotential & mp, const AppConfig & appConfig, DATA_TYPE temperature, int maxItr, int nprc = -1, int myid = -1, DATA_TYPE npt_pressure = 1.0);
void simLoop(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, DATA_TYPE temperature, int num_threads, int maxItr, int nprc = -1, int myid = -1, int simType = 0, DATA_TYPE npt_pressure = 1.0);
void generateInitialGofr(PARTICLE_SYSTEM & ps, const AppConfig & appConfig, DATA_TYPE temperature);

void temperingLoop(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, int simType, DATA_TYPE temperature, int maxItr, int nprc = -1, int myid = -1, DATA_TYPE npt_pressure = 1.0);
SIMULATION_RESULTS tempering(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, int simType, DATA_TYPE & temperature, int maxItr, int nprc = -1, int myid = -1, DATA_TYPE npt_pressure = 1.0);

void calculateBulkModulus(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, int maxItr, int nprc, int myid);
DATA_TYPE VolumeChangeEnergy(PARTICLE_SYSTEM & ps, ModelPotential & mp, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT * workunits, int nodeId);

void initializeGOFR(int nBins)
{
	if (gofrMonomerCM == NULL)
		gofrMonomerCM = (unsigned int*)calloc(nBins, sizeof(unsigned int));
	if (gofrMonomer == NULL)
		gofrMonomer = (unsigned int*)calloc(nBins, sizeof(unsigned int));
	if (gofrDopant == NULL)
		gofrDopant = (unsigned int*)calloc(nBins, sizeof(unsigned int));
	if (gofrAll == NULL)
		gofrAll = (unsigned int*)calloc(nBins, sizeof(unsigned int));
}

void optimizeLatticeParameters(AppConfig& appConfig, ModelPotential& mp);

void showHelp();
int listDevices();


int main(int argc, char * argv[])
{
	MPI_Init(&argc, &argv);
	int nprc = -1, myid = -1;
	MPI_Comm_size(MPI_COMM_WORLD, &nprc);
	MPI_Comm_rank(MPI_COMM_WORLD, &myid);

	int nprocs = omp_get_max_threads();  // default to the maximum number of threads
	string configFile = "appConfig.conf";
	string fileName = "";
	DATA_TYPE temperature = 300.0;
	int simType = 0; // default to NVT sim.  types are 0 => NVT , 1 => NPT
	DATA_TYPE npt_pressure = 1.0;
	string crystal_type = "fcc";
	int restart = 0;
	int unit_cell = 0;
	bool show_help = false;
	int group_size = 0;
	DATA_TYPE scale_factor = 1.0;
	int should_scale = 0;

	// read the command line input to get the number of threads as well as the data file
	GetOpt_pp ops(argc, argv);
	try
	{
		ops >> Option('n', nprocs);
		ops >> Option('t', temperature);
		ops >> Option('c', configFile);
		ops >> Option('f', fileName);
		ops >> Option('s', simType);
		ops >> Option('p', npt_pressure);
		ops >> Option("ct", crystal_type);
		ops >> Option('r', restart);
		ops >> Option("uc", unit_cell);
		ops >> Option("gp", group_size);
		ops >> Option("sf", scale_factor);
		ops >> Option("ss", should_scale);
		show_help = ops >> OptionPresent('h');
	}
	catch (GetOpt::GetOptEx ex)
	{
		cout << "Error processing command line arguments, using default values" << endl;
	}

	if (show_help)
	{
		showHelp();
	}

	AppConfig appConfig(configFile);

	setAngleLimit(appConfig.angle_limit);
	setUsePCB(appConfig.use_pcb);

	// initialize the model potential
	ModelPotential * mp = new ModelPotential(appConfig);

	// enable nested parallelism
	omp_set_nested(true);

	// initialize the reference monomers
	initReferenceMonomers();

	// initialize the log file
	stringstream sys_energy_ss;
	sys_energy_ss << "sys_energy_" << temperature << ".csv";
	logFile.open(sys_energy_ss.str().c_str());
	logFile << "itr,totBond,totBend,totTorsion,totDD,totAntiCoiling,totRest,totRot,totCoul,py-py,py-dop,dop-dop,tot-intra,tot-inter,tot,\
                pressure,density,enthalpy,avg-intra,avg-inter,avg-tot,x-size,y-size,z-size,volume,monomer-stepsize,monomer-acc-rate,\
                dopant-stepsize,dop-acc-rate,volume-stepsize,vol-acc-rate,tot_pe_stddev,avg-enthalpy,msd-mon,msd-dop,msd-olig,inter-coul,inter-other, \
                cutoff,force_x,force_y,force_z,avgMinDist,minx,maxx,miny,maxy,minz,maxz,avg-density,density-stddev" << endl;
	stringstream sys_stats_ss;
	sys_stats_ss << "sys_stats_" << temperature << ".csv";
	systemStatsFile.open(sys_stats_ss.str().c_str());
	systemStatsFile << "vectorOrder,orientationOrder,gyration,alen,lx,ly,lz" << endl;

	if (false)
	{
		optimizeLatticeParameters(appConfig, *mp);
	}
	else
	{
		PARTICLE_SYSTEM* ps;

		if (!fileName.empty())
		{
			if (restart == 1)
			{
				ps = loadParticleSystemRestartFile(fileName, appConfig);
			}
			else if (unit_cell == 1)
			{
				ps = buildParticleSystemFromReferenceUnitCell(appConfig, fileName, appConfig.x_rep, appConfig.y_rep, appConfig.z_rep);
			}
			else
			{
				cout << "Loading file: " << fileName << endl;
				ps = loadParticleSystemBinaryFile(fileName, appConfig);
			}
		}
		else
		{
			bool addDopants = false;
			if (appConfig.dopants_per_chain)
				addDopants = true;

			// build the reference chain
			if (appConfig.single_chain)
			{
				ps = buildReferencedChain(appConfig, addDopants);
			}
			else
			{
				if (crystal_type == "fcc")
				{
					cout << "             Lattice: FCC " << endl;
					ps = buildParticleSystem(appConfig, appConfig.x_rep, appConfig.y_rep, appConfig.z_rep, addDopants);
				}
				else if (crystal_type == "renne")
				{
					cout << "             Lattice: Renne " << endl;
					ps = buildRenneParticleSystem(appConfig, appConfig.x_rep, appConfig.y_rep, appConfig.z_rep, addDopants);
					setRhomboidPCB(true);
				}
				else if (crystal_type == "stacked")
				{
					cout << "             Lattice: Stacked " << endl;
					ps = buildStackedParticleSystem(appConfig, appConfig.x_rep, appConfig.y_rep, appConfig.z_rep, addDopants);
				}
				else
				{
					cout << "Unknown lattice type: " << crystal_type << endl;
					cout << "Allowed lattice types: fcc or renne" << endl;
					exit(0);
				}
			}
		}
		if (group_size)
		{
			generateOligomerGroupings(*ps, group_size);
		}

		// check to determine if scaling the system was requested
		if (should_scale)
		{
			// now check for the scale factor 
			if (scale_factor == 0.0)
			{
				cout << "System can not be 0 size!!!!!" << endl;
				exit(0);
			}
			else
			{
				cout << "Original particle system density: " << ps->density << endl;

				// respect the dimension scaling parameters from the appConfig file
				DATA_TYPE sfactor[] = { 1.0, 1.0, 1.0 };
				if (appConfig.scale_x)
					sfactor[0] = scale_factor;
				else
					sfactor[0] = 1.0;

				if (appConfig.scale_y)
					sfactor[1] = scale_factor;
				else
					sfactor[1] = 1.0;

				if (appConfig.scale_z)
					sfactor[2] = scale_factor;
				else
					sfactor[2] = 1.0;

				scaleParticleSystem(*ps, sfactor, true);
				cout << "Scaled particle system density: " << ps->density << endl;
			}
	}

#if defined __USE_SIMPLE_POLYNOMIAL__ || defined __USE_INLINE_HERMITE_SPLINE__ || defined __USE_INLINE_SPLINE__
		// generate initial interpolation parameters
		mp->calculateModelParameters(ps->r_cut, mp->getInterpolationDelta(), mp->getInterpolationOrder());
#endif
		simLoop(*ps, *mp, appConfig, temperature, nprocs, appConfig.max_itr, nprc, myid, simType, npt_pressure);
	}

	// clean up resources
	logFile.flush();
	logFile.close();
	systemStatsFile.flush();
	systemStatsFile.close();
	MPI_Finalize();

#if defined __USE_GPU__
	// free GPU resources
	void cleanup();
#endif

	return 0;
}

void simLoop(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, DATA_TYPE temperature, int num_threads, int maxItr, int nprc, int myid, int simType, DATA_TYPE npt_pressure)
{
	// initialize time data structures and start timer
#if defined WIN32
	LARGE_INTEGER start, finish, freq, elapsed;
#else
	timeval start, stop;
	float elapsedTime = 0;
#endif
	if (myid == 0)
	{
		if (simType == 0)
		{
			cout << "     Simulation Type: NVT " << endl;
		}
		else if (simType == 1)
		{
			cout << "     Simulation Type: NPT " << endl;
		}
		else if (simType == 2)
		{
			cout << "     Calculating Bulk Modulus " << endl;
		}
		else if (simType == 10)
		{
			cout << "     Simulation Type: Tempering NVT " << endl;
		}
		else
		{
			cout << "     Simulation Type: Tempering NPT " << endl;
		}
		
		cout << "     Number of Procs: " << nprc << endl;
		cout << "   Number of Threads: " << num_threads << endl;
		cout << "  System Temperature: " << temperature << endl;
		cout << "        NPT Pressure: " << npt_pressure << endl;
		cout << "Number of Iterations: " << maxItr << endl;
		cout << "  Number of Monomers: " << ps.nMonomers << endl;
		cout << "   Number of Dopants: " << ps.nDopants << endl;
		cout << "             Density: " <<ps.density << endl;
		cout << "              X Size: " << ps.x_size << endl;
		cout << "              Y Size: " << ps.y_size << endl;
		cout << "              Z Size: " << ps.z_size << endl;
		cout << "      Cut Off Radius: " << ps.r_cut << endl;

		saveParticleSystem(ps, "before.xyz");
		saveParticleSystemCM(ps, "before_cm.xyz");
#if !defined __MINIMIZE_OUTPUT__
		generateInitialGofr(ps, appConfig, temperature);
		saveDistanceHistogram(ps, "before_distance_histogram.csv");
#endif
#if defined WIN32
		QueryPerformanceFrequency(&freq);
		QueryPerformanceCounter(&start);
#else
		gettimeofday(&start, NULL);
#endif
	}

	omp_set_num_threads(num_threads);

	for (int i = 0; i < nprc; i++)
	{
		if (myid == i)
		{
			cout << "launching from node: " << i << endl;
			if (simType == 0)
			{
				SMMC_NVT(ps, mp, appConfig, temperature, maxItr, nprc, myid);
			}
			else if (simType == 1)
			{
				SMMC_NPT(ps, mp, appConfig, temperature, maxItr, nprc, myid, npt_pressure);
			}
			else if (simType == 2)
			{
				calculateBulkModulus(ps, mp, appConfig, maxItr, nprc, myid);
			}
			else
			{
				temperingLoop(ps, mp, appConfig, simType, temperature, maxItr, nprc, myid, npt_pressure);
			}
		}
	}
	
	// stop timer and display elapsed time
	if (myid == 0)
	{
#if defined WIN32

		QueryPerformanceCounter(&finish);
		elapsed.QuadPart = finish.QuadPart - start.QuadPart;
		elapsed.QuadPart *= 1000000;  // convert to microseconds
		elapsed.QuadPart /= freq.QuadPart;
		cout << "Total Elapsed time  " << (float)elapsed.QuadPart / 1000000.0 << " (s)" << endl << endl << endl;;
#else
		// Capture the stop time
		gettimeofday(&stop, NULL);
		// Retrieve time elapsed in milliseconds
		elapsedTime = (float)((stop.tv_sec - start.tv_sec) * 1000000u + stop.tv_usec - start.tv_usec) / 1.e6;
		cout << "Total Elapsed Time: " << (float)elapsedTime << endl;
#endif
		stringstream ss;
		ss << "after_" << temperature << ".xyz";
		saveParticleSystem(ps, ss.str().c_str());
		saveParticleSystemCM(ps, ss.str().c_str());
		stringstream ss1;
		ss1 << "after_" << temperature << ".bin";
		saveParticleSystemToBinaryFile(ps, ss1.str().c_str());
	}	
}

SIMULATION_RESULTS SMMC_NVT(PARTICLE_SYSTEM & ps, ModelPotential & mp, const AppConfig & appConfig, DATA_TYPE temperature, int maxItr, int nprc, int myid)
{
	SIMULATION_RESULTS results;



	return results;
}

SIMULATION_RESULTS SMMC_NPT(PARTICLE_SYSTEM & ps, ModelPotential & mp, const AppConfig & appConfig, DATA_TYPE temperature, int maxItr, int nprc, int myid, DATA_TYPE npt_pressure)
{
	SIMULATION_RESULTS results;
	int vol_chg_freq = appConfig.vol_chg_freq;
	int recenter_interval = appConfig.recenter_interval;
	int sampleRate = appConfig.stat_sample_rate;
	int movie_sample_rate = appConfig.movie_sample_rate;

	DATA_TYPE beta = 1.0 / (temperature / KEL_PER_HAR);
	DATA_TYPE rotationStepSize = appConfig.monomer_rotation_rate, translationStepSize = appConfig.monomer_init_step_size;
	DATA_TYPE translation_step_size = appConfig.monomer_step_size, translation_min_step_size = appConfig.monomer_min_step_size;
	DATA_TYPE translation_max_step_size = appConfig.monomer_max_step_size;
	DATA_TYPE dopantStepSize = appConfig.dopant_init_step_size;
	DATA_TYPE dopant_step_size = appConfig.dopant_step_size;
	DATA_TYPE volumeScaleFactor = appConfig.volume_init_step_size;
	DATA_TYPE p_x = 0.0, p_y = 0.0, p_z = 0.0, p_density = 0.0;
	DATA_TYPE p_dpm_x = 0.0, p_dpm_y = 0.0, p_dpm_z = 0.0;
	DATA_TYPE p_n_x = 0.0, p_n_y = 0.0, p_n_z = 0.0;
	DATA_TYPE p_heading = 0.0, p_pitch = 0.0, p_bank = 0.0;
	DATA_TYPE p_olig_cmx = 0.0, p_olig_cmy = 0.0, p_olig_cmz = 0.0;
	DATA_TYPE intra = 0.0, t_intra = 0.0, tr_intra = 0.0, totIntra = 0.0;
    DATA_TYPE inter = 0.0, t_inter = 0.0, tr_inter = 0.0, totInter = 0.0;
	DATA_TYPE t_de = 0.0, tr_de = 0.0;
	DATA_TYPE t_probability = 0.0, tr_probability = 0.0;
	DATA_TYPE upypy = 0.0, upydop = 0.0, udopdop = 0.0, ucoul = 0.0, uother = 0.0;
	DATA_TYPE volumeAcceptedRate = 0;
	bool rotate = false, reset = false;
	int translationsAccepted = 0, rotationsAttempted = 0, rotationsAccepted = 0;
	int dopantAccepted = 0, volumeAccepted = 0;
	DATA_TYPE r_rate = 0.0, d_rate = 0.0;
	DATA_TYPE volume_change_min_step_size = appConfig.volume_change_min_step_size;
	DATA_TYPE volume_change_max_step_size = appConfig.volume_change_max_step_size;
	DATA_TYPE volume_step_size = appConfig.volume_change_step_size;
	int volume_const_change_itrs = appConfig.volume_const_change_itrs;
	int volume_relaxation_itrs = appConfig.volume_relaxation_itrs == 0 ? 1 : appConfig.volume_relaxation_itrs;
	int volume_relaxation_period = appConfig.volume_relaxation_period;
	int volume_relaxation_ctr = 0;
	bool attemptVolumeChange = false;
	DATA_TYPE dopant_min_step_size = appConfig.dopant_min_step_size;
	DATA_TYPE dopant_max_step_size = appConfig.dopant_max_step_size;

	DATA_TYPE* systemScaleFactor = new DATA_TYPE[3];
	systemScaleFactor[0] = 1.0;
	systemScaleFactor[1] = 1.0;
	systemScaleFactor[2] = 1.0;
	DATA_TYPE* prevSystemScaleFactor = new DATA_TYPE[3];
	memset(prevSystemScaleFactor, 0, sizeof(DATA_TYPE) * 3);
	bool systemScaleDimension[] = { appConfig.scale_x, appConfig.scale_y, appConfig.scale_z };
	int oligomerId = 0;

	DATA_TYPE gofrDR = appConfig.g_of_r_dr;
	int nBins = (ps.r_cut / gofrDR) + 1;
	initializeGOFR(nBins);

	DATA_TYPE prevVol = 0.0, postVol = 0.0, preVolEnergy = 0.0, postVolEnerg = 0.0;
	DATA_TYPE deltaVol = 0.0, deltaVolEnergy = 0.0, logVol = 0.0, volProbability = 0.0;
	DATA_TYPE enthalpy = 0.0;
	bool updateCutoff = true;
	if (appConfig.r_cut != -1.0)
		updateCutoff = false;
	// used for running averages
	int itrs_to_skip = appConfig.itrs_to_skip;
	DATA_TYPE rInterDelta = 0.0, rInterMean = 0.0, rInterSumsqrd = 0.0, kInter = 0.0;
	DATA_TYPE rIntraDelta = 0.0, rIntraMean = 0.0, rIntraSumsqrd = 0.0, kIntra = 0.0;
	DATA_TYPE rTotalDelta = 0.0, rTotalMean = 0.0, rTotalSumsqrd = 0.0;
	DATA_TYPE rEnthalpyDelta = 0.0, rEnthalpyMean = 0.0, rEnthalpySumsqrd = 0.0;
	DATA_TYPE rDensityDelta = 0.0, rDensityMean = 0.0, rDensitySumsqrd = 0.0, kDensity = 0.0;
	DATA_TYPE avgMinDist = 0.0, minx = 0.0, maxx = 0.0, miny = 0.0, maxy = 0.0, minz = 0.0, maxz = 0.0;
	int volumeChangeAttemptFailed = 0;
	int volume_max_failed_attempts = appConfig.volume_max_failed_attempts;

	DATA_TYPE p = ATM_TO_GPA(npt_pressure);
	p = GPA_TO_ATU(p);

	stringstream ssTemperature;
	ssTemperature << "working_" << temperature << ".xyz";
	string temeratureFileName = ssTemperature.str();
	// get an initial capture of the system state
	saveParticleSystemCM(ps, temeratureFileName.c_str(), std::ofstream::app);

	COPROCESSOR_WORKUNIT * workunits = NULL;
	int nworkunits = 0;
#if defined __USE_GPU__
	setDevice(0);

	int numGPU = nworkunits = 1;

	if (appConfig.multiple_gpu)
	{
		numGPU = listDevices();

		// at the moment assume work is to be spread across all available gpus
		// create an array to contain the coprocessor work units
		nworkunits = numGPU;
	}

	workunits = new COPROCESSOR_WORKUNIT[nworkunits];
	for (int i = 0; i < nworkunits; i++)
	{
		COPROCESSOR_WORKUNIT * workunit = new COPROCESSOR_WORKUNIT(myid, i);
		// calculate the bounds for iterating over polymers
		int numPolymers = ps.nMonomers / ps.monomers_per_chain;
		int monomersPerNode = (numPolymers / nworkunits) * ps.monomers_per_chain;
		workunit->mlb = monomersPerNode * i;
		workunit->mub = workunit->mlb + monomersPerNode;
		int dopantsPerNode = ps.nDopants / nworkunits;
		workunit->dlb = dopantsPerNode * i;
		workunit->dub = workunit->dlb + dopantsPerNode;

		if (i == nworkunits - 1)
		{
			workunit->mub = ps.nMonomers;
			workunit->dub = ps.nDopants;
		}

		cout << " WorkUnit[" << i << "] is assigned monomers: " << workunit->mlb << " -> " << workunit->mub << endl;
		cout << " WorkUnit[" << i << "] is assigned dopants: " << workunit->dlb << " -> " << workunit->dub << endl;


		workunit->coprocessor_id = i;
		if (appConfig.gpu_id > -1 && nworkunits < appConfig.gpu_id)
			workunit->coprocessor_id = appConfig.gpu_id;

		workunit->nodeId = myid;
		workunit->upypy   = 0.0;
		workunit->upydop  = 0.0;
		workunit->udopdop = 0.0;

		workunits[i] = *workunit;

		// initialize the GPU with the data structures and constants
		workunits[i].initGPUDataStructures(ps, mp.getInterpolationParameters(), mp.getNumInterpolationRows(), mp.getNumInterpolationCols(),
			mp.getInterpolationDelta(), mp.getInterpolationOffset(), mp.getInterpolationOrder(), mp.getInterpolationCoeffOffset(),
			appConfig.number_gpu_threads_mon, appConfig.number_gpu_threads_dop);

		setModelConstants(appConfig.use_pcb, mp.getSigma_Dopant(), mp.getQ_Dop(), mp.getEpsilon_Dopant(), mp.getKappa(),
			ps.x_size, ps.y_size, ps.z_size, ps.r_cut, mp.getQ_Mono(), mp.getSigma_Inter(),
			mp.getSigma_inter_End(), mp.getEpsilon_Inter(), mp.getEpsilon_Inter_End(), mp.getDpMag(),
			mp.getA(), mp.getB(), mp.getC(), mp.getSigmaPyDopant(), workunits[i]);
	}
#endif

	// this needs to be done so that the columns line up correctly for the output
	logFile << 0 << ",";

	// display initial energy
	totIntra = mp.TotalIntra(ps, logFile, true);
	totInter = mp.TotalInter(ps, 0, ps.nMonomers, 0, ps.nDopants, nworkunits, workunits, upypy, upydop, udopdop, ucoul, uother);

	logFile << std::setprecision(15) << upypy << ',' << std::setprecision(15) << upydop << ',' << std::setprecision(15) << udopdop;
	logFile << ',' << totIntra << ',' << totInter << ',' << totIntra + totInter << ',' << "0.0" << ',' << ps.density << endl;
	cout << setw(5) << 0 << setw(10) << volumeAcceptedRate << setw(10) << totIntra << setw(10) << totInter << setw(10) << ps.density << endl;

#if defined WIN32
	LARGE_INTEGER start, finish, freq, elapsed;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&start);
#else
	timeval start, stop;
	float elapsedTime = 0;
	gettimeofday(&start, NULL);
#endif

	for (int itr = 0; itr < maxItr; itr++)
	{
		prevSystemScaleFactor[0] = systemScaleFactor[0];
		prevSystemScaleFactor[1] = systemScaleFactor[1];
		prevSystemScaleFactor[2] = systemScaleFactor[2];

		// attempt volume change
		if (attemptVolumeChange)
		{
			reset = false;

			// calculate initial system inter-potential energy
			preVolEnergy = VolumeChangeEnergy(ps, mp, 0, ps.nMonomers, 0, ps.nDopants, nworkunits, workunits, myid);
			prevVol = ANG_TO_BOHR(ps.x_size) * ANG_TO_BOHR(ps.y_size) * ANG_TO_BOHR(ps.z_size);
			systemScaleFactor = scaleSystem(ps, systemScaleDimension, volumeScaleFactor, updateCutoff);

#if defined __USE_SIMPLE_POLYNOMIAL__ || defined __USE_INLINE_HERMITE_SPLINE__ || defined __USE_INLINE_SPLINE__
			// generate initial interpolation parameters
			//mp.calculateModelParameters(ps.r_cut, mp.getInterpolationDelta(), mp.getInterpolationOrder());
#endif

#if defined __USE_GPU__
			int i = 0;
			#pragma omp parallel for private(i)
			for (i = 0; i < nworkunits; i++)
			{
				setDevice(workunits[i].coprocessor_id);
				deviceScaleSystem(ps, workunits[i], systemScaleFactor, updateCutoff);
#if defined __USE_SIMPLE_POLYNOMIAL__ || defined __USE_INLINE_HERMITE_SPLINE__ || defined __USE_INLINE_SPLINE__
				workunits[i].updateInterpolationparemeters(ps, mp.getInterpolationParameters(), mp.getNumInterpolationRows());
#endif
			}

#endif
			// now recalculate the energy
			postVolEnerg = VolumeChangeEnergy(ps, mp, 0, ps.nMonomers, 0, ps.nDopants, nworkunits, workunits, myid);
			postVol = ANG_TO_BOHR(ps.x_size) * ANG_TO_BOHR(ps.y_size) * ANG_TO_BOHR(ps.z_size);

			deltaVol = postVol - prevVol;
			logVol = log(postVol / prevVol);
			deltaVolEnergy = postVolEnerg - preVolEnergy;
			enthalpy = (deltaVolEnergy + p * deltaVol) - (1.0 / beta) * (ps.nMonomers + ps.nDopants + 1) * logVol;
			
			if (deltaVolEnergy >= 0.0)
			{
				volProbability = exp(-beta * deltaVolEnergy);
				if (volProbability > Random::getInstance().randomDouble())
				{
					// accepted the move, now rotate the monomer
					volumeAccepted++;
				}
				else
				{
					reset = true;
				}
			}
			else
			{
				volumeAccepted++;
			}

			if (reset)
			{
				DATA_TYPE tempScale[] = { 1.0, 1.0, 1.0 };
				tempScale[0] = 1.0 / systemScaleFactor[0];
				tempScale[1] = 1.0 / systemScaleFactor[1];
				tempScale[2] = 1.0 / systemScaleFactor[2];

				scaleParticleSystem(ps, tempScale, updateCutoff);
#if defined __USE_SIMPLE_POLYNOMIAL__ || defined __USE_INLINE_HERMITE_SPLINE__ || defined __USE_INLINE_SPLINE__
				// generate initial interpolation parameters
				//mp.calculateModelParameters(ps.r_cut, mp.getInterpolationDelta(), mp.getInterpolationOrder());
#endif
#if defined __USE_GPU__
				int i = 0;
				#pragma omp parallel for private(i)
				for (i = 0; i < nworkunits; i++)
				{
					setDevice(workunits[i].coprocessor_id);
					deviceScaleSystem(ps, workunits[i], tempScale, updateCutoff);
#if defined __USE_SIMPLE_POLYNOMIAL__ || defined __USE_INLINE_HERMITE_SPLINE__ || defined __USE_INLINE_SPLINE__
					workunits[i].updateInterpolationparemeters(ps, mp.getInterpolationParameters(), mp.getNumInterpolationRows());
#endif
			}
#endif
				systemScaleFactor[0] = prevSystemScaleFactor[0];
				systemScaleFactor[1] = prevSystemScaleFactor[1];
				systemScaleFactor[2] = prevSystemScaleFactor[2];

				volumeChangeAttemptFailed++;
			}

			
			{
				if (itr % sampleRate == 0 && itr > 0)
				{
					kDensity = kDensity + 1;
					rDensityDelta = ps.density - rDensityMean;
					rDensityMean = rDensityMean + rDensityDelta / kDensity;
					rDensitySumsqrd = rDensitySumsqrd + rDensityDelta * (ps.density - rDensityMean);
				}
			}
		}

		for (int mIdx = 0; mIdx < ps.nMonomers; mIdx++)
		{
			// reset rotation and reset flags
			rotate = reset = false;
			intra = t_intra = tr_intra = 0.0;
			inter = t_inter = tr_inter = 0.0;

			// calculate the oligomer id
			oligomerId = mIdx / ps.monomers_per_chain;

			// store originals incase a reset is required
			p_x = ps.cm_x[mIdx];
			p_y = ps.cm_y[mIdx];
			p_z = ps.cm_z[mIdx];
			p_n_x = ps.n_x[mIdx];
			p_n_y = ps.n_y[mIdx];
			p_n_z = ps.n_z[mIdx];
			p_dpm_x = ps.dpm_x[mIdx];
			p_dpm_y = ps.dpm_y[mIdx];
			p_dpm_z = ps.dpm_z[mIdx];
			p_heading = ps.heading[mIdx];
			p_pitch = ps.pitch[mIdx];
			p_bank = ps.bank[mIdx];
			p_olig_cmx = ps.olig_cm_x[oligomerId];
			p_olig_cmy = ps.olig_cm_y[oligomerId];
			p_olig_cmz = ps.olig_cm_z[oligomerId];

			//  now move the particle
			translateMonomer(ps, mIdx, translationStepSize);
			#pragma parallel sections
			{
				#pragma parallel section
				{
					rotateMonomer(ps, mIdx, rotationStepSize);
				}

				#pragma parallel section
				{
					calculateOligomerCenterOfMasses(ps, oligomerId);
				}
			}
			{
				mp.TotalMonomerPart(ps, mIdx, -1, 0, ps.nMonomers, 0, ps.nDopants, nworkunits, workunits, p_x, p_y, p_z, p_dpm_x, p_dpm_y, p_dpm_z,
					p_n_x, p_n_y, p_n_z, intra, t_intra, tr_intra, inter, t_inter, tr_inter);
			}

			t_de = (t_intra + t_inter) - (intra + inter);
			if (t_de > 0.0)
			{
				t_probability = exp(-beta * t_de);
				if (t_probability > Random::getInstance().randomDouble())
				{
					rotate = true;
					translationsAccepted++;
				}
				else
				{
					reset = true;
				}
			}
			else
			{
				rotate = true;
				translationsAccepted++;
			}

			if (rotate)
			{
				// now evaluate the rotation of the monomer
				tr_de = (tr_intra + tr_inter) - (intra + inter);
				if (tr_de > 0.0)
				{
					tr_probability = exp(-beta * tr_de);
					if (tr_probability > Random::getInstance().randomDouble())
					{
						rotationsAccepted++;
					}
					else
					{
						reset = true;
					}
				}
				else
				{
					rotationsAccepted++;
				}
			}

			if (reset)
			{
				ps.cm_x[mIdx] = p_x;
				ps.cm_y[mIdx] = p_y;
				ps.cm_z[mIdx] = p_z;
				ps.n_x[mIdx] = p_n_x;
				ps.n_y[mIdx] = p_n_y;
				ps.n_z[mIdx] = p_n_z;
				ps.dpm_x[mIdx] = p_dpm_x;
				ps.dpm_y[mIdx] = p_dpm_y;
				ps.dpm_z[mIdx] = p_dpm_z;
				ps.heading[mIdx] = p_heading;
				ps.pitch[mIdx] = p_pitch;
				ps.bank[mIdx] = p_bank;
				ps.olig_cm_x[oligomerId] = p_olig_cmx;
				ps.olig_cm_y[oligomerId] = p_olig_cmy;
				ps.olig_cm_z[oligomerId] = p_olig_cmz;
			}
			else
			{
				int i = 0;
				#pragma omp parallel for private(i)
				for (i = 0; i < nworkunits; i++)
				{
					setDevice(workunits[i].coprocessor_id);
					updateMonomer(ps, mIdx, workunits[i]);
				}
			}
		}

		for (int dIdx = 0; dIdx < ps.nDopants; dIdx++)
		{
			// reset the reset flag and other variables
			reset = false;
			inter = t_inter = 0.0;

			// save the original location 
			p_x = ps.dop_x[dIdx];
			p_y = ps.dop_y[dIdx];
			p_z = ps.dop_z[dIdx];

			// translate the dopant
			translateDopant(ps, dIdx, dopantStepSize);

			// calculate the energies
			mp.TotalDopantPart(ps, -1, dIdx, 0, ps.nMonomers, 0, ps.nDopants, nworkunits, workunits, p_x, p_y, p_z, inter, t_inter);

			t_de = t_inter - inter;
			if (t_de > 0.0)
			{
				t_probability = exp(-beta * t_de);
				if (t_probability > Random::getInstance().randomDouble())
				{
					// accepted the move
					dopantAccepted++;
				}
				else
				{
					reset = true;
				}
			}
			else
			{
				dopantAccepted++;
			}

			if (reset)
			{
				ps.dop_x[dIdx] = p_x;
				ps.dop_y[dIdx] = p_y;
				ps.dop_z[dIdx] = p_z;
			}
			else
			{
				int i = 0;
				#pragma omp parallel for private(i)
				for (i = 0; i < nworkunits; i++)
				{
					setDevice(workunits[i].coprocessor_id);
					updateDopant(ps, dIdx, workunits[i]);
				}
			}
		}


		if (attemptVolumeChange)
		{
#if defined __WEIGHTED_VOLUME_ACC_RATE__
			volumeAcceptedRate = (float)(volumeAccepted + rotationsAccepted + dopantAccepted) / (float)(ps.nMonomers + ps.nDopants + 1);
#elif defined __INDIVIDUAL_PARTICLE_VOLUME_ACC_RATE__
			volumeAcceptedRate = (float)(volumeAccepted + rotationsAccepted + dopantAccepted) / (float)(ps.nMonomers + ps.nDopants + 1);
			d_rate = (float)dopantAccepted / (float)ps.nDopants;
			r_rate = (float)rotationsAccepted / (float)ps.nMonomers;
#elif defined __COMBINED_PARTICLE_ACC_RATE__
			volumeAcceptedRate = (float)(volumeAccepted + rotationsAccepted + dopantAccepted) / (float)(ps.nMonomers + ps.nDopants + 1);
#endif
		}
		else
		{
#if defined __WEIGHTED_VOLUME_ACC_RATE__
			volumeAcceptedRate = (float)(volumeAccepted + rotationsAccepted + dopantAccepted) / (float)(ps.nMonomers + ps.nDopants);
#elif defined __INDIVIDUAL_PARTICLE_VOLUME_ACC_RATE__
			volumeAcceptedRate = (float)(volumeAccepted + rotationsAccepted + dopantAccepted) / (float)(ps.nMonomers + ps.nDopants);
			d_rate = (float)dopantAccepted / (float)ps.nDopants;
			r_rate = (float)rotationsAccepted / (float)ps.nMonomers;
#elif defined __COMBINED_PARTICLE_ACC_RATE__
			volumeAcceptedRate = (float)(volumeAccepted + rotationsAccepted + dopantAccepted) / (float)(ps.nMonomers + ps.nDopants);
#endif
		}

		volumeAccepted = rotationsAccepted = dopantAccepted = 0;

		if (volumeAcceptedRate < 0.40)
		{
			if (itr > volume_const_change_itrs)
				volumeScaleFactor *= volume_step_size;

#if defined __COMBINED_PARTICLE_ACC_RATE__
			translationStepSize *= translation_step_size;
			dopantStepSize *= dopant_step_size;
#endif
		}

#if defined __INDIVIDUAL_PARTICLE_VOLUME_ACC_RATE__
		//if (itr > volume_relaxation_period)
		{
			if (r_rate < 0.40)
			{
				translationStepSize *= translation_step_size;
			}
			if (d_rate < 0.40)
			{
				dopantStepSize *= dopant_step_size;
			}

			if (r_rate > 0.60)
			{
				translationStepSize *= (1 + (1 - translation_step_size));
			}
			if (d_rate > 0.60)
			{
				dopantStepSize *= (1 + (1 - dopant_step_size));
			}
		}
#endif
		if (volumeAcceptedRate > 0.60)
		{
			if (itr > volume_const_change_itrs)
				volumeScaleFactor *= (1 + (1 - volume_step_size));
#if defined __COMBINED_PARTICLE_ACC_RATE__
			translationStepSize *= (1 + (1 - translation_step_size));
			dopantStepSize *= (1 + (1 - dopant_step_size));
#endif
		}

		// may need to force an update of the volume scale factor because system be stuck
		if (volumeChangeAttemptFailed > volume_max_failed_attempts)
		{
			volumeScaleFactor *= volume_step_size;
			volumeChangeAttemptFailed = 0;

			// do make the scale factor too large or too small
			if (volumeScaleFactor > volume_change_max_step_size)
				volumeScaleFactor = volume_change_max_step_size;
			if (volumeScaleFactor < volume_change_min_step_size)
				volumeScaleFactor = volume_change_min_step_size;
		}

		if (itr > volume_const_change_itrs)
		{
			if (volumeScaleFactor > volume_change_max_step_size)
				volumeScaleFactor = volume_change_max_step_size;
			if (volumeScaleFactor < volume_change_min_step_size)
				volumeScaleFactor = volume_change_min_step_size;
		}

		if (translationStepSize < translation_min_step_size)
			translationStepSize = translation_min_step_size;
		if (translationStepSize > translation_max_step_size)
			translationStepSize = translation_max_step_size;
		if (dopantStepSize < dopant_min_step_size)
			dopantStepSize = dopant_min_step_size;
		if (dopantStepSize > dopant_max_step_size)
			dopantStepSize = dopant_max_step_size;

		if (volume_relaxation_period >= itr)
		{
			volume_relaxation_ctr++;
			if (volume_relaxation_ctr >= volume_relaxation_itrs)
			{
				attemptVolumeChange = true;
				volume_relaxation_ctr = 0;
			}
			else
			{
				attemptVolumeChange = false;
			}
		}
		else
		{
			if (itr % vol_chg_freq == 0)
				attemptVolumeChange = true;
			else
				attemptVolumeChange = false;
		}

		if (itr % sampleRate == 0 && itr > 0)
		{
			logFile << itr << ",";

			totIntra = mp.TotalIntra(ps, logFile, true);
			totInter = mp.TotalInter(ps, 0, ps.nMonomers, 0, ps.nDopants, nworkunits, workunits, upypy, upydop, udopdop, ucoul, uother);
			cout << setw(5) << itr << setw(10) << volumeAcceptedRate << setw(10) << totIntra << setw(10) << totInter << setw(10) << ps.density << endl;

			{
				logFile << std::setprecision(15) << upypy << ',' << std::setprecision(15) << upydop << ',' << std::setprecision(15) << udopdop;
				postVol = ANG_TO_BOHR(ps.x_size) * ANG_TO_BOHR(ps.y_size) * ANG_TO_BOHR(ps.z_size);
				enthalpy = (totIntra + totInter) + p * postVol + EV_TO_HA(1.5 * (ps.nMonomers + ps.nDopants) * K_ * temperature);

				if (itr >= itrs_to_skip)
				{
					kIntra = kIntra + 1;
					rIntraDelta = totIntra - rIntraMean;
					rIntraMean = rIntraMean + rIntraDelta / kIntra;
					rIntraSumsqrd = rIntraSumsqrd + rIntraDelta * (totIntra - rIntraMean);

					kInter = kInter + 1;
					rInterDelta = totInter - rInterMean;
					rInterMean = rInterMean + rInterDelta / kInter;
					rInterSumsqrd = rInterSumsqrd + rInterDelta * (totInter - rInterMean);

					rTotalDelta = (rInterMean + rIntraMean) - rTotalMean;
					rTotalMean = rTotalMean + rTotalDelta / kInter;
					rTotalSumsqrd = rTotalSumsqrd + rTotalDelta * ((rInterMean + rIntraMean) - rTotalMean);

					rEnthalpyDelta = enthalpy - rEnthalpyMean;
					rEnthalpyMean = rEnthalpyMean + rEnthalpyDelta / kIntra;
					rEnthalpySumsqrd = rEnthalpySumsqrd + rEnthalpyDelta * (enthalpy - rEnthalpyMean);
				}

				// this is temporary, until i figure a better way of logging the results
				avgMinDist = minx = miny = minz = maxx = maxy = maxz = 0.0;
#if !defined __MINIMIZE_OUTPUT__
				calculateMeanMonomerDopantClosestDistance(ps, avgMinDist, minx, maxx, miny, maxy, minz, maxz);
#endif
				logFile << ',' << totIntra << ',' << totInter << ',' << totIntra + totInter << ',' << p << ',' << ps.density << ',' << enthalpy << ',' <<
					rIntraMean << ',' << rInterMean << ',' << rTotalMean << ',' << ps.x_size << ',' << ps.y_size << ',' << ps.z_size << ',' <<
					postVol << ',' << translationStepSize << ',' << volumeAcceptedRate << ',' <<
					dopantStepSize << ',' << volumeAcceptedRate << ',' << volumeScaleFactor << ',' << volumeAcceptedRate << ',' << sqrt(rTotalSumsqrd / (kInter)) <<
					',' << rEnthalpyMean << ',' << totInter - upypy - upydop - udopdop << ',' << 0.0 << ',' << 0.0 << ',' << ucoul << ',' << uother << ',' << ps.r_cut << ',' << "0.0" << ',' <<
					"0.0" << ',' << "0.0" << ',' << avgMinDist << ',' << minx << ',' << maxx << ',' << miny << ',' << maxy << ',' << minz << ',' << maxz << ',' <<
					rDensityMean << ',' << sqrt(rDensitySumsqrd / kIntra) << endl;

#if !defined __MINIMIZE_OUTPUT__
				collectGofR(ps, gofrMonomerCM, gofrMonomer, gofrDopant, gofrAll, itr / sampleRate, nBins, gofrDR);
#endif
			}

			// save a restore point file, this will be used to restart simulation if it is terminated early
			stringstream ss1;
			ss1 << "intermediate/after_" << temperature << "_" << itr << ".bin";
			saveParticleSystemToBinaryFile(ps, ss1.str().c_str());

#if !defined __MINIMIZE_OUTPUT__
			stringstream ss;
			ss << "intermediate/histogram_" << temperature << "_" << itr << ".csv";
			// save the histograms
			saveHistograme(ps, itr / sampleRate, ss.str().c_str());

			collectParticleSystemStatistics(ps, appConfig, systemStatsFile);

			stringstream ss2;
			ss2 << "intermediate/after_" << temperature << "_" << itr << ".xyz";
			saveParticleSystemCM(ps, ss2.str().c_str());

			stringstream ss3;
			ss3 << "intermediate/hist_distances_" << temperature << "_" << itr << ".csv";
			saveDistanceHistogram(ps, ss3.str().c_str());
#endif
		}
#if !defined __MINIMIZE_OUTPUT__
		if (itr % movie_sample_rate == 0)
			saveParticleSystemCM(ps, temeratureFileName.c_str(), std::ofstream::app);
#endif

		if (itr == itrs_to_skip - 1)
		{
			kIntra = 0;
			rIntraDelta = 0.0;
			rIntraMean = 0.0;
			rIntraSumsqrd = 0.0;

			kInter = 0;
			rInterDelta = 0.0;
			rInterMean = 0.0;
			rInterSumsqrd = 0.0;

			rTotalDelta = 0.0;
			rTotalMean = 0.0;
			rTotalSumsqrd = 0.0;

			rEnthalpyDelta = 0.0;
			rEnthalpyMean = 0.0;
			rEnthalpySumsqrd = 0.0;

			kDensity = 0.0;
			rDensityMean = 0.0;
			rDensitySumsqrd = 0.0;
		}
	}

#if defined WIN32

	QueryPerformanceCounter(&finish);
	elapsed.QuadPart = finish.QuadPart - start.QuadPart;
	elapsed.QuadPart *= 1000000;  // convert to microseconds
	elapsed.QuadPart /= freq.QuadPart;
	cout << "Iterations Elapsed time  " << (float)elapsed.QuadPart / 1000000.0 << " (s)" << endl << endl << endl;;
#else
	// Capture the stop time
	gettimeofday(&stop, NULL);
	// Retrieve time elapsed in milliseconds
	elapsedTime = (float)((stop.tv_sec - start.tv_sec) * 1000000u + stop.tv_usec - start.tv_usec) / 1.e6;
	cout << "Iterations Elapsed Time: " << (float)elapsedTime << endl;
#endif

	// calculate final full system energy
	totIntra = mp.TotalIntra(ps, logFile, true);
	totInter = mp.TotalInter(ps, 0, ps.nMonomers, 0, ps.nDopants, nworkunits, workunits, upypy, upydop, udopdop, ucoul, uother);
	cout << totIntra << '\t' << totInter << endl;

	{
		logFile << std::setprecision(15) << upypy << ',' << std::setprecision(15) << upydop << ',' << std::setprecision(15) << udopdop;
		logFile << ',' << totIntra << ',' << totInter << ',' << totIntra + totInter << ',' << "0.0" << ',' << ps.density << endl;

		postVol = ANG_TO_BOHR(ps.x_size) * ANG_TO_BOHR(ps.y_size) * ANG_TO_BOHR(ps.z_size);
		enthalpy = (totIntra + totInter) + p * postVol + EV_TO_HA(1.5 * (ps.nMonomers + ps.nDopants) * K_ * temperature);

		// save the gofr to file
		stringstream ss0;
		ss0 << "gor_" << temperature << "_final.csv";
		generateGofR(ps, ss0.str().c_str(), gofrMonomerCM, gofrMonomer, gofrDopant, gofrAll, maxItr / sampleRate, nBins, gofrDR);

		stringstream ss;
		ss << "intermediate/histogram_" << temperature << "_final.csv";
		// save the histograms
		saveHistograme(ps, maxItr / 100, ss.str().c_str());
		collectParticleSystemStatistics(ps, appConfig, systemStatsFile);

		// display the final energy stats for the system
		// save the results of the system
		stringstream results_ss;
		results_ss << "result_" << temperature << ".txt";

		ofstream resultsOut(results_ss.str().c_str());

		resultsOut << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;
		resultsOut << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << endl;

		resultsOut << "Monomer Movement Stepsize: " << translationStepSize << endl;
		resultsOut << "Monomer Rotation Stepsize: " << rotationStepSize << endl;
		resultsOut << "Dopant Movement Stepsize:  " << dopantStepSize << endl;
		resultsOut << "  Volume Change Stepsize:  " << volumeScaleFactor << endl;

		// display final system stats
		DATA_TYPE vectorOrder = 0.0, orientationOrder = 0.0;
		calculateOrderParameters(ps, vectorOrder, orientationOrder);
		resultsOut << "     Binding Energy: " << totInter / (float)(ps.nMonomers / ps.monomers_per_chain) << " Ha" << endl;
		resultsOut << " Total Intra Energy: " << totIntra << " Ha\t" << "EV: " << totIntra * EVOLTS_PER_HARTREE << endl;
		resultsOut << " Total Inter Energy: " << totInter << " Ha\t" << "EV: " << totInter * EVOLTS_PER_HARTREE << endl;
		resultsOut << "Total System Energy: " << (totIntra + totInter) << " Ha\t" << "EV: " << (totIntra + totInter) * EVOLTS_PER_HARTREE << endl;

		resultsOut << " Running Intra Energy: " << rIntraMean << " Ha\t" << "EV: " << rIntraMean * EVOLTS_PER_HARTREE << endl;
		resultsOut << " Running Inter Energy: " << rInterMean << " Ha\t" << "EV: " << rInterMean * EVOLTS_PER_HARTREE << endl;
		resultsOut << "Running System Energy: " << (rIntraMean + rInterMean) << " Ha\t" << "EV: " << (rIntraMean + rInterMean) * EVOLTS_PER_HARTREE << endl;
		resultsOut << "Running System Energy Std. Dev.: " << sqrt(rTotalSumsqrd / (kInter)) << endl;

		resultsOut << "            Density: " << ps.density << endl;
		resultsOut << " Radius of Gyration: " << calculateRadiusOfGyration(ps) << endl;
		resultsOut << "     Polymer Length: " << averagePolymerLength(ps) << endl;
		resultsOut << "       Vector Order: " << vectorOrder << endl;
		resultsOut << "  Orientation Order: " << orientationOrder << endl;

		resultsOut.flush();
		resultsOut.close();

		results.inter = totInter;
		results.intra = totIntra;
		results.total = totIntra + totInter;
		results.enthalpy = enthalpy;
		results.intraMean = rIntraMean;
		results.intraStdDev = sqrt(rIntraSumsqrd / kInter);
		results.interMean = rInterMean;
		results.interStdDev = sqrt(rInterSumsqrd / kInter);
		results.totalMean = rTotalMean;
		results.totalStdDev = sqrt(rTotalSumsqrd / (kInter));
		results.enthalpyMean = rEnthalpyMean;
		results.enthalpyStdDev = sqrt(rEnthalpySumsqrd / kInter);
		results.monomerStepSize = translationStepSize;
		results.dopantStepSize = dopantStepSize;
		results.volumeChangeFactor = volumeScaleFactor;
		results.density = ps.density;
		results.densityMean = rDensityMean;
		results.densityStdDev = sqrt(rDensitySumsqrd / kDensity);
	}

	return results;
}

void generateInitialGofr(PARTICLE_SYSTEM & ps, const AppConfig & appConfig, DATA_TYPE temperature)
{
	DATA_TYPE gofrDR = appConfig.g_of_r_dr;
	int nBins = (ps.r_cut / gofrDR) + 1;
	initializeGOFR(nBins);

	collectGofR(ps, gofrMonomerCM, gofrMonomer, gofrDopant, gofrAll, 1, nBins, gofrDR);
	// save the gofr to file
	stringstream ss0;
	ss0 << "gor_" << temperature << "_init.csv";
	generateGofR(ps, ss0.str().c_str(), gofrMonomerCM, gofrMonomer, gofrDopant, gofrAll, 1, nBins, gofrDR);
}

void temperingLoop(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, int simType, DATA_TYPE temperature, int maxItr, int nprc, int myid, DATA_TYPE npt_pressure)
{
	ofstream resultsOut("tempering_events.csv");
	resultsOut <<"tempering_event,temperature,avg_intra,intra_std_dev,avg_inter,inter_std_dev,avg_tot_pot,pe_std_dev,avg_enthalpy,en_std_dev,x_size,y_size,z_size,avg_density,density_std_dev,volume" << endl;

	DATA_TYPE xSig = appConfig.atmc_xsig;
	DATA_TYPE dtMin = appConfig.atmc_dt_min;
	DATA_TYPE dtMax = appConfig.atmc_dt_max;
	DATA_TYPE tMin = appConfig.atmc_temp_min;
	DATA_TYPE tMax = appConfig.atmc_temp_max;
	int mItr = maxItr;
	int itr = 0;
	DATA_TYPE ptemp = temperature;

	while (temperature > tMin)
	{
		// insert temperature and iteration record break into detailed energy log file
		logFile << "tempering-event:" << itr << ":tempering-temperature:" << temperature << endl;
		// insert temperature and iteration record break into detailed energy log file
		systemStatsFile << "tempering-event:" << itr << ":tempering-temperature:" << temperature << endl;

		SIMULATION_RESULTS results = tempering(ps, mp, appConfig, simType, temperature, mItr, nprc, myid, npt_pressure);
		appConfig.dopant_step_size = results.dopantStepSize;
		appConfig.monomer_step_size = results.monomerStepSize;
		appConfig.volume_change_step_size = results.volumeChangeFactor;

		// write tempering results
		resultsOut << itr << ',' << ptemp << ',' << results.intraMean << ',' << results.intraStdDev << ',' << results.interMean << ',' << results.interStdDev << ',' << results.totalMean << ',' << results.totalStdDev << ',' << results.enthalpyMean << ',' << results.enthalpyStdDev << ',' << ps.x_size << ',' << ps.y_size << ',' << ps.z_size << ',' << results.densityMean << ',' << results.densityStdDev << ','<< std::setprecision(15) << ANG_TO_BOHR(ps.x_size) * ANG_TO_BOHR(ps.y_size) * ANG_TO_BOHR(ps.z_size) << endl;

		if (temperature > tMax)
			temperature = tMax;

		if (itr > 1000000)
		{
			cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
			cout << "!!!!!!!!!!!!!!!!!! Exceeded maximum number of iterations !!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
			cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;

			break;
		}

		ptemp = temperature;
		itr++;
	}

	resultsOut.flush();
	resultsOut.close();
}

SIMULATION_RESULTS tempering(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, int simType, DATA_TYPE & temperature, int maxItr, int nprc, int myid, DATA_TYPE npt_pressure)
{
	DATA_TYPE tStart = temperature;
	DATA_TYPE xSig = appConfig.atmc_xsig;
	DATA_TYPE dtMin = appConfig.atmc_dt_min / KEL_PER_HAR;
	DATA_TYPE dtMax = appConfig.atmc_dt_max / KEL_PER_HAR;
	DATA_TYPE tMin = appConfig.atmc_temp_min / KEL_PER_HAR;
	DATA_TYPE tMax = appConfig.atmc_temp_max / KEL_PER_HAR;

	DATA_TYPE beta = 1.0 / (temperature / KEL_PER_HAR);

	SIMULATION_RESULTS results;
	if (simType == 10)
		results = SMMC_NVT(ps, mp, appConfig, temperature, maxItr, nprc, myid);
	else
		results = SMMC_NPT(ps, mp, appConfig, temperature, maxItr, nprc, myid, npt_pressure);

	temperature /= KEL_PER_HAR;

	DATA_TYPE totInst = results.total;
	DATA_TYPE totMean = results.totalMean;
	DATA_TYPE totStdDev = results.totalStdDev;

	if (simType == 11)
	{
		totInst = results.enthalpy;
		totMean = results.enthalpyMean;
		totStdDev = results.enthalpyStdDev;
	}

	DATA_TYPE offset = totInst - totMean;
	DATA_TYPE delBeta = xSig / totStdDev;
	// new temperature
	DATA_TYPE tPlus = 1.0 / (beta + delBeta);

	if (temperature - tPlus < dtMin)
	{
		tPlus = temperature - dtMin;
	}
	else if (temperature - tPlus > dtMax)
	{
		tPlus = temperature - dtMax;
	}

	DATA_TYPE tMinus = 1.0 / (beta - delBeta);
	if (tMinus - temperature < dtMin)
	{
		tMinus = temperature + dtMin;
	}
	else if (tMinus - temperature > dtMax)
	{
		tMinus = temperature + dtMax;
	}

	tMinus = MIN(tMinus, tMax);

	// new beta values
	DATA_TYPE delBetaPlus = 1.0 / tPlus - beta;
	DATA_TYPE delBetaMinus = 1.0 / tMinus - beta;

	// get random number for temperautre environment
	DATA_TYPE slr = log(Random::getInstance().randomDouble()); 
	if (offset < 0.0)
	{
		if (slr < -delBetaMinus * offset)
		{
			temperature = tMinus;
		}
		else
		{
			temperature = tPlus;
		}
	}
	else if (offset > 0.0)
	{
		if (slr < -delBetaPlus * offset)
		{
			temperature = tPlus;
		}
		else
		{
			temperature = tMinus;
		}
	}

	temperature = int(temperature / dtMin) * dtMin;

	temperature *= KEL_PER_HAR;

	if (temperature == 700)
		int a = 1;

	return results;
}

void calculateBulkModulus(PARTICLE_SYSTEM & ps, ModelPotential & mp, AppConfig & appConfig, int maxItr, int nprc, int myid)
{
	int numDensities = 500;
	DATA_TYPE upypy = 0.0, upydop = 0.0, udopdop = 0.0, ucoul = 0.0, uother = 0.0;
	DATA_TYPE shared[6];
	memset(shared, 0, sizeof(DATA_TYPE) * 6);
	DATA_TYPE reduced[6];
	memset(reduced, 0, sizeof(DATA_TYPE) * 6);

	DATA_TYPE intra = 0.0, inter = 0.0;
	DATA_TYPE volumeScaleFactor = 0.0001;
	DATA_TYPE virialPressure = 0.0;

	DATA_TYPE *prevSystemScaleFactor = new DATA_TYPE[4];
	memset(prevSystemScaleFactor, 0, sizeof(DATA_TYPE) * 4);
	DATA_TYPE* systemScaleFactor = new DATA_TYPE[3];
	systemScaleFactor[0] = 1.0;
	systemScaleFactor[1] = 1.0;
	systemScaleFactor[2] = 1.0;
	memset(prevSystemScaleFactor, 0, sizeof(DATA_TYPE) * 3);
	bool systemScaleDimension[] = { appConfig.scale_x, appConfig.scale_y, appConfig.scale_z };
	bool updateCutoff = true;
	if (appConfig.r_cut != -1.0)
		updateCutoff = false;


	int lb = 0;
	int ub = ps.nMonomers;
	int dlb = 0;
	int dub = ps.nDopants;

	COPROCESSOR_WORKUNIT* workunits = NULL;
	int nworkunits = 0;
#if defined __USE_GPU__
	setDevice(0);

	int numGPU = nworkunits = 1;

	if (appConfig.multiple_gpu)
	{
		numGPU = listDevices();

		// at the moment assume work is to be spread across all available gpus
		// create an array to contain the coprocessor work units
		nworkunits = numGPU;
	}

	workunits = new COPROCESSOR_WORKUNIT[nworkunits];
	for (int i = 0; i < nworkunits; i++)
	{
		COPROCESSOR_WORKUNIT* workunit = new COPROCESSOR_WORKUNIT(myid, i);
		// calculate the bounds for iterating over polymers
		int numPolymers = ps.nMonomers / ps.monomers_per_chain;
		int monomersPerNode = (numPolymers / nworkunits) * ps.monomers_per_chain;
		workunit->mlb = monomersPerNode * i;
		workunit->mub = workunit->mlb + monomersPerNode;
		int dopantsPerNode = ps.nDopants / nworkunits;
		workunit->dlb = dopantsPerNode * i;
		workunit->dub = workunit->dlb + dopantsPerNode;

		if (i == nworkunits - 1)
		{
			workunit->mub = ps.nMonomers;
			workunit->dub = ps.nDopants;
		}

		cout << " WorkUnit[" << i << "] is assigned monomers: " << workunit->mlb << " -> " << workunit->mub << endl;
		cout << " WorkUnit[" << i << "] is assigned dopants: " << workunit->dlb << " -> " << workunit->dub << endl;


		workunit->coprocessor_id = i;
		if (appConfig.gpu_id > -1 && nworkunits < appConfig.gpu_id)
			workunit->coprocessor_id = appConfig.gpu_id;

		workunit->nodeId = myid;
		workunit->upypy = 0.0;
		workunit->upydop = 0.0;
		workunit->udopdop = 0.0;

		workunits[i] = *workunit;

		// initialize the GPU with the data structures and constants
		workunits[i].initGPUDataStructures(ps, mp.getInterpolationParameters(), mp.getNumInterpolationRows(), mp.getNumInterpolationCols(),
			mp.getInterpolationDelta(), mp.getInterpolationOffset(), mp.getInterpolationOrder(), mp.getInterpolationCoeffOffset(),
			appConfig.number_gpu_threads_mon, appConfig.number_gpu_threads_dop);

		setModelConstants(appConfig.use_pcb, mp.getSigma_Dopant(), mp.getQ_Dop(), mp.getEpsilon_Dopant(), mp.getKappa(),
			ps.x_size, ps.y_size, ps.z_size, ps.r_cut, mp.getQ_Mono(), mp.getSigma_Inter(),
			mp.getSigma_inter_End(), mp.getEpsilon_Inter(), mp.getEpsilon_Inter_End(), mp.getDpMag(),
			mp.getA(), mp.getB(), mp.getC(), mp.getSigmaPyDopant(), workunits[i]);
	}
#endif
	


	stringstream ssTemperature;
	ssTemperature << "bulk_modulus_working" << ".xyz";

	// scale the system
	systemScaleFactor = scaleSystem(ps, systemScaleDimension, -volumeScaleFactor * numDensities * 0.5, false);

	// attempt volume change
	systemScaleFactor = scaleSystem(ps, systemScaleDimension, volumeScaleFactor, updateCutoff);
#if defined __USE_GPU__
		int i = 0;
#pragma omp parallel for private(i)
		for (i = 0; i < nworkunits; i++)
		{
			setDevice(workunits[i].coprocessor_id);
			deviceScaleSystem(ps, workunits[i], systemScaleFactor, updateCutoff);
#if defined __USE_SIMPLE_POLYNOMIAL__
			workunits[i].updateInterpolationparemeters(ps, mp.getInterpolationParameters(), mp.getNumInterpolationRows());
#endif
		}

#endif


	saveParticleSystemCM(ps, ssTemperature.str().c_str(), std::ofstream::app);

	ofstream out;
	out.open("energy-volume.csv");
	out << "totIntra,totInter,total,x_size,y_size,z_size, volume, density" << endl;

	for (int i = 0; i < numDensities; i++)
	{
		DATA_TYPE fEnergy = 0.0;
		DATA_TYPE totIntra = mp.TotalIntra(ps, logFile, true);
		DATA_TYPE totInter = mp.TotalInter(ps, lb, ub, dlb, dub, 0, NULL, upypy, upydop, udopdop, ucoul, uother);

		if (myid == 0) 
		{
			totInter = reduced[0];
			out <<  HA_TO_EV(totIntra) / (float)ps.nPolymers << ',' << std::setprecision(15) <<  HA_TO_EV(totInter) / (float)(ps.nMonomers + ps.nDopants) << ',' << std::setprecision(15) <<  HA_TO_EV(totIntra + totInter) / (float)(ps.nMonomers + ps.nDopants) << ',' << ps.x_size << ',' << ps.y_size << ',' << ps.z_size << ',' << (ps.x_size * ps.y_size * ps.z_size) << ',' << ps.density << endl;
			cout << HA_TO_EV(totIntra) / (float)ps.nPolymers << ',' << std::setprecision(15) <<  HA_TO_EV(totInter) / (float)(ps.nMonomers + ps.nDopants) << ',' << std::setprecision(15) <<  HA_TO_EV(totIntra + totInter) / (float)(ps.nMonomers + ps.nDopants) << ',' << ps.x_size << ',' << ps.y_size << ',' << ps.z_size << ',' << (ps.x_size * ps.y_size * ps.z_size) << ',' << ps.density << endl;
		}

		// scale the system
		systemScaleFactor = scaleSystem(ps, systemScaleDimension, volumeScaleFactor, false);
#if defined __USE_GPU__
		int ii = 0;
#pragma omp parallel for private(i)
		for (ii = 0; ii < nworkunits; ii++)
		{
			setDevice(workunits[ii].coprocessor_id);
			deviceScaleSystem(ps, workunits[ii], systemScaleFactor, updateCutoff);
#if defined __USE_SIMPLE_POLYNOMIAL__
			workunits[ii].updateInterpolationparemeters(ps, mp.getInterpolationParameters(), mp.getNumInterpolationRows());
#endif
		}

#endif
	}
	out.flush();
	out.close();
}

DATA_TYPE VolumeChangeEnergy(PARTICLE_SYSTEM & ps, ModelPotential & mp, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT * workunits, int nodeId)
{
	DATA_TYPE upypy = 0.0, upydop = 0.0, udopdop = 0.0, ucoul = 0.0, uother = 0.0, t_intra_ucoul = 0.0;
	DATA_TYPE inter = mp.TotalInter(ps, mlb, mub, dlb, dub, nworkunits, workunits, upypy, upydop, udopdop, ucoul, uother);

	DATA_TYPE intra_ucoul = 0.0;
	int midx = 0;
	int monomers_per_chain = mp.getMonomersPerChain();
#pragma omp parallel for reduction(+:intra_ucoul) private(midx)
	for (midx = 0; midx < ps.nMonomers; midx = midx + monomers_per_chain)
	{
		mp.UcoulombPart(ps, midx, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], intra_ucoul, t_intra_ucoul);
	}

	return (inter + intra_ucoul);
}

void optimizeLatticeParameters(AppConfig& appConfig, ModelPotential& mp)
{
	ofstream resultsFile;
	resultsFile.open("results.csv");

	bool addDopants = false;
	if (appConfig.dopants_per_chain)
		addDopants = true;

	DATA_TYPE orig_x = appConfig.x_off;
	DATA_TYPE orig_y = appConfig.y_off;
	DATA_TYPE orig_z = appConfig.z_off;

	DATA_TYPE min_x = appConfig.x_off;
	DATA_TYPE min_y = appConfig.y_off;
	DATA_TYPE min_z = appConfig.z_off;
	DATA_TYPE cEnergy = 0.0, pEnergy = 0.0, minEnergy = 0.0;
	DATA_TYPE delta = 0.0001;
	DATA_TYPE pressure = 0.0;
	int num_data_points = 20;
	int ctr = 0;

	appConfig.x_off -= (delta * num_data_points * 0.5);
	resultsFile << "X lattice optimization" << endl;

	while (ctr < num_data_points)
	{

		PARTICLE_SYSTEM * ps = buildParticleSystem(appConfig, appConfig.x_rep, appConfig.y_rep, appConfig.z_rep, addDopants);
		cEnergy = VolumeChangeEnergy(*ps, mp, 0, ps->nMonomers, 0, ps->nDopants, 0, NULL, 0);

		ps->cleanUp();
		delete ps;
		ps = NULL;

		resultsFile << appConfig.x_off * 2 << ',' << appConfig.y_off << ',' << appConfig.z_off << ',' << appConfig.z_off / 2.0 << ',' << appConfig.x_off << ',' << cEnergy << endl;

		if (cEnergy < minEnergy)
		{
			min_x = appConfig.x_off;
			minEnergy = cEnergy;
		}

		appConfig.x_off += delta;

		ctr++;
	}
	cout << "min x: " << min_x << "\tmin energy: " << minEnergy << endl;

	resultsFile << "Y lattice optimization" << endl;
	ctr = 0;
	minEnergy = 0.0;
	appConfig.x_off = orig_x;
	appConfig.y_off -= (delta * num_data_points * 0.5);

	while (ctr < num_data_points)
	{

		PARTICLE_SYSTEM* ps = buildParticleSystem(appConfig, appConfig.x_rep, appConfig.y_rep, appConfig.z_rep, addDopants);
		cEnergy = VolumeChangeEnergy(*ps, mp, 0, ps->nMonomers, 0, ps->nDopants, 0, NULL, 0);

		ps->cleanUp();
		delete ps;
		ps = NULL;

		resultsFile << appConfig.x_off * 2 << ',' << appConfig.y_off << ',' << appConfig.z_off << ',' << appConfig.z_off / 2.0 << ',' << appConfig.x_off << ',' << cEnergy << endl;

		if (cEnergy < minEnergy)
		{
			min_y = appConfig.y_off;
			minEnergy = cEnergy;
		}

		appConfig.y_off += delta;

		ctr++;
	}

	cout << "min y: " << min_y << "\tmin energy: " << minEnergy << endl;
	resultsFile << "Z lattice optimization" << endl;
	ctr = 0;
	minEnergy = 0.0;
	appConfig.x_off = orig_x;
	appConfig.y_off = orig_y;
	appConfig.z_off -= (delta * num_data_points * 0.5);

	while (ctr < num_data_points)
	{

		PARTICLE_SYSTEM* ps = buildParticleSystem(appConfig, appConfig.x_rep, appConfig.y_rep, appConfig.z_rep, addDopants);
		cEnergy = VolumeChangeEnergy(*ps, mp, 0, ps->nMonomers, 0, ps->nDopants, 0, NULL, 0);

		ps->cleanUp();
		delete ps;
		ps = NULL;

		resultsFile << appConfig.x_off * 2 << ',' << appConfig.y_off << ',' << appConfig.z_off << ',' << appConfig.z_off / 2.0 << ',' << appConfig.x_off << ',' << cEnergy << endl;

		if (cEnergy < minEnergy)
		{
			min_z = appConfig.z_off;
			minEnergy = cEnergy;
		}

		appConfig.z_off += delta;

		ctr++;
	}

	cout << "min z: " << min_z << "\tmin energy: " << minEnergy << endl;
	cout << "new lattice parameters: " << min_x << '\t' << min_y << '\t' << min_z << endl;


	resultsFile.flush();
	resultsFile.close();
}

void showHelp()
{
	cout << "Options:" << endl;
	cout << "\t -n -> number of processors: -n 1" << endl;
	cout << "\t -t -> temperature of the system for the simulation, needs to be in degree kelvin: -t 300" << endl;
	cout << "\t -c -> configuration file (optional, it will default to appConfig.conf: -c appConfig.conf)" << endl;
	cout << "\t -f -> initial configuration file (optional): -f starting_point.bin" << endl;
	cout << "\t -s -> simulation type: 0 - NVT, 1 - NPT, 10 - Tempering NVT, 11 - Tempering NPT: -s 0" << endl;
	cout << "\t -p -> pressure of the system in atmospheres, only used in NPT related simulations: -p 1" << endl;
	cout << "\t --ct -> 2 crystal structures currently supported.  FCC and Renne; --ct fcc" << endl;
	cout << "\t --ss -> 1 - scale system, 0 - do not scale system (default)" << endl;
	cout << "\t --sf -> percentage to scale the x, y, and z dimensions of the volume" << endl;
	cout << "Example: mpirun -n 1 ./cuSMMC -n 1 -t 300 -p 1 -s 1 -ct fcc -c appConfig.conf" << endl;
	exit(0);
}

int listDevices()
{
	int deviceCount = 0;
	cudaError_t error_id = cudaGetDeviceCount(&deviceCount);

	if (error_id != cudaSuccess)
	{
		printf("cudaGetDeviceCount returned %d\n-> %s\n", (int)error_id, cudaGetErrorString(error_id));
		printf("Result = FAIL\n");
		exit(EXIT_FAILURE);
	}

	// This function call returns 0 if there are no CUDA capable devices.
	if (deviceCount == 0)
	{
		printf("There are no available device(s) that support CUDA\n");
	}
	else
	{
		printf("Detected %d CUDA Capable device(s)\n", deviceCount);
	}

	int dev, driverVersion = 0, runtimeVersion = 0;

	for (dev = 0; dev < deviceCount; ++dev)
	{
		cudaSetDevice(dev);
		cudaDeviceProp deviceProp;
		cudaGetDeviceProperties(&deviceProp, dev);

		printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);

		// Console log
		cudaDriverGetVersion(&driverVersion);
		cudaRuntimeGetVersion(&runtimeVersion);
		printf("  CUDA Driver Version / Runtime Version          %d.%d / %d.%d\n", driverVersion / 1000, (driverVersion % 100) / 10, runtimeVersion / 1000, (runtimeVersion % 100) / 10);
		printf("  CUDA Capability Major/Minor version number:    %d.%d\n", deviceProp.major, deviceProp.minor);
	}

	return deviceCount;
}