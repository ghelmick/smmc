#if !defined (__UTIL_H__)
#define       __UTIL_H__

#include <string>
#include <vector>
#include <ios>
#include <iostream>
#include <random>
#include <fstream>
#include <cmath>
#include <mpi.h>
#if defined __USE_SIMPLE_POLYNOMIAL__
#include "Eigen/QR"
#endif

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::ofstream;

#if defined __USE_FLOAT__
#define DATA_TYPE float
#define MPI_DATA_TYPE MPI_FLOAT
#define CUDA_PAIR float2
#define CUDA_TRIPLET float3
#else
#define DATA_TYPE double
#define MPI_DATA_TYPE MPI_DOUBLE
#define CUDA_PAIR double2
#define CUDA_TRIPLET double3
#endif

#define BUFFER_FACTOR 2

#define K_ .00008617332478
#define AVOGADRO_NUM 6.02214129e23
#define EVOLTS_PER_HARTREE 27.21138505
#define PI                 3.14159265358979323846
#define G				   9.8067
#define DEG_TO_RAD(angle)  (angle * PI / 180.0)
#define RAD_TO_DEG(radian) (radian * 180.0 / PI)
#define SIGN(a) ((a < 0.0f) ? -1.0 : 1.0)
#define SIGN2(a, b) ((b > 0.0) ? abs(a) : -abs(a))
#define SQUARE(X) X * X
#define CUBE(X) (X * X * X)
#define MAGNITUDE(X, Y, Z) sqrt(SQUARE(X) + SQUARE(Y) + SQUARE(Z))
#define DOT_PRODUCT(X1, Y1, Z1, X2, Y2, Z2) (X1*X2 + Y1*Y2 + Z1*Z2)
#define COS_SQRD(THETA) (0.5 + 0.5 * cos(2.0 *THETA))
//#define NINT(a) ((a) >= 0.0 ? (int)((a)+0.5) : (int)((a)-0.5))
#define NINT(a) (round(a))
#define MAX(a,b) ((a > b) ? a : b)
#define MIN(a,b) ((a < b) ? a : b)
#define TO_POW_2(X) (X * X)
#define TO_POW_3(X) (X * X * X)
#define TO_POW_6(X) (X * X * X * X * X * X)
#define HA_TO_EV(X) (X * EVOLTS_PER_HARTREE)
#define EV_TO_HA(X) (X / EVOLTS_PER_HARTREE)
#define DEBEYE_TO_HA(X) (X * 0.393430307)
#define A_0 0.52917720859
#define A_0_INV 1.889726132885643067222711130708
#define KEL_PER_HAR 315775.13
#define ANG_TO_BOHR(X) (X * A_0_INV)
#define BOHR_TO_ANG(X) (X * A_0)
#define NUM_BINS 180
#define ATM_TO_PA(X) (X * 101325)        // Pa
#define ATM_TO_GPA(X) (X * 0.000101325)        // GPa
#define GPA_TO_ATM(X) ( X / 0.000101325)
#define GPA_TO_ATU(X) (X / 29421.02648438959)  // Ha/Bohr^3
#define ATU_TO_GPA(X) (X * 29421.02648438959)
#define HA_PER_BOHR_TO_EV_PER_ANG(X) (X * 51.42208619083232)
#define EV_PER_ANG_TO_NEWTON(X) (X * 1.60217662e-9)
#define PCB_(SIZE, X) (X - SIZE * NINT(X / SIZE))  // only usable if system origin is the lower left corner of the box

inline void crossProduct(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1,
	                     DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2,
	                     DATA_TYPE &cx, DATA_TYPE & cy, DATA_TYPE & cz)
{
	cx = y1 * z2 - z1 * y2;
	cy = z1 * x2 - x1 * z2;
	cz = x1 * y2 - y1 * x2;
}

inline DATA_TYPE dotProduct(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1,
	DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2)
{
	return (x1 * x2 + y1 * y2 + z1 * z2);
}

inline DATA_TYPE dist(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1, DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2)
{
	DATA_TYPE xdif = x1 - x2;
	DATA_TYPE ydif = y1 - y2;
	DATA_TYPE zdif = z1 - z2;

	return sqrt(xdif * xdif + ydif * ydif + zdif * zdif);
}

inline void normalize(DATA_TYPE x, DATA_TYPE y, DATA_TYPE z, DATA_TYPE & nx, DATA_TYPE & ny, DATA_TYPE & nz)
{
	DATA_TYPE mag = MAGNITUDE(x, y, z);
	DATA_TYPE mag_inv = 1.0 / mag;
	if (mag > 0.0)
	{
		nx = x * mag_inv;
		ny = y * mag_inv;
		nz = z * mag_inv;
	}
	else
	{
		nx = 0.0;
		ny = 0.0;
		nz = 0.0;
	}
}

inline DATA_TYPE angleBetween2Vectors(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1,
	DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2)
{
	if (x1 == x2 && y1 == y2 && z1 == z2)
		return 0.0;
	if (x1 == 0 && y1 == 0 && z1 == 0)
		return 0.0;
	if (x2 == 0 && y2 == 0 && z2 == 0)
		return 0.0;

	DATA_TYPE dp = dotProduct(x1, y1, z1, x2, y2, z2) / (MAGNITUDE(x1, y1, z1) * MAGNITUDE(x2, y2, z2));
	if (fabs(dp) > 1.0)
		dp = (int)dp;

	DATA_TYPE angle = acos(dp);

	return angle;
}

inline vector<vector<DATA_TYPE> > createHistogram(DATA_TYPE * values, int numValues, int numBins)
{
	if (values == NULL)
		return vector<vector<DATA_TYPE> >();

	vector<vector<DATA_TYPE> > bins(numBins);
	DATA_TYPE maxV = 0.0, minV = 0.0, dt = 0.0;
	maxV = minV = values[0];

	for (int i = 0; i < numValues; i++)
	{
		if (values[i] < minV)
			minV = values[i];
		if (values[i] > maxV)
			maxV = values[i];
	}
	dt = (ceil(maxV) - floor(minV)) / numBins;

	int bidx = 0;

	for (int i = 0; i < numValues; i++)
	{
		bidx = (int)(values[i] / dt);
		if (bidx >= numBins)
			continue;
		bins[bidx].push_back(values[i]);
	}

	return bins;
}

inline vector<vector<DATA_TYPE> > createHistogram(DATA_TYPE * values, int numValues, int numBins, float min, float max)
{
	if (values == NULL)
		return vector<vector<DATA_TYPE> >();

	vector<vector<DATA_TYPE> > bins(numBins);
	DATA_TYPE dt = 0.0;
	dt = (ceil(max) - floor(min)) / numBins;

	int bidx = 0;

	for (int i = 0; i < numValues; i++)
	{
		bidx = (int)(values[i] / dt);
		if (bidx >= numBins)
			continue;
		bins[bidx].push_back(values[i]);
	}

	return bins;
}

inline void Tokenizer(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);

	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos)
	{
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));

		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);

		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}
}

// function to find factorial of given number 
inline unsigned long factorial(unsigned long n)
{
	if (n == 0)
		return 1;
	return n * factorial(n - 1);
}

inline DATA_TYPE polyVal(DATA_TYPE x, DATA_TYPE* coeffs, int pidx, int nCoeffs, int nInterpolationRows)
{
#if defined __USE_SIMPLE_POLYNOMIAL__
	// this evaluates a simply polynomial of order nCoeffs
	DATA_TYPE x1 = coeffs[pidx];
	DATA_TYPE x2 = coeffs[1 * nInterpolationRows + pidx];
	DATA_TYPE x3 = coeffs[2 * nInterpolationRows + pidx];
	DATA_TYPE x4 = coeffs[3 * nInterpolationRows + pidx];
	DATA_TYPE x5 = coeffs[4 * nInterpolationRows + pidx];
	DATA_TYPE x6 = coeffs[5 * nInterpolationRows + pidx];
	DATA_TYPE x7 = coeffs[6 * nInterpolationRows + pidx];
	DATA_TYPE xsqrd = x * x;

	return (x1 + x2 * x + x3 * xsqrd + x4 * xsqrd * x + x5 * xsqrd * xsqrd + x6 * x * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#else
	// this evaluates a hermite polynomial
	DATA_TYPE x2 = x * 2, tmp = 0.0, retVal = 0.0;
	int nd = nCoeffs;
	DATA_TYPE c0 = coeffs[pidx + 2 * nInterpolationRows];
	DATA_TYPE c1 = coeffs[pidx + 3 * nInterpolationRows];
	DATA_TYPE c2 = coeffs[pidx + 1 * nInterpolationRows];
	DATA_TYPE c3 = coeffs[pidx];

	tmp = c0;
	nd = nd - 1;
	c0 = c2 - c1 * (2.0 * (nd - 1));
	c1 = tmp + c1 * x2;

	tmp = c0;
	nd = nd - 1;
	c0 = c3 - c1 * (2.0 * (nd - 1));
	c1 = tmp + c1 * x2;

	retVal = c0 + c1 * x2;

	return retVal;
#endif
}

inline void polyfit(const std::vector<DATA_TYPE>& xv, const std::vector<DATA_TYPE>& yv, std::vector<DATA_TYPE>& coeff, int order)
{
#if defined __USE_SIMPLE_POLYNOMIAL__
	Eigen::MatrixXd A(xv.size(), order + 1);
	Eigen::VectorXd yv_mapped = Eigen::VectorXd::Map(&yv.front(), yv.size());
	Eigen::VectorXd result;

	assert(xv.size() == yv.size());
	assert(xv.size() >= order + 1);

	// create matrix
	for (size_t i = 0; i < xv.size(); i++)
		for (size_t j = 0; j < order + 1; j++)
			A(i, j) = pow(xv.at(i), j);

	// solve for linear least squares fit
	result = A.householderQr().solve(yv_mapped);

	coeff.resize(order + 1);
	for (size_t i = 0; i < order + 1; i++)
		coeff[i] = result[i];
#endif
}

/*
   Taken from: http://paulbourke.net/miscellaneous/interpolation/
   Tension: 1 is high, 0 normal, -1 is low
   Bias: 0 is even,
		 positive is towards first segment,
		 negative towards the other
*/
inline DATA_TYPE hermiteInterpolation(DATA_TYPE *y, DATA_TYPE x, DATA_TYPE bias=0, DATA_TYPE tension=0)
{
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;

	mu2 = x * x;
	mu3 = mu2 * x;
	m0 = (y[1] - y[0]) * (1 + bias) * (1 - tension) / 2;
	m0 += (y[2] - y[1]) * (1 - bias) * (1 - tension) / 2;
	m1 = (y[2] - y[1]) * (1 + bias) * (1 - tension) / 2;
	m1 += (y[3] - y[2]) * (1 - bias) * (1 - tension) / 2;
	a0 = 2 * mu3 - 3 * mu2 + 1;
	a1 = mu3 - 2 * mu2 + x;
	a2 = mu3 - mu2;
	a3 = -2 * mu3 + 3 * mu2;

	return(a0 * y[1] + a1 * m0 + a2 * m1 + a3 * y[2]);
}

/*
   Taken from: http://paulbourke.net/miscellaneous/interpolation/
*/
inline DATA_TYPE cubicInterpolation(DATA_TYPE* y, DATA_TYPE x)
{
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0, mu2 = 0.0;

	mu2 = x * x;
	a0 = y[3] - y[2] - y[0] + y[1];
	a1 = y[0] - y[1] - a0;
	a2 = y[2] - y[0];
	a3 = y[1];

	return(a0 * x * mu2 + a1 * mu2 + a2 * x + a3);
}

/*
   Taken from: http://paulbourke.net/miscellaneous/interpolation/
*/
inline DATA_TYPE catmullRomSplineInterpolation(DATA_TYPE* y, DATA_TYPE x)
{
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0, mu2 = 0.0;

	mu2 = x * x;
	a0 = -0.5 * y[0] + 1.5 * y[1] - 1.5 * y[2] + 0.5 * y[3];
	a1 = y[0] - 2.5 * y[1] + 2 * y[2] - 0.5 * y[3];
	a2 = -0.5 * y[0] + 0.5 * y[2];
	a3 = y[1];

	return(a0 * x * mu2 + a1 * mu2 + a2 * x + a3);
}

/*
* Taken from Numerical Recipes book
Given arrays x[1..n] and y[1..n] containing a tabulated function, i.e., yi = f(xi), with
x1 < x2 < ... < xN, and given values yp1and ypn for the first derivative of the interpolating
	function at points 1 and n, respectively, this routine returns an array y2[1..n] that contains
	the second derivatives of the interpolating function at the tabulated points xi.If yp1and /or
	ypn are equal to 1 � 1030 or larger, the routine is signaled to set the corresponding boundary
	condition for a natural spline, with zero second derivative on that boundary.
*/
inline void spline(DATA_TYPE x[], DATA_TYPE y[], int n, DATA_TYPE y2[])
{
	// calculate the first derivates for the 1st and last point
	DATA_TYPE h = x[1] - x[0];

	DATA_TYPE yp1 = (-y[4] + 8.0 * y[3] - 8.0 * y[1] + y[0]) / (12.0 * h);
	DATA_TYPE ypn = (-y[4] + 16.0 * y[3] - 30.0 * y[2] + 16.0 * y[1] - y[0]) / (12.0 * h * h);

	int i, k;
	DATA_TYPE p, qn, sig, un;
	/*
	The lower boundary condition is set either to be �natural y2[1] = u[1] = 0.0; ural� or else to have a specified first derivative.
	*/
	//u=vector(1,n-1);
	DATA_TYPE* u = (DATA_TYPE *)calloc(n, sizeof(DATA_TYPE));
	if (yp1 > 0.99e30)
	{
		y2[0] = u[0] = 0.0;
	}
	else 
	{
		y2[0] = -0.5;
		u[0] = (3.0 / (x[1] - x[0])) * ((y[1] - y[0]) / (x[1] - x[0]) - yp1);
	}
	/*
		This is the decomposition loop of the tridiagonal algorithm.y2and u are used for temporary storage of the decomposed factors.
		The upper boundary condition is set either to be  �natural� or else to have a specified first derivative.
	*/
	for (i = 1; i < n; i++) 
	{	
		sig = (x[i] - x[i - 1]) / (x[i + 1] - x[i - 1]);
		p = sig * y2[i - 1] + 2.0;
		y2[i] = (sig - 1.0) / p;
		u[i] = (y[i + 1] - y[i]) / (x[i + 1] - x[i]) - (y[i] - y[i - 1]) / (x[i] - x[i - 1]);
		u[i] = (6.0 * u[i] / (x[i + 1] - x[i - 1]) - sig * u[i - 1]) / p;
	}
	if (ypn > 0.99e30)
	{
		qn = un = 0.0;
	}
	else 
	{	
		qn = 0.5;
		un = (3.0 / (x[n - 1] - x[n - 2])) * (ypn - (y[n - 1] - y[n - 2]) / (x[n - 1] - x[n - 2]));
	}
	/*
	* This is the backsubstitution loop of the tridiagonal  algorithm.
	*/
	y2[n-1] = (un - qn * u[n - 2]) / (qn * y2[n - 2] + 1.0);
	for (k = n - 2; k >= 0; k--) 
		y2[k] = y2[k] * y2[k + 1] + u[k];
	free(u);
	//free_vector(u,1,n-1);
}
/*
* Taken from Numerical Recipes book
Given the arrays xa[1..n] and ya[1..n], which tabulate a function(with the xai�s in order),
and given the array y2a[1..n], which is the output from spline above, and given a value of
x, this routine returns a cubic - spline interpolated value y.
*/
inline void splint(DATA_TYPE xa[], DATA_TYPE ya[], DATA_TYPE y2a[], int n, DATA_TYPE x, DATA_TYPE* y)
{
	void nrerror(char error_text[]);
	int klo, khi, k;
	DATA_TYPE h, b, a;

	klo = 1;
	khi = n;
	while (khi - klo > 1)
	{
		k = (khi + klo) >> 1;
		if (xa[k] > x)
			khi = k;
		else
			klo = k;
	}
	h = xa[khi] - xa[klo];
	if (h == 0.0)
		cout << "Bad xa input to routine splint" << endl;
	a = (xa[khi] - x) / h;
	b = (x - xa[klo]) / h;
	*y = a * ya[klo] + b * ya[khi] + ((a * a * a - a) * y2a[klo] + (b * b * b - b) * y2a[khi]) * (h * h) / 6.0;
}

#endif
