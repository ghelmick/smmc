#if !defined (__MY_RANDOM_H__)
#define       __MY_RANDOM_H__

#include <math.h>
#include "util.h"

class MyRandom
{
public:
	inline MyRandom(int seed = 1) : xi(seed), m(2147483647), bitness(32)
	{
		m = pow(2.0, (bitness - 1)) - 1;
		nFactor = m;
		//a = 16807;
		a = 48271; // provides slightly better results
	}

	DATA_TYPE nextFloat()
	{
		xi = (a * xi) % m;

		return (DATA_TYPE)xi / (DATA_TYPE)nFactor;
	}

	DATA_TYPE nextFloat(DATA_TYPE lower, DATA_TYPE upper)
	{
		DATA_TYPE val = nextFloat();

		while (true)
		{
			if (val >= lower && val <= upper)
				return val;

			val = nextFloat();
		}
	}

	DATA_TYPE nextFloatNormal()
	{
		DATA_TYPE u1 = 0.f, u2 = 0.f, s = 0.f;
		do
		{
			u1 = 2.f * nextFloat() - 1;
			u2 = 2.f * nextFloat() - 1;

			s = u1 * u1 + u2 * u2;
		} while (s >= 1);

		s = sqrt((-2 * log(s)) / s);
		DATA_TYPE x = u1 * s;
		//DATA_TYPE y = u2 * s;

		return x;
	}

	DATA_TYPE nextFloatNormal(DATA_TYPE lower, DATA_TYPE upper)
	{
		DATA_TYPE val = nextFloatNormal();

		while (true)
		{
			if (val >= lower && val <= upper)
				return val;

			val = nextFloatNormal();
		}
	}

	unsigned int nextInt()
	{
		xi = (a * xi) % m;
		return (xi);
	}

private:

	unsigned int xi;			// the ith random number generated
	unsigned int a;				// multiplier for the modulus
	unsigned int m;				// number of members in the sequence, the "Mersenne Number"
	unsigned int nFactor;		// the normalization factor
	int bitness;				// bitness of the system
};

#endif
