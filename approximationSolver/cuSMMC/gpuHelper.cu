#include "gpuHelper.cuh"
#include "cuUtil.cuh"
#include "util.h"


__constant__ bool   d_usePCB[1];
__constant__ DATA_TYPE d_sigma_dopant[1];
__constant__ DATA_TYPE d_Q_dop[1];
__constant__ DATA_TYPE d_epsilon_dopant[1];
__constant__ DATA_TYPE d_kappa[1];
__device__ DATA_TYPE d_x_size[1];
__device__ DATA_TYPE d_y_size[1];
__device__ DATA_TYPE d_z_size[1];
__device__ DATA_TYPE d_r_cut[1];
__device__ DATA_TYPE d_r_cut_sqrd[1];
__constant__ DATA_TYPE d_Q_mono[1];
__constant__ DATA_TYPE d_sigma_inter[1];
__constant__ DATA_TYPE d_sigma_inter_end[1];
__constant__ DATA_TYPE d_epsilon_inter[1];
__constant__ DATA_TYPE d_epsilon_inter_end[1];
__constant__ DATA_TYPE d_dp_mag[1];
__constant__ DATA_TYPE d_A[1];
__constant__ DATA_TYPE d_B[1];
__constant__ DATA_TYPE d_C[1];
__constant__ DATA_TYPE d_sigmaPyDopant[1];
__constant__ int d_nInterpolationCols[1];
__constant__ int d_nInterpolationRows[1];
__constant__ int d_interpolation_offset[1];
__constant__ int d_interpolation_coeff_offset[1];
__constant__ int d_interpolation_order[1];
__constant__ DATA_TYPE d_interpolation_delta[1];


void synchronizeGPU()
{
	cudaError_t cudaStatus = cudaSuccess;
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
}

void allocateMemoryGPU(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit)
{
	cudaError err;
	if (ps.nDopants)
	{
		err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_dop_x, ps.nDopants, workunit.dopant_dopant_stream[0]);
		if (err != cudaSuccess)
		{
			return;
		}
		err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_dop_y, ps.nDopants, workunit.dopant_dopant_stream[1]);
		if (err != cudaSuccess)
		{
			return;
		}
		err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_dop_z, ps.nDopants, workunit.dopant_dopant_stream[2]);
		if (err != cudaSuccess)
		{
			return;
		}		
	}

	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_cm_x, ps.nMonomers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_cm_y, ps.nMonomers, workunit.monomer_monomer_stream[1]);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_cm_z, ps.nMonomers, workunit.monomer_monomer_stream[2]);
	if (err != cudaSuccess)
	{
		return;
	}

	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_dpm_x, ps.nMonomers, workunit.monomer_monomer_stream[3]);
	if (err != cudaSuccess)
	{
		return;
	}
	cudaStreamSynchronize(workunit.monomer_monomer_stream[0]);
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_dpm_y, ps.nMonomers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}
	cudaStreamSynchronize(workunit.monomer_monomer_stream[1]);
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_dpm_z, ps.nMonomers, workunit.monomer_monomer_stream[1]);
	if (err != cudaSuccess)
	{
		return;
	}

	cudaStreamSynchronize(workunit.monomer_monomer_stream[2]);
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_olig_cm_x, ps.nPolymers, workunit.monomer_monomer_stream[2]);
	if (err != cudaSuccess)
	{
		return;
	}

	cudaStreamSynchronize(workunit.monomer_monomer_stream[3]);
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_olig_cm_y, ps.nPolymers, workunit.monomer_monomer_stream[3]);
	if (err != cudaSuccess)
	{
		return;
	}

	cudaStreamSynchronize(workunit.monomer_monomer_stream[0]);
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_olig_cm_z, ps.nPolymers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}

	// this is wasteful but allocate twice as much memory as needed, allows for expansion of these parameters
	err = allocateMemoryOnDevice<DATA_TYPE>(&workunit.d_interpolationParameters, (workunit.nInterpolationRows*workunit.nInterpolationCols) * BUFFER_FACTOR, workunit.monomer_monomer_stream[2]);
	if (err != cudaSuccess)
	{
		return;
	}

}

void copyMemoryToGPU(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit)
{
	cudaError err;
	if (ps.nDopants)
	{
		err = copyToDevice<DATA_TYPE>(ps.dop_x, workunit.d_dop_x, ps.nDopants, workunit.dopant_dopant_stream[0]);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyToDevice<DATA_TYPE>(ps.dop_y, workunit.d_dop_y, ps.nDopants, workunit.dopant_dopant_stream[1]);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyToDevice<DATA_TYPE>(ps.dop_z, workunit.d_dop_z, ps.nDopants, workunit.dopant_dopant_stream[2]);
		if (err != cudaSuccess)
		{
			return;
		}
	}

	err = copyToDevice<DATA_TYPE>(ps.cm_x, workunit.d_cm_x, ps.nMonomers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.cm_y, workunit.d_cm_y, ps.nMonomers, workunit.monomer_monomer_stream[1]);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.cm_z, workunit.d_cm_z, ps.nMonomers, workunit.monomer_monomer_stream[2]);
	if (err != cudaSuccess)
	{
		return;
	}

	err = copyToDevice<DATA_TYPE>(ps.dpm_x, workunit.d_dpm_x, ps.nMonomers, workunit.monomer_monomer_stream[3]);
	if (err != cudaSuccess)
	{
		return;
	}

	cudaStreamSynchronize(workunit.monomer_monomer_stream[0]);
	err = copyToDevice<DATA_TYPE>(ps.dpm_y, workunit.d_dpm_y, ps.nMonomers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}
	cudaStreamSynchronize(workunit.monomer_monomer_stream[1]);
	err = copyToDevice<DATA_TYPE>(ps.dpm_z, workunit.d_dpm_z, ps.nMonomers, workunit.monomer_monomer_stream[1]);
	if (err != cudaSuccess)
	{
		return;
	}

	cudaStreamSynchronize(workunit.monomer_monomer_stream[2]);
	err = copyToDevice<DATA_TYPE>(ps.olig_cm_x, workunit.d_olig_cm_x, ps.nPolymers, workunit.monomer_monomer_stream[2]);
	if (err != cudaSuccess)
	{
		return;
	}
	cudaStreamSynchronize(workunit.monomer_monomer_stream[3]);
	err = copyToDevice<DATA_TYPE>(ps.olig_cm_y, workunit.d_olig_cm_y, ps.nPolymers, workunit.monomer_monomer_stream[3]);
	if (err != cudaSuccess)
	{
		return;
	}
	cudaStreamSynchronize(workunit.monomer_monomer_stream[0]);
	err = copyToDevice<DATA_TYPE>(ps.olig_cm_z, workunit.d_olig_cm_z, ps.nPolymers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}
}

void copyMemoryFromGPU(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit)
{
	cudaError err;
	if (ps.nDopants)
	{
		err = copyFromDevice<DATA_TYPE>(workunit.d_dop_x, ps.dop_x, ps.nDopants, workunit.dopant_dopant_stream[0]);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyFromDevice<DATA_TYPE>(workunit.d_dop_y, ps.dop_y, ps.nDopants, workunit.dopant_dopant_stream[1]);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyFromDevice<DATA_TYPE>(workunit.d_dop_z, ps.dop_z, ps.nDopants, workunit.dopant_dopant_stream[2]);
		if (err != cudaSuccess)
		{
			return;
		}
	}
	//
	err = copyFromDevice<DATA_TYPE>(workunit.d_cm_x, ps.cm_x, ps.nMonomers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyFromDevice<DATA_TYPE>(workunit.d_cm_y, ps.cm_y, ps.nMonomers, workunit.monomer_monomer_stream[1]);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyFromDevice<DATA_TYPE>(workunit.d_cm_z, ps.cm_z, ps.nMonomers, workunit.monomer_monomer_stream[2]);
	if (err != cudaSuccess)
	{
		return;
	}
	//
	err = copyFromDevice<DATA_TYPE>(workunit.d_dpm_x, ps.dpm_x, ps.nMonomers, workunit.monomer_monomer_stream[3]);
	if (err != cudaSuccess)
	{
		return;
	}
	cudaStreamSynchronize(workunit.monomer_monomer_stream[0]);
	err = copyFromDevice<DATA_TYPE>(workunit.d_dpm_y, ps.dpm_y, ps.nMonomers, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}
	cudaStreamSynchronize(workunit.monomer_monomer_stream[1]);
	err = copyFromDevice<DATA_TYPE>(workunit.d_dpm_z, ps.dpm_z, ps.nMonomers, workunit.monomer_monomer_stream[1]);
	if (err != cudaSuccess)
	{
		return;
	}
}

void updateFullSystem(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit)
{
	copyMemoryToGPU(ps, workunit);

	cudaError_t cudaStatus = cudaSuccess;
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
}

void updateSystemSize(DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_)
{
	cudaMemcpyToSymbol(d_x_size, &x_size_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(d_y_size, &y_size_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(d_z_size, &z_size_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaMemcpyToSymbol(d_r_cut, &r_cut_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);

	//d_updateSystemSize <<<1, 1 >>> (x_size_, y_size_, z_size_, r_cut_);
	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_updateSystemSize Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}
	cudaStatus = cudaSuccess;
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
}

void updateMonomer(PARTICLE_SYSTEM& ps, int midx, COPROCESSOR_WORKUNIT& workunit)
{
	// calculate the partial energy
	d_updateMonomer <<<1, 1 >>> (workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx],
		workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z, ps.dpm_x[midx], ps.dpm_y[midx], ps.dpm_z[midx], midx);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_updateMonomer Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaStatus = cudaSuccess;
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
}

void updateDopant(PARTICLE_SYSTEM& ps, int didx, COPROCESSOR_WORKUNIT& workunit)
{
	if (ps.nDopants)
	{
		d_updateDopant <<<1, 1 >>> (workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, ps.dop_x[didx], ps.dop_y[didx], ps.dop_z[didx], didx);
		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "d_updateDopant Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}
		cudaStatus = cudaSuccess;
		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;
	}
}

void getDeviceDopDopPart(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, int dIdx, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE & dopdop, DATA_TYPE & t_dopdop)
{
	dopdop = 0.0;
	t_dopdop = 0.0;
	if (ps.nDopants)
	{
		// calculate the partial energy
		d_DopDopPart <<<workunit.nBlocksDopants, workunit.nThreadsDopant, workunit.nThreadsDopant * sizeof(CUDA_PAIR), workunit.dopant_dopant_stream[0] >>> (
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters,
			workunit.d_dop_results, workunit.t_d_dop_results, 
			ps.nDopants, dIdx, dlb, dub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "d_DopDopPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}

		cudaError_t retVal = cudaSuccess;
		//retVal = cudaStreamSynchronize(dopant_dopant_stream[0]);
		retVal = cudaDeviceSynchronize();

		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		// Copy output to CPU output array
		retVal = cudaMemcpyAsync(
			workunit.dop_results,
			workunit.d_dop_results,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_dopant_stream[1]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		// Copy output to CPU output array
		retVal = cudaMemcpyAsync(
			workunit.t_dop_results,
			workunit.t_d_dop_results,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_dopant_stream[2]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < workunit.nBlocksDopants; i++)
		{
			dopdop += workunit.dop_results[i];
			t_dopdop += workunit.t_dop_results[i];
		}
	}
}

void startDeviceTotalInter(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, int mlb, int mub, int dlb, int dub, bool removeDouble)
{
	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "getDeviceTotalInter Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
	}

	// calculate the partial energy
	d_PyPyTotalInter <<<workunit.nBlocksMonomers, workunit.nThreadsMonomer, workunit.nThreadsMonomer * sizeof(DATA_TYPE), workunit.monomer_monomer_stream[0] >>> (
		workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z, workunit.d_interpolationParameters,
		workunit.d_mon_results, workunit.mlb, workunit.mub, ps.monomers_per_chain, ps.nMonomers, removeDouble);

	// Check for any errors launching the kernel
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "getDeviceTotalInter Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
	}

	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopTotalInter <<<workunit.nBlocksMonomers, workunit.nThreadsMonomer, workunit.nThreadsMonomer * sizeof(DATA_TYPE), workunit.dopant_monomer_stream[0] >>> (
			workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z,
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters, 
			workunit.d_pydop_dop_mov, ps.nDopants, ps.nMonomers, workunit.mlb, workunit.mub);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
		}

		// calculate the partial energy
		d_DopDopTotalInter <<<workunit.nBlocksDopants, workunit.nThreadsDopant, workunit.nThreadsDopant * sizeof(DATA_TYPE), workunit.dopant_dopant_stream[0] >>> (
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters, workunit.d_dop_results, ps.nDopants, workunit.dlb, workunit.dub);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
		}
	}
}

DATA_TYPE finishDeviceTotalInter(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, int mlb, int mub, int dlb, int dub, DATA_TYPE& upypy, DATA_TYPE& upydop, DATA_TYPE& udopdop)
{
	upypy = 0.0;
	upydop = 0.0;
	udopdop = 0.0;

	cudaError_t retVal = cudaSuccess;
	//retVal = cudaStreamSynchronize(monomer_monomer_stream[0]);
	retVal = cudaDeviceSynchronize();
	if (retVal != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

	// Copy output to CPU output array
	retVal = cudaMemcpyAsync(
		workunit.mon_results,
		workunit.d_mon_results,
		workunit.nBlocksMonomers * sizeof(DATA_TYPE),
		cudaMemcpyDeviceToHost, workunit.monomer_monomer_stream[1]);
	if (retVal != cudaSuccess)
	{
		cout << "Unable to copy energies to CPU" << endl;
		cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
	}

	if (ps.nDopants)
	{
		retVal = cudaDeviceSynchronize();

		cudaError_t retVal = cudaSuccess;
		retVal = cudaStreamSynchronize(workunit.dopant_monomer_stream[0]);
		if (retVal != cudaSuccess)
		{
			cout << "Unable to synchronize (likely due to a prior error)" << endl;
			cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
		}
		// now get the monomer-dopant values
		retVal = cudaMemcpyAsync(
			workunit.pydop_dop_mov,
			workunit.d_pydop_dop_mov,
			workunit.nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_monomer_stream[1]);
		if (retVal != cudaSuccess)
		{
			cout << "Unable to copy energies to CPU" << endl;
			cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
		}

		retVal = cudaSuccess;
		retVal = cudaStreamSynchronize(workunit.dopant_dopant_stream[0]);
		if (retVal != cudaSuccess)
		{
			cout << "Unable to synchronize (likely due to a prior error)" << endl;
			cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
		}

		// now get the dopant-dopant values
		retVal = cudaMemcpyAsync(
			workunit.dop_results,
			workunit.d_dop_results,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_dopant_stream[1]);
		if (retVal != cudaSuccess)
		{
			cout << "Unable to copy energies to CPU" << endl;
			cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
		}

		retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
		{
			cout << "Unable to synchronize (likely due to a prior error)" << endl;
			cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
		}

		for (int i = 0; i < workunit.nBlocksMonomers; i++)
			upydop += workunit.pydop_dop_mov[i];

		for (int i = 0; i < workunit.nBlocksDopants; i++)
			udopdop += workunit.dop_results[i];
	}

	retVal = cudaSuccess;
	retVal = cudaDeviceSynchronize();
	if (retVal != cudaSuccess)
	{
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
		cout << __FILE__ << "[" << __LINE__ << "]: " << __FUNCTION__ << " - WorkUnit: " << workunit.coprocessor_id << endl;
	}
	for (int i = 0; i < workunit.nBlocksMonomers; i++)
		upypy += workunit.mon_results[i];

	return (upypy + upydop + udopdop);

}

void getDevicePyDopPartMonMove(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, int mIdx, int dlb, int dub,
	                                DATA_TYPE & pydoppartmonmove, DATA_TYPE& t_pydoppartmonmove,
	                                DATA_TYPE& tr_pydoppartmonmove)
{
	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartMonMove <<<workunit.nBlocksDopants, workunit.nThreadsDopant, workunit.nThreadsDopant * sizeof(CUDA_TRIPLET), workunit.monomer_dopant_stream[0] >>> (
			workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z,
			workunit.d_interpolationParameters, workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_pydop_mon_mov, 
			workunit.t_d_pydop_mon_mov, workunit.tr_d_pydop_mon_mov, ps.nDopants, ps.nMonomers, mIdx, dlb, dub, 
			ps.cm_x[mIdx], ps.cm_y[mIdx], ps.cm_z[mIdx], ps.dpm_x[mIdx], ps.dpm_y[mIdx], ps.dpm_z[mIdx]);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}

		// Copy output to CPU output array
		cudaError_t retVal = cudaSuccess;
		//retVal = cudaStreamSynchronize(monomer_dopant_stream[0]);
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		retVal = cudaMemcpyAsync(
			workunit.pydop_mon_mov,
			workunit.d_pydop_mon_mov,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.monomer_dopant_stream[1]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaMemcpyAsync(
			workunit.t_pydop_mon_mov,
			workunit.t_d_pydop_mon_mov,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.monomer_dopant_stream[2]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaMemcpyAsync(
			workunit.tr_pydop_mon_mov,
			workunit.tr_d_pydop_mon_mov,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.monomer_dopant_stream[3]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < workunit.nBlocksDopants; i++)
		{
			pydoppartmonmove += workunit.pydop_mon_mov[i];
			t_pydoppartmonmove += workunit.t_pydop_mon_mov[i];
			tr_pydoppartmonmove += workunit.tr_pydop_mon_mov[i];
		}
	}
}

void getDevicePyDopPartDopMove(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, int dIdx, int mlb, int mub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE& pydoppartdop, DATA_TYPE& t_pydoppartdop)
{
	pydoppartdop = 0.0;
	t_pydoppartdop = 0.0;

	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartDopMove <<<workunit.nBlocksMonomers, workunit.nThreadsMonomer, workunit.nThreadsMonomer * sizeof(CUDA_PAIR), workunit.dopant_monomer_stream[0] >>> (
			workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z,
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters, 
			workunit.d_pydop_dop_mov, workunit.t_d_pydop_dop_mov, ps.nDopants, ps.nMonomers, 
			dIdx, mlb, mub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "PyDopPartDopMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}

		// Copy output to CPU output array
		cudaError_t retVal = cudaSuccess;
		//retVal = cudaStreamSynchronize(dopant_monomer_stream[0]);
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		retVal = cudaMemcpyAsync(
			workunit.pydop_dop_mov,
			workunit.d_pydop_dop_mov,
			workunit.nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_monomer_stream[1]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaMemcpyAsync(
			workunit.t_pydop_dop_mov,
			workunit.t_d_pydop_dop_mov,
			workunit.nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_monomer_stream[2]);

		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < workunit.nBlocksMonomers; i++)
		{
			pydoppartdop += workunit.pydop_dop_mov[i];
			t_pydoppartdop += workunit.t_pydop_dop_mov[i];
		}
	}
}

void setModelConstants(bool usePCB_, DATA_TYPE sigma_dopant_, DATA_TYPE Q_dop_, DATA_TYPE epsilon_dopant_, DATA_TYPE kappa_,
	DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_, DATA_TYPE Q_mono_,
	DATA_TYPE sigma_inter_, DATA_TYPE sigma_inter_end_, DATA_TYPE epsilon_inter_,
	DATA_TYPE epsilon_inter_end_, DATA_TYPE dp_mag_, DATA_TYPE A_, DATA_TYPE B_, DATA_TYPE C_, DATA_TYPE sigmaPyDopant_, 
	COPROCESSOR_WORKUNIT& workunit)
{
	setDevice(workunit.coprocessor_id);

	cudaMemcpyToSymbol(d_usePCB, &usePCB_, sizeof(bool), 0, cudaMemcpyHostToDevice);
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_sigma_dopant, &sigma_dopant_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", 0, cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_Q_dop, &Q_dop_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_epsilon_dopant, &epsilon_dopant_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_kappa, &kappa_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_x_size, &x_size_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_y_size, &y_size_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_z_size, &z_size_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_r_cut, &r_cut_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	DATA_TYPE rsqrd = r_cut_ * r_cut_;
	cudaMemcpyToSymbol(d_r_cut_sqrd, &rsqrd, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_Q_mono, &Q_mono_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_sigma_inter, &sigma_inter_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_sigma_inter_end, &sigma_inter_end_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_epsilon_inter, &epsilon_inter_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_epsilon_inter_end, &epsilon_inter_end_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_dp_mag, &dp_mag_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_A, &A_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_B, &B_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_C, &C_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaMemcpyToSymbol(d_sigmaPyDopant, &sigmaPyDopant_, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);

	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}
}

void updateDeviceInterpolationparemeters(PARTICLE_SYSTEM& ps, DATA_TYPE* interpolationParameters, COPROCESSOR_WORKUNIT& workunit)
{
	cudaError err;

	// set values to 0 for the interpolation parameters
	err = cudaMemset(workunit.d_interpolationParameters, 0, (workunit.nInterpolationRows * workunit.nInterpolationCols));
	if (err != cudaSuccess)
	{
		fprintf(stderr, "error reset interpolation parameter memory", cudaGetErrorString(err));
	}

	err = copyToDevice<DATA_TYPE>(interpolationParameters, workunit.d_interpolationParameters, workunit.nInterpolationRows * workunit.nInterpolationCols, workunit.monomer_monomer_stream[0]);
	if (err != cudaSuccess)
	{
		return;
	}

	cudaMemcpyToSymbol(d_nInterpolationCols, &workunit.nInterpolationCols, sizeof(int), 0, cudaMemcpyHostToDevice);
	err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(err));
	}
	cudaMemcpyToSymbol(d_nInterpolationRows, &workunit.nInterpolationRows, sizeof(int), 0, cudaMemcpyHostToDevice);
	err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(err));
	}
	cudaMemcpyToSymbol(d_interpolation_offset, &workunit.interpolation_offset, sizeof(int), 0, cudaMemcpyHostToDevice);
	err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(err));
	}
	cudaMemcpyToSymbol(d_interpolation_delta, &workunit.interpolation_delta, sizeof(DATA_TYPE), 0, cudaMemcpyHostToDevice);
	err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(err));
	}

	cudaMemcpyToSymbol(d_interpolation_order, &workunit.interpolationOrder, sizeof(int), 0, cudaMemcpyHostToDevice);
	err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(err));
	}
	cudaMemcpyToSymbol(d_interpolation_coeff_offset, &workunit.interpolationCoeffOffset, sizeof(int), 0, cudaMemcpyHostToDevice);
	err = cudaGetLastError();
	if (err != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(err));
	}

}

void getDeviceInterPartDopant(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, int midx, int didx, int mlb, int mub, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE& doppart, DATA_TYPE& t_doppart)
{
	DATA_TYPE upydop = 0.0, t_upydop = 0.0;
	DATA_TYPE udopdop = 0.0, t_udopdop = 0.0;

	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartDopMove <<<workunit.nBlocksMonomers, workunit.nThreadsMonomer, workunit.nThreadsMonomer * sizeof(DATA_TYPE), workunit.dopant_monomer_stream[0] >>> (
			workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z,
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters, 
			workunit.d_pydop_dop_mov, workunit.t_d_pydop_dop_mov, 
			ps.nDopants, ps.nMonomers, didx, mlb, mub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "PyDopPartDopMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}

		// calculate the partial energy
		d_DopDopPart <<<workunit.nBlocksDopants, workunit.nThreadsDopant, workunit.nThreadsDopant * sizeof(DATA_TYPE), workunit.dopant_dopant_stream[0] >>> (
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters,
			workunit.d_dop_results, workunit.t_d_dop_results, ps.nDopants, didx, dlb, dub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "d_DopDopPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}

		//cudaStatus = cudaStreamSynchronize(dopant_monomer_stream[0]);
		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		// Copy output to CPU output array
		cudaStatus = cudaMemcpyAsync(
			workunit.pydop_dop_mov,
			workunit.d_pydop_dop_mov,
			workunit.nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_monomer_stream[1]);
		if (cudaStatus != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		// Copy output to CPU output array
		cudaStatus = cudaMemcpyAsync(
			workunit.t_pydop_dop_mov,
			workunit.t_d_pydop_dop_mov,
			workunit.nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_monomer_stream[2]);
		if (cudaStatus != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		cudaStatus = cudaStreamSynchronize(workunit.dopant_dopant_stream[0]);
		if (cudaStatus != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		// Copy output to CPU output array
		cudaStatus = cudaMemcpyAsync(
			workunit.dop_results,
			workunit.d_dop_results,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_dopant_stream[1]);
		if (cudaStatus != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		// Copy output to CPU output array
		cudaStatus = cudaMemcpyAsync(
			workunit.t_dop_results,
			workunit.t_d_dop_results,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_dopant_stream[2]);
		if (cudaStatus != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		cudaError_t retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < workunit.nBlocksMonomers; i++)
		{
			upydop += workunit.pydop_dop_mov[i];
			t_upydop += workunit.t_pydop_dop_mov[i];
		}

		for (int i = 0; i < workunit.nBlocksDopants; i++)
		{
			udopdop += workunit.dop_results[i];
			t_udopdop += workunit.t_dop_results[i];
		}

	}
	doppart = udopdop + upydop;
	t_doppart = t_udopdop + t_upydop;
}

void startGetDeviceTotalMonomerPart(PARTICLE_SYSTEM& ps, int mIdx, int mlb, int mub, int dlb, int dub, COPROCESSOR_WORKUNIT & workunit,
	                        DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz,
	                        DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz,
							 bool removeDouble)
{
	// calculate the partial energy
	d_PyPyPart << <workunit.nBlocksMonomers, workunit.nThreadsMonomer, workunit.nThreadsMonomer * sizeof(CUDA_TRIPLET), workunit.monomer_monomer_stream[0] >> > (
		workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z, workunit.d_interpolationParameters, 
		workunit.d_mon_results, workunit.d_t_mon_results, workunit.d_tr_mon_results,
		mIdx, mlb, mub, ps.monomers_per_chain,
		ps.nMonomers, removeDouble, nx, ny, nz, ndpmx, ndpmy, ndpmz);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "getDevicePyPyPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		exit(0);
	}

	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartMonMove << <workunit.nBlocksDopants, workunit.nThreadsDopant, workunit.nThreadsDopant * sizeof(CUDA_TRIPLET), workunit.monomer_dopant_stream[0] >> > (
			workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z,
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters,
			workunit.d_pydop_mon_mov, workunit.t_d_pydop_mon_mov, workunit.tr_d_pydop_mon_mov, 
			ps.nDopants, ps.nMonomers, mIdx, dlb, dub, ps.cm_x[mIdx], ps.cm_y[mIdx], ps.cm_z[mIdx], 
			ps.dpm_x[mIdx], ps.dpm_y[mIdx], ps.dpm_z[mIdx]);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}
	}
}

void finishGetDeviceTotalMonomerPart(PARTICLE_SYSTEM& ps, int mIdx, int mlb, int mub, int dlb, int dub, COPROCESSOR_WORKUNIT& workunit,
	DATA_TYPE& upypy, DATA_TYPE& t_upypy, DATA_TYPE& tr_upypy,
	DATA_TYPE& pydoppartmonmove, DATA_TYPE& t_pydoppartmonmove,
	DATA_TYPE& tr_pydoppartmonmove)
{
	cudaError_t retVal = cudaSuccess;
	//retVal = cudaStreamSynchronize(workunit.monomer_monomer_stream[0]);
	retVal = cudaDeviceSynchronize();
	if (retVal != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

	// Copy output to CPU output array
	retVal = cudaMemcpyAsync(
		workunit.mon_results,
		workunit.d_mon_results,
		workunit.nBlocksMonomers * sizeof(DATA_TYPE),
		cudaMemcpyDeviceToHost, workunit.monomer_monomer_stream[1]);
	if (retVal != cudaSuccess)
		cout << "Unable to copy energies to CPU" << endl;

	// Copy output to CPU output array
	retVal = cudaMemcpyAsync(
		workunit.t_mon_results,
		workunit.d_t_mon_results,
		workunit.nBlocksMonomers * sizeof(DATA_TYPE),
		cudaMemcpyDeviceToHost, workunit.monomer_monomer_stream[2]);
	if (retVal != cudaSuccess)
		cout << "Unable to copy energies to CPU" << endl;

	// Copy output to CPU output array
	retVal = cudaMemcpyAsync(
		workunit.tr_mon_results,
		workunit.d_tr_mon_results,
		workunit.nBlocksMonomers * sizeof(DATA_TYPE),
		cudaMemcpyDeviceToHost, workunit.monomer_monomer_stream[3]);
	if (retVal != cudaSuccess)
		cout << "Unable to copy energies to CPU" << endl;

	if (ps.nDopants)
	{
		// Copy output to CPU output array
		//retVal = cudaStreamSynchronize(workunit.monomer_dopant_stream[0]);
		////retVal = cudaDeviceSynchronize();
		//if (retVal != cudaSuccess)
		//	cout << "Unable to synchronize (likely due to a prior error)" << endl;
		retVal = cudaSuccess;

		retVal = cudaMemcpyAsync(
			workunit.pydop_mon_mov,
			workunit.d_pydop_mon_mov,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.monomer_dopant_stream[1]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaMemcpyAsync(
			workunit.t_pydop_mon_mov,
			workunit.t_d_pydop_mon_mov,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.monomer_dopant_stream[2]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaMemcpyAsync(
			workunit.tr_pydop_mon_mov,
			workunit.tr_d_pydop_mon_mov,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.monomer_dopant_stream[3]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		pydoppartmonmove = 0.0;
		t_pydoppartmonmove = 0.0;
		tr_pydoppartmonmove = 0.0;

		for (int i = 0; i < workunit.nBlocksDopants; i++)
		{
			pydoppartmonmove += workunit.pydop_mon_mov[i];
			t_pydoppartmonmove += workunit.t_pydop_mon_mov[i];
			tr_pydoppartmonmove += workunit.tr_pydop_mon_mov[i];
		}
	}

	retVal = cudaSuccess;
	retVal = cudaDeviceSynchronize();
	if (retVal != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

	upypy = 0.0;
	t_upypy = 0.0;
	tr_upypy = 0.0;

	for (int i = 0; i < workunit.nBlocksMonomers; i++)
	{
		upypy += workunit.mon_results[i];
		t_upypy += workunit.t_mon_results[i];
		tr_upypy += workunit.tr_mon_results[i];
	}

}

void startGetDeviceTotalDopantPart(PARTICLE_SYSTEM& ps, int mlb, int mub, int dIdx, int dlb, int dub, COPROCESSOR_WORKUNIT& workunit, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	if (ps.nDopants)
	{
		// calculate the partial energy
		d_DopDopPart << <workunit.nBlocksDopants, workunit.nThreadsDopant, workunit.nThreadsDopant * sizeof(CUDA_PAIR), workunit.dopant_dopant_stream[0] >> > (
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters,
			workunit.d_dop_results, workunit.t_d_dop_results, ps.nDopants, dIdx, dlb, dub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "d_DopDopPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}

		// calculate the partial energy
		d_PyDopPartDopMove << <workunit.nBlocksMonomers, workunit.nThreadsMonomer, workunit.nThreadsMonomer * sizeof(CUDA_PAIR), workunit.dopant_monomer_stream[0] >> > (
			workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dpm_x, workunit.d_dpm_y, workunit.d_dpm_z,
			workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z, workunit.d_interpolationParameters, 
			workunit.d_pydop_dop_mov, workunit.t_d_pydop_dop_mov, 
			ps.nDopants, ps.nMonomers, dIdx, mlb, mub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "d_DopDopPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}
	}

}

void finishGetDeviceTotalDopantPart(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, DATA_TYPE& dopdop, DATA_TYPE& t_dopdop, DATA_TYPE& pydoppartdop, DATA_TYPE& t_pydoppartdop)
{
	dopdop = 0.0;
	t_dopdop = 0.0;
	pydoppartdop = 0.0;
	t_pydoppartdop = 0.0;

	if (ps.nDopants)
	{
		cudaError_t retVal = cudaSuccess;
		//retVal = cudaStreamSynchronize(dopant_dopant_stream[0]);
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		// Copy output to CPU output array
		retVal = cudaMemcpyAsync(
			workunit.dop_results,
			workunit.d_dop_results,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_dopant_stream[1]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		// Copy output to CPU output array
		retVal = cudaMemcpyAsync(
			workunit.t_dop_results,
			workunit.t_d_dop_results,
			workunit.nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_dopant_stream[2]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		retVal = cudaMemcpyAsync(
			workunit.pydop_dop_mov,
			workunit.d_pydop_dop_mov,
			workunit.nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_monomer_stream[3]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		retVal = cudaMemcpyAsync(
			workunit.t_pydop_dop_mov,
			workunit.t_d_pydop_dop_mov,
			workunit.nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, workunit.dopant_monomer_stream[1]);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaDeviceSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < workunit.nBlocksMonomers; i++)
		{
			pydoppartdop += workunit.pydop_dop_mov[i];
			t_pydoppartdop += workunit.t_pydop_dop_mov[i];
		}

		for (int i = 0; i < workunit.nBlocksDopants; i++)
		{
			dopdop += workunit.dop_results[i];
			t_dopdop += workunit.t_dop_results[i];
		}
	}
}

void deviceScaleSystem(PARTICLE_SYSTEM& ps, COPROCESSOR_WORKUNIT& workunit, DATA_TYPE* scaleFactor, bool updateCutoff)
{
	d_scaleSystem << <1, 1 >> > (workunit.d_cm_x, workunit.d_cm_y, workunit.d_cm_z, workunit.d_dop_x, workunit.d_dop_y, workunit.d_dop_z,
		workunit.d_olig_cm_x, workunit.d_olig_cm_y, workunit.d_olig_cm_z, ps.monomers_per_chain,
		ps.nMonomers, ps.nDopants, scaleFactor[0], scaleFactor[1], scaleFactor[2], updateCutoff);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_scaleSystem Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaStatus = cudaSuccess;
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

}


__global__ void d_updateMonomer(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	DATA_TYPE cm_x_, DATA_TYPE cm_y_, DATA_TYPE cm_z_,
	DATA_TYPE* d_dpm_x, DATA_TYPE* d_dpm_y, DATA_TYPE* d_dpm_z,
	DATA_TYPE dpm_x_, DATA_TYPE dpm_y_, DATA_TYPE dpm_z_,
	int midx)
{
	// thread 0 will perform the update
	if (blockIdx.x == 0 && threadIdx.x == 0)
	{
		d_cm_x[midx] = cm_x_;
		d_cm_y[midx] = cm_y_;
		d_cm_z[midx] = cm_z_;

		d_dpm_x[midx] = dpm_x_;
		d_dpm_y[midx] = dpm_y_;
		d_dpm_z[midx] = dpm_z_;

		//d_n_x[midx] = n_x_;
		//d_n_y[midx] = n_y_;
		//d_n_z[midx] = n_z_;
	}
}

__global__ void d_updateDopant(DATA_TYPE* d_dop_x, DATA_TYPE* d_dop_y, DATA_TYPE* d_dop_z,
	DATA_TYPE dop_x_, DATA_TYPE dop_y_, DATA_TYPE dop_z_, int didx)
{
	// thread 0 will perform the update
	if (blockIdx.x == 0 && threadIdx.x == 0)
	{
		d_dop_x[didx] = dop_x_;
		d_dop_y[didx] = dop_y_;
		d_dop_z[didx] = dop_z_;
	}
}

__global__ void d_DopDopPart(const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z, DATA_TYPE* d_interpolationParameters, 
	DATA_TYPE* d_udopdop, DATA_TYPE * t_d_udopdop,
	int size, int didx, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ CUDA_PAIR ddp_energy[];
	ddp_energy[threadIdx.x].x = 0;
	ddp_energy[threadIdx.x].y = 0;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0, rss = 0.0;
	DATA_TYPE src_dop_x = 0.0, src_dop_y = 0.0, src_dop_z = 0.0;
	DATA_TYPE test1 = 0.0, test2 = 0.0, result = 0.0;
	int pidx = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = d_interpolation_coeff_offset[0] + (d_interpolation_order[0] + 1);
#else
	int coefficient_offset = 8;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 2;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = d_interpolation_order[0];
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	while (idx < dlb)
		idx += stride;


	for (; idx < dub; idx += stride)
	{
		if (idx == didx)
			continue;
		src_dop_x = d_dop_x[idx];
		src_dop_y = d_dop_y[idx];
		src_dop_z = d_dop_z[idx];

		// before translation
		{
			xdif = src_dop_x - d_dop_x[didx];
			ydif = src_dop_y - d_dop_y[didx];
			zdif = src_dop_z - d_dop_z[didx];
			xdif = PCB_(d_x_size[0], xdif);
			ydif = PCB_(d_y_size[0], ydif);
			zdif = PCB_(d_z_size[0], zdif);

			rss = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rss < d_r_cut_sqrd[0])
			{
				rss = sqrt(rss);
				rss = ANG_TO_BOHR(rss);
				pidx = int((rss / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = rss * rss;

				test1 += (x1 + x2 * rss + x3 * xsqrd + x4 * xsqrd * rss + x5 * xsqrd * xsqrd + x6 * rss * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rss * rss;
				mu3 = mu2 * rss;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rss;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 2 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((rss / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, rss, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rss * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				d_polyVal(rss, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test1 += result;
#endif
			}
		}

		// after translation
		{
			xdif = src_dop_x - nx;
			ydif = src_dop_y - ny;
			zdif = src_dop_z - nz;
			xdif = PCB_(d_x_size[0], xdif);
			ydif = PCB_(d_y_size[0], ydif);
			zdif = PCB_(d_z_size[0], zdif);

			rss = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rss < d_r_cut_sqrd[0])
			{
				rss = sqrt(rss);
				rss = ANG_TO_BOHR(rss);
				pidx = int((rss / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = rss * rss;

				test2 += (x1 + x2 * rss + x3 * xsqrd + x4 * xsqrd * rss + x5 * xsqrd * xsqrd + x6 * rss * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rss * rss;
				mu3 = mu2 * rss;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rss;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 2 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((rss / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, rss, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rss * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				d_polyVal(rss, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test2 += result;
#endif
			}
		}
	}
	ddp_energy[threadIdx.x].x += test1;
	ddp_energy[threadIdx.x].y += test2;

	// Wait for all threads in block to finish computations
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			ddp_energy[threadIdx.x].x += ddp_energy[threadIdx.x + j].x;
			ddp_energy[threadIdx.x].y += ddp_energy[threadIdx.x + j].y;
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		  d_udopdop[blockIdx.x] = ddp_energy[0].x;
		t_d_udopdop[blockIdx.x] = ddp_energy[0].y;
	}
}

__global__ void d_PyPyPart(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z,
	DATA_TYPE* d_interpolationParameters, DATA_TYPE* d_umonmon, DATA_TYPE* d_t_umonmon, DATA_TYPE* d_tr_umonmon,
	int midx, int mlb, int mub, int monomers_per_chain, int nMonomers, bool removeDouble, 
	DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	int test_chain_id = 0;
	int input_chain_id = midx / monomers_per_chain; 
	int exc_lb = (midx / monomers_per_chain) * monomers_per_chain;
	int exc_ub = exc_lb + monomers_per_chain - 1;

	//TODO: fix this, shared memory has to be one big 1-D array, not multiple smaller arrays
	extern __shared__ CUDA_TRIPLET ppp_energy[];
	ppp_energy[threadIdx.x].x = 0.0;
	ppp_energy[threadIdx.x].y = 0.0;
	ppp_energy[threadIdx.x].z = 0.0;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE udd = 0.0, t_udd = 0.0, tr_udd = 0.0;

	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(d_dp_mag[0]) * DEBEYE_TO_HA(d_dp_mag[0]);
	DATA_TYPE rij3 = 0.0;
	DATA_TYPE b_ox = d_cm_x[midx], b_oy = d_cm_y[midx], b_oz = d_cm_z[midx];
	DATA_TYPE dst_x = 0.0, dst_y = 0.0, dst_z = 0.0;
	DATA_TYPE dst_dpm_x = 0.0, dst_dpm_y = 0.0, dst_dpm_z = 0.0;
	DATA_TYPE b_dpmx = d_dpm_x[midx], b_dpmy = d_dpm_y[midx], b_dpmz = d_dpm_z[midx];
	int pidx = 0;
	DATA_TYPE test1 = 0.0, test2 = 0.0, result = 0.0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = d_interpolation_coeff_offset[0];
#else
	int coefficient_offset = 4;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = d_interpolation_order[0];
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	while (idx < mlb)
		idx += stride;

	if (removeDouble)
	{
		while (idx < midx)
			idx += stride;
	}

	for (; idx < mub; idx += stride)
	{
		if (idx == midx)
			continue;

		test_chain_id = idx / monomers_per_chain;

		if (input_chain_id == test_chain_id)
			continue;
		dst_x = d_cm_x[idx];
		dst_y = d_cm_y[idx];
		dst_z = d_cm_z[idx];

		dst_dpm_x = d_dpm_x[idx];
		dst_dpm_y = d_dpm_y[idx];
		dst_dpm_z = d_dpm_z[idx];

		// before translation and rotation
		{
			ox = b_ox;
			oy = b_oy;
			oz = b_oz;

			xdif = dst_x - ox;
			ydif = dst_y - oy;
			zdif = dst_z - oz;
			xdif = PCB_(d_x_size[0], xdif);
			ydif = PCB_(d_y_size[0], ydif);
			zdif = PCB_(d_z_size[0], zdif);

			rij = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rij <= d_r_cut_sqrd[0])
			{
				dpmx = b_dpmx;
				dpmy = b_dpmy;
				dpmz = b_dpmz;

				xdif = ANG_TO_BOHR(xdif);
				ydif = ANG_TO_BOHR(ydif);
				zdif = ANG_TO_BOHR(zdif);

				rij = sqrt(xdif * xdif + ydif * ydif + zdif * zdif);
				rij3 = TO_POW_3(rij);

				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, d_dpm_x[idx], d_dpm_y[idx], d_dpm_z[idx]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(dst_dpm_x, dst_dpm_y, dst_dpm_z, xdif / rij, ydif / rij, zdif / rij);

				udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));

				pidx = int((rij / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = rij * rij;

				test1 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((rij / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = 4 * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];

				d_splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				d_polyVal(rij, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test1 += result;
#endif
			}
		}

		// after translation but before rotation
		{
			ox = nx;
			oy = ny;
			oz = nz;

			xdif = dst_x - ox;
			ydif = dst_y - oy;
			zdif = dst_z - oz;
			xdif = PCB_(d_x_size[0], xdif);
			ydif = PCB_(d_y_size[0], ydif);
			zdif = PCB_(d_z_size[0], zdif);

			rij = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rij <= d_r_cut_sqrd[0])
			{
				// before rotation
				dpmx = b_dpmx;
				dpmy = b_dpmy;
				dpmz = b_dpmz;

				xdif = ANG_TO_BOHR(xdif);
				ydif = ANG_TO_BOHR(ydif);
				zdif = ANG_TO_BOHR(zdif);

				rij = sqrt(xdif * xdif + ydif * ydif + zdif * zdif);

				rij3 = TO_POW_3(rij);
				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, d_dpm_x[idx], d_dpm_y[idx], d_dpm_z[idx]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(dst_dpm_x, dst_dpm_y, dst_dpm_z, xdif / rij, ydif / rij, zdif / rij);
				t_udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));


				pidx = int((rij / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = rij * rij;

				test2 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((rij / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = 4 * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				d_polyVal(rij, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test2 += result;
#endif
				// after rotation
				dpmx = ndpmx;
				dpmy = ndpmy;
				dpmz = ndpmz;

				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, d_dpm_x[idx], d_dpm_y[idx], d_dpm_z[idx]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(dst_dpm_x, dst_dpm_y, dst_dpm_z, xdif / rij, ydif / rij, zdif / rij);

				tr_udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
			}
		}
	}

	ppp_energy[threadIdx.x].x = test1 +   udd;
	ppp_energy[threadIdx.x].y = test2 + t_udd;
	ppp_energy[threadIdx.x].z = test2 + tr_udd;

	// Wait for all threads in block to finish computations
	__syncthreads();


	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			 ppp_energy[threadIdx.x].x += ppp_energy[threadIdx.x + j].x;
			 ppp_energy[threadIdx.x].y += ppp_energy[threadIdx.x + j].y;
			 ppp_energy[threadIdx.x].z += ppp_energy[threadIdx.x + j].z;
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		   d_umonmon[blockIdx.x] = ppp_energy[0].x;
		 d_t_umonmon[blockIdx.x] = ppp_energy[0].y;
		d_tr_umonmon[blockIdx.x] = ppp_energy[0].z;
	}
}

__global__ void d_PyDopPartMonMove(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z,
	const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z,
	DATA_TYPE* d_interpolationParameters, DATA_TYPE* d_pydop_mon_mov,
	DATA_TYPE* t_d_pydop_mon_mov, DATA_TYPE* tr_d_pydop_mon_mov,
	int nDopants, int nMonomers, int midx, int dlb, int dub,
	DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ CUDA_TRIPLET pdpm_energy[];
	pdpm_energy[threadIdx.x].x = 0;
	pdpm_energy[threadIdx.x].y = 0;
	pdpm_energy[threadIdx.x].z = 0;

	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE t_ucoul = 0.0, t_ucd = 0.0, t_udisp = 0.0;
	DATA_TYPE tr_ucoul = 0.0, tr_ucd = 0.0, tr_udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(d_Q_dop[0]) * DEBEYE_TO_HA(d_dp_mag[0]) * d_C[0];
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(d_sigmaPyDopant[0]);
	DATA_TYPE sigma_9 = sigma_6 * d_sigmaPyDopant[0] * d_sigmaPyDopant[0] * d_sigmaPyDopant[0];
	DATA_TYPE Q = d_Q_dop[0] * d_Q_mono[0];
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut[0]);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd[0]);
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(d_kappa[0] * bohr_r_cut) / bohr_r_cut;
	int pidx = 0;
	DATA_TYPE result = 0.0, test1 = 0.0, test2 = 0.0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = d_interpolation_coeff_offset[0] + (d_interpolation_order[0] * 2 + 2);
#else
	int coefficient_offset = 12;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 3;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0; 
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = d_interpolation_order[0];
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	while (idx < dlb)
		idx += stride;

	for (; idx < dub; idx += stride)
	{
		// before translation and before rotation
		{
			cmx = d_cm_x[midx];
			cmy = d_cm_y[midx];
			cmz = d_cm_z[midx];

			r_vec[0] = cmx - d_dop_x[idx];
			r_vec[1] = cmy - d_dop_y[idx];
			r_vec[2] = cmz - d_dop_z[idx];
			r_vec[0] = PCB_(d_x_size[0], r_vec[0]);
			r_vec[1] = PCB_(d_y_size[0], r_vec[1]);
			r_vec[2] = PCB_(d_z_size[0], r_vec[2]);

			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
			if (ris < d_r_cut_sqrd[0])
			{
				r_vec[0] = ANG_TO_BOHR(r_vec[0]);
				r_vec[1] = ANG_TO_BOHR(r_vec[1]);
				r_vec[2] = ANG_TO_BOHR(r_vec[2]);
				ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];

				ris = sqrt(ris);
				pidx = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = ris * ris;

				test1 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				d_polyVal(ris, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test1 += result;
#endif
			}
		}

		{
			cmx = nx;
			cmy = ny;
			cmz = nz;

			r_vec[0] = cmx - d_dop_x[idx];
			r_vec[1] = cmy - d_dop_y[idx];
			r_vec[2] = cmz - d_dop_z[idx];
			r_vec[0] = PCB_(d_x_size[0], r_vec[0]);
			r_vec[1] = PCB_(d_y_size[0], r_vec[1]);
			r_vec[2] = PCB_(d_z_size[0], r_vec[2]);

			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
			if (ris < d_r_cut_sqrd[0])
			{
				r_vec[0] = ANG_TO_BOHR(r_vec[0]);
				r_vec[1] = ANG_TO_BOHR(r_vec[1]);
				r_vec[2] = ANG_TO_BOHR(r_vec[2]);
				ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];

				ris = sqrt(ris);
				pidx = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = ris * ris;

				test2 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				d_polyVal(ris, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test2 += result;
#endif
			}
		}
	}
	pdpm_energy[threadIdx.x].x += test1;
	pdpm_energy[threadIdx.x].y += test2;
	pdpm_energy[threadIdx.x].z += test2;
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int i = blockDim.x / 2;
	while (0 < i)
	{
		if (threadIdx.x < i)
		{
			pdpm_energy[threadIdx.x].x += pdpm_energy[threadIdx.x + i].x;
			pdpm_energy[threadIdx.x].y += pdpm_energy[threadIdx.x + i].y;
			pdpm_energy[threadIdx.x].z += pdpm_energy[threadIdx.x + i].z;
		}
		__syncthreads();
		i /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_pydop_mon_mov[blockIdx.x] = pdpm_energy[0].x;
		t_d_pydop_mon_mov[blockIdx.x] = pdpm_energy[0].y;
		tr_d_pydop_mon_mov[blockIdx.x] = pdpm_energy[0].z;
	}
}

__global__ void d_PyDopPartDopMove(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z,
	const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z,
	DATA_TYPE* d_interpolationParameters, DATA_TYPE* d_pydop_dop_mov, 
	DATA_TYPE* t_d_pydop_dop_mov, int nDopants, int nMonomers, int didx, int mlb, int mub,
	DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ CUDA_PAIR pdpd_energy[];
	pdpd_energy[threadIdx.x].x = 0;
	pdpd_energy[threadIdx.x].y = 0;

	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE t_ucoul = 0.0, t_ucd = 0.0, t_udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(d_Q_dop[0]) * DEBEYE_TO_HA(d_dp_mag[0]) * d_C[0];
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(d_sigmaPyDopant[0]);
	DATA_TYPE sigma_9 = sigma_6 * d_sigmaPyDopant[0] * d_sigmaPyDopant[0] * d_sigmaPyDopant[0];
	DATA_TYPE Q = d_Q_dop[0] * d_Q_mono[0];
	DATA_TYPE vPressure = 0.0;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut[0]);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd[0]);
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = Q * erfc(d_kappa[0] * bohr_r_cut) / bohr_r_cut;
	int pidx = 0;
	DATA_TYPE result = 0.0, test1 = 0.0, test2 = 0.0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = d_interpolation_coeff_offset[0] + (d_interpolation_order[0] * 2 + 2);
#else
	int coefficient_offset = 12;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 3;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0; 
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = d_interpolation_order[0];
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	while (idx < mlb)
		idx += stride;

	for (; idx < mub; idx += stride)
	{
		cmx = d_cm_x[idx];
		cmy = d_cm_y[idx];
		cmz = d_cm_z[idx];

		// before translation
		{
			r_vec[0] = cmx - d_dop_x[didx];
			r_vec[1] = cmy - d_dop_y[didx];
			r_vec[2] = cmz - d_dop_z[didx];
			r_vec[0] = PCB_(d_x_size[0], r_vec[0]);
			r_vec[1] = PCB_(d_y_size[0], r_vec[1]);
			r_vec[2] = PCB_(d_z_size[0], r_vec[2]);

			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
			if (ris < d_r_cut_sqrd[0])
			{
				r_vec[0] = ANG_TO_BOHR(r_vec[0]);
				r_vec[1] = ANG_TO_BOHR(r_vec[1]);
				r_vec[2] = ANG_TO_BOHR(r_vec[2]);

				ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];

				ris = sqrt(ris);
				pidx = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = ris * ris;

				test1 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				d_polyVal(ris, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test1 += result;
#endif
			}
		}

		// after translation
		{
			r_vec[0] = cmx - nx;
			r_vec[1] = cmy - ny;
			r_vec[2] = cmz - nz;
			r_vec[0] = PCB_(d_x_size[0], r_vec[0]);
			r_vec[1] = PCB_(d_y_size[0], r_vec[1]);
			r_vec[2] = PCB_(d_z_size[0], r_vec[2]);

			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
			if (ris < d_r_cut_sqrd[0])
			{
				r_vec[0] = ANG_TO_BOHR(r_vec[0]);
				r_vec[1] = ANG_TO_BOHR(r_vec[1]);
				r_vec[2] = ANG_TO_BOHR(r_vec[2]);

				ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];

				ris = sqrt(ris);
				pidx = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = ris * ris;

				test2 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test2 += spline_temp;
#else

				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				d_polyVal(ris, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test2 += result;
#endif
			}
		}
	}
	__syncthreads();

	pdpd_energy[threadIdx.x].x += test1;
	pdpd_energy[threadIdx.x].y += test2;

	// Build summation tree over elements and
	// Sum the terms in the block
	int i = blockDim.x / 2;
	while (0 < i)
	{
		if (threadIdx.x < i)
		{
			pdpd_energy[threadIdx.x].x += pdpd_energy[threadIdx.x + i].x;
			pdpd_energy[threadIdx.x].y += pdpd_energy[threadIdx.x + i].y;
		}
		__syncthreads();
		i /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_pydop_dop_mov[blockIdx.x] = pdpd_energy[0].x;
		t_d_pydop_dop_mov[blockIdx.x] = pdpd_energy[0].y;
	}
}

__device__  void d_PCB(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z)
{
	if (!d_usePCB[0])
		return;

	if (x > sizex * 0.5) x = x - sizex;
	if (x <= -sizex * 0.5) x = x + sizex;

	if (y > sizey * 0.5) y = y - sizey;
	if (y <= -sizey * 0.5) y = y + sizey;

	if (z > sizez * 0.5) z = z - sizez;
	if (z <= -sizez * 0.5) z = z + sizez;
}

__global__ void d_scaleSystem(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	DATA_TYPE* d_dop_x, DATA_TYPE* d_dop_y, DATA_TYPE* d_dop_z,
	DATA_TYPE* d_olig_cm_x, DATA_TYPE* d_olig_cm_y, DATA_TYPE* d_olig_cm_z,
	int monomers_per_chain, int nMonomers, int nDopants,
	DATA_TYPE scaleFactorX, DATA_TYPE scaleFactorY,
	DATA_TYPE scaleFactorZ, bool updateCutoff)
{
	d_calculateOligomersCenterOfMasses(d_cm_x, d_cm_y, d_cm_z,
		d_olig_cm_x, d_olig_cm_y, d_olig_cm_z, monomers_per_chain, nMonomers);

	d_x_size[0] *= scaleFactorX;
	d_y_size[0] *= scaleFactorY;
	d_z_size[0] *= scaleFactorZ;

	DATA_TYPE min = MIN(d_x_size[0], d_y_size[0]);
	min = MIN(min, d_z_size[0]);
	if (updateCutoff)
		d_r_cut[0] = (min / 2.0) * 0.98; // appConfig.r_cut;

	d_r_cut_sqrd[0] = d_r_cut[0] * d_r_cut[0];

	for (int didx = 0; didx < nDopants; didx++)
	{
		d_dop_x[didx] = d_dop_x[didx] * scaleFactorX;
		d_dop_y[didx] = d_dop_y[didx] * scaleFactorY;
		d_dop_z[didx] = d_dop_z[didx] * scaleFactorZ;
	}

	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	DATA_TYPE ocmx = 0.0, ocmy = 0.0, ocmz = 0.0;

	int nPolymers = nMonomers / monomers_per_chain;
	for (int idx = 0; idx < nPolymers; idx++)
	{
		// original oligomer center of mass
		ocmx = d_olig_cm_x[idx];
		ocmy = d_olig_cm_y[idx];
		ocmz = d_olig_cm_z[idx];

		// scaled center of mass
		d_olig_cm_x[idx] *= scaleFactorX;
		d_olig_cm_y[idx] *= scaleFactorY;
		d_olig_cm_z[idx] *= scaleFactorZ;

		// differential between old center of mass and new center of mass
		dx = d_olig_cm_x[idx] - ocmx;
		dy = d_olig_cm_y[idx] - ocmy;
		dz = d_olig_cm_z[idx] - ocmz;

		// translate the oligomer to its new location
		//translateOligomer(ps, idx, dx, dy, dz);
		{
			int lb = idx * monomers_per_chain;
			int ub = lb + monomers_per_chain;

			for (int pidx = lb; pidx < ub; pidx++)
			{
				d_cm_x[pidx] += dx;
				d_cm_y[pidx] += dy;
				d_cm_z[pidx] += dz;
			}
		}
	}

	d_calculateOligomersCenterOfMasses(d_cm_x, d_cm_y, d_cm_z,
		d_olig_cm_x, d_olig_cm_y, d_olig_cm_z, monomers_per_chain, nMonomers);

}


__device__  void d_calculateOligomersCenterOfMasses(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	DATA_TYPE* d_olig_cm_x, DATA_TYPE* d_olig_cm_y, DATA_TYPE* d_olig_cm_z, int monomers_per_chain, int nMonomers)
{
	int ctr = 0;

	for (int j = 0; j < nMonomers; j = j + monomers_per_chain)
	{
		int lb = (j / monomers_per_chain) * monomers_per_chain;
		int ub = lb + monomers_per_chain;
		DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;

		for (int i = lb; i < ub; i++)
		{
			cmx += d_cm_x[i];
			cmy += d_cm_y[i];
			cmz += d_cm_z[i];
		}

		cmx = d_olig_cm_x[ctr] = cmx / monomers_per_chain;
		cmy = d_olig_cm_y[ctr] = cmy / monomers_per_chain;
		cmz = d_olig_cm_z[ctr] = cmz / monomers_per_chain;

		cmx = 0.0;
		cmy = 0.0;
		cmz = 0.0;

		ctr++;
	}
}

__device__ void warpReduce(volatile DATA_TYPE* sdata, int tid)
{
	sdata[tid] += sdata[tid + 32];
	sdata[tid] += sdata[tid + 16];
	sdata[tid] += sdata[tid + 8];
	sdata[tid] += sdata[tid + 4];
	sdata[tid] += sdata[tid + 2];
	sdata[tid] += sdata[tid + 1];
}

__global__ void d_PyPyTotalInter(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z, DATA_TYPE* d_interpolationParameters,
	DATA_TYPE* d_umonmon, int mlb, int mub, int monomers_per_chain, int nMonomers, bool removeDouble)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	int test_chain_id = 0;
	int input_chain_id = 0;
	int exc_lb = 0;
	int exc_ub = 0;
	int t_lb = 0;
	int t_ub = 0;
	bool isInputHeadTail = false, isTestHeadTail = false;

	extern __shared__ DATA_TYPE ppt_energy[];
	ppt_energy[threadIdx.x] = 0;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE ucoul = 0.0, uexc = 0.0, udd = 0.0;
	DATA_TYPE qsqrd = d_Q_mono[0] * d_Q_mono[0];
	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE sigma_6_mid = TO_POW_6(d_sigma_inter[0]);
	DATA_TYPE sigma_9_mid = sigma_6_mid * d_sigma_inter[0] * d_sigma_inter[0] * d_sigma_inter[0];
	DATA_TYPE sigma_6_end = TO_POW_6(d_sigma_inter_end[0]);
	DATA_TYPE sigma_9_end = sigma_6_end * d_sigma_inter_end[0] * d_sigma_inter_end[0] * d_sigma_inter_end[0];
	DATA_TYPE sigma_6 = 0.0, sigma_9 = 0.0;
	DATA_TYPE epsilon = d_epsilon_inter[0];
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(d_dp_mag[0]) * DEBEYE_TO_HA(d_dp_mag[0]);
	int tlb = 0, tub = 0;
	DATA_TYPE rij3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut[0]);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd[0]);
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(d_kappa[0] * bohr_r_cut) / bohr_r_cut;
	int monomer_id = 0;
	int pidx = 0;
	DATA_TYPE result = 0.0, test1 = 0.0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = d_interpolation_coeff_offset[0];
#else
	int coefficient_offset = 4;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0; 
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = d_interpolation_order[0];
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	while (idx < mlb)
		idx += stride;

	for (; idx < mub; idx += stride)
	{
		ox = d_cm_x[idx];
		oy = d_cm_y[idx];
		oz = d_cm_z[idx];

		dpmx = d_dpm_x[idx];
		dpmy = d_dpm_y[idx];
		dpmz = d_dpm_z[idx];

		test1 = 0.0;
		udd = 0.0;

		test_chain_id = 0;
		input_chain_id = idx / monomers_per_chain;

		for (int inner = 0; inner < nMonomers; inner++)
		{
			if (inner == idx)
				continue;

			if (removeDouble && (inner < idx)) // do not DATA_TYPE count end monomer interactions
				continue;

			test_chain_id = inner / monomers_per_chain;

			if (input_chain_id == test_chain_id)
				continue;

			xdif = d_cm_x[inner] - ox;
			ydif = d_cm_y[inner] - oy;
			zdif = d_cm_z[inner] - oz;
			xdif = PCB_(d_x_size[0], xdif);
			ydif = PCB_(d_y_size[0], ydif);
			zdif = PCB_(d_z_size[0], zdif);

			rij = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rij <= d_r_cut_sqrd[0])
			{
				xdif = ANG_TO_BOHR(xdif);
				ydif = ANG_TO_BOHR(ydif);
				zdif = ANG_TO_BOHR(zdif);

				rij = sqrt(xdif * xdif + ydif * ydif + zdif * zdif);

				rij3 = TO_POW_3(rij);

				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, d_dpm_x[inner], d_dpm_y[inner], d_dpm_z[inner]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(d_dpm_x[inner], d_dpm_y[inner], d_dpm_z[inner], xdif / rij, ydif / rij, zdif / rij);

				udd += (dp_mag_sqrd * (dij - (3.0 * dir * djr) / (rij * rij))) / (rij3);

				pidx = int((rij / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = rij * rij;

				test1 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((rij / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = 4 * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				d_polyVal(rij, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test1 += result;
#endif
			}
		}
		ppt_energy[threadIdx.x] += test1 + udd;
	}

	// Wait for all threads in block to finish computations
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			ppt_energy[threadIdx.x] += ppt_energy[threadIdx.x + j];
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_umonmon[blockIdx.x] = ppt_energy[0];
	}
}

__global__ void d_PyDopTotalInter(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z,
	const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z,
	DATA_TYPE* d_interpolationParameters, DATA_TYPE* d_pydop_dop_mov, int nDopants, 
	int nMonomers, int mlb, int mub)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ DATA_TYPE pdt_energy[];
	pdt_energy[threadIdx.x] = 0;

	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(d_Q_dop[0]) * DEBEYE_TO_HA(d_dp_mag[0]) * d_C[0];
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(d_sigmaPyDopant[0]);
	DATA_TYPE sigma_9 = sigma_6 * d_sigmaPyDopant[0] * d_sigmaPyDopant[0] * d_sigmaPyDopant[0];
	DATA_TYPE Q = d_Q_dop[0] * d_Q_mono[0];
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut[0]);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd[0]);
	int pidx = 0;
	DATA_TYPE result = 0.0, test1 = 0.0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = d_interpolation_coeff_offset[0] + (d_interpolation_order[0] * 2 + 2);
#else
	int coefficient_offset = 12;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 3;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = d_interpolation_order[0];
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	while (idx < mlb)
		idx += stride;

	for (; idx < mub; idx += stride)
	{
		cmx = d_cm_x[idx];
		cmy = d_cm_y[idx];
		cmz = d_cm_z[idx];

		dpmx = d_dpm_x[idx];
		dpmy = d_dpm_y[idx];
		dpmz = d_dpm_z[idx];

		for (int inner = 0; inner < nDopants; inner++)
		{
			r_vec[0] = cmx - d_dop_x[inner];
			r_vec[1] = cmy - d_dop_y[inner];
			r_vec[2] = cmz - d_dop_z[inner];
			r_vec[0] = PCB_(d_x_size[0], r_vec[0]);
			r_vec[1] = PCB_(d_y_size[0], r_vec[1]);
			r_vec[2] = PCB_(d_z_size[0], r_vec[2]);

			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
			if (ris < d_r_cut_sqrd[0])
			{
				r_vec[0] = ANG_TO_BOHR(r_vec[0]);
				r_vec[1] = ANG_TO_BOHR(r_vec[1]);
				r_vec[2] = ANG_TO_BOHR(r_vec[2]);
				ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
				
				ris = sqrt(ris);

				pidx = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = ris * ris;

				test1 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((ris / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test1 += spline_temp;
#else

				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				d_polyVal(ris, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test1 += result;
#endif
			}
		}
		pdt_energy[threadIdx.x] += test1;

	}

	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int i = blockDim.x / 2;
	while (0 < i)
	{
		if (threadIdx.x < i)
		{
			pdt_energy[threadIdx.x] += pdt_energy[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_pydop_dop_mov[blockIdx.x] = pdt_energy[0];
	}

}

__global__ void d_DopDopTotalInter(const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z, 
	DATA_TYPE* d_interpolationParameters, DATA_TYPE* d_udopdop, int size, int dlb, int dub)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ DATA_TYPE ddt_energy[];
	ddt_energy[threadIdx.x] = 0;

	DATA_TYPE sigma_dop = ANG_TO_BOHR(d_sigma_dopant[0]);
	DATA_TYPE sigma_9 = TO_POW_6(sigma_dop) * TO_POW_3(sigma_dop);
	DATA_TYPE q_sqrd = d_Q_dop[0] * d_Q_dop[0];
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0, rss = 0.0;
	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	DATA_TYPE rss3 = 0.0;
	DATA_TYPE temp = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut[0]);
	int pidx = 0;
	DATA_TYPE result = 0.0, test1 = 0.0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = d_interpolation_coeff_offset[0] + (d_interpolation_order[0] + 1);
#else
	int coefficient_offset = 8; 
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 2;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = d_interpolation_order[0];
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	while (idx < dlb)
		idx += stride;

	for (; idx < dub; idx += stride)
	{
		dx = d_dop_x[idx];
		dy = d_dop_y[idx];
		dz = d_dop_z[idx];

		temp = 0.0;

		for (int inner = idx + 1; inner < size; inner++)
		{
			xdif = d_dop_x[inner] - dx;
			ydif = d_dop_y[inner] - dy;
			zdif = d_dop_z[inner] - dz;
			xdif = PCB_(d_x_size[0], xdif);
			ydif = PCB_(d_y_size[0], ydif);
			zdif = PCB_(d_z_size[0], zdif);

			rss = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rss < d_r_cut_sqrd[0])
			{
				rss = sqrt(rss);

				rss = ANG_TO_BOHR(rss);
				pidx = int((rss / d_interpolation_delta[0])) - d_interpolation_offset[0];
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				x1 = d_interpolationParameters[pidx_speed];
				x2 = d_interpolationParameters[1 * d_nInterpolationRows[0] + pidx_speed];
				x3 = d_interpolationParameters[2 * d_nInterpolationRows[0] + pidx_speed];
				x4 = d_interpolationParameters[3 * d_nInterpolationRows[0] + pidx_speed];
				x5 = d_interpolationParameters[4 * d_nInterpolationRows[0] + pidx_speed];
				x6 = d_interpolationParameters[5 * d_nInterpolationRows[0] + pidx_speed];
				x7 = d_interpolationParameters[6 * d_nInterpolationRows[0] + pidx_speed];
				xsqrd = rss * rss;

				test1 += (x1 + x2 * rss + x3 * xsqrd + x4 * xsqrd * rss + x5 * xsqrd * xsqrd + x6 * rss * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 1];
				energy[1] = d_interpolationParameters[pidx_speed];
				energy[2] = d_interpolationParameters[pidx_speed + 1];
				energy[3] = d_interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rss * rss;
				mu3 = mu2 * rss;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rss;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 2 * d_nInterpolationRows[0] + pidx;
				energy[0] = d_interpolationParameters[pidx_speed - 2];
				energy[1] = d_interpolationParameters[pidx_speed - 1];
				energy[2] = d_interpolationParameters[pidx_speed];
				energy[3] = d_interpolationParameters[pidx_speed + 1];
				energy[4] = d_interpolationParameters[pidx_speed + 2];

				xofff = int((rss / d_interpolation_delta[0])) - d_interpolation_offset[0];
				x_dist[0] = d_interpolationParameters[xofff - 2];
				x_dist[1] = d_interpolationParameters[xofff - 1];
				x_dist[2] = d_interpolationParameters[xofff];
				x_dist[3] = d_interpolationParameters[xofff + 1];
				x_dist[4] = d_interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				spline_coeff[0] = d_interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = d_interpolationParameters[pidx_speed];
				spline_coeff[2] = d_interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = d_interpolationParameters[pidx_speed + 2];


				d_splint(x_dist, energy, spline_coeff, 4, rss, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rss * 2;
				tmp = 0.0;
				nd = d_interpolation_order[0];
				pidx_speed = coefficient_offset * d_nInterpolationRows[0] + pidx;
				c0 = d_interpolationParameters[pidx_speed + 2 * d_nInterpolationRows[0]];
				c1 = d_interpolationParameters[pidx_speed + 3 * d_nInterpolationRows[0]];
				c2 = d_interpolationParameters[pidx_speed + 1 * d_nInterpolationRows[0]];
				c3 = d_interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				d_polyVal(rss, d_interpolationParameters, coefficient_offset * d_nInterpolationRows[0] + pidx, d_interpolation_order[0], d_nInterpolationRows[0], result);
				test1 += result;
#endif
			}
		}

		ddt_energy[threadIdx.x] += test1;
	}

	// Wait for all threads in block to finish computations
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			ddt_energy[threadIdx.x] += ddt_energy[threadIdx.x + j];
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_udopdop[blockIdx.x] = ddt_energy[0];
	}
}

__device__ void d_polyVal(DATA_TYPE x, DATA_TYPE* interpolationParameters, int pidx, int nCoeffs, int nInterpolationRows, DATA_TYPE &result)
{
	result = 0.0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	// this evaluates a simply polynomial of order nCoeffs
	DATA_TYPE x1 = interpolationParameters[pidx];
	DATA_TYPE x2 = interpolationParameters[1 * nInterpolationRows + pidx];
	DATA_TYPE x3 = interpolationParameters[2 * nInterpolationRows + pidx];
	DATA_TYPE x4 = interpolationParameters[3 * nInterpolationRows + pidx];
	DATA_TYPE x5 = interpolationParameters[4 * nInterpolationRows + pidx];
	DATA_TYPE x6 = interpolationParameters[5 * nInterpolationRows + pidx];
	DATA_TYPE x7 = interpolationParameters[6 * nInterpolationRows + pidx];
	DATA_TYPE xsqrd = x * x;

	result =x1 + x2 * x + x3 * xsqrd + x4 * xsqrd * x + x5 * xsqrd * xsqrd + x6 * x * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd;

#else
	// this evaluates a hermite polynomial
	DATA_TYPE x2 = x * 2, tmp = 0.0;
	int nd = nCoeffs;
	DATA_TYPE c0 = interpolationParameters[pidx + 2 * nInterpolationRows];
	DATA_TYPE c1 = interpolationParameters[pidx + 3 * nInterpolationRows];
	DATA_TYPE c2 = interpolationParameters[pidx + 1 * nInterpolationRows];
	DATA_TYPE c3 = interpolationParameters[pidx];

	tmp = c0;
	nd = nd - 1;
	c0 = c2 - c1 * (2.0 * (nd - 1));
	c1 = tmp + c1 * x2;

	tmp = c0;
	nd = nd - 1;
	c0 = c3 - c1 * (2.0 * (nd - 1));
	c1 = tmp + c1 * x2;

	result = c0 + c1 * x2;

#endif
}

__device__ inline void d_splint(DATA_TYPE xa[], DATA_TYPE ya[], DATA_TYPE y2a[], int n, DATA_TYPE x, DATA_TYPE* y)
{
	void nrerror(char error_text[]);
	int klo, khi, k;
	DATA_TYPE h, b, a;

	klo = 1;
	khi = n;
	while (khi - klo > 1)
	{
		k = (khi + klo) >> 1;
		if (xa[k] > x)
			khi = k;
		else
			klo = k;
	}
	h = xa[khi] - xa[klo];

	a = (xa[khi] - x) / h;
	b = (x - xa[klo]) / h;
	*y = a * ya[klo] + b * ya[khi] + ((a * a * a - a) * y2a[klo] + (b * b * b - b) * y2a[khi]) * (h * h) / 6.0;
}
