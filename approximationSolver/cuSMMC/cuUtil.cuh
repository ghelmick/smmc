#if !defined (__CU_UTIL_CUH__)
#define       __CU_UTIL_CUH__

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "util.h"

#define NUMBER_GPU_THREADS_DOP 64
#define NUMBER_GPU_THREADS_MON 256


// utility functions for memory allocation and memory copying
template<typename T>
void freeMemory(T * mem)
{
	cudaFree(mem);
}

template<typename T>
cudaError_t copyToDevice(const T * src, T * dst, size_t size, cudaStream_t stream)
{
	size_t numbytes = size * sizeof(T);
	// Copy input vectors from host memory to GPU buffers.
	return cudaMemcpyAsync(dst, src, numbytes, cudaMemcpyHostToDevice, stream);
}

template<typename T>
cudaError_t copyFromDevice(const T * src, T * dst, size_t size, cudaStream_t stream)
{
	// Copy output vector from GPU buffer to host memory.
	return cudaMemcpyAsync(dst, src, size * sizeof(T), cudaMemcpyDeviceToHost , stream);
}

template<typename T>
cudaError_t zeroMemory(T * data, size_t size, cudaStream_t stream)
{
	return cudaMemsetAsync(data, '0', size * sizeof(T), stream);
}

template<typename T>
cudaError_t allocateMemoryOnDevice(T ** data, size_t size, cudaStream_t stream)
{
	cudaError_t cudaStatus;

	cudaStatus = cudaMalloc((void**)data, size * sizeof(T));
	if (cudaStatus != cudaSuccess)
	{
		return cudaStatus;
	}

	cudaStreamSynchronize(stream);

	cudaStatus = cudaMemsetAsync(*data, '0', size * sizeof(T));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
	}

	return cudaStatus;
}

template<typename T>
cudaError_t allocateMemoryOnHost(T* data, size_t size, unsigned int flags)
{
	return cudaMallocHost(data, size * sizeof(T), cudaHostAllocWriteCombined | cudaHostAllocMapped);
}

inline bool setDevice(unsigned short devId)
{
	bool retVal = true;
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(devId);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		retVal = false;
	}

	return retVal;
}

inline int getDeviceId()
{
	int dev_id = -1;
	cudaError_t cudaStatus = cudaGetDevice(&dev_id);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaGetDevice failed!  Device id could not be determined?");
	}
	return dev_id;
}

inline bool initialize(unsigned short devId)
{
	return setDevice(devId);
}

#endif