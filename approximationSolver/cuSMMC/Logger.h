#if !defined (__LOG_H__)
#define       __LOG_H__

#include <string>
#include <fstream>

using std::string;
using std::ofstream;

class Logger
{
public:
	static Logger & getInstance()
	{
		static Logger instance;
		return instance;
	}

	// The copy constructor is deleted, to prevent client code from creating new
	// instances of this class by copying the instance returned by get_instance()
	Logger(Logger const&) = delete;

	// The move constructor is deleted, to prevent client code from moving from
	// the object returned by get_instance(), which could result in other clients
	// retrieving a reference to an object with unspecified state.
	Logger(Logger&&) = delete;

	~Logger(); 

	void Write(const string & message);
	void Write(const string & file, int lineNumber, const string & message);

private:
	Logger();

	ofstream _outputStream;
};

#endif

