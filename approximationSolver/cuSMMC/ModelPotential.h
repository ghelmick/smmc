#if !defined (__MODEL_POTENTIAL_H__)
#define       __MODEL_POTENTIAL_H__

#include "polymerUtil.h"
#include <fstream>
#include "GPUWorkUnit.cuh"

using std::ofstream;
struct AppConfig;

class ModelPotential
{
public:
	ModelPotential(const AppConfig & appConfig);

	// partial energy functions
	DATA_TYPE UbondPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE UbendPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE UtorsionPart(PARTICLE_SYSTEM & ps, int midx);
	void UdipoleDipolePart(PARTICLE_SYSTEM& ps, int midx,
		DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
		DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
		DATA_TYPE & udd, DATA_TYPE & t_udd, DATA_TYPE & rt_udd);
	void UcoulombPart(PARTICLE_SYSTEM& ps, int midx,
		DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
		DATA_TYPE& ucoulomb, DATA_TYPE& t_ucoulomb);
	void UantiCoilingPart(PARTICLE_SYSTEM& ps, int midx,
		DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
		DATA_TYPE& uac, DATA_TYPE& t_uac);
	DATA_TYPE URotationPart(PARTICLE_SYSTEM & ps, int midx);
	void URestorationPart(PARTICLE_SYSTEM& ps, int midx,
		DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
		DATA_TYPE& urest, DATA_TYPE& t_urest);
	void PyPyPart(PARTICLE_SYSTEM& ps, int midx, int mlb, int mub,
					DATA_TYPE& ucoul, DATA_TYPE& uexc, DATA_TYPE& udd,
					DATA_TYPE& t_ucoul, DATA_TYPE& t_uexc, DATA_TYPE& t_udd,
					DATA_TYPE& tr_ucoul, DATA_TYPE& tr_uexc, DATA_TYPE& tr_udd,
					DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
					DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
					DATA_TYPE& upypy, DATA_TYPE& t_upypy, DATA_TYPE& tr_upypy,
					bool removeDouble = false);
	void PyDopPartMonMove(PARTICLE_SYSTEM& ps, int midx, int dlb, int dub,
					DATA_TYPE& ucoul, DATA_TYPE& ucd, DATA_TYPE& udisp,
					DATA_TYPE& t_ucoul, DATA_TYPE& t_ucd, DATA_TYPE& t_udisp,
					DATA_TYPE& tr_ucoul, DATA_TYPE& tr_ucd, DATA_TYPE& tr_udisp,
					DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
					DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
					DATA_TYPE& upydop, DATA_TYPE& t_upydop, DATA_TYPE& tr_upydop);
	void PyDopPartDopMove(PARTICLE_SYSTEM& ps, int didx, int mlb, int mub,
		                  DATA_TYPE p_dop_x, DATA_TYPE p_dop_y, DATA_TYPE p_dop_z,
		                  DATA_TYPE& ucoul, DATA_TYPE& ucd, DATA_TYPE& udisp,
		                  DATA_TYPE& t_ucoul, DATA_TYPE& t_ucd, DATA_TYPE& t_udisp,
		                  DATA_TYPE& upydop, DATA_TYPE& t_upydop);
	void PyPyPartImageInteraction(PARTICLE_SYSTEM& ps, int midx, int mlb, int mub,
                                       DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
                                       DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
                                       DATA_TYPE& upypy, DATA_TYPE& t_upypy, DATA_TYPE& tr_upypy);

	void DopDopPart(PARTICLE_SYSTEM& ps, int didx, int dlb, int dub,
		            DATA_TYPE p_dop_x, DATA_TYPE p_dop_y, DATA_TYPE p_dop_z,
		            DATA_TYPE& ucoul, DATA_TYPE& uother,
		            DATA_TYPE& t_ucoul, DATA_TYPE& t_uother,
		            DATA_TYPE& udopdop, DATA_TYPE& t_udopdop);
	DATA_TYPE TotalIntra(PARTICLE_SYSTEM & ps, ofstream & logFile, bool logResults = false);
	DATA_TYPE TotalInter(PARTICLE_SYSTEM& ps, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT * workunits,
		                 DATA_TYPE& upypy, DATA_TYPE& upydop, DATA_TYPE& udopdop,
		                 DATA_TYPE& ucoul, DATA_TYPE& uother);
	void TotalIntraPart(PARTICLE_SYSTEM& ps, int midx,
		DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
		DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
		DATA_TYPE p_n_x, DATA_TYPE p_n_y, DATA_TYPE p_n_z,
		DATA_TYPE& intra, DATA_TYPE& t_intra, DATA_TYPE& tr_intra);
	void TotalMonomerPart(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT* workunits,
		DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
		DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
		DATA_TYPE p_n_x, DATA_TYPE p_n_y, DATA_TYPE p_n_z,
		DATA_TYPE& intra, DATA_TYPE& t_intra, DATA_TYPE& tr_intra,
		DATA_TYPE& inter, DATA_TYPE& t_inter, DATA_TYPE& tr_inter);

	void TotalDopantPart(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT * workunits,
		DATA_TYPE p_dop_x, DATA_TYPE p_dop_y, DATA_TYPE p_dop_z,
		DATA_TYPE& inter, DATA_TYPE& t_inter);

	// utility methods to access the common model potential constants
	int getMonomersPerChain() { return monomers_per_chain; }
	DATA_TYPE getA0() { return a0; }
	DATA_TYPE getDe() { return De; }
	DATA_TYPE getR0() { return r0; }
	DATA_TYPE getAlpha_() { return alpha_; }
	DATA_TYPE getDpMag() { return dp_mag; }
	DATA_TYPE getK_Theta() { return k_theta; }
	DATA_TYPE getTheta_0() { return theta_0; }
	DATA_TYPE getK1() { return k1; }
	DATA_TYPE getK2() { return k2; }
	DATA_TYPE getGamma_0() { return gamma_0; }
	DATA_TYPE getEpsilon_Intra() { return epsilon_intra; }
	DATA_TYPE getSigma_Intra() { return sigma_intra; }
	DATA_TYPE getK4() { return k4; }
	DATA_TYPE getK5() { return k5; }
	DATA_TYPE getK5_End() { return k5_end; }
	DATA_TYPE getQ_Mono() { return Q_mono; }
	DATA_TYPE getQ_Dop() { return Q_dop; }
	DATA_TYPE getKappa() { return kappa; }
	DATA_TYPE getA() { return A; }
	DATA_TYPE getB() { return B; }
	DATA_TYPE getC() { return C; }
	DATA_TYPE getSigmaPyDopant() { return sigmaPyDopant; }
	DATA_TYPE getEpsilon_Inter() { return epsilon_inter; }
	DATA_TYPE getSigma_Inter() { return sigma_inter; }
	DATA_TYPE getEpsilon_Inter_End() { return epsilon_inter_end; }
	DATA_TYPE getSigma_inter_End() { return sigma_inter_end; }
	DATA_TYPE getEpsilon_Dopant() { return epsilon_dopant; }
	DATA_TYPE getSigma_Dopant() { return sigma_dopant; }
	DATA_TYPE * getInterpolationParameters() { return interpolationParameters; }
	int getNumInterpolationRows() { return nInterpolationRows; }
	int getNumInterpolationCols() { return nInterpolationCols; }
	int getInterpolationOffset() { return interpolation_offset; }
	int getInterpolationOrder() { return interpolationOrder; }
	int getInterpolationCoeffOffset() { return interpolation_coeff_offset; }
	DATA_TYPE getInterpolationDelta() { return interpolation_delta; }

	void evalInter(DATA_TYPE dist, DATA_TYPE r_cut, DATA_TYPE& upypy, DATA_TYPE& upydop, DATA_TYPE& udopdop);
	void calculateModelParameters(DATA_TYPE r_cut, DATA_TYPE interpolation_delta = 0.01, int order = 6);

private:
	void pairwiseIntraTerms(PARTICLE_SYSTEM& ps, int midx,
		DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
		DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
		DATA_TYPE p_n_x, DATA_TYPE p_n_y, DATA_TYPE p_n_z,
		DATA_TYPE& bond, DATA_TYPE& t_bond,
		DATA_TYPE& bend, DATA_TYPE& t_bend,
		DATA_TYPE& torsion, DATA_TYPE& r_torsion,
		DATA_TYPE& coulomb, DATA_TYPE& t_coulomb,
		DATA_TYPE& rotation, DATA_TYPE& t_rotation, DATA_TYPE& tr_rotation);

	int monomers_per_chain;

	DATA_TYPE a0;
	DATA_TYPE De;
	DATA_TYPE r0;
	DATA_TYPE alpha_;
	DATA_TYPE dp_mag;
	DATA_TYPE k_theta;
	DATA_TYPE theta_0;
	DATA_TYPE k1;
	DATA_TYPE k2;
	DATA_TYPE gamma_0;
	DATA_TYPE epsilon_intra;
	DATA_TYPE sigma_intra;
	DATA_TYPE k4;
	DATA_TYPE k5;
	DATA_TYPE k5_end;
	DATA_TYPE Q_mono;
	DATA_TYPE Q_dop;
	DATA_TYPE QSqrtd;
	DATA_TYPE kappa;
	DATA_TYPE A;
	DATA_TYPE B;
	DATA_TYPE C;
	DATA_TYPE sigmaPyDopant;
	DATA_TYPE epsilon_inter;
	DATA_TYPE sigma_inter;
	DATA_TYPE epsilon_inter_end;
	DATA_TYPE sigma_inter_end;
	DATA_TYPE epsilon_dopant;
	DATA_TYPE sigma_dopant;
	DATA_TYPE sigma_dopant_9;

	int intra_coul_offset;
	DATA_TYPE intra_kappa;
	bool self_interaction;

	DATA_TYPE * interpolationParameters;
	DATA_TYPE interpolation_delta;
	int interpolation_offset;
	int interpolation_coeff_offset;
	int nInterpolationRows;
	int nInterpolationCols;
	int interpolationOrder;
};
#endif

