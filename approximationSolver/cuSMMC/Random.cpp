#include "Random.h"

Random::Random()
{
#if !defined USE_TEST_DATA
	generator = std::mt19937(271);
	dis = std::uniform_real_distribution<DATA_TYPE>(0.0, 1.0);
	myRandom = MyRandom(111);
#else
	tData = TestData("random_numbers.txt");
#endif
}

Random::~Random()
{

}

DATA_TYPE Random::randomDouble()
{
#if !defined USE_TEST_DATA
	return dis(generator);
	//return myRandom.nextFloat();
#else
	return tData.getValue();
#endif
}

