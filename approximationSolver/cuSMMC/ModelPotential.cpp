#include "ModelPotential.h"
#include "util.h"
#include "polymerUtil.h"
#include "AppConfig.h"
#include "gpuHelper.cuh"
#include "cuUtil.cuh"
#include <iomanip>

#define DELTA 0.01
#define OFFSET 100

ModelPotential::ModelPotential(const AppConfig& appConfig)
{
	self_interaction = appConfig.self_interaction;
	dp_mag = appConfig.dp_mag;
	intra_coul_offset = 1; // default to nearest neighbor 
	intra_kappa = 0.085;

	map<string, DATA_TYPE>::const_iterator itr;

	// Bonding Constants
	itr = appConfig.constants.find("a0");
	if (itr != appConfig.constants.end())
		a0 = itr->second;
	else
		cout << "throw exception, constant a0 must have a value" << endl;

	itr = appConfig.constants.find("De");
	if (itr != appConfig.constants.end())
		De = itr->second;
	else
		cout << "throw exception, constant De must have a value" << endl;

	itr = appConfig.constants.find("r0");
	if (itr != appConfig.constants.end())
		r0 = itr->second;
	else
		cout << "throw exception, constant r0 must have a value" << endl;
	itr = appConfig.constants.find("alpha_");
	if (itr != appConfig.constants.end())
		alpha_ = itr->second;
	else
		cout << "throw exception, constant alpha_ must have a value" << endl;

	// Bending Constants
	itr = appConfig.constants.find("k_theta");
	if (itr != appConfig.constants.end())
		k_theta = itr->second;
	else
		cout << "throw exception, constant k_theta must have a value" << endl;
	itr = appConfig.constants.find("theta_0");
	if (itr != appConfig.constants.end())
		theta_0 = itr->second;
	else
		cout << "throw exception, constant theta_0 must have a value" << endl;

	// Torsion Constants
	itr = appConfig.constants.find("k1");
	if (itr != appConfig.constants.end())
		k1 = itr->second;
	else
		cout << "throw exception, constant k1 must have a value" << endl;
	itr = appConfig.constants.find("k2");
	if (itr != appConfig.constants.end())
		k2 = itr->second;
	else
		cout << "throw exception, constant k2 must have a value" << endl;
	itr = appConfig.constants.find("gamma_0");
	if (itr != appConfig.constants.end())
		gamma_0 = itr->second;
	else
		cout << "throw exception, constant gamma_0 must have a value" << endl;

	// Anti - Coiling Constants
	itr = appConfig.constants.find("epsilon_intra");
	if (itr != appConfig.constants.end())
		epsilon_intra = itr->second;
	else
		cout << "throw exception, constant epsilon_intra must have a value" << endl;
	itr = appConfig.constants.find("sigma_intra");
	if (itr != appConfig.constants.end())
		sigma_intra = itr->second;
	else
		cout << "throw exception, constant sigma_intra must have a value" << endl;

	// Rotation Constants
	itr = appConfig.constants.find("k4");
	if (itr != appConfig.constants.end())
		k4 = itr->second;
	else
		cout << "throw exception, constant k4 must have a value" << endl;

	// Restoration Constants
	itr = appConfig.constants.find("k5");
	if (itr != appConfig.constants.end())
		k5 = itr->second;
	else
		cout << "throw exception, constant k5 must have a value" << endl;
	itr = appConfig.constants.find("k5_end");
	if (itr != appConfig.constants.end())
		k5_end = itr->second;
	else
		cout << "throw exception, constant k5 must have a value" << endl;

	// InterPotential Constants
	// UCoulomb Py - Dopant constants
	itr = appConfig.constants.find("q");
	if (itr != appConfig.constants.end())
		Q_mono = itr->second;
	else
		cout << "throw exception, constant q must have a value" << endl;
	itr = appConfig.constants.find("Q");
	if (itr != appConfig.constants.end())
		Q_dop = itr->second;
	else
		cout << "throw exception, constant Q must have a value" << endl;
	itr = appConfig.constants.find("QSqrtd");
	if (itr != appConfig.constants.end())
		QSqrtd = itr->second;
	else
		cout << "throw exception, constant QSqrtd must have a value" << endl;
	itr = appConfig.constants.find("kappa");
	if (itr != appConfig.constants.end())
		kappa = itr->second;
	else
		cout << "throw exception, constant kappa must have a value" << endl;

	// UDisp Py - Dopant
	itr = appConfig.constants.find("A");
	if (itr != appConfig.constants.end())
		A = itr->second;
	else
		cout << "throw exception, constant A must have a value" << endl;
	itr = appConfig.constants.find("B");
	if (itr != appConfig.constants.end())
		B = itr->second;
	else
		cout << "throw exception, constant B must have a value" << endl;
	itr = appConfig.constants.find("sigmaPyDopant");
	if (itr != appConfig.constants.end())
		sigmaPyDopant = itr->second;
	else
		cout << "throw exception, constant sigmaPyDopant must have a value" << endl;

	// UExcluded Py - Py
	itr = appConfig.constants.find("epsilon_inter");
	if (itr != appConfig.constants.end())
		epsilon_inter = itr->second;
	else
		cout << "throw exception, constant epsilon_inter must have a value" << endl;
	itr = appConfig.constants.find("sigma_inter");
	if (itr != appConfig.constants.end())
		sigma_inter = itr->second;
	else
		cout << "throw exception, constant sigma_inter must have a value" << endl;
	itr = appConfig.constants.find("epsilon_inter_end");
	if (itr != appConfig.constants.end())
		epsilon_inter_end = itr->second;
	else
		cout << "throw exception, constant epsilon_inter_end must have a value" << endl;
	itr = appConfig.constants.find("sigma_inter_end");
	if (itr != appConfig.constants.end())
		sigma_inter_end = itr->second;
	else
		cout << "throw exception, constant sigma_inter_end must have a value" << endl;

	// UCD Py - Dopant
	itr = appConfig.constants.find("C");
	if (itr != appConfig.constants.end())
		C = itr->second;
	else
		cout << "throw exception, constant C must have a value" << endl;

	// UDopant - Dopant
	itr = appConfig.constants.find("epsilon_dopant");
	if (itr != appConfig.constants.end())
		epsilon_dopant = itr->second;
	else
		cout << "throw exception, constant epsilon_dopant must have a value" << endl;
	itr = appConfig.constants.find("sigma_dopant");
	if (itr != appConfig.constants.end())
		sigma_dopant = itr->second;
	else
		cout << "throw exception, constant sigma_dopant must have a value" << endl;
	itr = appConfig.constants.find("sigma_dopant_9");
	if (itr != appConfig.constants.end())
		sigma_dopant_9 = itr->second;
	else
		cout << "throw exception, constant sigma_dopant_9 must have a value" << endl;

	monomers_per_chain = appConfig.monomers_per_chain;

	itr = appConfig.constants.find("intra_coul_offset");
	if (itr != appConfig.constants.end())
		intra_coul_offset = itr->second;
	else
		cout << "throw exception, constant sigma_dopant_9 must have a value" << endl;

	itr = appConfig.constants.find("intra_kappa");
	if (itr != appConfig.constants.end())
		intra_kappa = itr->second;
	else
	{
		intra_kappa = kappa;
		cout << "no value specified for the intra kappa value, default to using the same value for both inter and intra!!!" << endl;
	}

	// load parameters that will be used for energy interpolation
	// try to load the optimized model parameters
	interpolation_coeff_offset = 4;
#if defined __USE_SIMPLE_POLYNOMIAL__ 
	interpolationParameters = NULL;
	interpolation_delta = appConfig.interpolation_delta;
	interpolationOrder = appConfig.simple_polynomial_order;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	interpolation_coeff_offset = 1;
	interpolationParameters = NULL;
	interpolation_delta = appConfig.interpolation_delta;
	interpolationOrder = appConfig.simple_polynomial_order;
#elif defined __USE_INLINE_SPLINE__
	interpolation_coeff_offset = 1;
	interpolationParameters = NULL;
	interpolation_delta = appConfig.interpolation_delta;
	interpolationOrder = 3; // appConfig.simple_polynomial_order;
#else
	interpolationParameters = loadModelParams("model-params.txt", nInterpolationRows, nInterpolationCols, interpolationOrder);
	interpolation_delta = interpolationParameters[1] - interpolationParameters[0];
#endif
	interpolation_offset = (1.0 / interpolation_delta);
}

DATA_TYPE ModelPotential::TotalIntra(PARTICLE_SYSTEM& ps, ofstream& logFile, bool logResults)
{
	int midx = 0;
	DATA_TYPE dd = 0.0, t_dd = 0.0, tr_dd = 0.0;
	DATA_TYPE ac = 0.0, t_ac = 0.0;
	DATA_TYPE bond = 0.0, t_bond = 0.0, bend = 0.0, t_bend = 0.0, torsion = 0.0, t_torsion = 0.0;
	DATA_TYPE coulomb2 = 0.0, t_coulomb2 = 0.0, coulomb = 0.0, t_coulomb = 0.0;
	DATA_TYPE rot = 0.0, t_rot = 0.0, tr_rot = 0.0;
	DATA_TYPE ubond = 0.0, ubend = 0.0, ucoulomb = 0.0, utorsion = 0.0, udd = 0.0, uac = 0.0, urot = 0.0;

	#pragma omp parallel for reduction(+:ubond, ubend, ucoulomb, utorsion, udd, uac, urot) private(midx, bond, bend, coulomb, torsion, dd, ac, rot)
	for (midx = 0; midx < ps.nMonomers; midx = midx + monomers_per_chain)
	{
		bond = bend = coulomb = torsion = dd = ac = rot = 0.0;

		UdipoleDipolePart(ps, midx, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], ps.dpm_x[midx], ps.dpm_y[midx], ps.dpm_z[midx], dd, t_dd, tr_dd);
		UantiCoilingPart(ps, midx, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], ac, t_ac);
		pairwiseIntraTerms(ps, midx, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx],
			ps.dpm_x[midx], ps.dpm_y[midx], ps.dpm_z[midx], ps.n_x[midx], ps.n_y[midx], ps.n_z[midx],
			bond, t_bond, bend, t_bend, torsion, t_torsion, coulomb, t_coulomb,
			rot, t_rot, tr_rot);
		// reusing the variable so it needs to be reset before use
		coulomb = 0.0;
		UcoulombPart(ps, midx, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], coulomb, t_coulomb2);

		ubond += bond;
		ubend += bend;
		ucoulomb += coulomb;
		utorsion += torsion;
		udd += dd;
		uac += ac;
		urot += rot;
	}

	DATA_TYPE total = ubond + ubend + ucoulomb + utorsion + udd + uac + urot;

	if (logResults)
	{
		logFile << std::setprecision(15) << ubond << ',' << std::setprecision(15) << ubend << ',' << std::setprecision(15) << utorsion << ',' << std::setprecision(15) << udd << ',' << std::setprecision(15) << uac << ',' << std::setprecision(15) << 0.0 << ',' << std::setprecision(15) << urot << ',' << std::setprecision(15) << ucoulomb << ',';
	}
	return total;
}

DATA_TYPE ModelPotential::TotalInter(PARTICLE_SYSTEM& ps, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT * workunits,
	                                 DATA_TYPE& upypy, DATA_TYPE& upydop, DATA_TYPE& udopdop,
	                                 DATA_TYPE& ucoul, DATA_TYPE& uother)
{
	DATA_TYPE pypy = 0.0, t_pypy = 0.0, tr_pypy = 0.0;
	DATA_TYPE pydop = 0.0, t_pydop = 0.0, tr_pydop = 0.0;
	DATA_TYPE dopdop = 0.0, t_dopdop = 0.0;
	DATA_TYPE tempDopDop = 0.0, t1 = 0.0, t2 = 0.0, t3 = 0.0, t4 = 0.0, t5 = 0.0, t6 = 0.0, t7 = 0.0, t8 = 0.0, t9 = 0.0, t10 = 0.0, t11 = 0.0, t12 = 0.0;
	DATA_TYPE mon_coul = 0.0, mon_exc = 0.0, mon_dd = 0.0;
	DATA_TYPE mon_dop_coul = 0.0, mon_dop_cd = 0.0, mon_dop_disp = 0.0;
	DATA_TYPE dop_coul = 0.0, dop_other = 0.0;
	int midx = 0, didx = 0;
	upypy = upydop = udopdop = ucoul = uother = 0.0;
	DATA_TYPE self_interaction_correction = 0.0, t_self_interaction_correction = 0.0, tr_self_interaction_correction = 0.0;

	DATA_TYPE dampingShiftMonMon = 0.0;
	DATA_TYPE dampingShiftMonDop = 0.0;
	DATA_TYPE dampingShiftDopDop = 0.0;

#if defined __USE_FULL_WOLF__
	dampingShiftMonMon = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers);
	dampingShiftMonDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers + Q_dop * Q_dop * ps.nDopants);
	dampingShiftDopDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_dop * Q_dop * ps.nDopants);
#endif
	DATA_TYPE upypy_ = 0.0, upydop_ = 0.0, udopdop_ = 0.0;
	DATA_TYPE inter_pe = 0.0, inter_pe_ = 0.0;
	DATA_TYPE temp_upypy = 0.0, temp_upydop = 0.0, temp_udopdop = 0.0;

#if defined __USE_GPU__
#pragma omp parallel sections
	{
		#pragma omp section
		{
			int i = 0;
			#pragma parallel for private(i)
			for (i = 0; i < nworkunits; i++)
			{
				setDevice(workunits[i].coprocessor_id);
				startDeviceTotalInter(ps, workunits[i], workunits[i].mlb, workunits[i].mub, workunits[i].dlb, workunits[i].dub, true);
			}

            #pragma parallel for reduction(+:inter_pe, _upypy, _t_upypy, _tr_upypy, _upydop, _t_upydop, _rt_upydop) private(i, upypy_, upydop_, udopdop_, inter_pe_)
			for (i = 0; i < nworkunits; i++)
			{
				setDevice(workunits[i].coprocessor_id);
				upypy_ = upydop_ = udopdop_ = inter_pe_ = 0.0;
				inter_pe_ = finishDeviceTotalInter(ps, workunits[i], workunits[i].mlb, workunits[i].mub, workunits[i].dlb, workunits[i].dub, upypy_, upydop_, udopdop_);
				inter_pe += inter_pe_;
				temp_upypy += upypy_;
				temp_upydop += upydop_;
				temp_udopdop += udopdop_;
			}
		}

		#pragma omp section
		{
#endif

			for (midx = 0; midx < ps.nMonomers; midx++)
			{
#if !defined __USE_GPU__
				PyPyPart(ps, midx, mlb, mub, t1, t2, t3, t4, t5, t6, t7, t8, t9,
					ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], ps.dpm_x[midx],
					ps.dpm_y[midx], ps.dpm_z[midx], pypy, t_pypy, tr_pypy, true);
				upypy += pypy;
				mon_coul += t1;
				mon_exc += t2;
				mon_dd += t3;
				ucoul = t1 = t2 = t3 = t4 = t5 = t6 = t7 = t8 = t9 = t10 = t11 = t12 = 0.0;
				PyDopPartMonMove(ps, midx, dlb, dub, t1, t2, t3, t4, t5, t6, t7, t8, t9,
					ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], ps.dpm_x[midx], ps.dpm_y[midx],
					ps.dpm_z[midx], pydop, t_pydop, tr_pydop);
				upydop += pydop;
				mon_dop_coul += t1;
				mon_dop_cd += t2;
				mon_dop_disp += t3;
#endif
				t10 = t11 = t12 = 0.0;
				PyPyPartImageInteraction(ps, midx, mlb, mub, 
                                         ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx],
                                         ps.dpm_x[midx], ps.dpm_y[midx], ps.dpm_z[midx],
					                     t10, t11, t12);
				self_interaction_correction += t10;
				t_self_interaction_correction += t11;
				tr_self_interaction_correction += t12;
			}
			self_interaction_correction *= 0.5;
			t_self_interaction_correction *= 0.5;
			tr_self_interaction_correction *= 0.5;
#if defined __USE_GPU__
		}
	}
	upypy = temp_upypy;
	upydop = temp_upydop;
	udopdop = temp_udopdop;

	return (inter_pe + self_interaction_correction - dampingShiftMonMon - dampingShiftMonDop - dampingShiftDopDop);

#else
			for (didx = 0; didx < ps.nDopants; didx++)
			{
				DopDopPart(ps, didx, dlb, dub, ps.dop_x[didx], ps.dop_y[didx], ps.dop_z[didx], t1, t2, t3, t4, dopdop, t_dopdop);
				udopdop += dopdop;
				dop_coul += t1;
				dop_other += t2;
			}
			udopdop *= 0.5;

			ucoul = mon_coul + mon_dop_coul + dop_coul * 0.5;
			uother = mon_exc + mon_dd + mon_dop_cd + mon_dop_disp + dop_other * 0.5;

			DATA_TYPE retVal = upypy + upydop + udopdop - dampingShiftMonMon - dampingShiftMonDop - dampingShiftDopDop;
			DATA_TYPE correctedRetVal = retVal + self_interaction_correction;
			return correctedRetVal;
#endif
}

void ModelPotential::TotalIntraPart(PARTICLE_SYSTEM& ps, int midx,
	DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
	DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
	DATA_TYPE p_n_x, DATA_TYPE p_n_y, DATA_TYPE p_n_z,
	DATA_TYPE& intra, DATA_TYPE& t_intra, DATA_TYPE & tr_intra)
{
	// calculate the initial energy
	DATA_TYPE ubond = 0.0, t_ubond = 0.0, ubend = 0.0, t_ubend = 0.0, ucoul = 0.0, t_ucoul = 0.0, ucoul2 = 0.0;
	DATA_TYPE utor = 0.0, r_torsion = 0.0, udd = 0.0, t_udd = 0.0, tr_udd = 0.0;
	DATA_TYPE uac = 0.0, t_uac = 0.0;
	DATA_TYPE urot = 0.0, t_urot = 0.0, tr_urot = 0.0, urest = 0.0, t_urest = 0.0;

	{
		UdipoleDipolePart(ps, midx, p_cm_x, p_cm_y, p_cm_z, p_dpm_x, p_dpm_y, p_dpm_z, udd, t_udd, tr_udd);
		UantiCoilingPart(ps, midx, p_cm_x, p_cm_y, p_cm_z, uac, t_uac);
		pairwiseIntraTerms(ps, midx, p_cm_x, p_cm_y, p_cm_z, 
			                         p_dpm_x, p_dpm_y, p_dpm_z, 
			                         p_n_x, p_n_y, p_n_z, 
			                         ubond, t_ubond, ubend, t_ubend, utor, r_torsion, ucoul2, ucoul2,
			                         urot, t_urot, tr_urot);
		UcoulombPart(ps, midx, p_cm_x, p_cm_y, p_cm_z, ucoul, t_ucoul);

	}
	   intra =   ubond +   ubend +   ucoul +   utor +       udd +   uac +    urot + urest;
	 t_intra = t_ubond + t_ubend + t_ucoul +   utor +     t_udd + t_uac +  t_urot + urest;
	tr_intra = t_ubond + t_ubend + t_ucoul + r_torsion + tr_udd + t_uac + tr_urot + urest;
}



void ModelPotential::TotalDopantPart(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT * workunits,
	DATA_TYPE p_dop_x, DATA_TYPE p_dop_y, DATA_TYPE p_dop_z,
	DATA_TYPE& inter, DATA_TYPE& t_inter)
{
	DATA_TYPE upydop = 0.0, t_upydop = 0.0;
	DATA_TYPE udopdop = 0.0, t_udopdop = 0.0;
	DATA_TYPE virialPressure = 0.0;
	DATA_TYPE t1 = 0.0, t2 = 0.0, t3 = 0.0, t4 = 0.0, t5 = 0.0, t6 = 0.0; // dummy placeholder variables

#if defined __USE_GPU__
	int i = 0;
	#pragma omp parallel for private(i)
	for (i = 0; i < nworkunits; i++)
	{		
		setDevice(workunits[i].coprocessor_id);
		startGetDeviceTotalDopantPart(ps, workunits[i].mlb, workunits[i].mub, didx, workunits[i].dlb, workunits[i].dub, workunits[i], ps.dop_x[didx], ps.dop_y[didx], ps.dop_z[didx]);
	}

	DATA_TYPE udopdop_ = 0.0, t_udopdop_ = 0.0, upydop_ = 0.0, t_upydop_ = 0.0;
	i = 0;
	#pragma omp parallel for reduction(+:udopdop, t_udopdop, upydop, t_upydop) private(i, udopdop_, t_udopdop_, upydop_, t_upydop_)
	for (i = 0; i < nworkunits; i++)
	{		
		setDevice(workunits[i].coprocessor_id);
		finishGetDeviceTotalDopantPart(ps, workunits[i], udopdop_, t_udopdop_, upydop_, t_upydop_);
		udopdop   += udopdop_;
		t_udopdop += t_udopdop_;
		upydop    += upydop_;
		t_upydop  += t_upydop_;
	}

#else
	DopDopPart(ps, didx, dlb, dub, p_dop_x, p_dop_y, p_dop_z, t1, t2, t3, t5, udopdop, t_udopdop);
	t1 = t2 = t3 = t4 = t5 = t6 = 0.0;
	PyDopPartDopMove(ps, didx, mlb, mub, p_dop_x, p_dop_y, p_dop_z, t1, t2, t3, t4, t5, t6, upydop, t_upydop);

#endif
	DATA_TYPE dampingShiftMonDop = 0.0;
	DATA_TYPE dampingShiftDopDop = 0.0;
#if defined __USE_FULL_WOLF__
	dampingShiftMonDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers + Q_dop * Q_dop * ps.nDopants);
	dampingShiftDopDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_dop * Q_dop * ps.nDopants);
#endif
	inter = upydop + udopdop - dampingShiftDopDop - dampingShiftMonDop;
	t_inter = t_upydop + t_udopdop - dampingShiftDopDop - dampingShiftMonDop;

}

void ModelPotential::TotalMonomerPart(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, int dlb, int dub, int nworkunits, COPROCESSOR_WORKUNIT * workunits,
	DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
	DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
	DATA_TYPE p_n_x, DATA_TYPE p_n_y, DATA_TYPE p_n_z,
	DATA_TYPE& intra, DATA_TYPE& t_intra, DATA_TYPE& tr_intra,
	DATA_TYPE& inter, DATA_TYPE& t_inter, DATA_TYPE& tr_inter)
{
	// intra variables
	DATA_TYPE ubond = 0.0, t_ubond = 0.0, ubend = 0.0, t_ubend = 0.0, ucoul = 0.0, t_ucoul = 0.0, ucoul2 = 0.0;
	DATA_TYPE utor = 0.0, r_torsion = 0.0, udd = 0.0, t_udd = 0.0, tr_udd = 0.0;
	DATA_TYPE uac = 0.0, t_uac = 0.0;
	DATA_TYPE urot = 0.0, t_urot = 0.0, tr_urot = 0.0, urest = 0.0, t_urest = 0.0;
	// inter variables
	DATA_TYPE t1 = 0.0, t2 = 0.0, t3 = 0.0; // dummy placeholder variables
	DATA_TYPE t4 = 0.0, t5 = 0.0, t6 = 0.0; // dummy placeholder variables
	DATA_TYPE t7 = 0.0, t8 = 0.0, t9 = 0.0; // dummy placeholder variables
	DATA_TYPE upypy = 0.0, t_upypy = 0.0, tr_upypy = 0.0;
	DATA_TYPE upydop = 0.0, t_upydop = 0.0, rt_upydop = 0.0;


	DATA_TYPE self_interaction_correction = 0.0, t_self_interaction_correction = 0.0, tr_self_interaction_correction = 0.0;

#if defined __USE_GPU__
#pragma omp parallel sections
	{
		#pragma omp section
		{
			for (int i = 0; i < nworkunits; i++)
			{
				setDevice(workunits[i].coprocessor_id);
				startGetDeviceTotalMonomerPart(ps, midx, workunits[i].mlb, workunits[i].mub, workunits[i].dlb, workunits[i].dub, workunits[i],
					ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], ps.dpm_x[midx], ps.dpm_y[midx], ps.dpm_z[midx]);
			}

			int i = 0;
			DATA_TYPE upypy_ = 0.0, t_upypy_ = 0.0, tr_upypy_ = 0.0;
			DATA_TYPE upydop_ = 0.0, t_upydop_ = 0.0, rt_upydop_ = 0.0;
			DATA_TYPE _upypy = 0.0, _t_upypy = 0.0, _tr_upypy = 0.0, _upydop = 0.0, _t_upydop = 0.0, _rt_upydop = 0.0;

//#pragma omp parallel for reduction(+:_upypy, _t_upypy, _tr_upypy, _upydop, _t_upydop, _rt_upydop) private(i, upypy_, t_upypy_, tr_upypy_, upydop_, t_upydop_, rt_upydop_)
			for (i = 0; i < nworkunits; i++)
			{
				setDevice(workunits[i].coprocessor_id);
				finishGetDeviceTotalMonomerPart(ps, midx, workunits[i].mlb, workunits[i].mub, workunits[i].dlb, workunits[i].dub, workunits[i],
					upypy_, t_upypy_, tr_upypy_,
					upydop_, t_upydop_, rt_upydop_);

				_upypy += upypy_;
				_t_upypy += t_upypy_;
				_tr_upypy += tr_upypy_;
				_upydop += upydop_;
				_t_upydop += t_upydop_;
				_rt_upydop += rt_upydop_;
			}
			upypy = _upypy;
			t_upypy = _t_upypy;
			tr_upypy = _tr_upypy;
			upydop = _upydop;
			t_upydop = _t_upydop;
			rt_upydop = _rt_upydop;
	}
#else
	PyPyPart(ps, midx, mlb, mub,
		t1, t2, t3, t4, t5, t6,
		t7, t8, t9, p_cm_x, p_cm_y, p_cm_z,
		p_dpm_x, p_dpm_y, p_dpm_z,
		upypy, t_upypy, tr_upypy);
	t1 = t2 = t2 = t3 = t4 = t5 = t6 = t7 = t8 = t9 = 0.0;
	PyDopPartMonMove(ps, midx, dlb, dub,
		t1, t2, t3, t4, t5, t6,
		t7, t8, t9,
		p_cm_x, p_cm_y, p_cm_z,	p_dpm_x, p_dpm_y, p_dpm_z,
		upydop, t_upydop, rt_upydop);
#pragma omp parallel sections
	{
#endif

		#pragma omp section
		{
			UdipoleDipolePart(ps, midx, p_cm_x, p_cm_y, p_cm_z, p_dpm_x, p_dpm_y, p_dpm_z, udd, t_udd, tr_udd);
		}

		#pragma omp section
		{
			UantiCoilingPart(ps, midx, p_cm_x, p_cm_y, p_cm_z, uac, t_uac);
		}

		#pragma omp section
		{
			pairwiseIntraTerms(ps, midx, p_cm_x, p_cm_y, p_cm_z,
				p_dpm_x, p_dpm_y, p_dpm_z,
				p_n_x, p_n_y, p_n_z,
				ubond, t_ubond, ubend, t_ubend, utor, r_torsion, ucoul2, ucoul2,
				urot, t_urot, tr_urot);
		}

		#pragma omp section
		{

			UcoulombPart(ps, midx, p_cm_x, p_cm_y, p_cm_z, ucoul, t_ucoul);
		}


		#pragma omp section
		{
			PyPyPartImageInteraction(ps, midx, mlb, mub,
				p_cm_x, p_cm_y, p_cm_z, p_dpm_x, p_dpm_y, p_dpm_z,
				self_interaction_correction, t_self_interaction_correction, tr_self_interaction_correction);
		}
	}


	DATA_TYPE dampingShiftMonMon = 0.0;
	DATA_TYPE dampingShiftMonDop = 0.0;
#if defined __USE_FULL_WOLF__
	dampingShiftMonMon = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers);
	dampingShiftMonDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers + Q_dop * Q_dop * ps.nDopants);
#endif

	intra = ubond + ubend + ucoul + utor + udd + uac + urot + urest;
	t_intra = t_ubond + t_ubend + t_ucoul + utor + t_udd + t_uac + t_urot + urest;
	tr_intra = t_ubond + t_ubend + t_ucoul + r_torsion + tr_udd + t_uac + tr_urot + urest;

	inter = upypy + upydop + self_interaction_correction - dampingShiftMonMon - dampingShiftMonDop;
	t_inter = t_upypy + t_upydop + t_self_interaction_correction - dampingShiftMonMon - dampingShiftMonDop;
	tr_inter = tr_upypy + rt_upydop + tr_self_interaction_correction - dampingShiftMonMon - dampingShiftMonDop;
}

void ModelPotential::pairwiseIntraTerms(PARTICLE_SYSTEM& ps, int midx,
	DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
	DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
	DATA_TYPE p_n_x, DATA_TYPE p_n_y, DATA_TYPE p_n_z,
	DATA_TYPE& bond, DATA_TYPE & t_bond, 
	DATA_TYPE& bend, DATA_TYPE & t_bend,
	DATA_TYPE& torsion, DATA_TYPE &r_torsion,
	DATA_TYPE& coulomb, DATA_TYPE & t_coulomb,
	DATA_TYPE& rotation, DATA_TYPE & t_rotation, DATA_TYPE & tr_rotation)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE dif = 0.0, e = 0.0;
	DATA_TYPE theta0 = cos(DEG_TO_RAD(theta_0));
	DATA_TYPE cangle = 0.0;
	DATA_TYPE a[3], b[3];
	DATA_TYPE gamma_0_rad = DEG_TO_RAD(gamma_0);
	DATA_TYPE cos_gamma_0 = cos(gamma_0_rad);
	DATA_TYPE cosGamma = 0.0, cosGamma0 = cos_gamma_0, sinGamma = 0.0, sinGamma0 = sin(gamma_0_rad), cosDeltaGamma = 0.0, cos2DeltaGamma = 0.0;
	DATA_TYPE qsrd = Q_mono * Q_mono;
	DATA_TYPE r = 0.0;
	DATA_TYPE rij[3], rji[3], rij_dot = 0.0, rji_dot = 0.0;
	DATA_TYPE cos_theta0 = cos(DEG_TO_RAD(theta_0 / 2.0));
	DATA_TYPE dei1 = 0.0, dei2 = 0.0;
	DATA_TYPE i_x = 0.0, i_y = 0.0, i_z = 0.0;
    DATA_TYPE ip_x = 0.0, ip_y = 0.0, ip_z = 0.0;
    DATA_TYPE im_x = 0.0, im_y = 0.0, im_z = 0.0;
	DATA_TYPE i_dpm_x = 0.0, i_dpm_y = 0.0, i_dpm_z = 0.0;
    DATA_TYPE ip_dpm_x = 0.0, ip_dpm_y = 0.0, ip_dpm_z = 0.0;
    DATA_TYPE im_dpm_x = 0.0, im_dpm_y = 0.0, im_dpm_z = 0.0;
	DATA_TYPE i_n_x = 0.0, i_n_y = 0.0, i_n_z = 0.0;
    DATA_TYPE ip_n_x = 0.0, ip_n_y = 0.0, ip_n_z = 0.0;
    DATA_TYPE im_n_x = 0.0, im_n_y = 0.0, im_n_z = 0.0;
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);


	for (int i = lb; i < ub - 1; i++)
	{
		// before translation and rotation
		{
			i_x = ps.cm_x[i];
			i_y = ps.cm_y[i];
			i_z = ps.cm_z[i];

			i_n_x = ps.n_x[i];
			i_n_y = ps.n_y[i];
			i_n_z = ps.n_z[i];

			i_dpm_x = ps.dpm_x[i];
			i_dpm_y = ps.dpm_y[i];
			i_dpm_z = ps.dpm_z[i];

			ip_x = ps.cm_x[i + 1];
			ip_y = ps.cm_y[i + 1];
			ip_z = ps.cm_z[i + 1];

			ip_n_x = ps.n_x[i + 1];
			ip_n_y = ps.n_y[i + 1];
			ip_n_z = ps.n_z[i + 1];

			ip_dpm_x = ps.dpm_x[i + 1];
			ip_dpm_y = ps.dpm_y[i + 1];
			ip_dpm_z = ps.dpm_z[i + 1];

			im_x = ps.cm_x[i - 1];
			im_y = ps.cm_y[i - 1];
			im_z = ps.cm_z[i - 1];

			im_n_x = ps.n_x[i - 1];
			im_n_y = ps.n_y[i - 1];
			im_n_z = ps.n_z[i - 1];

			im_dpm_x = ps.dpm_x[i - 1];
			im_dpm_y = ps.dpm_y[i - 1];
			im_dpm_z = ps.dpm_z[i - 1];
			
			if (i == midx)
            {
                i_x = p_cm_x;
                i_y = p_cm_y;
                i_z = p_cm_z;

                i_dpm_x = p_dpm_x;
                i_dpm_y = p_dpm_y;
                i_dpm_z = p_dpm_z;

                i_n_x = p_n_x;
                i_n_y = p_n_y;
                i_n_z = p_n_z;
            }

		    if (i + 1 == midx)
            {
                ip_x = p_cm_x;
                ip_y = p_cm_y;
                ip_z = p_cm_z;

                ip_dpm_x = p_dpm_x;
                ip_dpm_y = p_dpm_y;
                ip_dpm_z = p_dpm_z;

                ip_n_x = p_n_x;
                ip_n_y = p_n_y;
                ip_n_z = p_n_z;
            }

            if (i - 1 == midx)
            {
                im_x = p_cm_x;
                im_y = p_cm_y;
                im_z = p_cm_z;

                im_dpm_x = p_dpm_x;
                im_dpm_y = p_dpm_y;
                im_dpm_z = p_dpm_z;

                im_n_x = p_n_x;
                im_n_y = p_n_y;
                im_n_z = p_n_z;
            }

            if (i - 1 < lb && i - 1 == midx)
            {
                im_x = p_cm_x;
                im_y = p_cm_y;
                im_z = p_cm_z;

                im_dpm_x = p_dpm_x;
                im_dpm_y = p_dpm_y;
                im_dpm_z = p_dpm_z;

                im_n_x = p_n_x;
                im_n_y = p_n_y;
                im_n_z = p_n_z;
            }

            a[0] = im_x - i_x;
            a[1] = im_y - i_y;
            a[2] = im_z - i_z;

            b[0] = ip_x - i_x;
            b[1] = ip_y - i_y;
            b[2] = ip_z - i_z;
            dif = MAGNITUDE(b[0], b[1], b[2]);

            // bond term
            e = exp(-alpha_ * (dif / r0 - 1));
            bond += ((1.0 - e) * (1.0 - e)) - 1.0;

            // torsion
            cosGamma = DOT_PRODUCT(i_n_x, i_n_y, i_n_z, ip_n_x, ip_n_y, ip_n_z);
            if (cosGamma > 1.0)
                cosGamma = 1.0;
            if (cosGamma < -1.0)
                cosGamma = -1.0;

            sinGamma = sqrt(1.0 - cosGamma * cosGamma);
            sinGamma0 = sqrt(1.0 - cosGamma0 * cosGamma0);
            cosDeltaGamma = cosGamma * cosGamma0 + sinGamma * sinGamma0;
            cos2DeltaGamma = 2.0 * cosDeltaGamma * cosDeltaGamma - 1.0;
            torsion += (k1 * (1.0 - cosDeltaGamma) + k2 * (1.0 - cos2DeltaGamma));

            // coulomb
            r = ANG_TO_BOHR(dif);
            coulomb += qsrd * ((erfc(intra_kappa * r) / r) - dampingFactor);

            // rotation
            rij[0] = b[0];
            rij[1] = b[1];
            rij[2] = b[2];
            normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);

            rji[0] = -rij[0];
            rji[1] = -rij[1];
            rji[2] = -rij[2];

            rij_dot = DOT_PRODUCT(i_dpm_x, i_dpm_y, i_dpm_z, rij[0], rij[1], rij[2]);
            dei1 = rij_dot - cos_theta0;

            rji_dot = DOT_PRODUCT(ip_dpm_x, ip_dpm_y, ip_dpm_z, rji[0], rji[1], rji[2]);
            dei2 = rji_dot - cos_theta0;

            rotation += ((dei1 * dei1) + (dei2 * dei2));

            // bend term
            normalize(b[0], b[1], b[2], b[0], b[1], b[2]);
            normalize(a[0], a[1], a[2], a[0], a[1], a[2]);
            cangle = cos(angleBetween2Vectors(a[0], a[1], a[2], b[0], b[1], b[2]));

            if (i > lb)
                bend += ((cangle - theta0) * (cangle - theta0));
		}

		// after translation but before rotation
		{
			i_x = ps.cm_x[i];
			i_y = ps.cm_y[i];
			i_z = ps.cm_z[i];

			i_n_x = ps.n_x[i];
			i_n_y = ps.n_y[i];
			i_n_z = ps.n_z[i];

			i_dpm_x = ps.dpm_x[i];
			i_dpm_y = ps.dpm_y[i];
			i_dpm_z = ps.dpm_z[i];

			ip_x = ps.cm_x[i + 1];
			ip_y = ps.cm_y[i + 1];
			ip_z = ps.cm_z[i + 1];

			ip_n_x = ps.n_x[i + 1];
			ip_n_y = ps.n_y[i + 1];
			ip_n_z = ps.n_z[i + 1];

			ip_dpm_x = ps.dpm_x[i + 1];
			ip_dpm_y = ps.dpm_y[i + 1];
			ip_dpm_z = ps.dpm_z[i + 1];

			im_x = ps.cm_x[i - 1];
			im_y = ps.cm_y[i - 1];
			im_z = ps.cm_z[i - 1];

			im_n_x = ps.n_x[i - 1];
			im_n_y = ps.n_y[i - 1];
			im_n_z = ps.n_z[i - 1];

			im_dpm_x = ps.dpm_x[i - 1];
			im_dpm_y = ps.dpm_y[i - 1];
			im_dpm_z = ps.dpm_z[i - 1];
		
			if (i == midx)
			{
				i_dpm_x = p_dpm_x;
				i_dpm_y = p_dpm_y;
				i_dpm_z = p_dpm_z;

				i_n_x = p_n_x;
				i_n_y = p_n_y;
				i_n_z = p_n_z;
			}

            if (i + 1 == midx)
            {
                ip_dpm_x = p_dpm_x;
                ip_dpm_y = p_dpm_y;
                ip_dpm_z = p_dpm_z;

                ip_n_x = p_n_x;
                ip_n_y = p_n_y;
                ip_n_z = p_n_z;
            }

            if (i - 1 == midx)
            {
                im_dpm_x = p_dpm_x;
                im_dpm_y = p_dpm_y;
                im_dpm_z = p_dpm_z;

                im_n_x = p_n_x;
                im_n_y = p_n_y;
                im_n_z = p_n_z;
            }

            if (i - 1 < lb && i - 1 == midx)
            {
                im_dpm_x = p_dpm_x;
                im_dpm_y = p_dpm_y;
                im_dpm_z = p_dpm_z;

                im_n_x = p_n_x;
                im_n_y = p_n_y;
                im_n_z = p_n_z;
            }

            a[0] = im_x - i_x;
            a[1] = im_y - i_y;
            a[2] = im_z - i_z;

            b[0] = ip_x - i_x;
            b[1] = ip_y - i_y;
            b[2] = ip_z - i_z;
            dif = MAGNITUDE(b[0], b[1], b[2]);

            // bond term
            e = exp(-alpha_ * (dif / r0 - 1));
            t_bond += ((1.0 - e) * (1.0 - e)) - 1.0;

            // coulomb
            r = ANG_TO_BOHR(dif);
            t_coulomb += qsrd * ((erfc(intra_kappa * r) / r) - dampingFactor);

            // rotation
            rij[0] = b[0];
            rij[1] = b[1];
            rij[2] = b[2];
            normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);

            rji[0] = -rij[0];
            rji[1] = -rij[1];
            rji[2] = -rij[2];

            rij_dot = DOT_PRODUCT(i_dpm_x, i_dpm_y, i_dpm_z, rij[0], rij[1], rij[2]);
            dei1 = rij_dot - cos_theta0;

            rji_dot = DOT_PRODUCT(ip_dpm_x, ip_dpm_y, ip_dpm_z, rji[0], rji[1], rji[2]);
            dei2 = rji_dot - cos_theta0;

            t_rotation += ((dei1 * dei1) + (dei2 * dei2));

            // bend term
            normalize(b[0], b[1], b[2], b[0], b[1], b[2]);
            normalize(a[0], a[1], a[2], a[0], a[1], a[2]);
            cangle = cos(angleBetween2Vectors(a[0], a[1], a[2], b[0], b[1], b[2]));

            if (i > lb)
                t_bend += ((cangle - theta0) * (cangle - theta0));
		}

		// after translation and rotations
		{
			i_n_x = ps.n_x[i];
			i_n_y = ps.n_y[i];
			i_n_z = ps.n_z[i];

			i_dpm_x = ps.dpm_x[i];
			i_dpm_y = ps.dpm_y[i];
			i_dpm_z = ps.dpm_z[i];

			ip_n_x = ps.n_x[i + 1];
			ip_n_y = ps.n_y[i + 1];
			ip_n_z = ps.n_z[i + 1];

			ip_dpm_x = ps.dpm_x[i + 1];
			ip_dpm_y = ps.dpm_y[i + 1];
			ip_dpm_z = ps.dpm_z[i + 1];

			// torsion
			cosGamma = DOT_PRODUCT(i_n_x, i_n_y, i_n_z, ip_n_x, ip_n_y, ip_n_z);
			if (cosGamma > 1.0)
				cosGamma = 1.0;
			if (cosGamma < -1.0)
				cosGamma = -1.0;

			sinGamma = sqrt(1.0 - cosGamma * cosGamma);
			sinGamma0 = sqrt(1.0 - cosGamma0 * cosGamma0);
			cosDeltaGamma = cosGamma * cosGamma0 + sinGamma * sinGamma0;
			cos2DeltaGamma = 2.0 * cosDeltaGamma * cosDeltaGamma - 1.0;
            r_torsion += (k1 * (1.0 - cosDeltaGamma) + k2 * (1.0 - cos2DeltaGamma));

			// rotation
			rij[0] = b[0];
			rij[1] = b[1];
			rij[2] = b[2];
			normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);

			rji[0] = -rij[0];
			rji[1] = -rij[1];
			rji[2] = -rij[2];

			rij_dot = DOT_PRODUCT(i_dpm_x, i_dpm_y, i_dpm_z, rij[0], rij[1], rij[2]);
			dei1 = rij_dot - cos_theta0;

			rji_dot = DOT_PRODUCT(ip_dpm_x, ip_dpm_y, ip_dpm_z, rji[0], rji[1], rji[2]);
			dei2 = rji_dot - cos_theta0;

            tr_rotation += ((dei1 * dei1) + (dei2 * dei2));
		}
	}
	bond = EV_TO_HA((bond * De));
	bend = EV_TO_HA((bend * k_theta));
	torsion = EV_TO_HA(torsion);
    rotation = EV_TO_HA((rotation * k4));

    t_bond = EV_TO_HA((t_bond * De));
    t_bend = EV_TO_HA((t_bend * k_theta));
    t_rotation = EV_TO_HA((t_rotation * k4));

    r_torsion = EV_TO_HA(r_torsion);
    tr_rotation = EV_TO_HA((tr_rotation * k4));

}

DATA_TYPE ModelPotential::UbondPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE ubond = 0.0;
	DATA_TYPE dif = 0.0;
	DATA_TYPE e = 0.0;

	for (int i = lb; i < ub - 1; i++)
	{
		dif = dist(ps.cm_x[i], ps.cm_y[i], ps.cm_z[i], ps.cm_x[i + 1], ps.cm_y[i + 1], ps.cm_z[i + 1]);
		e = exp(-alpha_ * (dif / r0 - 1));
		ubond += ((1.0 - e) * (1.0 - e)) - 1.0;
	}
	ubond *= De;

	return EV_TO_HA(ubond);
}

DATA_TYPE ModelPotential::UbendPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE ubend = 0.0;
	DATA_TYPE theta0 = cos(DEG_TO_RAD(theta_0));
	DATA_TYPE cangle = 0.0;
	DATA_TYPE a[3];
	DATA_TYPE b[3];

	for (int i = lb + 1; i < ub - 1; i++)
	{
		a[0] = ps.cm_x[i - 1] - ps.cm_x[i];
		a[1] = ps.cm_y[i - 1] - ps.cm_y[i];
		a[2] = ps.cm_z[i - 1] - ps.cm_z[i];
		normalize(a[0], a[1], a[2], a[0], a[1], a[2]);

		b[0] = ps.cm_x[i + 1] - ps.cm_x[i];
		b[1] = ps.cm_y[i + 1] - ps.cm_y[i];
		b[2] = ps.cm_z[i + 1] - ps.cm_z[i];
		normalize(b[0], b[1], b[2], b[0], b[1], b[2]);

		cangle = cos(angleBetween2Vectors(a[0], a[1], a[2], b[0], b[1], b[2]));

		ubend += ((cangle - theta0) * (cangle - theta0));
	}

	ubend *= k_theta;

	return EV_TO_HA(ubend);
}

DATA_TYPE ModelPotential::UtorsionPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE utor = 0.0;
	DATA_TYPE gamma_0_rad = DEG_TO_RAD(gamma_0);
	DATA_TYPE cos_gamma_0 = cos(gamma_0_rad);

	DATA_TYPE cosGamma = 0.0, cosGamma0 = cos_gamma_0, sinGamma = 0.0, sinGamma0 = sin(gamma_0_rad), cosDeltaGamma = 0.0, cos2DeltaGamma = 0.0;

	for (int i = lb; i < ub - 1; i++)
	{
		cosGamma = DOT_PRODUCT(ps.n_x[i], ps.n_y[i], ps.n_z[i], ps.n_x[i + 1], ps.n_y[i + 1], ps.n_z[i + 1]);
		if (cosGamma > 1.0)
			cosGamma = 1.0;
		if (cosGamma < -1.0)
			cosGamma = -1.0;

		sinGamma = sqrt(1.0 - cosGamma * cosGamma);
		sinGamma0 = sqrt(1.0 - cosGamma0 * cosGamma0);
		cosDeltaGamma = cosGamma * cosGamma0 + sinGamma * sinGamma0;
		cos2DeltaGamma = 2.0 * cosDeltaGamma * cosDeltaGamma - 1.0;
		utor += (k1 * (1.0 - cosDeltaGamma) + k2 * (1.0 - cos2DeltaGamma));

		//gamma = angleBetween2Vectors(ps.n_x[i], ps.n_y[i], ps.n_z[i], ps.n_x[i + 1], ps.n_y[i + 1], ps.n_z[i + 1]);
		//utor += (K_1 * (1.0 - cos(gamma - gamma_0_rad)) + K_2 * (1.0 - cos(2.0 * (gamma - gamma_0_rad))));
	}

	return EV_TO_HA(utor);
}

void ModelPotential::UcoulombPart(PARTICLE_SYSTEM& ps, int midx,
	DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
	DATA_TYPE& ucoulomb, DATA_TYPE& t_ucoulomb)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;
	int offset = intra_coul_offset;
	DATA_TYPE i_x = 0.0, i_y = 0.0, i_z = 0.0, j_x = 0.0, j_y = 0.0, j_z = 0.0;

	DATA_TYPE qsrd = Q_mono * Q_mono;
	DATA_TYPE r = 0.0;
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(intra_kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);

	int i = 0;

	for (i = lb; i < ub - offset; i++)
	{
		for (int j = i + offset; j < ub; j++)
		{
			// before translate
			{
				i_x = ps.cm_x[i];
				i_y = ps.cm_y[i];
				i_z = ps.cm_z[i];

				if (i == midx)
				{
					i_x = p_cm_x;
					i_y = p_cm_y;
					i_z = p_cm_z;
				}

				j_x = ps.cm_x[j];
				j_y = ps.cm_y[j];
				j_z = ps.cm_z[j];

				if (j == midx)
				{
					j_x = p_cm_x;
					j_y = p_cm_y;
					j_z = p_cm_z;
				}
				
				r = ANG_TO_BOHR(dist(i_x, i_y, i_z, j_x, j_y, j_z));
				ucoulomb += qsrd * ((erfc(intra_kappa * r) / r) - dampingFactor);
			}

			// after translate
			{
				i_x = ps.cm_x[i];
				i_y = ps.cm_y[i];
				i_z = ps.cm_z[i];

				j_x = ps.cm_x[j];
				j_y = ps.cm_y[j];
				j_z = ps.cm_z[j];

				r = ANG_TO_BOHR(dist(i_x, i_y, i_z, j_x, j_y, j_z));
				t_ucoulomb += qsrd * ((erfc(intra_kappa * r) / r) - dampingFactor);
			}
		}
	}
}

void ModelPotential::UdipoleDipolePart(PARTICLE_SYSTEM& ps, int midx,
	                                        DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
	                                        DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
 	                                        DATA_TYPE& udd, DATA_TYPE& t_udd, DATA_TYPE& tr_udd)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;
	int i = 0, j = 0;

	DATA_TYPE dpm_dot = 0.0, mag = 0.0;
	DATA_TYPE rij[3];
	DATA_TYPE mu_i = 0.0, mu_j = 0.0;
	DATA_TYPE p_mu_i = 0.0, p_mu_j = 0.0;
	DATA_TYPE mu_sqrd = DEBEYE_TO_HA(dp_mag) * DEBEYE_TO_HA(dp_mag);
	DATA_TYPE i_x = 0.0, i_y = 0.0, i_z = 0.0, j_x = 0.0, j_y = 0.0, j_z = 0.0;
	DATA_TYPE i_dpm_x = 0.0, i_dpm_y = 0.0, i_dpm_z = 0.0, j_dpm_x = 0.0, j_dpm_y = 0.0, j_dpm_z = 0.0;

	for (i = lb; i < ub - 1; i++)
	{
		for (j = i + 1; j < ub; j++)
		{
			// before translation and rotation
			{
				i_x = ps.cm_x[i];
				i_y = ps.cm_y[i];
				i_z = ps.cm_z[i];

				i_dpm_x = ps.dpm_x[i];
				i_dpm_y = ps.dpm_y[i];
				i_dpm_z = ps.dpm_z[i];

				if (i == midx)
				{
					i_x = p_cm_x;
					i_y = p_cm_y;
					i_z = p_cm_z;

					i_dpm_x = p_dpm_x;
					i_dpm_y = p_dpm_y;
					i_dpm_z = p_dpm_z;
				}

				j_x = ps.cm_x[j];
				j_y = ps.cm_y[j];
				j_z = ps.cm_z[j];


				j_dpm_x = ps.dpm_x[j];
				j_dpm_y = ps.dpm_y[j];
				j_dpm_z = ps.dpm_z[j];

				if (j == midx)
				{
					j_x = p_cm_x;
					j_y = p_cm_y;
					j_z = p_cm_z;

					j_dpm_x = p_dpm_x;
					j_dpm_y = p_dpm_y;
					j_dpm_z = p_dpm_z;
				}

				rij[0] = ANG_TO_BOHR((j_x - i_x));
				rij[1] = ANG_TO_BOHR((j_y - i_y));
				rij[2] = ANG_TO_BOHR((j_z - i_z));

				mag = MAGNITUDE(rij[0], rij[1], rij[2]);
				normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);
				dpm_dot = dotProduct(i_dpm_x, i_dpm_y, i_dpm_z, j_dpm_x, j_dpm_y, j_dpm_z);
				mu_i = dotProduct(i_dpm_x, i_dpm_y, i_dpm_z, rij[0], rij[1], rij[2]);
				mu_j = dotProduct(j_dpm_x, j_dpm_y, j_dpm_z, rij[0], rij[1], rij[2]);

				udd += (mu_sqrd * ((dpm_dot - (3.0 * mu_i * mu_j) / (mag * mag)) / (mag * mag * mag)));
			}

			
			{
				i_x = ps.cm_x[i];
				i_y = ps.cm_y[i];
				i_z = ps.cm_z[i];

				i_dpm_x = ps.dpm_x[i];
				i_dpm_y = ps.dpm_y[i];
				i_dpm_z = ps.dpm_z[i];

				if (i == midx)
				{
					i_dpm_x = p_dpm_x;
					i_dpm_y = p_dpm_y;
					i_dpm_z = p_dpm_z;
				}

				j_x = ps.cm_x[j];
				j_y = ps.cm_y[j];
				j_z = ps.cm_z[j];

				j_dpm_x = ps.dpm_x[j];
				j_dpm_y = ps.dpm_y[j];
				j_dpm_z = ps.dpm_z[j];

				if (j == midx)
				{
					j_dpm_x = p_dpm_x;
					j_dpm_y = p_dpm_y;
					j_dpm_z = p_dpm_z;
				}

				rij[0] = ANG_TO_BOHR((j_x - i_x));
				rij[1] = ANG_TO_BOHR((j_y - i_y));
				rij[2] = ANG_TO_BOHR((j_z - i_z));

				mag = MAGNITUDE(rij[0], rij[1], rij[2]);
				normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);

				// after translation but before rotation
				dpm_dot = dotProduct(i_dpm_x, i_dpm_y, i_dpm_z, j_dpm_x, j_dpm_y, j_dpm_z);
				mu_i = dotProduct(i_dpm_x, i_dpm_y, i_dpm_z, rij[0], rij[1], rij[2]);
				mu_j = dotProduct(j_dpm_x, j_dpm_y, j_dpm_z, rij[0], rij[1], rij[2]);

				t_udd += (mu_sqrd * ((dpm_dot - (3.0 * mu_i * mu_j) / (mag * mag)) / (mag * mag * mag)));

				// after translation and rotation
				i_dpm_x = ps.dpm_x[i];
				i_dpm_y = ps.dpm_y[i];
				i_dpm_z = ps.dpm_z[i];

				j_dpm_x = ps.dpm_x[j];
				j_dpm_y = ps.dpm_y[j];
				j_dpm_z = ps.dpm_z[j];

				dpm_dot = dotProduct(i_dpm_x, i_dpm_y, i_dpm_z, j_dpm_x, j_dpm_y, j_dpm_z);
				mu_i = dotProduct(i_dpm_x, i_dpm_y, i_dpm_z, rij[0], rij[1], rij[2]);
				mu_j = dotProduct(j_dpm_x, j_dpm_y, j_dpm_z, rij[0], rij[1], rij[2]);

				tr_udd += (mu_sqrd * ((dpm_dot - (3.0 * mu_i * mu_j) / (mag * mag)) / (mag * mag * mag)));

			}
		}
	}
}

void ModelPotential::UantiCoilingPart(PARTICLE_SYSTEM& ps, int midx,
	                      DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z, 
	                      DATA_TYPE & uac, DATA_TYPE & t_uac)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;
	int i = 0, j = 0;

	DATA_TYPE r = 0.0, r_6 = 0.0, r_9 = 0.0;
	DATA_TYPE sigma_6 = sigma_intra * sigma_intra * sigma_intra * sigma_intra * sigma_intra * sigma_intra;
	DATA_TYPE sigma_9 = sigma_6 * sigma_intra * sigma_intra * sigma_intra;
	DATA_TYPE i_x = 0.0, i_y = 0.0, i_z = 0.0, j_x = 0.0, j_y = 0.0, j_z = 0.0;

	for (i = lb; i < ub - 2; i++)
	{
		for (j = i + 2; j < ub; j++)
		{
			// before translate
			{
				i_x = ps.cm_x[i];
				i_y = ps.cm_y[i];
				i_z = ps.cm_z[i];

				if (i == midx)
				{
					i_x = p_cm_x;
					i_y = p_cm_y;
					i_z = p_cm_z;
				}
				
				j_x = ps.cm_x[j];
				j_y = ps.cm_y[j];
				j_z = ps.cm_z[j];
				
				if (j == midx)
				{
					j_x = p_cm_x;
					j_y = p_cm_y;
					j_z = p_cm_z;
				}
				
				r = dist(i_x, i_y, i_z, j_x, j_y, j_z);

				r_6 = r * r * r * r * r * r;
				r_9 = r_6 * r * r * r;

				uac += (sigma_9 / r_9 - 1.5 * sigma_6 / r_6);
			}

			// after translate
			{
				i_x = ps.cm_x[i];
				i_y = ps.cm_y[i];
				i_z = ps.cm_z[i];

				j_x = ps.cm_x[j];
				j_y = ps.cm_y[j];
				j_z = ps.cm_z[j];

				r = dist(i_x, i_y, i_z, j_x, j_y, j_z);
				r_6 = r * r * r * r * r * r;
				r_9 = r_6 * r * r * r;

				t_uac += (sigma_9 / r_9 - 1.5 * sigma_6 / r_6);
			}
		}
	}

	uac = 2 * epsilon_intra * uac;
	uac = EV_TO_HA(uac);

	t_uac = 2 * epsilon_intra * t_uac;
	t_uac = EV_TO_HA(t_uac);

}

DATA_TYPE ModelPotential::URotationPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE urot = 0.0;
	DATA_TYPE rij[3], rji[3], rij_dot = 0.0, rji_dot = 0.0;
	DATA_TYPE cos_theta0 = cos(DEG_TO_RAD(theta_0 / 2.0));
	DATA_TYPE dei1 = 0.0, dei2 = 0.0;

	for (int i = lb; i < ub - 1; i++)
	{
		rij[0] = ps.cm_x[i + 1] - ps.cm_x[i];
		rij[1] = ps.cm_y[i + 1] - ps.cm_y[i];
		rij[2] = ps.cm_z[i + 1] - ps.cm_z[i];
		normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);

		rji[0] = -rij[0];
		rji[1] = -rij[1];
		rji[2] = -rij[2];

		rij_dot = DOT_PRODUCT(ps.dpm_x[i], ps.dpm_y[i], ps.dpm_z[i], rij[0], rij[1], rij[2]);
		dei1 = rij_dot - cos_theta0;

		rji_dot = DOT_PRODUCT(ps.dpm_x[i + 1], ps.dpm_y[i + 1], ps.dpm_z[i + 1], rji[0], rji[1], rji[2]);
		dei2 = rji_dot - cos_theta0;

		urot += ((dei1 * dei1) + (dei2 * dei2));
	}
	DATA_TYPE temp = urot * k4;
	return EV_TO_HA(temp);
}

void ModelPotential::URestorationPart(PARTICLE_SYSTEM& ps, int midx,
	DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
	DATA_TYPE& urest, DATA_TYPE& t_urest)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE z = 0.0, temp = 0.0;
	int polyIdx = midx / monomers_per_chain;
	DATA_TYPE z0 = ps.z0[polyIdx];

	z0 = ANG_TO_BOHR(z0);
	
	DATA_TYPE avg_z = 0.0;

	// calculate the median Z value of the chain
	for (int i = lb; i < ub; i++)
	{
		avg_z += ps.cm_z[i];
	}

	avg_z /= monomers_per_chain;

	z0 = ANG_TO_BOHR(avg_z);

	// do not lef the monomer move too far from the average Z plane
	for (int i = lb; i < ub; i++)
	{
		z = ANG_TO_BOHR(ps.cm_z[i]);
		temp = z - z0;

		if (i == lb || i == ub - 1)
			urest += (k5_end * (temp * temp));
		else
			urest += (k5 * temp * temp);
	}
}

void ModelPotential::PyPyPart(PARTICLE_SYSTEM& ps, int midx, int mlb, int mub,
                                   DATA_TYPE& ucoul, DATA_TYPE& uexc, DATA_TYPE& udd,
                                   DATA_TYPE& t_ucoul, DATA_TYPE& t_uexc, DATA_TYPE& t_udd,
                                   DATA_TYPE& tr_ucoul, DATA_TYPE& tr_uexc, DATA_TYPE& tr_udd,
								   DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
	                               DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
                                   DATA_TYPE& upypy, DATA_TYPE& t_upypy, DATA_TYPE& tr_upypy,
                                   bool removeDouble)
{
	int test_chain_id = 0;
	int input_chain_id = midx / monomers_per_chain; 
	int exc_lb = input_chain_id * monomers_per_chain;
	int exc_ub = exc_lb + monomers_per_chain - 1;
	int t_lb = 0;
	int t_ub = 0;
	bool isInputHeadTail = false, isTestHeadTail = false;
	if (midx == exc_lb || midx == exc_ub)
		isInputHeadTail = true; 

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE qsqrd = Q_mono * Q_mono;
	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE sigma_6_mid = TO_POW_6(sigma_inter);
	DATA_TYPE sigma_9_mid = sigma_6_mid * sigma_inter * sigma_inter * sigma_inter;
	DATA_TYPE sigma_6_end = TO_POW_6(sigma_inter_end * 1.0);
	DATA_TYPE sigma_9_end = sigma_6_end * ((sigma_inter_end * sigma_inter_end * sigma_inter_end) * 1.0);
	DATA_TYPE sigma_6 = 0.0, sigma_9 = 0.0;
	DATA_TYPE epsilon = epsilon_inter;
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(dp_mag) * DEBEYE_TO_HA(dp_mag);
	int i = 0;
	DATA_TYPE rij3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);

	DATA_TYPE* _cm_x = ps.cm_x;
	DATA_TYPE* _cm_y = ps.cm_y;
	DATA_TYPE* _cm_z = ps.cm_z;

	DATA_TYPE* _dpm_x = ps.dpm_x;
	DATA_TYPE* _dpm_y = ps.dpm_y;
	DATA_TYPE* _dpm_z = ps.dpm_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;
	int _nMonomers = ps.nMonomers;
	DATA_TYPE dd = 0.0,t_dd = 0.0, tr_dd = 0.0;
	ox = _cm_x[midx];
	oy = _cm_y[midx];
	oz = _cm_z[midx];

	dpmx = _dpm_x[midx];
	dpmy = _dpm_y[midx];
	dpmz = _dpm_z[midx];

    DATA_TYPE p_ox = p_cm_x;
    DATA_TYPE p_oy = p_cm_y;
    DATA_TYPE p_oz = p_cm_z;

    DATA_TYPE p_dpmx = p_dpm_x;
    DATA_TYPE p_dpmy = p_dpm_y;
    DATA_TYPE p_dpmz = p_dpm_z;
	DATA_TYPE test1 = 0.0, test2 = 0.0;
	int pidx = 0;
	int pidx_speed = 0;

#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#pragma omp parallel for reduction(+:dd, t_dd, tr_dd, test1, test2) private(i, xdif, ydif, zdif, rij, dij, dir, djr, rij3, pidx, test_chain_id, x1, x2, x3, x4, x5, x6, x7, xsqrd, pidx_speed)
#elif defined __USE_INLINE_HERMITE_SPLINE__
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#pragma omp parallel for reduction(+:test1, test2) private(i, xdif, ydif, zdif, rij, dij, dir, djr, rij3, pidx, energy, pidx_speed, m0, m1, mu2, mu3, a0, a1, a2, a3)
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(i, xdif, ydif, zdif, rij, dij, dir, djr, rij3, pidx, energy, pidx_speed, spline_coeff, x_dist, xofff, spline_temp)
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = interpolationOrder;;
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#pragma omp parallel for reduction(+:dd, t_dd, tr_dd, test1, test2) private(i, xdif, ydif, zdif, rij, dij, dir, djr, rij3, pidx, test_chain_id, x2, tmp, nd, c0, c1, c2, c3, pidx_speed)
#endif
#else
#pragma omp parallel for reduction(+:dd, t_dd, tr_dd, test1, test2) private(i, xdif, ydif, zdif, rij, dij, dir, djr, rij3, pidx, test_chain_id)
#endif
	for (i = mlb; i < mub; i++)
	{
		if (i == midx)
			continue;

		if (removeDouble && (i < midx)) // do not double count end monomer interactions
			continue;

		test_chain_id = i / monomers_per_chain;

		if (input_chain_id == test_chain_id)
			continue;

        // before translation and rotation
        {
            xdif = PCB_(_x_size, (_cm_x[i] - p_ox));
			ydif = PCB_(_y_size, (_cm_y[i] - p_oy));
			zdif = PCB_(_z_size, (_cm_z[i] - p_oz));

            xdif = ANG_TO_BOHR(xdif);
            ydif = ANG_TO_BOHR(ydif);
            zdif = ANG_TO_BOHR(zdif);

            rij = MAGNITUDE(xdif, ydif, zdif);
            if (rij <= ANG_TO_BOHR(_r_cut))
            {
                rij3 = TO_POW_3(rij);

                dij = DOT_PRODUCT(p_dpmx, p_dpmy, p_dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
                dir = DOT_PRODUCT(p_dpmx, p_dpmy, p_dpmz, xdif / rij, ydif / rij, zdif / rij);
                djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

                dd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));

				pidx = int((rij / interpolation_delta)) - interpolation_offset;
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = interpolation_coeff_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test1 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = interpolation_coeff_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				test1 += polyVal(rij, interpolationParameters, interpolation_coeff_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
			}
        }

        // after translation but before rotation
        {
            xdif = PCB_(_x_size, (_cm_x[i] - ox));
			ydif = PCB_(_y_size, (_cm_y[i] - oy));
			zdif = PCB_(_z_size, (_cm_z[i] - oz));

            xdif = ANG_TO_BOHR(xdif);
            ydif = ANG_TO_BOHR(ydif);
            zdif = ANG_TO_BOHR(zdif);

            rij = MAGNITUDE(xdif, ydif, zdif);
            if (rij <= ANG_TO_BOHR(_r_cut))
            {
                rij3 = TO_POW_3(rij);

                dij = DOT_PRODUCT(p_dpmx, p_dpmy, p_dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
                dir = DOT_PRODUCT(p_dpmx, p_dpmy, p_dpmz, xdif / rij, ydif / rij, zdif / rij);
                djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

                t_dd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));

				pidx = int((rij / interpolation_delta)) - interpolation_offset;
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = interpolation_coeff_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test2 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = interpolation_coeff_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				test2 += polyVal(rij, interpolationParameters, interpolation_coeff_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
			}
        }

        // after translation and rotation
        {
            xdif = PCB_(_x_size, (_cm_x[i] - ox));
			ydif = PCB_(_y_size, (_cm_y[i] - oy));
			zdif = PCB_(_z_size, (_cm_z[i] - oz));

            xdif = ANG_TO_BOHR(xdif);
            ydif = ANG_TO_BOHR(ydif);
            zdif = ANG_TO_BOHR(zdif);

            rij = MAGNITUDE(xdif, ydif, zdif);
            if (rij <= ANG_TO_BOHR(_r_cut))
            {
                rij3 = TO_POW_3(rij);

                dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
                dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
                djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

                tr_dd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));

			}
        }
	}

	udd = dd;
	t_udd = t_dd;
	tr_udd = tr_dd;

	upypy    = test1 + udd;
	t_upypy  = test2 + t_udd;
	tr_upypy = test2 + tr_udd;
	
}

void ModelPotential::PyDopPartMonMove(PARTICLE_SYSTEM& ps, int midx, int dlb, int dub,
                                        DATA_TYPE& ucoul, DATA_TYPE& ucd, DATA_TYPE& udisp,
                                        DATA_TYPE& t_ucoul, DATA_TYPE& t_ucd, DATA_TYPE& t_udisp,
                                        DATA_TYPE& tr_ucoul, DATA_TYPE& tr_ucd, DATA_TYPE& tr_udisp,
										DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
										DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
										DATA_TYPE & upydop, DATA_TYPE & t_upydop, DATA_TYPE & tr_upydop)
{
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(Q_dop) * DEBEYE_TO_HA(dp_mag) * C;
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(sigmaPyDopant);
	DATA_TYPE sigma_9 = sigma_6 * sigmaPyDopant * sigmaPyDopant * sigmaPyDopant;
	DATA_TYPE rdist = 0.0;
	DATA_TYPE Q = Q_dop * Q_mono;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);

	DATA_TYPE* _dop_x = ps.dop_x;
	DATA_TYPE* _dop_y = ps.dop_y;
	DATA_TYPE* _dop_z = ps.dop_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;

	int d_lb = dlb, d_ub = dub, dIdx = 0;

	cmx = ps.cm_x[midx];
	cmy = ps.cm_y[midx];
	cmz = ps.cm_z[midx];

	dpmx = ps.dpm_x[midx];
	dpmy = ps.dpm_y[midx];
	dpmz = ps.dpm_z[midx];

    DATA_TYPE p_cmx = p_cm_x;
    DATA_TYPE p_cmy = p_cm_y;
    DATA_TYPE p_cmz = p_cm_z;

    DATA_TYPE p_dpmx = p_dpm_x;
    DATA_TYPE p_dpmy = p_dpm_y;
    DATA_TYPE p_dpmz = p_dpm_z;
	int pidx = 0;
	DATA_TYPE test1 = 0.0, test2 = 0.0;

#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = interpolation_coeff_offset + (interpolationOrder * 2 + 2);
#else
	int coefficient_offset = 12;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(dIdx, r_vec, ris, ris3, pidx, x1, x2, x3, x4, x5, x6, x7, xsqrd, pidx_speed)
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 3;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#pragma omp parallel for reduction(+:test1, test2) private(dIdx, r_vec, ris, ris3, energy, pidx_speed, m0, m1, mu2, mu3, a0, a1, a2, a3)
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(dIdx, r_vec, ris, ris3, energy, pidx_speed, spline_coeff, x_dist, xofff, spline_temp)
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = interpolationOrder;;
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(dIdx, r_vec, ris, ris3, pidx, x2, tmp, nd, c0, c1, c2, c3, pidx_speed)
#endif
#else
	DATA_TYPE energy[4];
#pragma omp parallel for reduction(+:test1, test2) private(dIdx, r_vec, ris, ris3, pidx)
#endif
	for (dIdx = d_lb; dIdx < d_ub; dIdx++)
	{
	    // before translation and rotation
        {
            r_vec[0] = PCB_(_x_size, (p_cmx - _dop_x[dIdx]));
			r_vec[1] = PCB_(_y_size, (p_cmy - _dop_y[dIdx]));
			r_vec[2] = PCB_(_z_size, (p_cmz - _dop_z[dIdx]));

            r_vec[0] = ANG_TO_BOHR(r_vec[0]);
            r_vec[1] = ANG_TO_BOHR(r_vec[1]);
            r_vec[2] = ANG_TO_BOHR(r_vec[2]);

            ris = sqrt(r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2]);
            if (ris < ANG_TO_BOHR(_r_cut))
            {
				pidx = int((ris / interpolation_delta)) - interpolation_offset;
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = ris * ris;

				test1 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((ris / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				test1 += polyVal(ris, interpolationParameters, coefficient_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
			}
        }

        // after translation but before rotation
        {
            r_vec[0] = PCB_(_x_size, (cmx - _dop_x[dIdx]));
			r_vec[1] = PCB_(_y_size, (cmy - _dop_y[dIdx]));
			r_vec[2] = PCB_(_z_size, (cmz - _dop_z[dIdx]));

            r_vec[0] = ANG_TO_BOHR(r_vec[0]);
            r_vec[1] = ANG_TO_BOHR(r_vec[1]);
            r_vec[2] = ANG_TO_BOHR(r_vec[2]);

            ris = sqrt(r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2]);
            if (ris < ANG_TO_BOHR(_r_cut))
            {
				pidx = int((ris / interpolation_delta)) - interpolation_offset;
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = ris * ris;

				test2 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((ris / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				test2 += polyVal(ris, interpolationParameters, coefficient_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
			}
        }
	}

	   upydop = test1;
	 t_upydop = test2;
	tr_upydop = test2;
}

void ModelPotential::PyDopPartDopMove(PARTICLE_SYSTEM& ps, int didx, int mlb, int mub,
	                                  DATA_TYPE p_dop_x, DATA_TYPE p_dop_y, DATA_TYPE p_dop_z,
	                                  DATA_TYPE& ucoul, DATA_TYPE& ucd, DATA_TYPE & udisp,
	                                  DATA_TYPE& t_ucoul, DATA_TYPE& t_ucd, DATA_TYPE& t_udisp,
	                                  DATA_TYPE& upydop, DATA_TYPE& t_upydop)
{
	DATA_TYPE coul = 0.0, cd = 0.0, disp = 0.0;
	DATA_TYPE t_coul = 0.0, t_cd = 0.0, t_disp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(Q_dop) * DEBEYE_TO_HA(dp_mag) * C;
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(sigmaPyDopant);
	DATA_TYPE sigma_9 = sigma_6 * sigmaPyDopant * sigmaPyDopant * sigmaPyDopant;
	DATA_TYPE Q = Q_dop * Q_mono;
	DATA_TYPE vPressure = 0.0;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = Q * erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);

	DATA_TYPE* _cm_x = ps.cm_x;
	DATA_TYPE* _cm_y = ps.cm_y;
	DATA_TYPE* _cm_z = ps.cm_z;

	DATA_TYPE* _dpm_x = ps.dpm_x;
	DATA_TYPE* _dpm_y = ps.dpm_y;
	DATA_TYPE* _dpm_z = ps.dpm_z;

	DATA_TYPE* _dop_x = ps.dop_x;
	DATA_TYPE* _dop_y = ps.dop_y;
	DATA_TYPE* _dop_z = ps.dop_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;

	int m_lb = mlb, m_ub = mub;
	int mIdx = 0;
	int pidx = 0;

#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = interpolation_coeff_offset + (interpolationOrder * 2 + 2);
#else
	int coefficient_offset = 12;
#endif

	DATA_TYPE test1 = 0.0, test2 = 0.0;

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(mIdx, cmx, cmy, cmz, dpmx, dpmy, dpmz, r_vec, ris, ris3, pidx, x1, x2, x3, x4, x5, x6, x7, xsqrd, pidx_speed)
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 3;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#pragma omp parallel for reduction(+:test1, test2) private(mIdx, cmx, cmy, cmz, dpmx, dpmy, dpmz, r_vec, ris, ris3, energy, pidx_speed, m0, m1, mu2, mu3, a0, a1, a2, a3)
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(mIdx, r_vec, ris, ris3, energy, pidx_speed, spline_coeff, x_dist, xofff, spline_temp)
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = interpolationOrder;;
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(mIdx, cmx, cmy, cmz, dpmx, dpmy, dpmz, r_vec, ris, ris3, pidx, x2, tmp, nd, c0, c1, c2, c3, pidx_speed)
#endif
#else
	DATA_TYPE energy[4];
#pragma omp parallel for reduction(+:test1, test2) private(mIdx, cmx, cmy, cmz, dpmx, dpmy, dpmz, r_vec, ris, ris3, pidx)
#endif
	for (mIdx = m_lb; mIdx < m_ub; mIdx++)
	{
		cmx = _cm_x[mIdx];
		cmy = _cm_y[mIdx];
		cmz = _cm_z[mIdx];

		dpmx = _dpm_x[mIdx];
		dpmy = _dpm_y[mIdx];
		dpmz = _dpm_z[mIdx];

		// before translation
		{
			r_vec[0] = PCB_(_x_size, (cmx - p_dop_x));
			r_vec[1] = PCB_(_y_size, (cmy - p_dop_y));
			r_vec[2] = PCB_(_z_size, (cmz - p_dop_z));

			r_vec[0] = ANG_TO_BOHR(r_vec[0]);
			r_vec[1] = ANG_TO_BOHR(r_vec[1]);
			r_vec[2] = ANG_TO_BOHR(r_vec[2]);

			ris = sqrt(r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2]);
			if (ris < ANG_TO_BOHR(_r_cut))
			{
				pidx = int((ris / interpolation_delta)) - interpolation_offset;

#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = ris * ris;

				test1 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((ris / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				test1 += polyVal(ris, interpolationParameters, coefficient_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
			}
		}

		// after translation
		{
			r_vec[0] = PCB_(_x_size, (cmx - _dop_x[didx]));
			r_vec[1] = PCB_(_y_size, (cmy - _dop_y[didx]));
			r_vec[2] = PCB_(_z_size, (cmz - _dop_z[didx]));

			r_vec[0] = ANG_TO_BOHR(r_vec[0]);
			r_vec[1] = ANG_TO_BOHR(r_vec[1]);
			r_vec[2] = ANG_TO_BOHR(r_vec[2]);

			ris = sqrt(r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2]);
			if (ris < ANG_TO_BOHR(_r_cut))
			{
				pidx = int((ris / interpolation_delta)) - interpolation_offset;
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = ris * ris;

				test2 += (x1 + x2 * ris + x3 * xsqrd + x4 * xsqrd * ris + x5 * xsqrd * xsqrd + x6 * ris * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = ris * ris;
				mu3 = mu2 * ris;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + ris;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 3 * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((ris / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 4, ris, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = ris * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				test2 += polyVal(ris, interpolationParameters, coefficient_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif

			}
		}
	}

	upydop = test1;
	t_upydop = test2;
}

void ModelPotential::DopDopPart(PARTICLE_SYSTEM& ps, int didx, int dlb, int dub,
	                            DATA_TYPE p_dop_x, DATA_TYPE p_dop_y, DATA_TYPE p_dop_z,
	                            DATA_TYPE& ucoul, DATA_TYPE& uother,
	                            DATA_TYPE& t_ucoul, DATA_TYPE& t_uother,
	                            DATA_TYPE& udopdop, DATA_TYPE& t_udopdop)
{
	DATA_TYPE sigma_dop = ANG_TO_BOHR(sigma_dopant);
	DATA_TYPE sigma_9 = TO_POW_6(sigma_dop) * TO_POW_3(sigma_dop);
	DATA_TYPE q_sqrd = Q_dop * Q_dop;
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0, rss = 0.0;
	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	DATA_TYPE coul = 0.0, other = 0.0, rss3 = 0.0;
	DATA_TYPE t_coul = 0.0, t_other = 0.0;
	int j = 0;
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
	DATA_TYPE* _dop_x = ps.dop_x;
	DATA_TYPE* _dop_y = ps.dop_y;
	DATA_TYPE* _dop_z = ps.dop_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;

	int _nDopants = ps.nDopants;

	dx = _dop_x[didx];
	dy = _dop_y[didx];
	dz = _dop_z[didx];

	DATA_TYPE p_dx = p_dop_x;
	DATA_TYPE p_dy = p_dop_y;
	DATA_TYPE p_dz = p_dop_z;
	DATA_TYPE test1 = 0.0, test2 = 0.0;
	int pidx = 0;

#if defined __USE_SIMPLE_POLYNOMIAL__
	int coefficient_offset = interpolation_coeff_offset + (interpolationOrder + 1);
#else
	int coefficient_offset = 8;
#endif

#if defined __MAXIMUM_SPEED__
	int pidx_speed = 0;
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(j, xdif, ydif, zdif, rss, rss3, pidx, x1, x2, x3, x4, x5, x6, x7, xsqrd, pidx_speed)
#elif defined __USE_INLINE_HERMITE_SPLINE__
	coefficient_offset = 2;
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#pragma omp parallel for reduction(+:test1, test2) private(j, xdif, ydif, zdif, rss, rss3, pidx, energy, pidx_speed, m0, m1, mu2, mu3, a0, a1, a2, a3)
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(j, xdif, ydif, zdif, rss, rss3, pidx, energy, pidx_speed, spline_coeff, x_dist, xofff, spline_temp)
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = interpolationOrder;
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#pragma omp parallel for reduction(+:test1, test2) private(j, xdif, ydif, zdif, rss, rss3, pidx, x2, tmp, nd, c0, c1, c2, c3, pidx_speed)
#endif
#else
#pragma omp parallel for reduction(+:test1, test2) private(j, xdif, ydif, zdif, rss, rss3, pidx)
#endif
	for (j = dlb; j < dub; j++)
	{
		if (j == didx)
			continue;


		// before translation
		{
			xdif = PCB_(_x_size, (_dop_x[j] - p_dx));
			ydif = PCB_(_y_size, (_dop_y[j] - p_dy));
			zdif = PCB_(_z_size, (_dop_z[j] - p_dz));

			rss = MAGNITUDE(xdif, ydif, zdif);
			if (rss < _r_cut)
			{
				rss = ANG_TO_BOHR(rss);
				pidx = int((rss / interpolation_delta)) - interpolation_offset;

#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rss * rss;

				test1 += (x1 + x2 * rss + x3 * xsqrd + x4 * xsqrd * rss + x5 * xsqrd * xsqrd + x6 * rss * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rss * rss;
				mu3 = mu2 * rss;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rss;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 2 * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rss / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 4, rss, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rss * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				test1 += polyVal(rss, interpolationParameters, coefficient_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
			}
		}

		// after translation
		{
			xdif = _dop_x[j] - dx;
			ydif = _dop_y[j] - dy;
			zdif = _dop_z[j] - dz;

			PCB(_x_size, _y_size, _z_size, xdif, ydif, zdif);

			rss = MAGNITUDE(xdif, ydif, zdif);
			if (rss < _r_cut)
			{
				rss = ANG_TO_BOHR(rss);
				pidx = int((rss / interpolation_delta)) - interpolation_offset;

#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rss * rss;

				test2 += (x1 + x2 * rss + x3 * xsqrd + x4 * xsqrd * rss + x5 * xsqrd * xsqrd + x6 * rss * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];
				
				m0 =  m1 = mu2 = mu3 = 0.0;
				a0 =  a1 = a2 = a3 = 0.0;

				mu2 = rss * rss;
				mu3 = mu2 * rss;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rss;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = 2 * nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rss / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 4, rss, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rss * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = coefficient_offset * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				test2 += polyVal(rss, interpolationParameters, coefficient_offset * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
			}
		}
	}

	udopdop = test1;
	t_udopdop = test2;

}


void ModelPotential::PyPyPartImageInteraction(PARTICLE_SYSTEM& ps, int midx, int mlb, int mub,
	                                               DATA_TYPE p_cm_x, DATA_TYPE p_cm_y, DATA_TYPE p_cm_z,
                                                   DATA_TYPE p_dpm_x, DATA_TYPE p_dpm_y, DATA_TYPE p_dpm_z,
                                                   DATA_TYPE& upypy, DATA_TYPE& t_upypy, DATA_TYPE& tr_upypy)
{
	if (!self_interaction)
	{
		upypy = t_upypy = tr_upypy = 0.0;
		return;
	}
	// only calculate the interaction if it is within your work assignment
	if (midx < mlb || midx >= mub)
	{
		upypy = t_upypy = tr_upypy = 0.0;
	}

	int exc_lb = (midx / monomers_per_chain) * monomers_per_chain;
	int exc_ub = exc_lb + monomers_per_chain;
	int t_lb = 0;
	int t_ub = 0;
	DATA_TYPE kappa_ = kappa;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE qsqrd = Q_mono * Q_mono;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE sigma_6_mid = TO_POW_6(sigma_inter);
	DATA_TYPE sigma_9_mid = sigma_6_mid * sigma_inter * sigma_inter * sigma_inter;
	DATA_TYPE sigma_6_end = TO_POW_6(sigma_inter_end);
	DATA_TYPE sigma_9_end = sigma_6_end * sigma_inter_end * sigma_inter_end * sigma_inter_end;
	DATA_TYPE sigma_6 = sigma_6_mid, sigma_9 = sigma_9_mid;
	DATA_TYPE epsilon = epsilon_inter;
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(dp_mag) * DEBEYE_TO_HA(dp_mag);
	int i = 0;
	int tlb = 0, tub = 0;
	DATA_TYPE vPressure = 0.0, rij3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;
	dampingFactor = erfc(kappa_ * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
	int pidx = 0;
	DATA_TYPE test1 = 0.0, test2 = 0.0, test3 = 0.0, udd = 0.0, t_udd = 0.0, tr_udd = 0.0;
	int pidx_speed = 0;

#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
	DATA_TYPE x1 = 0.0, x2 = 0.0, x3 = 0.0, x4 = 0.0, x5 = 0.0, x6 = 0.0, x7 = 0.0;
	DATA_TYPE xsqrd = 0.0;
#elif defined __USE_INLINE_HERMITE_SPLINE__
	double m0 = 0.0, m1 = 0.0, mu2 = 0.0, mu3 = 0.0;
	double a0 = 0.0, a1 = 0.0, a2 = 0.0, a3 = 0.0;
	DATA_TYPE bias = 0, tension = 0;
	DATA_TYPE energy[4];
#elif defined __USE_INLINE_SPLINE__
	DATA_TYPE energy[5];
	DATA_TYPE spline_coeff[4];
	DATA_TYPE x_dist[5];
	int xofff = 0;
	DATA_TYPE spline_temp = 0.0;
#else
	DATA_TYPE x2 = 0.0, tmp = 0.0;
	int nd = interpolationOrder;
	DATA_TYPE c0 = 0.0, c1 = 0.0, c2 = 0.0, c3 = 0.0;
#endif
#endif

	DATA_TYPE* _cm_x = ps.cm_x;
	DATA_TYPE* _cm_y = ps.cm_y;
	DATA_TYPE* _cm_z = ps.cm_z;

	DATA_TYPE* _dpm_x = ps.dpm_x;
	DATA_TYPE* _dpm_y = ps.dpm_y;
	DATA_TYPE* _dpm_z = ps.dpm_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;
	int _nMonomers = ps.nMonomers;


	bool srcInsideBox = true;
	bool destInsideBox = true;
	DATA_TYPE ox = p_cm_x;
	DATA_TYPE oy = p_cm_y;
	DATA_TYPE oz = p_cm_z;
	DATA_TYPE dpmx = p_dpm_x;
	DATA_TYPE dpmy = p_dpm_y;
	DATA_TYPE dpmz = p_dpm_z;

	if (fabs(ox) > ps.x_size / 2.0 ||
		fabs(oy) > ps.y_size / 2.0 ||
		fabs(oz) > ps.z_size / 2.0)
	{
		srcInsideBox = false;

		if (ox < -_x_size * 0.5) ox = ox + _x_size;
		if (ox >= _x_size * 0.5) ox = ox - _x_size;

		if (oy < -_y_size * 0.5) oy = oy + _y_size;
		if (oy >= _y_size * 0.5) oy = oy - _y_size;

		if (oz < -_z_size * 0.5) oz = oz + _z_size;
		if (oz >= _z_size * 0.5) oz = oz - _z_size;
	}

	bool t_srcInsideBox = true;

	DATA_TYPE t_ox = _cm_x[midx];
	DATA_TYPE t_oy = _cm_y[midx];
	DATA_TYPE t_oz = _cm_z[midx];
	DATA_TYPE r_dpmx = _dpm_x[midx];
	DATA_TYPE r_dpmy = _dpm_y[midx];
	DATA_TYPE r_dpmz = _dpm_z[midx];

	if (fabs(t_ox) > ps.x_size / 2.0 ||
		fabs(t_oy) > ps.y_size / 2.0 ||
		fabs(t_oz) > ps.z_size / 2.0)
	{
		t_srcInsideBox = false;

		if (t_ox < -_x_size * 0.5) t_ox = t_ox + _x_size;
		if (t_ox >= _x_size * 0.5) t_ox = t_ox - _x_size;

		if (t_oy < -_y_size * 0.5) t_oy = t_oy + _y_size;
		if (t_oy >= _y_size * 0.5) t_oy = t_oy - _y_size;

		if (t_oz < -_z_size * 0.5) t_oz = t_oz + _z_size;
		if (t_oz >= _z_size * 0.5) t_oz = t_oz - _z_size;
	}


	for (i = exc_lb; i < exc_ub; i++)
	{
		if (i == midx)
			continue;

		if (fabs(_cm_x[i]) > ps.x_size / 2.0 ||
			fabs(_cm_y[i]) > ps.y_size / 2.0 ||
			fabs(_cm_z[i]) > ps.z_size / 2.0)
		{
			destInsideBox = false;
		}
		else
		{
			destInsideBox = true;
		}


		DATA_TYPE tx = _cm_x[i];
		if (tx < -_x_size * 0.5) tx = tx + _x_size;
		if (tx >= _x_size * 0.5) tx = tx - _x_size;

		DATA_TYPE ty = _cm_y[i];
		if (ty < -_y_size * 0.5) ty = ty + _y_size;
		if (ty >= _y_size * 0.5) ty = ty - _y_size;

		DATA_TYPE tz = _cm_z[i];
		if (tz < -_z_size * 0.5) tz = tz + _z_size;
		if (tz >= _z_size * 0.5) tz = tz - _z_size;

		// before translation and rotation
		{
			if (srcInsideBox && destInsideBox)
				continue;

			xdif = tx - ox;
			ydif = ty - oy;
			zdif = tz - oz;

			xdif = ANG_TO_BOHR(xdif);
			ydif = ANG_TO_BOHR(ydif);
			zdif = ANG_TO_BOHR(zdif);

			rij = MAGNITUDE(xdif, ydif, zdif);
			DATA_TYPE tcut = ANG_TO_BOHR(_r_cut);
			if (rij > tcut)
			{
				continue;
			}

			rij3 = TO_POW_3(rij);

			if (srcInsideBox && !destInsideBox)
			{
				pidx = int((rij / interpolation_delta)) - interpolation_offset;

#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = 4 * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test1 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);

				DATA_TYPE xs[4] = { 0.0, 0.0, 0.0, 0.0 };
				int xofff = int((rij / interpolation_delta));
				xs[0] = interpolationParameters[xofff - 1];
				xs[1] = interpolationParameters[xofff];
				xs[2] = interpolationParameters[xofff + 1];
				xs[3] = interpolationParameters[xofff + 2];

				DATA_TYPE tt[4] = { 0.0, 0.0, 0.0, 0.0 };
				spline(xs, energy, 4, tt);
				DATA_TYPE ttt = 0.0;
				splint( xs, energy,  tt, 4, rij,  &ttt);
				int a = 1;
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = 4 * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				test1 += polyVal(rij, interpolationParameters, 4 * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

				udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
			}
			else if (!srcInsideBox && destInsideBox)
			{
				pidx = int((rij / interpolation_delta)) - interpolation_offset;
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = 4 * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test1 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test1 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test1 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = 4 * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test1 += c0 + c1 * x2;
#endif
#else
				test1 += polyVal(rij, interpolationParameters, 4 * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

				udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
			}
		}

		// after translation and before rotation
		{
			if (t_srcInsideBox && destInsideBox)
				continue;

			xdif = tx - t_ox;
			ydif = ty - t_oy;
			zdif = tz - t_oz;

			xdif = ANG_TO_BOHR(xdif);
			ydif = ANG_TO_BOHR(ydif);
			zdif = ANG_TO_BOHR(zdif);

			rij = MAGNITUDE(xdif, ydif, zdif);
			DATA_TYPE tcut = ANG_TO_BOHR(_r_cut);
			if (rij > tcut)
			{
				continue;
			}
			pidx = int((rij / interpolation_delta)) - interpolation_offset;

			rij3 = TO_POW_3(rij);

			if (t_srcInsideBox && !destInsideBox)
			{
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = 4 * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test2 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = 4 * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				test2 += polyVal(rij, interpolationParameters, 4 * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

				t_udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
			}
			else if (!t_srcInsideBox && destInsideBox)
			{
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = 4 * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test2 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test2 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test2 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;
				pidx_speed = 4 * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test2 += c0 + c1 * x2;
#endif
#else
				test2 += polyVal(rij, interpolationParameters, 4 * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif

				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

				t_udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
			}

			// after translation and rotation
			if (t_srcInsideBox && !destInsideBox)
			{
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = 4 * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test3 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test3 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test3 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = 4 * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test3 += c0 + c1 * x2;
#endif
#else
				test3 += polyVal(rij, interpolationParameters, 4 * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
				dij = DOT_PRODUCT(r_dpmx, r_dpmy, r_dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
				dir = DOT_PRODUCT(r_dpmx, r_dpmy, r_dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

				tr_udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
			}
			else if (!t_srcInsideBox && destInsideBox)
			{
#if defined __MAXIMUM_SPEED__
#if defined __USE_SIMPLE_POLYNOMIAL__
				// this evaluates a simply polynomial of order nCoeffs
				pidx_speed = 4 * nInterpolationRows + pidx;
				x1 = interpolationParameters[pidx_speed];
				x2 = interpolationParameters[1 * nInterpolationRows + pidx_speed];
				x3 = interpolationParameters[2 * nInterpolationRows + pidx_speed];
				x4 = interpolationParameters[3 * nInterpolationRows + pidx_speed];
				x5 = interpolationParameters[4 * nInterpolationRows + pidx_speed];
				x6 = interpolationParameters[5 * nInterpolationRows + pidx_speed];
				x7 = interpolationParameters[6 * nInterpolationRows + pidx_speed];
				xsqrd = rij * rij;

				test3 += (x1 + x2 * rij + x3 * xsqrd + x4 * xsqrd * rij + x5 * xsqrd * xsqrd + x6 * rij * xsqrd * xsqrd + x7 * xsqrd * xsqrd * xsqrd);
#elif defined __USE_INLINE_HERMITE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 1];
				energy[1] = interpolationParameters[pidx_speed];
				energy[2] = interpolationParameters[pidx_speed + 1];
				energy[3] = interpolationParameters[pidx_speed + 2];

				m0 = m1 = mu2 = mu3 = 0.0;
				a0 = a1 = a2 = a3 = 0.0;

				mu2 = rij * rij;
				mu3 = mu2 * rij;
				m0 = (energy[1] - energy[0]) * (1 + bias) * (1 - tension) / 2;
				m0 += (energy[2] - energy[1]) * (1 - bias) * (1 - tension) / 2;
				m1 = (energy[2] - energy[1]) * (1 + bias) * (1 - tension) / 2;
				m1 += (energy[3] - energy[2]) * (1 - bias) * (1 - tension) / 2;
				a0 = 2 * mu3 - 3 * mu2 + 1;
				a1 = mu3 - 2 * mu2 + rij;
				a2 = mu3 - mu2;
				a3 = -2 * mu3 + 3 * mu2;

				test3 += (a0 * energy[1] + a1 * m0 + a2 * m1 + a3 * energy[2]);
#elif defined __USE_INLINE_SPLINE__
				pidx_speed = nInterpolationRows + pidx;
				energy[0] = interpolationParameters[pidx_speed - 2];
				energy[1] = interpolationParameters[pidx_speed - 1];
				energy[2] = interpolationParameters[pidx_speed];
				energy[3] = interpolationParameters[pidx_speed + 1];
				energy[4] = interpolationParameters[pidx_speed + 2];

				xofff = int((rij / interpolation_delta)) - interpolation_offset;
				x_dist[0] = interpolationParameters[xofff - 2];
				x_dist[1] = interpolationParameters[xofff - 1];
				x_dist[2] = interpolationParameters[xofff];
				x_dist[3] = interpolationParameters[xofff + 1];
				x_dist[4] = interpolationParameters[xofff + 2];

				pidx_speed = 4 * nInterpolationRows + pidx;
				spline_coeff[0] = interpolationParameters[pidx_speed - 1];
				spline_coeff[1] = interpolationParameters[pidx_speed];
				spline_coeff[2] = interpolationParameters[pidx_speed + 1];
				spline_coeff[3] = interpolationParameters[pidx_speed + 2];


				splint(x_dist, energy, spline_coeff, 5, rij, &spline_temp);
				test3 += spline_temp;
#else
				// this evaluates a hermite polynomial
				x2 = rij * 2;
				tmp = 0.0;
				nd = interpolationOrder;;
				pidx_speed = 4 * nInterpolationRows + pidx;
				c0 = interpolationParameters[pidx_speed + 2 * nInterpolationRows];
				c1 = interpolationParameters[pidx_speed + 3 * nInterpolationRows];
				c2 = interpolationParameters[pidx_speed + 1 * nInterpolationRows];
				c3 = interpolationParameters[pidx_speed];

				tmp = c0;
				nd = nd - 1;
				c0 = c2 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				tmp = c0;
				nd = nd - 1;
				c0 = c3 - c1 * (2.0 * (nd - 1));
				c1 = tmp + c1 * x2;

				test3 += c0 + c1 * x2;
#endif
#else
				test3 += polyVal(rij, interpolationParameters, 4 * nInterpolationRows + pidx, interpolationOrder, nInterpolationRows);
#endif
				dij = DOT_PRODUCT(r_dpmx, r_dpmy, r_dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
				dir = DOT_PRODUCT(r_dpmx, r_dpmy, r_dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

				tr_udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
			}
		}
	}

	upypy    = test1 + udd;
	t_upypy  = test2 + t_udd;
	tr_upypy = test3 + tr_udd;

}

void ModelPotential::evalInter(DATA_TYPE dist, DATA_TYPE r_cut, DATA_TYPE& upypy, DATA_TYPE& upydop, DATA_TYPE& udopdop)
{
	upypy = upydop = udopdop = 0.0;

	DATA_TYPE sigma_6 = TO_POW_6(sigma_inter);
	DATA_TYPE sigma_9 = sigma_6 * TO_POW_3(sigma_inter);;
	DATA_TYPE qsqrd = Q_mono * Q_mono;
	DATA_TYPE dampingFactor = erfc(kappa * ANG_TO_BOHR(r_cut)) / ANG_TO_BOHR(r_cut);
	DATA_TYPE sigma_dop = ANG_TO_BOHR(sigma_dopant);

	DATA_TYPE dist3 = TO_POW_3(dist);

	// calculate the pypy terms
	upypy += (qsqrd * (erfc(kappa * dist) / dist - dampingFactor));
	upypy += 2.0 * epsilon_inter * ((sigma_9 / (dist3 * dist3 * dist3) - 1.5 * (sigma_6 / (dist3 * dist3))));

	// calculate the pydop terms
	sigma_6 = TO_POW_6(sigmaPyDopant);
	sigma_9 = sigma_6 * sigmaPyDopant * sigmaPyDopant * sigmaPyDopant;
	upydop += (Q_dop * Q_mono) * ((erfc(kappa * dist) / dist) - dampingFactor);
	upydop += (A * (sigma_9 / (dist3 * dist3 * dist3)) - B * (sigma_6 / (dist3 * dist3)));

	// calculate the dopdop terms
	sigma_9 = TO_POW_6(ANG_TO_BOHR(sigma_dopant)) * TO_POW_3(ANG_TO_BOHR(sigma_dopant));
	udopdop += ((Q_dop * Q_dop) * ((erfc(kappa * dist) / dist) - dampingFactor));
	udopdop += epsilon_dopant * pow(sigma_dop / dist, 9);

}

void ModelPotential::calculateModelParameters(DATA_TYPE r_cut, DATA_TYPE interpolation_delta, int order)
{
	vector<vector<DATA_TYPE> > rows;
	DATA_TYPE dist = 1.0;
	DATA_TYPE upypy = 0.0, upydop = 0.0, udopdop = 0.0;
#if defined __USE_INLINE_SPLINE__
	order = 4;
	DATA_TYPE pypy_spline_coeff[4] = { 0.0, 0.0, 0.0, 0.0 };
	DATA_TYPE pydop_spline_coeff[4] = { 0.0, 0.0, 0.0, 0.0 };
	DATA_TYPE dopdop_spline_coeff[4] = { 0.0, 0.0, 0.0, 0.0 };
#endif
	while (dist <= 1.5 * ANG_TO_BOHR(r_cut) + (1.0 / interpolation_delta))
	{
		evalInter(dist, r_cut, upypy, upydop, udopdop);
		vector<DATA_TYPE> row{ dist, upypy, udopdop, upydop };
		rows.push_back(row);
		dist += interpolation_delta;
	}

	for (int i = 0; i < rows.size() - (order + 1); i++)
	{
		vector< DATA_TYPE> dists, pypys, pydops, dopdops;

		for (int inner = 0; inner < order + 1; inner++)
		{
			dists.push_back(rows[i + inner][0]);
			pypys.push_back(rows[i + inner][1]);
			dopdops.push_back(rows[i + inner][2]);
			pydops.push_back(rows[i + inner][3]);
		}
#if defined __USE_INLINE_SPLINE__

		spline(dists.data(), pypys.data(), 4, pypy_spline_coeff);
		spline(dists.data(), dopdops.data(), 4, dopdop_spline_coeff);
		spline(dists.data(), pydops.data(), 4, pydop_spline_coeff);

		// now append the coefficients
		for (int inner = 0; inner < order; inner++)
		{
			rows[i].push_back(pypy_spline_coeff[inner]);
		}
		// now append the coefficients
		for (int inner = 0; inner < order; inner++)
		{
			rows[i].push_back(dopdop_spline_coeff[inner]);
		}
		// now append the coefficients
		for (int inner = 0; inner < order; inner++)
		{
			rows[i].push_back(pydop_spline_coeff[inner]);
		}

#elif defined __USE_SIMPLE_POLYNOMIAL__
		vector<DATA_TYPE> pypy_coeff, pydop_coeff, dopdop_coeff;
#pragma omp parallel sections
		{
#pragma omp section
			{
				polyfit(dists, pypys, pypy_coeff, order);
			}

#pragma omp section
			{
				polyfit(dists, pydops, pydop_coeff, order);
			}

#pragma omp section
			{
				polyfit(dists, dopdops, dopdop_coeff, order);
			}
	}
		// now append the coefficients
		for (int inner = 0; inner < order + 1; inner++)
		{
			rows[i].push_back(pypy_coeff[inner]);
}

		// now append the coefficients
		for (int inner = 0; inner < order + 1; inner++)
		{
			rows[i].push_back(dopdop_coeff[inner]);
		}

		// now append the coefficients
		for (int inner = 0; inner < order + 1; inner++)
		{
			rows[i].push_back(pydop_coeff[inner]);
		}
#endif
	}

	nInterpolationRows = rows.size();
	nInterpolationCols = rows[0].size();
	int size = (nInterpolationRows * nInterpolationCols);
	if (interpolationParameters == NULL)
	{
		interpolationParameters = new DATA_TYPE[size * BUFFER_FACTOR];
	}

	memset(interpolationParameters, 0, sizeof(DATA_TYPE) * size);

#if defined __USE_SIMPLE_POLYNOMIAL__ || __USE_INLINE_SPLINE__
	for (int i = 0; i < rows.size() - (order + 1); i++)
#else
	for (int i = 0; i < rows.size() - 1; i++)
#endif
	{
		for (int j = 0; j < nInterpolationCols; j++)
		{
			int idx = j * nInterpolationRows + i;
			interpolationParameters[j * nInterpolationRows + i] = rows[i][j];
		}
	}

	interpolation_offset = (1.0 / interpolation_delta);

#if !defined __MINIMIZE_OUTPUT__
	ofstream params("models_params.txt");
	params << "dist, py-py-total, dop-dop-total,py-dop-total,";
	for (int i = 0; i < interpolationOrder + 1; i++)
		params << "py-py-x" << i << ',';
	for (int i = 0; i < interpolationOrder + 1; i++)
		params << "dop-dop-x" << i << ',';
	for (int i = 0; i < interpolationOrder + 1; i++)
		params << "py-dop-x" << i << ',';

	params << endl;
	for (int i = 0; i < rows.size() - (order + 1); i++)
	{
		for (int j = 0; j < rows[i].size(); j++)
		{
			params << std::setprecision(18) << rows[i][j] << ',';
		}
		params << endl;
	}
	params.flush();
	params.close();
#endif
}


