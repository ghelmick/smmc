#include "Logger.h"
#include <time.h>
#include <sstream>
#include <iomanip>

using std::stringstream;
using std::endl;

Logger::Logger()
{	
	time_t rawtime;
	struct tm * ptm;

	time ( &rawtime );
	ptm = gmtime ( &rawtime );

	stringstream ss;
	ss << "log-" << ptm->tm_year << "-" << ptm->tm_yday << "-" << ptm->tm_hour << ptm->tm_min << ptm->tm_sec << ".txt";

	_outputStream.open(ss.str());
	
	if (!_outputStream.is_open())
	{

	}
}

Logger::~Logger()
{
	if (_outputStream.is_open())
	{
		_outputStream.close();
	}
}

void Logger::Write(const string & message)
{
	time_t rawtime;
	struct tm * ptm;

	time ( &rawtime );
	ptm = gmtime ( &rawtime );

	stringstream ss;
	ss << ptm->tm_hour << ":" << ptm->tm_min << ":" << ptm->tm_sec;

	_outputStream << "time : " << ss.str() << " Message : " <<  message << endl;
}

void Logger::Write(const string & file, int lineNumber, const string & message)
{
	time_t rawtime;
	struct tm * ptm;

	time ( &rawtime );
	ptm = gmtime ( &rawtime );

	stringstream ss;
	ss << std::setfill('0') << std::setw(2) << ptm->tm_hour << ":" << ptm->tm_min << ":" << ptm->tm_sec << " - [ " << file << " : " << lineNumber << " ] ";

	_outputStream << "time : " << ss.str() << " Message : " <<  message << endl;
}
