# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 23:35:37 2019

@author: greg
"""

import os
import shlex
import subprocess
import numpy as np
import pandas as pd


K                 = .00008617333262145 # eV/K
dopants_per_chain = 4
b_to_a            = 0.529177
h_to_e            = 27.2114
a_to_nm           = 0.1
atm_to_gpa        = 0.000101325 
gpa_to_atu        = 1.0 / 29421.02648438959
ev_to_joule       = 1.6022e-19
nm3_to_cm3        = 1e-21
nm3_to_m3         = 1e-27
avogadro          = 6.02214076e23
amu_to_kg         = 1.66053892173e-27


def bohrToAngstrom(bohr):
    return (b_to_a * bohr)


def hatreeToEv(ha):
    return (h_to_e * ha)


def best_fit_slope(xs,ys):
    m = (((mean(xs)*mean(ys)) - mean(xs*ys)) /
         ((mean(xs)**2) - mean(xs**2)))
    return m


def parseSysEnergy(fname):
    # determine how many rows need to be skipped
    df = pd.read_csv(fname, header=[0])
    avg_tot = df['avg-tot']

    skip_rows = 0
    for i in range(len(avg_tot)):
        if avg_tot[i] != 0 and np.isnan(avg_tot[i]) != True:
            skip_rows = i + 2
            break

    header = df.columns

    # now reload the dataframe
    df = pd.read_csv(fname, header=[0], skiprows=skip_rows)
    df.columns = header

    return df


def main(nprocesses=1, nprocs=12, exe_name='cuSMMC',
         config_file='appConfig.conf', init_config=None, sim_type=0,
         temperature=300, dt=0.1, tmin=0.8, tmax=1.2, anneal=True):
    
    results = list()
    
    pressure = pmin
    while pressure <= pmax:
        cmd = 'mpirun -n ' + str(nprocesses) + ' ./' + exe_name + ' -n ' + \
              str(nprocs) + ' -t ' + str(temperature) + ' -p ' + str(pressure) + \
              ' -s ' + str(sim_type) + ' -c ' + config_file
        if anneal is True:
            if init_config is not None:
                cmd = cmd + ' -f ' + init_config

            init_config = 'after_' + str(temperature) + '.bin'

        print(cmd)

        # format command for use with the subprocess command
        cmd_array = shlex.split(cmd)
        process = subprocess.Popen(cmd_array, stdout=subprocess.PIPE,
                                   universal_newlines=True)
        # display the output
        fout = open('stdout-' + str(temperature) + 'K.txt', 'wt')
        while True:
            output = process.stdout.readline()
            print(output.strip())
            fout.write(output.strip())
            fout.write('\n')
            fout.flush()
            # Do something like plot the acceptance rates
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                # process has finished, the rest of the output
                for output in process.stdout.readlines():
                    print(output.strip())
                    fout.write(output.strip())
                    fout.write('\n')
                    fout.flush()
                break

        df = parseSysEnergy('sys_energy_300.csv')
        
        row = list()
        row.append(pressure)
        row.append(temperature)
        row.append(df['density'].mean())
        row.append(df['density'].std())
        row.append(df['volume'].mean() * b_to_a**3 * a_to_nm**3)
        row.append(df['volume'].std() * b_to_a**3 * a_to_nm**3) 
        row.append(hatreeToEv(df['enthalpy'].mean()))
        row.append(hatreeToEv(df['enthalpy'].std()))
        row.append(hatreeToEv(df['tot'].mean()))
        row.append(hatreeToEv(df['tot'].std()))
        results.append(row)
        
        pressure += dt

        fout.flush()
        fout.close()

    return results
            
            
if __name__ == '__main__':
    nprocesses = 1
    sim_type = 1
    temperature = 300
    nprocs = 12
    config_file = 'appConfig.conf'
    exe_name = 'cuSMMC'
    init_config = None
    anneal = True
    src_data = 'init'
    sys_energy_root = 'after_3'
    sys_energy_file_pattern = '.bin'
    
    dt = 0.1
    pmin = 0.8
    pmax = 1.1 + dt

    total_results = list()
    
    for root, dirs, files in os.walk(src_data):  
        for filename in files:
            if filename.find(sys_energy_root) != -1:
                name, ext = os.path.splitext(filename)
                if ext == sys_energy_file_pattern:
                    results = main(nprocesses, nprocs, exe_name, config_file, root + '/' + filename, sim_type, 
                                   temperature, dt, pmin, pmax, anneal) 
            
                    total_results.extend(results)
    
        # save the results
    resultsDF = pd.DataFrame(total_results)
    header = ['pressure', 'temperature', 'density', 'std', 'volume', 'std', 'enthalpy', 'std', 'pe', 'std']
    resultsDF.columns = header
    resultsDF.to_csv('bm-results.csv')
