# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os
import sys
import math
import statistics
import numpy as np
import pandas as pd
from statistics import mean
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import matplotlib as mpl

sys_energy_root = 'sys_energy_'
sys_stats_root = 'sys_stats_'

DATA_ROOT_DIR = 'C:/Users/greg/Documents/'
REPORT_DIR = DATA_ROOT_DIR + 'reports/large-chlorine-system-manuscript/'
src_data = REPORT_DIR + 'npt-27648'

#
K                 = .00008617333262145 # eV/K
dopants_per_chain = 4
b_to_a            = 0.529177
h_to_e            = 27.2114
a_to_nm           = 0.1
atm_to_gpa        = 0.000101325 
gpa_to_atu        = 1.0 / 29421.02648438959
ev_to_joule       = 1.6022e-19
nm3_to_cm3        = 1e-21
nm3_to_m3         = 1e-27
avogadro          = 6.02214076e23
amu_to_kg         = 1.66053892173e-27
ha_to_kj_per_mol  = 2625.5 # A hartree is equal to 2625.5 kJ/mol
oligomer_mass   = 782.334238 # mass of 12-Py oligomer
dopant_mass     = 35.453       # mass of chlorine
#dopant_mass     = 18.998403       # mass of fluorine
#dopant_mass    = 0.0
kappa           = 0.06
mon_q           = 1.0 / 3.0
dop_q           = -1.0

#n_monomers = 768
#n_oligomers = 64
#n_dopants = 256
#n_dopants = 0

n_monomers = 6144
n_oligomers = 512
n_dopants = 2048

n_monomers = 20736
n_oligomers = 1728
n_dopants = 6912

#n_monomers = 49152
#n_oligomers = 4096
#n_dopants = 16384

#n_monomers = 96000
#n_oligomers = 8000
#n_dopants = 32000


n_particles = n_monomers + n_dopants
n_chains_dopants = n_oligomers + n_dopants
n_units = n_oligomers + (n_dopants / dopants_per_chain)

sys_mass = n_oligomers * oligomer_mass + n_dopants * dopant_mass
print(sys_mass)

def bohrToAngstrom(bohr):
    return (b_to_a * bohr)

def hartreeToEv(ha):
    return (h_to_e * ha)

def atmosphereToATU(pressure):
    pressure *= atm_to_gpa
    pressure *= gpa_to_atu
    return pressure

def calculateEnthalpyMean(df, temperature, pressureAtm=1.0):
    enthalpy = 0.0 #(totIntra + totInter) + p * curVol
    p = atmosphereToATU(pressureAtm)
    
    totPE  = df['tot']
    volume = df['volume']
    
    pe = list()
    for e in totPE:
        pe.append(e)
    
    vol = list()
    for v in volume:
        vol.append(v)
        
    pe  = pe[2:len(pe)-2]
    vol = vol[2:len(vol)-2]
    
    nSamplesPE = len(pe)
    for i in range(nSamplesPE):
        e = pe[i]
        v = vol[i]
        
        enthalpy += e + p * v + 1.5 * n_particles * float(temperature) * K

    if nSamplesPE == 0:
        return 0.0
    
    enthalpy /= nSamplesPE

    return (hartreeToEv(enthalpy) / n_particles)

def parseSysStats(fname):
    # now reload the dataframe
    df = pd.read_csv(fname, skiprows=1)
    nCols = df.shape[1]

    if nCols == 8:
        df.columns = ['vectorOrder', 'orientationOrder', 'gyration','alen', 'lx', 'ly', 'lz', 'lambda']
    if nCols == 12:
        df.columns = ['vectorOrder', 'orientationOrder', 'gyration','alen', 'lx', 'ly', 'lz', 'lambda', 'dlx', 'dly', 'dlz', 'dlambda']
        
    return df

def parseSysEnergy(fname, offset=0, sample_data=False):
    print(fname)
    # determine how many rows need to be skipped
    df = pd.read_csv(fname, header=[0])
    avg_tot = df['avg-tot']
    
    skip_rows = 0
    for i in range(len(avg_tot)):
        if avg_tot[i] != 0 and np.isnan(avg_tot[i]) != True:
            skip_rows = i
            break
    skip_rows = skip_rows + offset
    header = df.columns

    # now reload the dataframe
    df = pd.read_csv(fname, header=[0], skiprows=skip_rows)
    if len(header) == df.shape[1]:
        header = header
    else:
        header = header[0:-1]
        
    df.columns = header
    
    # sample the data
    new_data = list()
    if sample_data == True:
        # first determine data sample rate, then sample row
        srate = df['itr'][1] - df['itr'][0]
        
        rows = df.values
        for row in rows:
            if row[0] % 1000 == 0:
                new_data.append(row)
            
        
        df = pd.DataFrame(new_data)
        df.columns = header
        
    return df
    
def generatePotentialEnergyGraphs(temperature, tot_pe_energy, inter_pe_energy, intra_pe_energy, enthalpy):
    #%%% Potential Energy Plot    
    x1 = temperature 
    y1 = tot_pe_energy
    
    fig, ax1 = plt.subplots()
    #ax1.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.3e'))
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Potential Energy", marker=11) 
            
    # naming the x axis 
    ax1.set_xlabel('Temperature (K)') 
    # naming the y axis 
    ax1.set_ylabel('Energy/Particle(eV)') 
      
    x2 = temperature
    y2 = enthalpy  
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    #ax2.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.3e'))

    ax2.set_ylabel('Enthalpy/Particle(eV)')  # we already handled the x-label with ax1
    lns2 = ax2.plot(x2, y2, 'g', label='Enthalpy', marker=11)


    # show a legend on the plot 
    lns = lns1 + lns2
    labs = [l.get_label() for l in lns]

    ax1.legend(lns, labs, loc=0)
      
    # function to show the plot 
    #plt.show() 
 
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/pe-enthalpy-plot.png')

    #%%% Potential Energy Plot    
    x = temperature 
    y = tot_pe_energy
    
    fig, ax = plt.subplots()
    #ax1.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.3e'))
    
    # plotting the line 1 points  
    lns1 = ax.plot(x, y, 'r', label = "Potential Energy", marker=11) 
            
    # naming the x axis 
    ax.set_xlabel('Temperature (K)') 
    # naming the y axis 
    ax.set_ylabel('Energy/Particle(eV)') 


    # show a legend on the plot 
    lns = lns1
    labs = [l.get_label() for l in lns]

    ax.legend(lns, labs, loc=0)

    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/pe-plot.png')
    
    #%%% Enthalpy Plot       
    x = temperature
    y = enthalpy  
    fig, ax = plt.subplots()
    #ax2.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.3e'))

    ax.set_ylabel('Enthalpy/Particle(eV)')  # we already handled the x-label with ax1
    lns2 = ax.plot(x, y, 'g', label='Enthalpy', marker=11)


    # show a legend on the plot 
    lns = lns2
    labs = [l.get_label() for l in lns]

    ax.legend(lns, labs, loc=0)
      
    # function to show the plot 
    #plt.show() 
 
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/enthalpy-plot.png')

def generateDensityGraphs(temperature, density, volume):
    #%%% Density Plot        
    x1 = temperature 
    y1 = density
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Density", marker=11) 
            
    # naming the x axis 
    ax1.set_xlabel('Temperature (K)') 
    # naming the y axis 
    ax1.set_ylabel('Density (g/cm^3)') 
    
    x2 = temperature
    y2 = volume
    y2 = volume #[x * b_to_a**3 * a_to_nm**3 for x in volume]
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    #ax2.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.3e'))
    
    ax2.set_ylabel('Volume (nm^3)')  # we already handled the x-label with ax1
    lns2 = ax2.plot(x2, y2, 'g', label = 'Volume', marker=11)
    

    # show a legend on the plot 
    # show a legend on the plot 
    lns = lns1 + lns2
    labs = [l.get_label() for l in lns]

    ax1.legend(lns, labs, loc=0)
     
    # function to show the plot 
    #plt.show() 

    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/density-volume-plot.png')

def generateDensityProfile(temperature, density):
    
    # generate mean, std dev lines
    std_dev = statistics.stdev(density)
    mean = statistics.mean(density)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in density:
        avg.append(mean)
        dev_p1.append(mean + 2 * std_dev)
        dev_m1.append(mean - 2 * std_dev)
        
    #%%% Density Plot        
    x1 = [i for i in range(len(density))] 
    y1 = density
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Density") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
        
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Density (g/cm^3)') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/density-' + str(temperature) + '-plot.png')

    ctr = 0
    for e in y1:
        if float(e) >= float(dev_m1[0]) and float(e) <= float(dev_p1[0]):
            ctr = ctr + 1
            
    print('Density within std dev: ' + str(ctr/ len(y1)))
 
    return y1

def generateEnthalpyProfile(temperature, enthalpy):

    #%%% Density Plot        
    x1 = [i for i in range(len(enthalpy))] 
    y1 = [hartreeToEv(eth) / n_particles for eth in enthalpy]

    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + 2 * std_dev)
        dev_m1.append(mean - 2 * std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Enthalpy") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
            
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Enthalpy (eV/particle)') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/enthalpy-' + str(temperature) + '-plot.png')

    ctr = 0
    for e in y1:
        if float(e) >= float(dev_m1[0]) and float(e) <= float(dev_p1[0]):
            ctr = ctr + 1
            
    print('Enth within std dev: ' + str(ctr/ len(y1)))
    
    return y1

def generateInterPEProfile(temperature, inter_pe):
    #%%% Density Plot        
    x1 = [i for i in range(len(inter_pe))] 
    y1 = [hartreeToEv(v) / n_particles for v in inter_pe]
    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + std_dev)
        dev_m1.append(mean - std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Inter-Potential Energy") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
           
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Energy (eV / particle)') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/inter-pe-' + str(temperature) + '-plot.png')

    return y1


def generateIntraPEProfile(temperature, intra_pe):
    #%%% Density Plot        
    x1 = [i for i in range(len(intra_pe))] 
    y1 = [hartreeToEv(v) / n_oligomers for v in intra_pe]
    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + std_dev)
        dev_m1.append(mean - std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Intra-Potential Energy") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
           
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Energy (eV / oligomer)') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/intra-pe-' + str(temperature) + '-plot.png')

    return y1

def generateVolumeVsIteration(temperature, volume):
    #%%% Density Plot        
    x1 = [i for i in range(len(volume))]  
    y1 = [vol * b_to_a**3 * a_to_nm**3 for vol in volume]
    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + std_dev)
        dev_m1.append(mean - std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Volume") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
            
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Angstrom^3') 
        

    # show a legend on the plot 
    # show a legend on the plot 
    lns = lns1# + lns2
    labs = [l.get_label() for l in lns]

    ax1.legend(lns, labs, loc=0)
     
    # function to show the plot 
    #plt.show() 

    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/volume-' + str(temperature) + '-plot.png')

    return y1


def generateMolarVolumeProfile(temperature, mol_vol):
    #%%% Density Plot        
    x1 = [i for i in range(len(mol_vol))] 
    y1 = mol_vol
    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + std_dev)
        dev_m1.append(mean - std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Molar Volume") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
            
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Molar Volume (nm^3)') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/molar-volume-' + str(temperature) + '-plot.png')

    return y1


def generateCohesiveEnergyProfile(temperature, coh_energy):
    #%%% Density Plot        
    x1 = [i for i in range(len(coh_energy))] 
    y1 = coh_energy
    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + std_dev)
        dev_m1.append(mean - std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Cohesive Energy") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
            
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Cohesive Energy (kJ)') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/cohesive-energy-' + str(temperature) + '-plot.png')

    return y1


def generateCohesiveEnergyDensityProfile(temperature, coh_energy_den):
    #%%% Density Plot        
    x1 = [i for i in range(len(coh_energy_den))] 
    y1 = coh_energy_den
    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + std_dev)
        dev_m1.append(mean - std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Cohesive Energy Density") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
            
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Cohesive Energy Density(J/cm^3)') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/cohesive-energy-density-' + str(temperature) + '-plot.png')

    return y1


def generateHildebrandParameterProfile(temperature, h_param):
    #%%% Density Plot        
    x1 = [i for i in range(len(h_param))] 
    y1 = h_param
    # generate mean, std dev lines
    std_dev = statistics.stdev(y1)
    mean = statistics.mean(y1)
    dev_p1 = list()
    dev_m1 = list()
    avg = list()
    
    for d in y1:
        avg.append(mean)
        dev_p1.append(mean + std_dev)
        dev_m1.append(mean - std_dev)
    
    fig, ax1 = plt.subplots()
    
    # plotting the line 1 points  
    lns1 = ax1.plot(x1, y1, 'r', label = "Hildebrand Parameter") 
    lns2 = ax1.plot(x1, avg, 'black', label='Mean')
    lns3 = ax1.plot(x1, dev_p1, 'black', label='+sigma')
    lns4 = ax1.plot(x1, dev_m1, 'black', label='-sigma')
            
    # naming the x axis 
    ax1.set_xlabel('Iteration') 
    # naming the y axis 
    ax1.set_ylabel('Hildebrand Parameter(J/cm^3)^1/2') 
    
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/hildebrand-parameter-' + str(temperature) + '-plot.png')

    return y1


def best_fit_slope(xs,ys):
    return 1,1

    m_xs_sqrd = [xs[i] * xs[i] for i in range(len(xs))]
    m_xs_ys = [xs[i]*ys[i] for i in range(len(xs))]

    m = (((mean(xs)*mean(ys)) - mean(m_xs_ys)) /
         ((mean(xs)**2) - mean(m_xs_sqrd)))
    b = mean(ys) - m*mean(xs)
    return m,b

def calculateCohesiveEnergy(df):
    volume_   = [vol * b_to_a**3 * a_to_nm**3 for vol in df['volume']]
    molar_volume_            = [vol / n_units for vol in volume_]
    cohesive_energy_density_ = list()
    cohesive_energy_ = [-(ti * ha_to_kj_per_mol * 1000) for ti in df['tot-inter']]
    
    for i in range(len(cohesive_energy_)):
        temp = cohesive_energy_[i] / n_particles # appar
        cohesive_energy_density_.append(temp / (molar_volume_[i] * nm3_to_cm3 * avogadro))
        
    hildebrand_parameter_    = [math.sqrt(ced) for ced in cohesive_energy_density_ ]
    return volume_, molar_volume_, cohesive_energy_, cohesive_energy_density_, hildebrand_parameter_
    
def process_raw_data():
    pressure = 1.0
    systemEnergyMap = {}
    sig_fig_format = '0.3f'
    offset = 0

    for root, dirs, files in os.walk(src_data):  
        for filename in files:
            if filename.find(sys_energy_root) != -1:
                temperature = (filename[filename.rfind('_') + 1:filename.rfind('.')])
                df = parseSysEnergy(root + '/' + filename, offset)
                df = df.dropna(axis=0)
                systemEnergyMap.setdefault(temperature, df)
    
    temperature                 = list()
    tot_pe_energy               = list()
    inter_pe_energy             = list()
    intra_pe_energy             = list()
    enthalpy                    = list()
    volume                      = list()
    density                     = list()
    molar_volume                = list()
    cohesive_energy             = list()
    cohesive_energy_density     = list()
    hildebrand_parameter        = list()
    systemEnergyMapStats        = {}
    systemEnergyMapStats_no_std = {}
        
    for temp, df in systemEnergyMap.items():
        cp, alpha, kappa_t = attrByFluctuationMethod(df, temp)
        
        temperature.append(temp)
        density_values = [dens * 1000.0 for dens in df['density']]
        
        tot_pe   = hartreeToEv(statistics.mean(df['tot']))/n_particles
        inter_pe = hartreeToEv(statistics.mean(df['tot-inter']))/n_particles
        intra_pe = hartreeToEv(statistics.mean(df['tot-intra']))/n_particles
        enthalpy_ = hartreeToEv(statistics.mean(df['enthalpy']))/n_particles
        density_  = statistics.mean(density_values)

        volume_, molar_volume_, cohesive_energy_, cohesive_energy_density_, hildebrand_parameter_ = calculateCohesiveEnergy(df)

        # generate density profile for this temperature
        generateDensityProfile(temp, density_values)
        generateEnthalpyProfile(temp, df['enthalpy'])
        generateInterPEProfile(temp, df['tot-inter'])
        generateIntraPEProfile(temp, df['tot-intra'])
        generateVolumeVsIteration(temp, df['volume'])
        generateMolarVolumeProfile(temp, molar_volume_)
        generateCohesiveEnergyProfile(temp, cohesive_energy_)
        generateCohesiveEnergyDensityProfile(temp, cohesive_energy_density_)
        generateHildebrandParameterProfile(temp, hildebrand_parameter_)


        tot_pe_energy.append(tot_pe)
        inter_pe_energy.append(inter_pe)
        intra_pe_energy.append(intra_pe)
        enthalpy.append(enthalpy_)
        density.append(density_values)
        volume.append(statistics.mean(volume_)) 	
        molar_volume.append(statistics.mean(molar_volume_))
        cohesive_energy.append(statistics.mean(cohesive_energy_))
        cohesive_energy_density.append(statistics.mean(cohesive_energy_density_))
        hildebrand_parameter.append(statistics.mean(hildebrand_parameter_))
        r_cut = [rc * 0.5 * 0.98 * 0.1 for rc in df['z-size']]
 		# calculate Wolf 3rd term	
        wolf_shift = list()
        
        for rc in r_cut:
            b_rc = rc / b_to_a
            shiftMonMon = math.erfc(kappa * b_rc) / (2.0 * b_rc) + (kappa / math.sqrt(math.pi)) * mon_q**2 * n_monomers
            shiftDopDop = math.erfc(kappa * b_rc) / (2.0 * b_rc) + (kappa / math.sqrt(math.pi)) * dop_q**2 * n_dopants
            shiftMonDop = math.erfc(kappa * b_rc) / (2.0 * b_rc) + (kappa / math.sqrt(math.pi)) * (mon_q**2 * n_monomers + dop_q**2 * n_dopants)
            
            wolf_shift.append(hartreeToEv(shiftMonMon + shiftDopDop + shiftMonDop) / (n_particles))
            
        
        stats = list()
        val = str(format(tot_pe, sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(hartreeToEv(df['tot'])/n_particles), sig_fig_format))
        stats.append(val)
        
        val = str(format(inter_pe, sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(hartreeToEv(df['tot-inter'])/n_particles), sig_fig_format))
        stats.append(val)
        val = str(format(intra_pe, sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(hartreeToEv(df['tot-intra'])/n_oligomers), sig_fig_format))
        stats.append(val)
        val = str(format(enthalpy_, sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(hartreeToEv(df['enthalpy'])/n_particles), sig_fig_format))
        stats.append(val)
        val = str(format(density_, sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(density_values), sig_fig_format))
        stats.append(val)
        val = str(format(statistics.mean(volume_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(df['volume'] * b_to_a**3 * a_to_nm**3), sig_fig_format))
        stats.append(val)        
        val = str(format(statistics.mean(r_cut), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(r_cut), sig_fig_format))
        stats.append(val)      
        val = str(format(statistics.mean(molar_volume_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(molar_volume_), sig_fig_format))
        stats.append(val)      
        val = str(format(statistics.mean(cohesive_energy_ )/1000.0, sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev([ce / 1000.0 for ce in cohesive_energy_]), sig_fig_format))
        stats.append(val)      
        val = str(format(statistics.mean(cohesive_energy_density_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(cohesive_energy_density_), sig_fig_format))
        stats.append(val)      
        val = str(format(statistics.mean(hildebrand_parameter_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(hildebrand_parameter_), sig_fig_format))
        stats.append(val)      
        val = str(format(statistics.mean(wolf_shift), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(wolf_shift), sig_fig_format))
        stats.append(val)      
        val = str(format(cp, sig_fig_format))
        stats.append(val)      
        val = str(format(alpha, sig_fig_format))
        stats.append(val)      
        val = str(format(kappa_t, sig_fig_format))
        stats.append(val)      
       
        systemEnergyMapStats.setdefault(temp, stats)
        
        # no standard deviation
        stats_no_std = list()
        stats_no_std.append(str(format(tot_pe, sig_fig_format)))
        stats_no_std.append(str(format(statistics.stdev(hartreeToEv(df['tot'])/n_particles), sig_fig_format)))
        
        stats_no_std.append(str(format(inter_pe, sig_fig_format)))
        stats_no_std.append(str(format(statistics.stdev(hartreeToEv(df['tot-inter'])/n_particles), sig_fig_format)))
        
        stats_no_std.append(str(format(intra_pe, sig_fig_format)))
        stats_no_std.append(str(format(statistics.stdev(hartreeToEv(df['tot-intra'])/n_oligomers), sig_fig_format)))
        
        stats_no_std.append(str(format(enthalpy_, sig_fig_format)))
        stats_no_std.append(str(format(statistics.stdev(hartreeToEv(df['enthalpy'])/n_particles), sig_fig_format)))
        
        stats_no_std.append(str(format(density_, sig_fig_format)))
        stats_no_std.append(str(format(statistics.stdev(density_values), sig_fig_format)))
        
        stats_no_std.append(str(format(statistics.mean(volume_), sig_fig_format)))        
        stats_no_std.append(str(format(statistics.stdev(df['volume'] * b_to_a**3 * a_to_nm**3), sig_fig_format)))        

        stats_no_std.append(str(format(statistics.mean(r_cut), sig_fig_format)))        
        stats_no_std.append(str(format(statistics.stdev(r_cut), sig_fig_format)))        

        val = str('{:.4}'.format(statistics.mean(molar_volume_)))
        stats_no_std.append(val)
        stats_no_std.append(str(format(statistics.stdev(molar_volume_), sig_fig_format)))   
        
        val = str('{:.4}'.format(statistics.mean(cohesive_energy_) / 1000.0))
        stats_no_std.append(val)
        stats_no_std.append(str(format(statistics.stdev([ce / 1000.0 for ce in cohesive_energy_]), sig_fig_format)))   
        
        val = str('{:.4}'.format(statistics.mean(cohesive_energy_density_)))
        stats_no_std.append(val)
        stats_no_std.append(str(format(statistics.stdev(cohesive_energy_density_), sig_fig_format)))   
        
        val = str('{:.4}'.format(statistics.mean(hildebrand_parameter_)))
        stats_no_std.append(val)
        stats_no_std.append(str(format(statistics.stdev(hildebrand_parameter_), sig_fig_format)))   

        stats_no_std.append(str(format(statistics.mean(wolf_shift), sig_fig_format)))        
        stats_no_std.append(str(format(statistics.stdev(wolf_shift), sig_fig_format)))        

        stats_no_std.append(str(format(cp, sig_fig_format)))        
        stats_no_std.append(str(format(alpha, sig_fig_format)))        
        stats_no_std.append(str(format(kappa_t, sig_fig_format)))        

        systemEnergyMapStats_no_std.setdefault(temp, stats_no_std)
        
    generatePotentialEnergyGraphs(temperature, tot_pe_energy, inter_pe_energy,
                                  intra_pe_energy, enthalpy)
    generateDensityGraphs(temperature, density, volume)
    columns = ['Total Potential Energy (eV)', 'Inter Potential Energy (eV)', 
               'Intra Potential Energy (eV)', 'Enthalpy (eV)', 
               'Density (kg/m^3)', 'Volume (nm^3)', 'CutOff(nm)',
               'molar-volume(nm^3)', 'cohesive-energy(kJ)', 
               'cohesive-energy-density(J/cm^3)',
                      'hildebrand(J/cm^3)^1/2', 'LR Coulomb Shift(eV)',
                      'Cp(J/kg*K', 'Alpha', 'Kappa_t']
    energyDF = pd.DataFrame.from_dict(systemEnergyMapStats, orient='index', 
                                      columns=columns)
    energyDF.to_csv(src_data + '/sys_energies.csv')

    columns_no_std = ['Total Potential Energy (eV)', 'std-dev', 
                      'Inter Potential Energy (eV)', 'std-dev', 
                      'Intra Potential Energy (eV)', 'std-dev', 
                      'Enthalpy (eV)', 'std-dev', 'Density (kg/m^3)', 'std-dev', 
                      'Volume (nm^3)', 'std-dev', 'CutOff(nm)', 'std-dev',
                      'molar-volume(nm^3)', 'std-dev', 'cohesive-energy(kJ)',
                      'std-dev', 'cohesive-energy-density(J/cm^3)', 'std-dev',
                      'hildebrand(J/cm^3)^1/2', 'std-dev', 
                      'LR Coulomb Shift(eV)', 'std-dev',
                      'Cp(J/kg*K', 'Alpha', 'Kappa_t']
    energyDF = pd.DataFrame.from_dict(systemEnergyMapStats_no_std, 
                                      orient='index', columns=columns_no_std)
    energyDF.to_csv(src_data + '/sys_energies-no-std.csv')
    
    
    systemStatsMap = {}
    for root, dirs, files in os.walk(src_data):  
        for filename in files:
            if filename.find(sys_stats_root) != -1:
                temperature = float(filename[filename.rfind('_') + 1:filename.rfind('.')])
                df = parseSysStats(root + '/' + filename)
                df = df.dropna()
                systemStatsMap.setdefault(temperature, df)
                
    temperature       = list()
    vector_order      = list()
    orientation_order = list()
    gyration          = list()
    avg_len           = list()
    systemStatsMapStats = {}
    systemStatsMapStats_no_std = {}
    

    for temp, df in systemStatsMap.items():
        temperature.append(temp)
        vector_order.append(df['vectorOrder'].mean())
        orientation_order.append(df['orientationOrder'].mean())
        gyration.append(df['gyration'].mean())
        avg_len.append(df['alen'].mean())

        stats = list()
        val = str(format(df['vectorOrder'].mean(), sig_fig_format)) + ' ' + '\xB1' + str(format(df['vectorOrder'].std(), sig_fig_format))
        stats.append(val)

        val = str(format(df['orientationOrder'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['orientationOrder'].std(), sig_fig_format))
        stats.append(val)

        val = str(format(df['gyration'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['gyration'].std(), sig_fig_format))
        stats.append(val)
        
        val = str(format(df['alen'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['alen'].std(), sig_fig_format))
        stats.append(val)

        if 'lambda' in df.columns:
            val = str(format(df['lambda'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['lambda'].std(), sig_fig_format))
            stats.append(val)

        if 'dlambda' in df.columns:
            val = str(format(df['dlambda'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['dlambda'].std(), sig_fig_format))
            stats.append(val)
            
        systemStatsMapStats.setdefault(temp, stats)

        # no standard deviation
        stats_no_std = list()
        val = str(format(df['vectorOrder'].mean(), sig_fig_format))
        stats_no_std.append(val)

        val = str(format(df['orientationOrder'].mean(), sig_fig_format))
        stats_no_std.append(val)

        val = str(format(df['gyration'].mean(), sig_fig_format))
        stats_no_std.append(val)
        
        val = str(format(df['alen'].mean(), sig_fig_format))
        stats_no_std.append(val)

        if 'lambda' in df.columns:
            val = str(format(df['lambda'].mean(), sig_fig_format))
            stats_no_std.append(val)

        if 'dlambda' in df.columns:
            val = str(format(df['dlambda'].mean(), sig_fig_format))
            stats_no_std.append(val)
            
        systemStatsMapStats_no_std.setdefault(temp, stats_no_std)
   
    columns = ['Vector Order (S)', 'Orientation Order (Z)', 'Radius of Gyration (A)', 'Length (A)']    
    if len(systemStatsMapStats[list(systemStatsMapStats.keys())[0]]) == 6:
        columns = ['Vector Order (S)', 'Orientation Order (Z)', 'Radius of Gyration (A)', 'Length (A)', 'Monomer MSD (A)', 'Dopant MSD (A)']
        
    statsDF = pd.DataFrame.from_dict(systemStatsMapStats, orient='index', columns=columns)
    statsDF.to_csv(src_data + '/sys_stats.csv')

    statsDF = pd.DataFrame.from_dict(systemStatsMapStats_no_std, orient='index', columns=columns)
    statsDF.to_csv(src_data + '/sys_stats-no-std.csv')
    
    # calculate molar volume, specific heat at constant volume, specific heat \
    # at constant pressure, binding energy, cohesive energy, cohesive energy \
    # density, and hildebrand solubility paramter
    tx = np.zeros(len(temperature))
    py = np.zeros(len(tot_pe_energy))
    ey = np.zeros(len(enthalpy))
        
    for idx in range(len(temperature)):
        tx[idx] = (sys_mass * amu_to_kg) * float(temperature[idx])
        
    for idx in range(len(tot_pe_energy)):
        py[idx] = ev_to_joule * tot_pe_energy[idx] * n_particles

    for idx in range(len(enthalpy)):
        ey[idx] = ev_to_joule * enthalpy[idx] * n_particles
    
    # calculate low temperature cp and cv
    interval = int(len(tx) / 3)
    cp_low,b    = best_fit_slope(tx[0:interval+1]          ,ey[0:interval+1])
    cp_medium,b = best_fit_slope(tx[interval:interval*2 +1],ey[interval:interval*2 +1])
    cp_high,b   = best_fit_slope(tx[interval*2:len(tx)]    ,ey[interval*2:len(tx)])
    
    fout = open(src_data + '/specific-heat.txt', 'wt')
    fout.write('cp_low,' + str(cp_low) + '\n')
    fout.write('cp_medium,' + str(cp_medium) + '\n')
    fout.write('cp_high,' + str(cp_high) + '\n')
    fout.flush()
    fout.close()
    
    print('cp_low,' + str(cp_low) + '\n')
    print('cp_medium,' + str(cp_medium) + '\n')
    print('cp_high,' + str(cp_high) + '\n')

    # Plot
    fig = plt.figure()
    miny = min(tot_pe_energy)
    maxy = max(tot_pe_energy)
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(temperature, tot_pe_energy)
    regression_line = [(m*x)+b for x in temperature]
    plt.plot(temperature, regression_line)

    plt.scatter(temperature, tot_pe_energy)
    plt.xlabel('Temperature (K)')
    plt.ylabel('Potential Energy (eV)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/combined-pe-plot.png')
    
    fig = plt.figure()
    miny = min(enthalpy)
    maxy = max(enthalpy)
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(temperature, enthalpy)
    regression_line = [(m*x)+b for x in temperature]
    plt.plot(temperature, regression_line)

    plt.scatter(temperature, enthalpy)
    plt.xlabel('Temperature (K)')
    plt.ylabel('Enthalpy (eV)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/combined-enthalpy-plot.png')
    
    fig = plt.figure()
    miny = min(density_values)
    maxy = max(density_values)
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(temperature, density_values)
    regression_line = [(m*x)+b for x in temperature]
    plt.plot(temperature, regression_line)

    plt.scatter(temperature, density_values)
    plt.xlabel('Temperature (K)')
    plt.ylabel('Density (kg/m^3)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/combined-density-plot.png')

    fig = plt.figure()
    miny = min(molar_volume)
    maxy = max(molar_volume)
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(temperature, molar_volume)
    regression_line = [(m*x)+b for x in temperature]
    plt.plot(temperature, regression_line)

    plt.scatter(temperature, molar_volume)
    plt.xlabel('Temperature (K)')
    plt.ylabel('Molar Volume (cm^3)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/combined-molar-volume-plot.png')

    fig = plt.figure()
    miny = min(cohesive_energy_density)
    maxy = max(cohesive_energy_density)
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(temperature, cohesive_energy_density)
    regression_line = [(m*x)+b for x in temperature]
    plt.plot(temperature, regression_line)

    plt.scatter(temperature, cohesive_energy_density)
    plt.xlabel('Temperature (K)')
    plt.ylabel('Cohesive Energy Density (J/m^3)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/combined-cohesive-energy-density-plot.png')

    fig = plt.figure()
    miny = min(cohesive_energy)
    maxy = max(cohesive_energy)
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(temperature, cohesive_energy)
    regression_line = [(m*x)+b for x in temperature]
    plt.plot(temperature, regression_line)

    ax = plt.scatter(temperature, cohesive_energy)
    plt.xlabel('Temperature (K)')
    plt.ylabel('Cohesive Energy (J/mol)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/combined-cohesive-energy-plot.png')

    fig = plt.figure()
    miny = min(hildebrand_parameter)
    maxy = max(hildebrand_parameter)
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(temperature, hildebrand_parameter)
    regression_line = [(m*x)+b for x in temperature]
    plt.plot(temperature, regression_line)

    plt.scatter(temperature, hildebrand_parameter)
    plt.xlabel('Temperature (K)')
    plt.ylabel('Hildebrand Parameter')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/combined-hildebrand-plot.png')
   

def generateCombinedProfiles():
    pressure = 1.0
    systemEnergyMap = {}
    sig_fig_format = '0.4f'
    offset = 0
    
    for root, dirs, files in os.walk(src_data):  
        for filename in files:
            if filename.find(sys_energy_root) != -1:
                label = (filename[filename.rfind('_') + 1:filename.rfind('.')])
                df = parseSysEnergy(root + '/' + filename, offset=offset, sample_data=True)
                df = df.dropna(axis=0)
                systemEnergyMap.setdefault(label, df)

    print(src_data)
    density_list = list()
    enthalpy_list = list()
    inter_pe_list = list()
    intra_pe_list = list()
    vol_list = list()
    mol_vol_list = list()
    coh_list = list()
    coh_den_list = list()
    sol_list = list()
    stats = list()
    sample_rate = list()
    
    for label, df in systemEnergyMap.items():
        epos = label.find('-')
        if epos == -1:
            temp = float(label)
        else:
            temp = float(label[0:label.find('-')])
            
        try:
            cp, alpha, kappa_t = attrByFluctuationMethod(df, temp)
        except Exception as exp:
            print(exp)
        
        volume_, molar_volume_, cohesive_energy_, cohesive_energy_density_, hildebrand_parameter_ = calculateCohesiveEnergy(df)
        cohesive_energy_ = [ce / 1000000.0 for ce in cohesive_energy_]
    
        # calculate the sample rate of the data frame
        offset = df['itr'][0]
        srate = df['itr'][1] - df['itr'][0]
        sample_rate.append(srate)
        # generate density profile for this temperature
        density_values = [dens * 1000.0 for dens in df['density']]
        density_list.append([label, density_values])
        enthalpy_list.append([label, [hartreeToEv(eth) / n_particles for eth in df['enthalpy']]])
        inter_pe_list.append([label, [hartreeToEv(v) / n_particles for v in  df['tot-inter']]])
        intra_pe_list.append([label, [hartreeToEv(v) / n_oligomers for v in  df['tot-intra']]])
        vol_list.append([label, volume_])
        mol_vol_list.append([label, molar_volume_])
        coh_list.append([label, cohesive_energy_])
        coh_den_list.append([label, cohesive_energy_density_])
        sol_list.append([label, hildebrand_parameter_])
        
        # collect the average statistics for the systems
        row = list()
        row.append(label)
        row.append(str(format(hartreeToEv(statistics.mean(df['tot']))/n_particles, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(statistics.stdev(df['tot']))/n_particles, sig_fig_format)))
        row.append(str(format(hartreeToEv(statistics.mean(df['tot-inter']))/n_particles, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(statistics.stdev(df['tot-inter']))/n_particles, sig_fig_format)))
        row.append(str(format(hartreeToEv(statistics.mean(df['tot-intra']))/n_particles, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(statistics.stdev(df['tot-intra']))/n_particles, sig_fig_format)))
        row.append(str(format(hartreeToEv(statistics.mean(df['enthalpy']))/n_particles, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(statistics.stdev(df['enthalpy']))/n_particles, sig_fig_format)))
        row.append(str(format(statistics.mean(density_values), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(density_values), sig_fig_format)))
        row.append(str(format(statistics.mean(volume_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(volume_), sig_fig_format)))
        row.append(str(format(statistics.mean(molar_volume_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(molar_volume_), sig_fig_format)))
        row.append(str(format(statistics.mean(cohesive_energy_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(cohesive_energy_), sig_fig_format)))
        row.append(str(format(statistics.mean(cohesive_energy_density_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(cohesive_energy_density_), sig_fig_format)))
        row.append(str(format(statistics.mean(hildebrand_parameter_), sig_fig_format)) + ' ' + '\xB1' + str(format(statistics.stdev(hildebrand_parameter_), sig_fig_format)))
        row.append(str(format(cp, sig_fig_format)))
        row.append(str(format(alpha, sig_fig_format)))
        row.append(str(format(kappa_t, sig_fig_format)))
 
        stats.append(row)


    columns = ['Temperature (K)', 'Total Potential Energy (eV)', 'Inter Potential Energy (eV)', 
               'Intra Potential Energy (eV)', 'Enthalpy (eV)', 
               'Density (kg/m^3)', 'Volume (nm^3)','molar-volume(nm^3)',
               'cohesive-energy(MJ/mol)', 'cohesive-energy-density(J/cm^3)',
                      'hildebrand(J/cm^3)^1/2', 'Cp(J/kg*K', 'Alpha', 
                      'Kappa_t']
    
    out_df = pd.DataFrame(stats, columns=columns)
    out_df.to_csv(src_data + '/sys_energies.csv', index=False)
    

    showlegend = True
    i_sample_rate = int(sample_rate[0])
    offset /= i_sample_rate
    #offset += 1000
    
    
    # density plot
    plotProfile(density_list, 'density', 'Iteration (x' + str(i_sample_rate) +')', 'Density ( $\mathregular{kg/m^{3}}$ )', offset, showlegend)
    plotProfile(enthalpy_list, 'enthalpy', 'Iteration (x' + str(i_sample_rate) +')', 'Enthalpy / particle (eV)', offset, showlegend)
    plotProfile(inter_pe_list, 'inter-pe', 'Iteration (x' + str(i_sample_rate) +')', 'Inter-Potential (eV / particle)', offset, showlegend)
    plotProfile(intra_pe_list, 'intra_pe', 'Iteration (x' + str(i_sample_rate) +')', 'Intra-Potential (eV / particle)', offset, showlegend)
    plotProfile(vol_list, 'volume', 'Iteration (x' + str(i_sample_rate) +')', 'Volume ($\mathregular{nm^{3}}$)', offset, showlegend)
    plotProfile(mol_vol_list, 'mol-vol', 'Iteration (x' + str(i_sample_rate) +')', 'Molar Volume ($\mathregular{nm^{3}}$)', offset, showlegend)
    plotProfile(coh_list, 'coh', 'Iteration (x' + str(i_sample_rate) +')', 'Cohesive Energy (MJ/mol)', offset, showlegend)
    plotProfile(coh_den_list, 'coh-den', 'Iteration (x' + str(i_sample_rate) +')', 'Cohesive Energy Density($\mathregular{J/cm^3}}$)', offset, showlegend)
    plotProfile(sol_list, 'solubility', 'Iteration (x' + str(i_sample_rate) +')', 'Hildebrand Parameter$\mathregular{(J/cm^{3})^{1/2}}$', offset, showlegend)


def plotProfile(data_list, name, xlabel, ylabel, offset=0, showlegend=True):
    minv = data_list[0][1][0]
    maxv = data_list[0][1][0]
    minx = data_list[0][0][0]
    maxx = data_list[0][0][0]

    mpl.rcParams['font.family'] = 'Avenir'
    plt.rcParams['font.size'] = 25
    plt.rcParams['axes.linewidth'] = 2

    lns = list()
    fig, ax1 = plt.subplots(figsize=(10,6))
    ax1.xaxis.set_tick_params(which='major', size=10, width=2, direction='in', top='on')
    ax1.yaxis.set_tick_params(which='major', size=10, width=2, direction='in', right='on')
    for row in data_list:
        x = [i + offset for i in range(len(row[1]))] 
        lns = lns + ax1.plot(x, row[1], label=row[0], linewidth=1) 
        if minv > min(row[1]):
            minv = min(row[1])
        if maxv < max(row[1]):
            maxv = max(row[1])

        if minx > min(row[0]):
            minx = min(row[0])
        if maxx < max(row[0]):
            maxx = max(row[0])

    # show a legend on the plot 
    if showlegend == True:
        labs = [l.get_label() for l in lns]
        ax1.legend(lns, labs, loc=0)

    miny = minv - (abs(minv - maxv) * 0.15)
    maxy = maxv + (abs(minv - maxv) * 0.15)
    plt.ylim(miny, maxy)
    plt.margins(x=0)

    
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(src_data + '/' + name + '-profile-plot.pdf', dpi=1200)


def combine_data():
    sys_energy_file_pattern = 'sys_energies-no-std'
    sys_stats_file_pattern = 'sys_stats-no-std'
    df_list = list()
    
    for root, dirs, files in os.walk(REPORT_DIR):  
        for filename in files:
            name, ext = os.path.splitext(filename)
            if ext != '.csv':
                continue
            
            if filename.find(sys_energy_file_pattern) != -1:
                # determine how many rows need to be skipped
                df_list.append(pd.read_csv(root + '/' + filename, header=[0]))
                
    # concatenate the datframes
    combined_df = pd.concat(df_list)
    combined_df = combined_df.fillna(0)

    
    # Plot
    fig = plt.figure()
    miny = min(combined_df.iloc[:, 1])
    maxy = max(combined_df.iloc[:, 1])
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(combined_df.iloc[:, 0], combined_df.iloc[:, 1])
    regression_line = [(m*x)+b for x in combined_df.iloc[:, 0]]
    plt.plot(combined_df.iloc[:, 0], regression_line)
    
    plt.scatter(combined_df.iloc[:, 0], combined_df.iloc[:, 1])
    plt.xlabel('Temperature (K)')
    plt.ylabel('Potential Energy (eV)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(REPORT_DIR + '/combined-pe-plot.png')
    
    fig = plt.figure()
    miny = min(combined_df.iloc[:, 7])
    maxy = max(combined_df.iloc[:, 7])
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(combined_df.iloc[:, 0], combined_df.iloc[:, 7])
    regression_line = [(m*x)+b for x in combined_df.iloc[:, 0]]
    plt.plot(combined_df.iloc[:, 0], regression_line)

    plt.scatter(combined_df.iloc[:, 0], combined_df.iloc[:, 7])
    plt.xlabel('Temperature (K)')
    plt.ylabel('Enthalpy (eV)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(REPORT_DIR + '/combined-enthalpy-plot.png')

    fig = plt.figure()
    miny = min(combined_df.iloc[:, 9])
    maxy = max(combined_df.iloc[:, 9])
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(combined_df.iloc[:, 0], combined_df.iloc[:, 9])
    regression_line = [(m*x)+b for x in combined_df.iloc[:, 0]]
    plt.plot(combined_df.iloc[:, 0], regression_line)

    plt.scatter(combined_df.iloc[:, 0], combined_df.iloc[:, 9])
    plt.xlabel('Temperature (K)')
    plt.ylabel('Density (g/cm^3)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(REPORT_DIR + '/combined-density-plot.png')

    fig = plt.figure()
    miny = min(combined_df.iloc[:, 13])
    maxy = max(combined_df.iloc[:, 13])
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(combined_df.iloc[:, 0], combined_df.iloc[:, 13])
    regression_line = [(m*x)+b for x in combined_df.iloc[:, 0]]
    plt.plot(combined_df.iloc[:, 0], regression_line)

    plt.scatter(combined_df.iloc[:, 0], combined_df.iloc[:, 13])
    plt.xlabel('Temperature (K)')
    plt.ylabel('Molar Volume (cm^3)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(REPORT_DIR + '/combined-molar-volume-plot.png')

    fig = plt.figure()
    miny = min(combined_df.iloc[:, 15])
    maxy = max(combined_df.iloc[:, 15])
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(combined_df.iloc[:, 0], combined_df.iloc[:, 15])
    regression_line = [(m*x)+b for x in combined_df.iloc[:, 0]]
    plt.plot(combined_df.iloc[:, 0], regression_line)

    plt.scatter(combined_df.iloc[:, 0], combined_df.iloc[:, 15])
    plt.xlabel('Temperature (K)')
    plt.ylabel('Cohesive Energy Density (J/m^3)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(REPORT_DIR + '/combined-cohesive-energy-density-plot.png')

    fig = plt.figure()
    miny = min(combined_df.iloc[:, 14])
    maxy = max(combined_df.iloc[:, 14])
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(combined_df.iloc[:, 0], combined_df.iloc[:, 14])
    regression_line = [(m*x)+b for x in combined_df.iloc[:, 0]]
    plt.plot(combined_df.iloc[:, 0], regression_line)

    ax = plt.scatter(combined_df.iloc[:, 0], combined_df.iloc[:, 14])
    plt.xlabel('Temperature (K)')
    plt.ylabel('Cohesive Energy (J/mol)')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(REPORT_DIR + '/combined-cohesive-energy-plot.png')

    fig = plt.figure()
    miny = min(combined_df.iloc[:, 16])
    maxy = max(combined_df.iloc[:, 16])
    plt.ylim(miny, maxy)
    # calculate best fit line
    m,b = best_fit_slope(combined_df.iloc[:, 0], combined_df.iloc[:, 16])
    regression_line = [(m*x)+b for x in combined_df.iloc[:, 0]]
    plt.plot(combined_df.iloc[:, 0], regression_line)

    plt.scatter(combined_df.iloc[:, 0], combined_df.iloc[:, 16])
    plt.xlabel('Temperature (K)')
    plt.ylabel('Hildebrand Parameter')
    # save the figure
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.savefig(REPORT_DIR + '/combined-hildebrand-plot.png')
    
    # now calculate average 
    avg_combined_df = combined_df.groupby(combined_df.iloc[:,0]).mean()
    avg_combined_df.to_csv(REPORT_DIR + '/sys_energies.csv')


def calculateBulkModulus():
    cuSMMC_HOME = 'bin/Release'
    cuSMMC = cuSMMC_HOME + '/cuSMMC'
    conf_file = 'cuSMMC/appConfig.conf'
    sys_config_dir = 'C:/Users/greg/Documents/analysis/bm-test2'
    sys_configs = list()
    sys_energy_file_pattern = '.bin'
    
    cnt = 0
    bulk_modulus = 0
    bulk_modulusMean = 0
    bulk_modulusSumsqrd = 0

    
    for root, dirs, files in os.walk(sys_config_dir):  
        for filename in files:
            name, ext = os.path.splitext(filename)
            if ext == sys_energy_file_pattern:
                fullFileName = root + '/' + filename
                cmd = 'mpiexec -n 1 ' + cuSMMC + ' -n 8 -t 600 -p 1 -s 2 -c ' + conf_file + ' -f ' + fullFileName

                # execute the simulation
                os.system(cmd)
                
                # now parse the results
                bm_df = pd.read_csv('energy-volume.csv')
                
                # now calculate the bulk modulus
                plt.figure()
                x = bm_df[' volume']
                y = bm_df['total']
                
                val, idxy = min((val, idx) for (idx, val) in enumerate(y))
                idxx = idxy
                miny = min(y[idxy-3:idxy+3])
                maxy = max(y[idxy-3:idxy+3])
                
                plt.ylim(miny, maxy)
                plt.scatter(x[idxx-3:idxx+3], y[idxy-3:idxy+3])
                z = np.polyfit(x[idxx-3:idxx+3], y[idxy-3:idxy+3], 2)

                bulk_modulus = 160.2*2*min(x) *1000*z[0]
                
                cnt += 1
                bulk_modulusDelta = bulk_modulus - bulk_modulusMean
                bulk_modulusMean = bulk_modulusMean + bulk_modulusDelta / cnt
                bulk_modulusSumsqrd = bulk_modulusSumsqrd + bulk_modulusDelta * (bulk_modulus - bulk_modulusMean)

                print('Instant: ' + str(bulk_modulus) + '\tAverage: ' + str(bulk_modulusMean) + '\tStd Dev: ' + str(math.sqrt(bulk_modulusSumsqrd / cnt)))
                #sys.exit()


def processTemperingResults():
    systemEnergyMap = {}
    temperature                 = list()
    tot_pe_energy               = list()
    inter_pe_energy             = list()
    intra_pe_energy             = list()
    enthalpy                    = list()
    volume                      = list()
    density                     = list()
    molar_volume                = list()
    cohesive_energy             = list()
    cohesive_energy_density     = list()
    hildebrand_parameter        = list()
    systemEnergyMapStats        = {}
    systemEnergyMapStats_no_std = {}
    sig_fig_format = '0.2f'
    
    print(src_data)
    for root, dirs, files in os.walk(src_data):  
        for filename in files:
            fname, ext = os.path.splitext(filename)
            if fname.find('tempering_events') != -1 and ext == '.csv':
                df = pd.read_csv(root + '/' + filename, header="infer")
                
                averaged_by_temperature = df.groupby(['temperature']).mean()
                for temp, data in averaged_by_temperature.iterrows():
                    inter_pe = hartreeToEv(data['avg_inter'])/n_particles
                    intra_pe = hartreeToEv(data['avg_intra'])/n_particles
                    tot_pe   = hartreeToEv(data['avg_tot_pot']) / n_particles
                    enthalpy_ = hartreeToEv(data['avg_enthalpy']) / n_particles
                    density_  = data['density']
                    volume_   = data['volume'] * b_to_a**3 * a_to_nm**3
                    molar_volume_            = volume_ / n_units 
                    cohesive_energy_         = -inter_pe * ev_to_joule * avogadro
                    cohesive_energy_density_ = -inter_pe * ev_to_joule / (molar_volume_ * nm3_to_cm3)
                    hildebrand_parameter_    = math.sqrt(cohesive_energy_density_)
                    
                    temperature.append(temp)
                    tot_pe_energy.append(tot_pe)
                    inter_pe_energy.append(inter_pe)
                    intra_pe_energy.append(intra_pe)
                    enthalpy.append(enthalpy_)
                    density.append(density_)
                    volume.append(volume_) 	
                    molar_volume.append(molar_volume_)
                    cohesive_energy.append(cohesive_energy_)
                    cohesive_energy_density.append(cohesive_energy_density_)
                    hildebrand_parameter.append(hildebrand_parameter_)
             		
                    '''
                    stats = list()
                    val = str(format(tot_pe, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(df['tot'].std())/n_particles, sig_fig_format))
                    stats.append(val)
                    val = str(format(inter_pe, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(df['tot-inter'].std()), sig_fig_format))
                    stats.append(val)
                    val = str(format(intra_pe, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(df['tot-intra'].std()), sig_fig_format))
                    stats.append(val)
                    val = str(format(enthalpy_, sig_fig_format)) + ' ' + '\xB1' + str(format(hartreeToEv(df[' enthalpy'].std()), sig_fig_format))
                    stats.append(val)
                    val = str(format(density_, sig_fig_format)) + ' ' + '\xB1' + str(format(df[' density'].std(), sig_fig_format))
                    stats.append(val)
                    val = str(format(volume_, sig_fig_format)) + ' ' + '\xB1' + str(format(df['volume'].std() * b_to_a**3 * a_to_nm**3, sig_fig_format))
                    stats.append(val)        
                    systemEnergyMapStats.setdefault(temp, stats)
                    '''
                    
                    # no standard deviation
                    stats_no_std = list()
                    stats_no_std.append(str(format(tot_pe, sig_fig_format)))
                    #stats_no_std.append(str(format(hartreeToEv(df['tot'].std())/n_particles, sig_fig_format)))
                    
                    stats_no_std.append(str(format(inter_pe, sig_fig_format)))
                    #stats_no_std.append(str(format(hartreeToEv(df['tot-inter'].std()), sig_fig_format)))
                    
                    stats_no_std.append(str(format(intra_pe, sig_fig_format)))
                    #stats_no_std.append(str(format(hartreeToEv(df['tot-intra'].std()), sig_fig_format)))
                    
                    stats_no_std.append(str(format(enthalpy_, sig_fig_format)))
                    #stats_no_std.append(str(format(hartreeToEv(df[' enthalpy'].std()), sig_fig_format)))
                    
                    stats_no_std.append(str(format(density_, sig_fig_format)))
                    #stats_no_std.append(str(format(df[' density'].std(), sig_fig_format)))
                    
                    stats_no_std.append(str(format(volume_, sig_fig_format)))        
                    #stats_no_std.append(str(format(df['volume'].std() * b_to_a**3 * a_to_nm**3, sig_fig_format)))        
            
                    val = str('{:.4e}'.format(molar_volume_))
                    stats_no_std.append(val)
                    val = str('{:.4e}'.format(cohesive_energy_))
                    stats_no_std.append(val)
                    val = str('{:.4e}'.format(cohesive_energy_density_))
                    stats_no_std.append(val)
                    val = str('{:.4e}'.format(hildebrand_parameter_))
                    stats_no_std.append(val)
            
                    systemEnergyMapStats_no_std.setdefault(temp, stats_no_std)
                    

    
                generatePotentialEnergyGraphs(temperature, tot_pe_energy, inter_pe_energy,
                                              intra_pe_energy, enthalpy)
                generateDensityGraphs(temperature, density, volume)
    
                columns_no_std = ['Total Potential Energy (eV)', 'Inter Potential Energy (eV)', 
                                  'Intra Potential Energy (eV)', 'Enthalpy (eV)', 
                                  'Density (g/cm^3)', 'Volume (nm^3)', 'molar-volume(nm^3)', 
                                  'cohesive-energy(J)', 'cohesive-energy-density(J/cm^3)',
                                  'hildebrand']
                energyDF = pd.DataFrame.from_dict(systemEnergyMapStats_no_std, 
                                                  orient='index', columns=columns_no_std)
                energyDF.to_csv(src_data + '/sys_energies-no-std.csv')
                
                # calculate molar volume, specific heat at constant volume, specific heat \
                # at constant pressure, binding energy, cohesive energy, cohesive energy \
                # density, and hildebrand solubility paramter
                tx = np.zeros(len(temperature))
                py = np.zeros(len(tot_pe_energy))
                ey = np.zeros(len(enthalpy))
                    
                for idx in range(len(temperature)):
                    tx[idx] = (sys_mass * amu_to_kg) * float(temperature[idx])
                    
                for idx in range(len(tot_pe_energy)):
                    py[idx] = ev_to_joule * tot_pe_energy[idx] * n_particles
            
                for idx in range(len(enthalpy)):
                    ey[idx] = ev_to_joule * enthalpy[idx] * n_particles
                
                # calculate low temperature cp and cv
                interval = int(len(tx) / 4)
                cp_low,b    = best_fit_slope(tx[0:interval+1]          ,ey[0:interval+1])
                cp_medium,b = best_fit_slope(tx[interval:interval*2 +1],ey[interval:interval*2 +1])
                cp_high,b   = best_fit_slope(tx[interval*2:len(tx)]    ,ey[interval*2:len(tx)])
                
                fout = open(src_data + '/specific-heat.txt', 'wt')
                fout.write('cp_low,' + str(cp_low) + '\n')
                fout.write('cp_medium,' + str(cp_medium) + '\n')
                fout.write('cp_high,' + str(cp_high) + '\n')
                fout.flush()
                fout.close()
                
                print('cp_low,' + str(cp_low) + '\n')
                print('cp_medium,' + str(cp_medium) + '\n')
                print('cp_high,' + str(cp_high) + '\n')

                # Plot
                fig = plt.figure()
                miny = min(tot_pe_energy)
                maxy = max(tot_pe_energy)
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, tot_pe_energy)
                regression_line = [(m*x)+b for x in temperature]
                plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, tot_pe_energy)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Potential Energy (eV)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-pe-plot.png')
                
                fig = plt.figure()
                miny = min(enthalpy)
                maxy = max(enthalpy)
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, enthalpy)
                regression_line = [(m*x)+b for x in temperature]
                plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, enthalpy)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Enthalpy (eV)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-enthalpy-plot.png')
                
                fig = plt.figure()
                miny = min(density)
                maxy = max(density)
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, density)
                regression_line = [(m*x)+b for x in temperature]
                plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, density)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Density (g/cm^3)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-density-plot.png')
            
                fig = plt.figure()
                miny = min(molar_volume)
                maxy = max(molar_volume)
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, molar_volume)
                regression_line = [(m*x)+b for x in temperature]
                plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, molar_volume)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Molar Volume (cm^3)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-molar-volume-plot.png')
            
                fig = plt.figure()
                miny = min(cohesive_energy_density)
                maxy = max(cohesive_energy_density)
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, cohesive_energy_density)
                regression_line = [(m*x)+b for x in temperature]
                plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, cohesive_energy_density)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Cohesive Energy Density (J/m^3)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-cohesive-energy-density-plot.png')
            
                fig = plt.figure()
                miny = min(cohesive_energy)
                maxy = max(cohesive_energy)
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, cohesive_energy)
                regression_line = [(m*x)+b for x in temperature]
                plt.plot(temperature, regression_line)
            
                ax = plt.scatter(temperature, cohesive_energy)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Cohesive Energy (J/mol)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-cohesive-energy-plot.png')
            
                fig = plt.figure()
                miny = min(hildebrand_parameter)
                maxy = max(hildebrand_parameter)
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, hildebrand_parameter)
                regression_line = [(m*x)+b for x in temperature]
                plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, hildebrand_parameter)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Hildebrand Parameter')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-hildebrand-plot.png')

                
def processTemperingResultsGroupBy():
    systemEnergyMap = {}
    temperature                 = list()
    tot_pe_energy               = list()
    inter_pe_energy             = list()
    intra_pe_energy             = list()
    enthalpy                    = list()
    enthalpy_configurational    = list()
    volume                      = list()
    density                     = list()
    molar_volume                = list()
    cohesive_energy             = list()
    cohesive_energy_density     = list()
    hildebrand_parameter        = list()
    systemEnergyMapStats        = {}
    systemEnergyMapStats_no_std = {}
    bins = {}
    bin_size = 1
    pressure = 1.0
    point_size = 1
    
    for root, dirs, files in os.walk(src_data):  
        for filename in files:
            fname, ext = os.path.splitext(filename)

            if fname.find('tempering_events') != -1 and ext == '.csv':
                df = pd.read_csv(root + '/' + filename, header="infer")

                for temp, data in df.iterrows():
                    idx = int(data['temperature'] / bin_size)
                    values = list()
                    if idx not in bins:
                        bins[idx] = values

                    if idx in bins:
                        values = bins[idx]
                        values.append(data)

                results = {}
                for key, values in bins.items():
                    avg_intra                   = 0.0
                    avg_inter                   = 0.0
                    avg_tot                     = 0.0
                    avg_enthalpy                = 0.0
                    avg_density                 = 0.0
                    avg_volume                  = 0.0
                    avg_cohesive_energy         = 0.0
                    avg_cohesive_energy_density = 0.0
                    avg_hildebrande_parameter   = 0.0
                    avg_enthalpy_config         = 0.0
                    
                    for entry in values:
                        avg_intra                   += entry[2]
                        avg_inter                   += entry[4]
                        avg_tot                     += entry[6]
                        avg_enthalpy                += entry[8]
                        avg_enthalpy_config         += (entry[2] + entry[4] + pressure * entry[15])
                        avg_density                 += entry[13]
                        avg_volume                  += entry[15]                       
                                            
                    avg_intra = hartreeToEv(avg_intra / len(values)) /n_particles
                    avg_inter = hartreeToEv(avg_inter / len(values)) /n_particles
                    avg_tot = hartreeToEv(avg_tot / len(values)) /n_particles
                    avg_enthalpy = hartreeToEv(avg_enthalpy / len(values)) /n_particles
                    avg_enthalpy_config = hartreeToEv(avg_enthalpy_config / len(values)) / n_particles
                    avg_density = avg_density / len(values)
                    avg_volume = avg_volume / len(values) * b_to_a**3 * a_to_nm**3
                    avg_mol_volume = avg_volume / n_units
                    avg_cohesive_energy = -avg_inter * ev_to_joule * avogadro
                    avg_cohesive_energy_density = -avg_inter * ev_to_joule / (avg_mol_volume * nm3_to_cm3)
                    avg_hildebrand_parameter = math.sqrt(avg_cohesive_energy_density)
                    
                    row = list()
                    row.append(avg_intra)
                    row.append(avg_inter)
                    row.append(avg_tot)
                    row.append(avg_enthalpy)
                    row.append(avg_density)                    
                    row.append(avg_volume)
                    row.append(avg_mol_volume)
                    row.append(avg_cohesive_energy)
                    row.append(avg_cohesive_energy_density)
                    row.append(avg_hildebrand_parameter)
                    
                    results[key * bin_size] = row
                
                header = ['avg_intra', 'avg_inter', 'avg_tot', 'avg_enthalpy',
                          'avg_density', 'avg_volume', 'avg_mol_volume', 
                          'avg_cohesive_energy', 'avg_cohesive_energy_density',
                          'avg_hildebrand_parameter']
                results_df = pd.DataFrame.from_dict(results, orient='index', columns=header) 
                results_df.to_csv(src_data + '/binned_tempering_results.csv')

                temperature = list(results.keys())
                tot_pe_energy = list(results_df['avg_tot'])
                enthalpy_ = list(results_df['avg_enthalpy'])

                # calculate molar volume, specific heat at constant volume, specific heat \
                # at constant pressure, binding energy, cohesive energy, cohesive energy \
                # density, and hildebrand solubility paramter
                tx = np.zeros(len(temperature))
                py = np.zeros(len(tot_pe_energy))
                ey = np.zeros(len(enthalpy_))
                    
                for idx in range(len(temperature)):
                    tx[idx] = (sys_mass * amu_to_kg) * float(temperature[idx])
                
                
                for idx in range(len(tot_pe_energy)):
                    py[idx] = ev_to_joule * hartreeToEv(tot_pe_energy[idx]) * n_particles
            
                for idx in range(len(enthalpy_)):
                    ey[idx] = ev_to_joule * hartreeToEv(enthalpy_[idx]) * n_particles
                
                # calculate low temperature cp and cv
                interval = int(len(tx) / 3)
                cp_low,b    = best_fit_slope(tx[0:interval+1]          ,ey[0:interval+1])
                cp_medium,b = best_fit_slope(tx[interval:interval*2 +1],ey[interval:interval*2 +1])
                cp_high,b   = best_fit_slope(tx[interval*2:len(tx)]    ,ey[interval*2:len(tx)])
                
                fout = open(src_data + '/specific-heat.txt', 'wt')
                fout.write('cp_low,' + str(cp_low) + '\n')
                fout.write('cp_medium,' + str(cp_medium) + '\n')
                fout.write('cp_high,' + str(cp_high) + '\n')
                fout.flush()
                fout.close()
                
                print('cp_low,' + str(cp_low) + '\n')
                print('cp_medium,' + str(cp_medium) + '\n')
                print('cp_high,' + str(cp_high) + '\n')


                # Plot
                fig = plt.figure()
                miny = min(results_df['avg_tot'])
                maxy = max(results_df['avg_tot'])
                plt.ylim(miny, maxy)
                # calculate best fit line
                print(results_df['avg_tot'])
                m,b = best_fit_slope(temperature, list(results_df['avg_tot']))
                regression_line = [(m*x)+b for x in temperature]
                #plt.plot(temperature, regression_line)
                
                plt.scatter(temperature, results_df['avg_tot'], color='black', s=point_size)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Potential Energy (eV)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-pe-plot.png')
                
                fig = plt.figure()
                miny = min(results_df['avg_enthalpy'])
                maxy = max(results_df['avg_enthalpy'])
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, list(results_df['avg_enthalpy']))
                regression_line = [(m*x)+b for x in temperature]
                #plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, list(results_df['avg_enthalpy']), color='black', s=point_size)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Enthalpy (eV)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-enthalpy-plot.png')
            
                fig = plt.figure()
                miny = min(results_df['avg_density'])
                maxy = max(results_df['avg_density'])
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, list(results_df['avg_density']))
                regression_line = [(m*x)+b for x in temperature]
                #plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, list(results_df['avg_density']), color='black', s=point_size)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Density (g/cm^3)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-density-plot.png')
            
                fig = plt.figure()
                miny = min(results_df['avg_mol_volume'])
                maxy = max(results_df['avg_mol_volume'])
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, list(results_df['avg_mol_volume']))
                regression_line = [(m*x)+b for x in temperature]
                #plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, list(results_df['avg_mol_volume']), color='black', s=point_size)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Molar Volume (cm^3)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-molar-volume-plot.png')
            
                fig = plt.figure()
                miny = min(results_df['avg_cohesive_energy_density'])
                maxy = max(results_df['avg_cohesive_energy_density'])
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, list(results_df['avg_cohesive_energy_density']))
                regression_line = [(m*x)+b for x in temperature]
                #plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, list(results_df['avg_cohesive_energy_density']), color='black', s=point_size)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Cohesive Energy Density (J/m^3)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-cohesive-energy-density-plot.png')
            
                fig = plt.figure()
                miny = min(results_df['avg_cohesive_energy'])
                maxy = max(results_df['avg_cohesive_energy'])
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, list(results_df['avg_cohesive_energy']))
                regression_line = [(m*x)+b for x in temperature]
                #plt.plot(temperature, regression_line)
            
                ax = plt.scatter(temperature, list(results_df['avg_cohesive_energy']), color='black', s=point_size)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Cohesive Energy (J/mol)')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-cohesive-energy-plot.png')
            
                fig = plt.figure()
                miny = min(results_df['avg_hildebrand_parameter'])
                maxy = max(results_df['avg_hildebrand_parameter'])
                plt.ylim(miny, maxy)
                # calculate best fit line
                m,b = best_fit_slope(temperature, list(results_df['avg_hildebrand_parameter']))
                regression_line = [(m*x)+b for x in temperature]
                #plt.plot(temperature, regression_line)
            
                plt.scatter(temperature, list(results_df['avg_hildebrand_parameter']), color='black', s=point_size)
                plt.xlabel('Temperature (K)')
                plt.ylabel('Hildebrand Parameter')
                # save the figure
                fig.tight_layout()  # otherwise the right y-label is slightly clipped
                plt.savefig(src_data + '/combined-hildebrand-plot.png')
                        
        
def attrByFluctuationMethod(df, temp1):
    if isinstance(temp1, str):
        epos = temp1.find('-')
        if epos == -1:
            temp = float(temp1)
        else:
            temp = float(temp1[0:temp1.find('-')])
    else:
        temp = temp1
        
    # calculate specific heat    
    enthalpy = [hartreeToEv(enth) for enth in df['enthalpy']]
    enthalpy_var = statistics.variance(enthalpy) 
    
    cp = ev_to_joule * (enthalpy_var / (K * float(temp) * float(temp) * sys_mass * amu_to_kg))
    print(str(temp) + ' ----- ' + str(cp) + ' ----')

    vol_m = [vol * b_to_a**3 * a_to_nm**3  for vol in df['volume']]
    vol_var = statistics.variance(vol_m)
    kappa_t = nm3_to_m3  * (vol_var) / ((K * float(temp) * statistics.mean(vol_m)) * ev_to_joule)
    print('kappa_t ------ ' + str(kappa_t) + ' -------')
    
    alpha = (enthalpy_var * vol_var) / (K * float(temp) * float(temp) * statistics.mean(vol_m))
    print('alpha_p ----- ' + str(alpha))       

    # new way to calculate cp
    pressure = 1.0 * 0.000101325
    kt_inverse = 1.0 / (K * temp**2)
    nk = n_particles * K
    
    t1 = list()
    t2 = list()
    enthalpy_configurational = list()
    
    tot_pe = [hartreeToEv(i) for i in df['tot']]
    
    for idx in range(len(vol_m)):
        enthalpy_config_ = tot_pe[idx] + pressure * vol_m[idx]
        enthalpy_configurational.append(enthalpy_config_)
        
        t1_ = tot_pe[idx] * enthalpy_config_
        t1.append(t1_)
        
        t2_ = vol_m[idx] * enthalpy_config_
        t2.append(t2_)
        
    cp_id = kt_inverse * (statistics.mean(t1) - statistics.mean(tot_pe) * statistics.mean(enthalpy_configurational))
    cp_res = kt_inverse * (statistics.mean(t2) - statistics.mean(vol_m) * statistics.mean(enthalpy_configurational))
    cp_2 = cp_id + cp_res - n_particles * K
    print('cp-2( '+ str(temp) + ') ----- ' + str(cp_2))
    
    return cp, alpha, kappa_t

def generateSystemStatsProfiles():
    sig_fig_format = '0.4f'
    offset = 0


    systemStatsMap = {}
    for root, dirs, files in os.walk(src_data):  
        for filename in files:
            if filename.find(sys_stats_root) != -1:
                temperature = (filename[filename.rfind('_') + 1:filename.rfind('.')])
                df = parseSysStats(root + '/' + filename)
                df = df.dropna()
                systemStatsMap.setdefault(temperature, df)
                
    temperature       = list()
    vector_order      = list()
    orientation_order = list()
    gyration          = list()
    avg_len           = list()
    systemStatsMapStats = {}
    systemStatsMapStats_no_std = {}
    vo_values_list = list()
    oo_values_list = list()
    rg_values_list = list()
    al_values_list = list()

    for temp, df in systemStatsMap.items():
        temperature.append(temp)
        vector_order.append(df['vectorOrder'].mean())
        orientation_order.append(df['orientationOrder'].mean())
        gyration.append(df['gyration'].mean())
        avg_len.append(df['alen'].mean())

        stats = list()
        val = str(format(df['vectorOrder'].mean(), sig_fig_format)) + ' ' + '\xB1' + str(format(df['vectorOrder'].std(), sig_fig_format))
        stats.append(val)

        val = str(format(df['orientationOrder'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['orientationOrder'].std(), sig_fig_format))
        stats.append(val)

        val = str(format(df['gyration'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['gyration'].std(), sig_fig_format))
        stats.append(val)
        
        val = str(format(df['alen'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['alen'].std(), sig_fig_format))
        stats.append(val)

        if 'lambda' in df.columns:
            val = str(format(df['lambda'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['lambda'].std(), sig_fig_format))
            stats.append(val)

        if 'dlambda' in df.columns:
            val = str(format(df['dlambda'].mean(), sig_fig_format)) + ' '  + "\u00B1" + str(format(df['dlambda'].std(), sig_fig_format))
            stats.append(val)
            
        systemStatsMapStats.setdefault(temp, stats)

        # no standard deviation
        stats_no_std = list()
        val = str(format(df['vectorOrder'].mean(), sig_fig_format))
        stats_no_std.append(val)

        val = str(format(df['orientationOrder'].mean(), sig_fig_format))
        stats_no_std.append(val)

        val = str(format(df['gyration'].mean(), sig_fig_format))
        stats_no_std.append(val)
        
        val = str(format(df['alen'].mean(), sig_fig_format))
        stats_no_std.append(val)

        if 'lambda' in df.columns:
            val = str(format(df['lambda'].mean(), sig_fig_format))
            stats_no_std.append(val)

        if 'dlambda' in df.columns:
            val = str(format(df['dlambda'].mean(), sig_fig_format))
            stats_no_std.append(val)
            
        systemStatsMapStats_no_std.setdefault(temp, stats_no_std)
        
        vo_values = [vo for vo in df['vectorOrder']]
        vo_values_list.append([temp, vo_values])

        oo_values = [vo for vo in df['orientationOrder']]
        oo_values_list.append([temp, oo_values])

        rg_values = [vo for vo in df['gyration']]
        rg_values_list.append([temp, rg_values])

        al_values = [vo for vo in df['alen']]
        al_values_list.append([temp, al_values])

    
    columns = ['Vector Order (S)', 'Orientation Order (Z)', 'Radius of Gyration (A)', 'Length (A)']    
    if len(systemStatsMapStats[list(systemStatsMapStats.keys())[0]]) == 6:
        columns = ['Vector Order (S)', 'Orientation Order (Z)', 'Radius of Gyration (A)', 'Length (A)', 'Monomer MSD (A)', 'Dopant MSD (A)']
        
    statsDF = pd.DataFrame.from_dict(systemStatsMapStats, orient='index', columns=columns)
    statsDF.to_csv(src_data + '/sys_stats.csv')

    statsDF = pd.DataFrame.from_dict(systemStatsMapStats_no_std, orient='index', columns=columns)
    statsDF.to_csv(src_data + '/sys_stats-no-std.csv')
    
    '''
    showlegend = False
    offset = offset
    i_sample_rate = 1
    
    print(vo_values_list)
    # density plot
    plotProfile(vo_values_list, 'vectorOrder', 'Iteration (x' + str(i_sample_rate) +')', 'units', offset, showlegend)
    plotProfile(oo_values_list, 'orientationOrder', 'Iteration (x' + str(i_sample_rate) +')', 'units', offset, showlegend)
    plotProfile(rg_values_list, 'gyration', 'Iteration (x' + str(i_sample_rate) +')', 'units', offset, showlegend)
    plotProfile(al_values_list, 'alen', 'Iteration (x' + str(i_sample_rate) +')', 'units', offset, showlegend)
    '''
        
if __name__== "__main__":
    #process_raw_data()
    #combine_data()
    #calculateBulkModulus()
    #processTemperingResults()
    #processTemperingResultsGroupBy()
    generateCombinedProfiles()
    
    #generateSystemStatsProfiles()
    