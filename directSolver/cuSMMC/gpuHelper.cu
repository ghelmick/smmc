#include "gpuHelper.cuh"
#include "cuUtil.cuh"

#define NUMBER_GPU_THREADS_DOP 64
#define NUMBER_GPU_THREADS_MON 256

DATA_TYPE* d_cm_x = NULL;
DATA_TYPE* d_cm_y = NULL;
DATA_TYPE* d_cm_z = NULL;
DATA_TYPE* d_dpm_x = NULL;
DATA_TYPE* d_dpm_y = NULL;
DATA_TYPE* d_dpm_z = NULL;
//DATA_TYPE * d_n_x = NULL;
//DATA_TYPE * d_n_y = NULL;
//DATA_TYPE * d_n_z = NULL;
DATA_TYPE* d_mon_results = NULL;
DATA_TYPE* mon_results = NULL;
DATA_TYPE* pydop_mon_mov = NULL;
DATA_TYPE* pydop_dop_mov = NULL;

DATA_TYPE* d_olig_cm_x = NULL;
DATA_TYPE* d_olig_cm_y = NULL;
DATA_TYPE* d_olig_cm_z = NULL;

DATA_TYPE* d_dop_x = NULL;
DATA_TYPE* d_dop_y = NULL;
DATA_TYPE* d_dop_z = NULL;
DATA_TYPE* d_dop_results = NULL;
DATA_TYPE* dop_results = NULL;
DATA_TYPE* d_pydop_mon_mov = NULL;
DATA_TYPE* d_pydop_dop_mov = NULL;

int nBlocksMonomers = 1;
int nBlocksDopants = 1;
int nThreadsMonomer = NUMBER_GPU_THREADS_MON;
int nThreadsDopant = NUMBER_GPU_THREADS_DOP;

cudaStream_t monomer_monomer_stream;
cudaStream_t monomer_dopant_stream;
cudaStream_t dopant_dopant_stream;
cudaStream_t dopant_monomer_stream;


__device__ bool   d_usePCB;
__device__ DATA_TYPE d_sigma_dopant;
__device__ DATA_TYPE d_Q_dop;
__device__ DATA_TYPE d_epsilon_dopant;
__device__ DATA_TYPE d_kappa;
__device__ DATA_TYPE d_kappa_inter_end;
__device__ DATA_TYPE d_x_size;
__device__ DATA_TYPE d_y_size;
__device__ DATA_TYPE d_z_size;
__device__ DATA_TYPE d_r_cut;
__device__ DATA_TYPE d_r_cut_sqrd;
__device__ DATA_TYPE d_Q_mono;
__device__ DATA_TYPE d_sigma_inter;
__device__ DATA_TYPE d_sigma_inter_end;
__device__ DATA_TYPE d_epsilon_inter;
__device__ DATA_TYPE d_epsilon_inter_end;
__device__ DATA_TYPE d_dp_mag;
__device__ DATA_TYPE d_A;
__device__ DATA_TYPE d_B;
__device__ DATA_TYPE d_C;
__device__ DATA_TYPE d_sigmaPyDopant;


void cleanup()
{
	// free the device memory
	cudaFree(d_cm_x);
	cudaFree(d_cm_y);
	cudaFree(d_cm_z);
	cudaFree(d_dpm_x);
	cudaFree(d_dpm_y);
	cudaFree(d_dpm_z);
	cudaFree(d_mon_results);
	cudaFree(mon_results);

	cudaFree(pydop_mon_mov);
	cudaFree(pydop_dop_mov);
	cudaFree(d_dop_x);
	cudaFree(d_dop_y);
	cudaFree(d_dop_z);
	cudaFree(d_dop_results);
	cudaFree(dop_results);
	cudaFree(d_pydop_mon_mov);
	cudaFree(d_pydop_dop_mov);

	// destroy the streams
	cudaStreamDestroy(monomer_monomer_stream);
	cudaStreamDestroy(monomer_dopant_stream);

	cudaStreamDestroy(dopant_dopant_stream);
	cudaStreamDestroy(dopant_monomer_stream);
}

void synchronizeGPU()
{
	cudaError_t cudaStatus = cudaSuccess;
	cudaStatus = cudaThreadSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
}

void allocateMemoryGPU(PARTICLE_SYSTEM& ps)
{
	cudaError err;
	if (ps.nDopants)
	{
		err = allocateMemoryOnDevice<DATA_TYPE>(&d_dop_x, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
		err = allocateMemoryOnDevice<DATA_TYPE>(&d_dop_y, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
		err = allocateMemoryOnDevice<DATA_TYPE>(&d_dop_z, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
	}

	err = allocateMemoryOnDevice<DATA_TYPE>(&d_cm_x, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&d_cm_y, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&d_cm_z, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}

	err = allocateMemoryOnDevice<DATA_TYPE>(&d_dpm_x, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&d_dpm_y, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&d_dpm_z, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}

	err = allocateMemoryOnDevice<DATA_TYPE>(&d_olig_cm_x, ps.nPolymers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&d_olig_cm_y, ps.nPolymers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = allocateMemoryOnDevice<DATA_TYPE>(&d_olig_cm_z, ps.nPolymers);
	if (err != cudaSuccess)
	{
		return;
	}

	//err = allocateMemoryOnDevice<DATA_TYPE>(&d_n_x, ps.nMonomers);
	//if (err != cudaSuccess)
	//{
	//	return;
	//}
	//err = allocateMemoryOnDevice<DATA_TYPE>(&d_n_y, ps.nMonomers);
	//if (err != cudaSuccess)
	//{
	//	return;
	//}
	//err = allocateMemoryOnDevice<DATA_TYPE>(&d_n_z, ps.nMonomers);
	//if (err != cudaSuccess)
	//{
	//	return;
	//}

}

void copyMemoryToGPU(PARTICLE_SYSTEM& ps)
{
	cudaError err;
	if (ps.nDopants)
	{
		err = copyToDevice<DATA_TYPE>(ps.dop_x, d_dop_x, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyToDevice<DATA_TYPE>(ps.dop_y, d_dop_y, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyToDevice<DATA_TYPE>(ps.dop_z, d_dop_z, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
	}

	err = copyToDevice<DATA_TYPE>(ps.cm_x, d_cm_x, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.cm_y, d_cm_y, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.cm_z, d_cm_z, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}

	err = copyToDevice<DATA_TYPE>(ps.dpm_x, d_dpm_x, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.dpm_y, d_dpm_y, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.dpm_z, d_dpm_z, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}

	err = copyToDevice<DATA_TYPE>(ps.olig_cm_x, d_olig_cm_x, ps.nPolymers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.olig_cm_y, d_olig_cm_y, ps.nPolymers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyToDevice<DATA_TYPE>(ps.olig_cm_z, d_olig_cm_z, ps.nPolymers);
	if (err != cudaSuccess)
	{
		return;
	}

	//err = copyToDevice<DATA_TYPE>(ps.n_x, d_n_x, ps.nMonomers);
	//if (err != cudaSuccess)
	//{
	//	return;
	//}
	//err = copyToDevice<DATA_TYPE>(ps.n_y, d_n_y, ps.nMonomers);
	//if (err != cudaSuccess)
	//{
	//	return;
	//}
	//err = copyToDevice<DATA_TYPE>(ps.n_z, d_n_z, ps.nMonomers);
	//if (err != cudaSuccess)
	//{
	//	return;
	//}

}

void copyMemoryFromGPU(PARTICLE_SYSTEM& ps)
{
	cudaError err;
	if (ps.nDopants)
	{
		err = copyFromDevice<DATA_TYPE>(d_dop_x, ps.dop_x, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyFromDevice<DATA_TYPE>(d_dop_y, ps.dop_y, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
		err = copyFromDevice<DATA_TYPE>(d_dop_z, ps.dop_z, ps.nDopants);
		if (err != cudaSuccess)
		{
			return;
		}
	}
	//
	err = copyFromDevice<DATA_TYPE>(d_cm_x, ps.cm_x, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyFromDevice<DATA_TYPE>(d_cm_y, ps.cm_y, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyFromDevice<DATA_TYPE>(d_cm_z, ps.cm_z, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	//
	err = copyFromDevice<DATA_TYPE>(d_dpm_x, ps.dpm_x, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyFromDevice<DATA_TYPE>(d_dpm_y, ps.dpm_y, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	err = copyFromDevice<DATA_TYPE>(d_dpm_z, ps.dpm_z, ps.nMonomers);
	if (err != cudaSuccess)
	{
		return;
	}
	//
		//err = copyFromDevice<DATA_TYPE>(d_n_x, ps.n_x, ps.nMonomers);
		//if (err != cudaSuccess)
		//{
		//	return;
		//}
		//err = copyFromDevice<DATA_TYPE>(d_n_y, ps.n_y, ps.nMonomers);
		//if (err != cudaSuccess)
		//{
		//	return;
		//}
		//err = copyFromDevice<DATA_TYPE>(d_n_z, ps.n_z, ps.nMonomers);
		//if (err != cudaSuccess)
		//{
		//	return;
		//}
}

void updateFullSystem(PARTICLE_SYSTEM& ps)
{
#if defined USE_GPU
	copyMemoryToGPU(ps);

	cudaError_t cudaStatus = cudaSuccess;
	cudaStatus = cudaThreadSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
#endif
}

void updateSystemSize(DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_)
{
#if defined USE_GPU
	d_updateSystemSize <<<1, 1 >>> (x_size_, y_size_, z_size_, r_cut_);
	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_updateSystemSize Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}
#endif
}

void updateMonomer(PARTICLE_SYSTEM& ps, int midx)
{
#if defined USE_GPU
	// calculate the partial energy
	d_updateMonomer <<<1, 1 >>> (d_cm_x, d_cm_y, d_cm_z, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx],
		d_dpm_x, d_dpm_y, d_dpm_z, ps.dpm_x[midx], ps.dpm_y[midx], ps.dpm_z[midx], midx);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_updateMonomer Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	cudaStatus = cudaSuccess;
	cudaStatus = cudaThreadSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

	cudaStatus = cudaSuccess;
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;
#endif

}

void updateDopant(PARTICLE_SYSTEM& ps, int didx)
{
#if defined USE_GPU
	if (ps.nDopants)
	{
		d_updateDopant <<<1, 1 >>> (d_dop_x, d_dop_y, d_dop_z, ps.dop_x[didx], ps.dop_y[didx], ps.dop_z[didx], didx);
		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "d_updateDopant Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}
		cudaStatus = cudaSuccess;
		cudaStatus = cudaThreadSynchronize();
		if (cudaStatus != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		cudaStatus = cudaSuccess;
		cudaStatus = cudaDeviceSynchronize();
		if (cudaStatus != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;
	}
#endif
}

void initGPUDataStructures(PARTICLE_SYSTEM& ps, int mlb, int mub, int dlb, int dub, int number_gpu_threads_mon, int number_gpu_threads_dop)
{
	nThreadsMonomer = number_gpu_threads_mon;
	nThreadsDopant = number_gpu_threads_dop;

	nBlocksDopants =
		((dub - dlb) + nThreadsDopant - 1)
		/ nThreadsDopant;
	nBlocksMonomers =
		((mub - mlb) + nThreadsMonomer - 1)
		/ nThreadsMonomer;

	cudaError_t retVal = cudaSuccess;

	if (ps.nDopants)
	{
		// setup and copy data to the gpu
		// first allocate memory on the host to store the results
		retVal = cudaMallocHost(
			(void**)&dop_results,
			nBlocksDopants * sizeof(DATA_TYPE));
		if (retVal != cudaSuccess)
			cout << "Unable to allocate block energy on host" << endl;

		// allocate energies in shared memory
		retVal = cudaMalloc(
			(void**)&d_dop_results,
			nBlocksDopants * sizeof(DATA_TYPE));
		if (retVal != cudaSuccess)
			cout << "Unable to allocate monomer energies on GPU" << endl;

		// allocate energies in shared memory
		retVal = cudaMallocHost(
			(void**)&pydop_mon_mov,
			nBlocksDopants * sizeof(DATA_TYPE));
		if (retVal != cudaSuccess)
			cout << "Unable to allocate block energy on host" << endl;

		// allocate energies in shared memory
		retVal = cudaMalloc(
			(void**)&d_pydop_mon_mov,
			nBlocksDopants * sizeof(DATA_TYPE));
		if (retVal != cudaSuccess)
			cout << "Unable to allocate monomer energies on GPU" << endl;

		// allocate energies in shared memory
		retVal = cudaMallocHost(
			(void**)&pydop_dop_mov,
			nBlocksMonomers * sizeof(DATA_TYPE));
		if (retVal != cudaSuccess)
			cout << "Unable to allocate block energy on host" << endl;

		// allocate energies in shared memory
		retVal = cudaMalloc(
			(void**)&d_pydop_dop_mov,
			nBlocksMonomers * sizeof(DATA_TYPE));
		if (retVal != cudaSuccess)
			cout << "Unable to allocate monomer energies on GPU" << endl;

		retVal = cudaStreamCreate(&monomer_dopant_stream);
		if (retVal != cudaSuccess)
		{
			fprintf(stderr, "Failed to create stream (monomer_dopant_stream): %s\n", cudaGetErrorString(retVal));
		}
		retVal = cudaStreamCreate(&dopant_dopant_stream);
		if (retVal != cudaSuccess)
		{
			fprintf(stderr, "Failed to create stream (dopant_dopant_stream): %s\n", cudaGetErrorString(retVal));
		}
		retVal = cudaStreamCreate(&dopant_monomer_stream);
		if (retVal != cudaSuccess)
		{
			fprintf(stderr, "Failed to create stream (dopant_monomer_stream): %s\n", cudaGetErrorString(retVal));
		}
	}

	// allocate energies in shared memory
	retVal = cudaMallocHost(
		(void**)&mon_results,
		nBlocksMonomers * sizeof(DATA_TYPE));
	if (retVal != cudaSuccess)
		cout << "Unable to allocate block energy on host" << endl;

	// allocate energies in shared memory
	retVal = cudaMalloc(
		(void**)&d_mon_results,
		nBlocksMonomers * sizeof(DATA_TYPE));
	if (retVal != cudaSuccess)
		cout << "Unable to allocate monomer energies on GPU" << endl;


	// create the streams
	retVal = cudaStreamCreate(&monomer_monomer_stream);
	if (retVal != cudaSuccess)
	{
		fprintf(stderr, "Failed to create stream (monomer_monomer_stream): %s\n", cudaGetErrorString(retVal));
	}

	// allocate memory
	allocateMemoryGPU(ps);
	// copy data to the GPU
	copyMemoryToGPU(ps);
}

DATA_TYPE getDeviceDopDopPart(PARTICLE_SYSTEM& ps, int dIdx, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	DATA_TYPE dopdop = 0.0;
	if (ps.nDopants)
	{
		// calculate the partial energy
		d_DopDopPart <<<nBlocksDopants, nThreadsDopant, nThreadsDopant * sizeof(DATA_TYPE), dopant_dopant_stream >>> (d_dop_x, d_dop_y, d_dop_z, d_dop_results, ps.nDopants, dIdx, dlb, dub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "d_DopDopPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}

		// Copy output to CPU output array
		cudaError_t retVal = cudaMemcpyAsync(
			dop_results,
			d_dop_results,
			nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, dopant_dopant_stream);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaSuccess;
		retVal = cudaThreadSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksDopants; i++)
			dopdop += dop_results[i];
	}
	return dopdop;
}

DATA_TYPE getDeviceTotalInter(PARTICLE_SYSTEM& ps, int mlb, int mub, int dlb, int dub, DATA_TYPE& upypy, DATA_TYPE& upydop, DATA_TYPE& udopdop, bool removeDouble)
{
	upypy = 0.0;
	upydop = 0.0;
	udopdop = 0.0;

	// calculate the partial energy
	d_PyPyTotalInter <<<nBlocksMonomers, nThreadsMonomer, nThreadsMonomer * sizeof(DATA_TYPE), monomer_monomer_stream >>> (d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z,
		d_mon_results, mlb, mub, ps.monomers_per_chain, ps.nMonomers, removeDouble);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "getDevicePyPyPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopTotalInter <<<nBlocksMonomers, nThreadsMonomer, nThreadsMonomer * sizeof(DATA_TYPE), dopant_monomer_stream >>> (d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z,
			d_dop_x, d_dop_y, d_dop_z, d_pydop_dop_mov, ps.nDopants, ps.nMonomers, mlb, mub);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}

		// calculate the partial energy
		d_DopDopTotalInter <<<nBlocksDopants, nThreadsDopant, nThreadsDopant * sizeof(DATA_TYPE), dopant_dopant_stream >>> (d_dop_x, d_dop_y, d_dop_z, d_dop_results, ps.nDopants, dlb, dub);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}
	}

	// Copy output to CPU output array
	cudaError_t retVal = cudaMemcpyAsync(
		mon_results,
		d_mon_results,
		nBlocksMonomers * sizeof(DATA_TYPE),
		cudaMemcpyDeviceToHost, monomer_monomer_stream);
	if (retVal != cudaSuccess)
		cout << "Unable to copy energies to CPU" << endl;

	retVal = cudaSuccess;
	retVal = cudaThreadSynchronize();
	if (retVal != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

	for (int i = 0; i < nBlocksMonomers; i++)
		upypy += mon_results[i];

	if (ps.nDopants)
	{
		// now get the monomer-dopant values
		retVal = cudaMemcpyAsync(
			pydop_dop_mov,
			d_pydop_dop_mov,
			nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, dopant_monomer_stream);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaThreadSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksMonomers; i++)
			upydop += pydop_dop_mov[i];

		// now get the dopant-dopant values
		retVal = cudaMemcpyAsync(
			dop_results,
			d_dop_results,
			nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, dopant_dopant_stream);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaThreadSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksDopants; i++)
			udopdop += dop_results[i];
	}

	return (upypy + upydop + udopdop);

}

DATA_TYPE getDevicePyPyPart(PARTICLE_SYSTEM& ps, int mIdx, int mlb, int mub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz,
	                        DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz, bool removeDouble)
{
	// calculate the partial energy
	d_PyPyPart <<<nBlocksMonomers, nThreadsMonomer, nThreadsMonomer * sizeof(DATA_TYPE), monomer_monomer_stream >>> (
		d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z, d_mon_results, 
		mIdx, mlb, mub, ps.monomers_per_chain, 
		ps.nMonomers, removeDouble, nx, ny, nz, ndpmx, ndpmy, ndpmz);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "getDevicePyPyPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		exit(0);
	}

	// Copy output to CPU output array
	cudaError_t retVal = cudaMemcpyAsync(
		mon_results,
		d_mon_results,
		nBlocksMonomers * sizeof(DATA_TYPE),
		cudaMemcpyDeviceToHost, monomer_monomer_stream);
	if (retVal != cudaSuccess)
		cout << "Unable to copy energies to CPU" << endl;

	retVal = cudaSuccess;
	retVal = cudaThreadSynchronize();
	if (retVal != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

	DATA_TYPE pypy = 0.0;
	for (int i = 0; i < nBlocksMonomers; i++)
		pypy += mon_results[i];

	return pypy;
}

DATA_TYPE getDevicePyDopPartMonMove(PARTICLE_SYSTEM& ps, int mIdx, int dlb, int dub)
{
	DATA_TYPE result = 0.0;
	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartMonMove <<<nBlocksDopants, nThreadsDopant, nThreadsDopant * sizeof(DATA_TYPE), monomer_dopant_stream >>> (d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z,
			d_dop_x, d_dop_y, d_dop_z, d_pydop_mon_mov, ps.nDopants, ps.nMonomers, mIdx, dlb, dub, ps.cm_x[mIdx], ps.cm_y[mIdx], ps.cm_z[mIdx], ps.dpm_x[mIdx], ps.dpm_y[mIdx], ps.dpm_z[mIdx]);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}

		// Copy output to CPU output array
		cudaError_t retVal = cudaSuccess;

		retVal = cudaMemcpyAsync(
			pydop_mon_mov,
			d_pydop_mon_mov,
			nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, monomer_dopant_stream);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaThreadSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksDopants; i++)
			result += pydop_mon_mov[i];
	}
	return result;
}

DATA_TYPE getDevicePyDopPartDopMove(PARTICLE_SYSTEM& ps, int dIdx, int mlb, int mub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	DATA_TYPE result = 0.0;
	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartDopMove <<<nBlocksMonomers, nThreadsMonomer, nThreadsMonomer * sizeof(DATA_TYPE), dopant_monomer_stream >>> (d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z,
			d_dop_x, d_dop_y, d_dop_z, d_pydop_dop_mov, ps.nDopants, ps.nMonomers, dIdx, mlb, mub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "PyDopPartDopMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
			exit(0);
		}

		// Copy output to CPU output array
		cudaError_t retVal = cudaSuccess;

		retVal = cudaMemcpyAsync(
			pydop_dop_mov,
			d_pydop_dop_mov,
			nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, dopant_monomer_stream);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaThreadSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksMonomers; i++)
			result += pydop_dop_mov[i];
	}
	return result;
}

void setModelConstants(bool usePCB_, DATA_TYPE sigma_dopant_, DATA_TYPE Q_dop_, DATA_TYPE epsilon_dopant_, DATA_TYPE kappa_, DATA_TYPE kappa_inter_end_,
	DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_, DATA_TYPE Q_mono_,
	DATA_TYPE sigma_inter_, DATA_TYPE sigma_inter_end_, DATA_TYPE epsilon_inter_,
	DATA_TYPE epsilon_inter_end_, DATA_TYPE dp_mag_, DATA_TYPE A_, DATA_TYPE B_, DATA_TYPE C_, DATA_TYPE sigmaPyDopant_)
{
	d_setModelConstants <<<1, 1 >>> (usePCB_, sigma_dopant_, Q_dop_, epsilon_dopant_, kappa_, kappa_inter_end_,
		x_size_, y_size_, z_size_, r_cut_, Q_mono_, sigma_inter_,
		sigma_inter_end_, epsilon_inter_, epsilon_inter_end_, dp_mag_, A_, B_, C_, sigmaPyDopant_);
	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_setModelConstants Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}
}

DATA_TYPE getDeviceInterPartMonomer(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, 
	                                int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz,
	                                DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz,
	                                bool removeDouble)
{
	DATA_TYPE upypy = 0.0;
	DATA_TYPE upydop = 0.0;

	// calculate the partial energy
	d_PyPyPart <<<nBlocksMonomers, nThreadsMonomer, nThreadsMonomer * sizeof(DATA_TYPE), monomer_monomer_stream >>> (
		d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z, d_mon_results, 
		midx, mlb, mub, ps.monomers_per_chain, ps.nMonomers, removeDouble, 
		nx, ny, nz, ndpmx, ndpmy, ndpmz);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "getDevicePyPyPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartMonMove <<<nBlocksDopants, nThreadsDopant, nThreadsDopant * sizeof(DATA_TYPE), monomer_dopant_stream >>> (d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z,
			d_dop_x, d_dop_y, d_dop_z, d_pydop_mon_mov, ps.nDopants, ps.nMonomers, midx, dlb, dub,
			nx, ny, nz, ndpmx, ndpmy, ndpmz);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "getDevicePyDopPartMonMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}
	}

	// Copy output to CPU output array
	cudaError_t retVal = cudaMemcpyAsync(
		mon_results,
		d_mon_results,
		nBlocksMonomers * sizeof(DATA_TYPE),
		cudaMemcpyDeviceToHost, monomer_monomer_stream);
	if (retVal != cudaSuccess)
		cout << "Unable to copy energies to CPU" << endl;

	retVal = cudaSuccess;
	retVal = cudaThreadSynchronize();
	if (retVal != cudaSuccess)
		cout << "Unable to synchronize (likely due to a prior error)" << endl;

	for (int i = 0; i < nBlocksMonomers; i++)
		upypy += mon_results[i];

	if (ps.nDopants)
	{
		retVal = cudaMemcpyAsync(
			pydop_mon_mov,
			d_pydop_mon_mov,
			nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, monomer_dopant_stream);
		if (retVal != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		retVal = cudaThreadSynchronize();
		if (retVal != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksDopants; i++)
			upydop += pydop_mon_mov[i];
	}

	return (upypy + upydop);
}

DATA_TYPE getDeviceInterPartDopant(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	DATA_TYPE upydop = 0.0;
	DATA_TYPE udopdop = 0.0;

	if (ps.nDopants)
	{
		// calculate the partial energy
		d_PyDopPartDopMove <<<nBlocksMonomers, nThreadsMonomer, nThreadsMonomer * sizeof(DATA_TYPE), dopant_monomer_stream >>> (d_cm_x, d_cm_y, d_cm_z, d_dpm_x, d_dpm_y, d_dpm_z,
			d_dop_x, d_dop_y, d_dop_z, d_pydop_dop_mov, ps.nDopants, ps.nMonomers, didx, mlb, mub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaError_t cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess)
		{
			fprintf(stderr, "PyDopPartDopMove Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}

		// calculate the partial energy
		d_DopDopPart <<<nBlocksDopants, nThreadsDopant, nThreadsDopant * sizeof(DATA_TYPE), dopant_dopant_stream >>> (d_dop_x, d_dop_y, d_dop_z, d_dop_results, ps.nDopants, didx, dlb, dub, nx, ny, nz);

		// Check for any errors launching the kernel
		cudaStatus = cudaGetLastError();
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "d_DopDopPart Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
		}

		// Copy output to CPU output array
		cudaStatus = cudaMemcpyAsync(
			pydop_dop_mov,
			d_pydop_dop_mov,
			nBlocksMonomers * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, dopant_monomer_stream);
		if (cudaStatus != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		cudaStatus = cudaThreadSynchronize();
		if (cudaStatus != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksMonomers; i++)
			upydop += pydop_dop_mov[i];

		// Copy output to CPU output array
		cudaStatus = cudaMemcpyAsync(
			dop_results,
			d_dop_results,
			nBlocksDopants * sizeof(DATA_TYPE),
			cudaMemcpyDeviceToHost, dopant_dopant_stream);
		if (cudaStatus != cudaSuccess)
			cout << "Unable to copy energies to CPU" << endl;

		cudaStatus = cudaThreadSynchronize();
		if (cudaStatus != cudaSuccess)
			cout << "Unable to synchronize (likely due to a prior error)" << endl;

		for (int i = 0; i < nBlocksDopants; i++)
			udopdop += dop_results[i];

	}
	return (upydop + udopdop);
}

void deviceScaleSystem(PARTICLE_SYSTEM& ps, DATA_TYPE * scaleFactor, bool updateCutoff)
{
	d_scaleSystem <<<1, 1 >>> (d_cm_x, d_cm_y, d_cm_z, d_dop_x, d_dop_y, d_dop_z,
		d_olig_cm_x, d_olig_cm_y, d_olig_cm_z, ps.monomers_per_chain,
		ps.nMonomers, ps.nDopants, scaleFactor[0], scaleFactor[1], scaleFactor[2], updateCutoff);

	// Check for any errors launching the kernel
	cudaError_t cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "d_scaleSystem Kernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

}

__global__ void d_updateMonomer(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	DATA_TYPE cm_x_, DATA_TYPE cm_y_, DATA_TYPE cm_z_,
	DATA_TYPE* d_dpm_x, DATA_TYPE* d_dpm_y, DATA_TYPE* d_dpm_z,
	DATA_TYPE dpm_x_, DATA_TYPE dpm_y_, DATA_TYPE dpm_z_,
	int midx)
{
	// thread 0 will perform the update
	if (blockIdx.x == 0 && threadIdx.x == 0)
	{
		d_cm_x[midx] = cm_x_;
		d_cm_y[midx] = cm_y_;
		d_cm_z[midx] = cm_z_;

		d_dpm_x[midx] = dpm_x_;
		d_dpm_y[midx] = dpm_y_;
		d_dpm_z[midx] = dpm_z_;

		//d_n_x[midx] = n_x_;
		//d_n_y[midx] = n_y_;
		//d_n_z[midx] = n_z_;
	}
}

__global__ void d_updateDopant(DATA_TYPE* d_dop_x, DATA_TYPE* d_dop_y, DATA_TYPE* d_dop_z,
	DATA_TYPE dop_x_, DATA_TYPE dop_y_, DATA_TYPE dop_z_, int didx)
{
	// thread 0 will perform the update
	if (blockIdx.x == 0 && threadIdx.x == 0)
	{
		d_dop_x[didx] = dop_x_;
		d_dop_y[didx] = dop_y_;
		d_dop_z[didx] = dop_z_;
	}
}

__global__ void d_updateSystemSize(DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_)
{
	d_x_size = x_size_;
	d_y_size = y_size_;
	d_z_size = z_size_;
	d_r_cut = r_cut_;
	d_r_cut_sqrd = r_cut_ * r_cut_;
}

__global__ void d_setModelConstants(bool usePCB_, DATA_TYPE sigma_dopant_, DATA_TYPE Q_dop_, DATA_TYPE epsilon_dopant_,
	DATA_TYPE kappa_, DATA_TYPE kappa_inter_end_, DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_,
	DATA_TYPE Q_mono_, DATA_TYPE sigma_inter_, DATA_TYPE sigma_inter_end_, DATA_TYPE epsilon_inter_,
	DATA_TYPE epsilon_inter_end_, DATA_TYPE dp_mag_, DATA_TYPE A_, DATA_TYPE B_, DATA_TYPE C_, DATA_TYPE sigmaPyDopant_)
{
	d_usePCB = usePCB_;
	d_sigma_dopant = sigma_dopant_;
	d_Q_dop = Q_dop_;
	d_epsilon_dopant = epsilon_dopant_;
	d_kappa = kappa_;
	d_kappa_inter_end = kappa_inter_end_;
	d_x_size = x_size_;
	d_y_size = y_size_;
	d_z_size = z_size_;
	d_r_cut = r_cut_;
	d_r_cut_sqrd = r_cut_ * r_cut_;
	d_Q_mono = Q_mono_;
	d_sigma_inter = sigma_inter_;
	d_sigma_inter_end = sigma_inter_end_;
	d_epsilon_inter = epsilon_inter_;
	d_epsilon_inter_end = epsilon_inter_end_;
	d_dp_mag = dp_mag_;
	d_A = A_;
	d_B = B_;
	d_C = C_;
	d_sigmaPyDopant = sigmaPyDopant_;
}

__global__ void d_DopDopPart(const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z, DATA_TYPE* d_udopdop, 
	int size, int didx, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ DATA_TYPE energy[];
	energy[threadIdx.x] = 0;

	DATA_TYPE sigma_dop = ANG_TO_BOHR(d_sigma_dopant);
	DATA_TYPE sigma_9 = TO_POW_6(sigma_dop) * TO_POW_3(sigma_dop);
	DATA_TYPE q_sqrd = d_Q_dop * d_Q_dop;
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0, rss = 0.0;
	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	DATA_TYPE rss3 = 0.0;
	DATA_TYPE temp = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut);
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(d_kappa * bohr_r_cut) / bohr_r_cut;
#endif
	DATA_TYPE ucoul = 0.0;
	DATA_TYPE other = 0.0;

	dx = nx;
	dy = ny;
	dz = nz;

	while (idx < dlb)
		idx += stride;


	for (; idx < dub; idx += stride)
	{
		if (idx == didx)
			continue;

		xdif = PCB_(d_x_size, (d_dop_x[idx] - dx));
		ydif = PCB_(d_y_size, (d_dop_y[idx] - dy));
		zdif = PCB_(d_z_size, (d_dop_z[idx] - dz));

		rss = xdif * xdif + ydif * ydif + zdif * zdif;

		if (rss < d_r_cut_sqrd)
		{
			rss = sqrt(rss);

			rss = ANG_TO_BOHR(rss);
			rss3 = TO_POW_3(rss);
			ucoul += (q_sqrd * ((erfc(d_kappa * rss) / rss) - dampingFactor));
			other += (d_epsilon_dopant * (sigma_9 / (rss3 * rss3 * rss3)));
		}
	}
	temp = ucoul + other;
	energy[threadIdx.x] += temp;

	// Wait for all threads in block to finish computations
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			energy[threadIdx.x] += energy[threadIdx.x + j];
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_udopdop[blockIdx.x] = energy[0];
	}
}

__global__ void d_PyPyPart(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z, DATA_TYPE* d_umonmon,
	int midx, int mlb, int mub, int monomers_per_chain, int nMonomers, bool removeDouble, 
	DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	int test_chain_id = 0;
	int input_chain_id = midx / monomers_per_chain; 
	int exc_lb = (midx / monomers_per_chain) * monomers_per_chain;
	int exc_ub = exc_lb + monomers_per_chain - 1;
	int t_lb = 0;
	int t_ub = 0;
	bool isInputHeadTail = false, isTestHeadTail = false;
	if (midx == exc_lb || midx == exc_ub)
		isInputHeadTail = true; 

	extern __shared__ DATA_TYPE energy[];
	energy[threadIdx.x] = 0;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE ucoul = 0.0, uexc = 0.0, udd = 0.0;
	DATA_TYPE qsqrd = d_Q_mono * d_Q_mono;
	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE sigma_6_mid = TO_POW_6(d_sigma_inter);
	DATA_TYPE sigma_9_mid = sigma_6_mid * d_sigma_inter * d_sigma_inter * d_sigma_inter;
	DATA_TYPE sigma_6_end = TO_POW_6(d_sigma_inter_end);
	DATA_TYPE sigma_9_end = sigma_6_end * d_sigma_inter_end * d_sigma_inter_end * d_sigma_inter_end;
	DATA_TYPE sigma_6 = 0.0, sigma_9 = 0.0;
	DATA_TYPE epsilon = d_epsilon_inter;
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(d_dp_mag) * DEBEYE_TO_HA(d_dp_mag);
	int tlb = 0, tub = 0;
	DATA_TYPE rij3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd);
	DATA_TYPE dampingFactor = 0.0;
	DATA_TYPE kappa_ = d_kappa;

	ox = nx;
	oy = ny;
	oz = nz;

	dpmx = ndpmx;
	dpmy = ndpmy;
	dpmz = ndpmz;

	while (idx < mlb)
		idx += stride;

	for (; idx < mub; idx += stride)
	{
		isTestHeadTail = false;

		if (idx == midx)
			continue;

		if (removeDouble && (idx < midx))
			continue;

		test_chain_id = idx / monomers_per_chain;

		if (input_chain_id == test_chain_id)
			continue;

		xdif = PCB_(d_x_size, (d_cm_x[idx] - ox));
		ydif = PCB_(d_y_size, (d_cm_y[idx] - oy));
		zdif = PCB_(d_z_size, (d_cm_z[idx] - oz));

		rij = xdif * xdif + ydif * ydif + zdif * zdif;

		if (rij < d_r_cut_sqrd)
		{
			t_lb = (idx / monomers_per_chain) * monomers_per_chain;
			t_ub = t_lb + monomers_per_chain - 1;

			if (idx == t_lb || idx == t_ub)
		            isTestHeadTail = true; 
		        
	        if (isInputHeadTail && isTestHeadTail)
			{
				sigma_6 = sigma_6_end;
				sigma_9 = sigma_9_end;
				epsilon = d_epsilon_inter_end;
				kappa_ = d_kappa_inter_end;
			}
			else
			{
				sigma_6 = sigma_6_mid;
				sigma_9 = sigma_9_mid;
				epsilon = d_epsilon_inter;
				kappa_ = d_kappa;
			}

			xdif = ANG_TO_BOHR(xdif);
			ydif = ANG_TO_BOHR(ydif);
			zdif = ANG_TO_BOHR(zdif);

			rij = sqrt(xdif * xdif + ydif * ydif + zdif * zdif);

			rij3 = TO_POW_3(rij);
#if defined __USE_PARTIAL_WOLF__
			dampingFactor = erfc(kappa_ * bohr_r_cut) / bohr_r_cut;
#endif

			ucoul += (qsqrd * (erfc(kappa_ * rij) / rij - dampingFactor));
			uexc += 2.0 * epsilon * ((sigma_9 / (rij3 * rij3 * rij3) - 1.5 * (sigma_6 / (rij3 * rij3))));
			dij = DOT_PRODUCT(dpmx, dpmy, dpmz, d_dpm_x[idx], d_dpm_y[idx], d_dpm_z[idx]);
			dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
			djr = DOT_PRODUCT(d_dpm_x[idx], d_dpm_y[idx], d_dpm_z[idx], xdif / rij, ydif / rij, zdif / rij);

			udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));

		}
	}
	energy[threadIdx.x] += ucoul + uexc + udd;

	// Wait for all threads in block to finish computations
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			energy[threadIdx.x] += energy[threadIdx.x + j];
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_umonmon[blockIdx.x] = energy[0];
	}
}

__global__ void d_PyDopPartMonMove(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z,
	const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z,
	DATA_TYPE* d_pydop_mon_mov, int nDopants, int nMonomers, int midx, int dlb, int dub,
	DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ DATA_TYPE energy[];
	energy[threadIdx.x] = 0;

	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(d_Q_dop) * DEBEYE_TO_HA(d_dp_mag) * d_C;
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(d_sigmaPyDopant);
	DATA_TYPE sigma_9 = sigma_6 * d_sigmaPyDopant * d_sigmaPyDopant * d_sigmaPyDopant;
	DATA_TYPE Q = d_Q_dop * d_Q_mono;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd);
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(d_kappa * bohr_r_cut) / bohr_r_cut;
#endif

	cmx = nx;
	cmy = ny;
	cmz = nz;

	dpmx = ndpmx;
	dpmy = ndpmy;
	dpmz = ndpmz;

	while (idx < dlb)
		idx += stride;

	for (; idx < dub; idx += stride)
	{
		r_vec[0] = PCB_(d_x_size, (cmx - d_dop_x[idx]));
		r_vec[1] = PCB_(d_y_size, (cmy - d_dop_y[idx]));
		r_vec[2] = PCB_(d_z_size, (cmz - d_dop_z[idx]));

		ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
		if (ris < d_r_cut_sqrd)
		{
			r_vec[0] = ANG_TO_BOHR(r_vec[0]);
			r_vec[1] = ANG_TO_BOHR(r_vec[1]);
			r_vec[2] = ANG_TO_BOHR(r_vec[2]);
			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];

			ris = sqrt(ris);
			ris3 = TO_POW_3(ris);
			ucoul += (Q * (erfc(d_kappa * ris) / ris - dampingFactor));
			udisp += (d_A * (sigma_9 / (ris3 * ris3 * ris3)) - d_B * (sigma_6 / (ris3 * ris3)));
		}
	}
	energy[threadIdx.x] += (ucoul + ucd + udisp);
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int i = blockDim.x / 2;
	while (0 < i)
	{
		if (threadIdx.x < i)
		{
			energy[threadIdx.x] += energy[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_pydop_mon_mov[blockIdx.x] = energy[0];
	}
}

__global__ void d_PyDopPartDopMove(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z,
	const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z,
	DATA_TYPE* d_pydop_dop_mov, int nDopants, int nMonomers, int didx, int mlb, int mub,
	DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ DATA_TYPE energy[];
	energy[threadIdx.x] = 0;

	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(d_Q_dop) * DEBEYE_TO_HA(d_dp_mag) * d_C;
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(d_sigmaPyDopant);
	DATA_TYPE sigma_9 = sigma_6 * d_sigmaPyDopant * d_sigmaPyDopant * d_sigmaPyDopant;
	DATA_TYPE Q = d_Q_dop * d_Q_mono;
	DATA_TYPE vPressure = 0.0;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd);
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = Q * erfc(d_kappa * bohr_r_cut) / bohr_r_cut;
#endif

	while (idx < mlb)
		idx += stride;

	for (; idx < mub; idx += stride)
	{
		cmx = d_cm_x[idx];
		cmy = d_cm_y[idx];
		cmz = d_cm_z[idx];

		dpmx = d_dpm_x[idx];
		dpmy = d_dpm_y[idx];
		dpmz = d_dpm_z[idx];

		r_vec[0] = PCB_(d_x_size, (cmx - nx));
		r_vec[1] = PCB_(d_y_size, (cmy - ny));
		r_vec[2] = PCB_(d_z_size, (cmz - nz));

		ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
		if (ris < d_r_cut_sqrd)
		{
			r_vec[0] = ANG_TO_BOHR(r_vec[0]);
			r_vec[1] = ANG_TO_BOHR(r_vec[1]);
			r_vec[2] = ANG_TO_BOHR(r_vec[2]);

			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];

			ris = sqrt(ris);
			ris3 = TO_POW_3(ris);
			ucoul += (Q * (erfc(d_kappa * ris) / ris - dampingFactor));
			udisp += (d_A * (sigma_9 / (ris3 * ris3 * ris3)) - d_B * (sigma_6 / (ris3 * ris3)));
		}
	}
	energy[threadIdx.x] += (ucoul + ucd + udisp);
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int i = blockDim.x / 2;
	while (0 < i)
	{
		if (threadIdx.x < i)
		{
			energy[threadIdx.x] += energy[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_pydop_dop_mov[blockIdx.x] = energy[0];
	}
}

__device__  void d_PCB(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z)
{
	if (!d_usePCB)
		return;

	if (x > sizex * 0.5) x = x - sizex;
	if (x <= -sizex * 0.5) x = x + sizex;

	if (y > sizey * 0.5) y = y - sizey;
	if (y <= -sizey * 0.5) y = y + sizey;

	if (z > sizez * 0.5) z = z - sizez;
	if (z <= -sizez * 0.5) z = z + sizez;
}

__global__ void d_scaleSystem(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	DATA_TYPE* d_dop_x, DATA_TYPE* d_dop_y, DATA_TYPE* d_dop_z,
	DATA_TYPE* d_olig_cm_x, DATA_TYPE* d_olig_cm_y, DATA_TYPE* d_olig_cm_z,
	int monomers_per_chain, int nMonomers, int nDopants,
	DATA_TYPE scaleFactorX, DATA_TYPE scaleFactorY, 
	DATA_TYPE scaleFactorZ, bool updateCutoff)
{
	d_calculateOligomersCenterOfMasses(d_cm_x, d_cm_y, d_cm_z,
		d_olig_cm_x, d_olig_cm_y, d_olig_cm_z, monomers_per_chain, nMonomers);

	d_x_size *= scaleFactorX;
	d_y_size *= scaleFactorY;
	d_z_size *= scaleFactorZ;

	DATA_TYPE min = MIN(d_x_size, d_y_size);
	min = MIN(min, d_z_size);
	if (updateCutoff)
		d_r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;

	d_r_cut_sqrd = d_r_cut * d_r_cut;

	for (int didx = 0; didx < nDopants; didx++)
	{
		d_dop_x[didx] = d_dop_x[didx] * scaleFactorX;
		d_dop_y[didx] = d_dop_y[didx] * scaleFactorY;
		d_dop_z[didx] = d_dop_z[didx] * scaleFactorZ;
	}

	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	DATA_TYPE ocmx = 0.0, ocmy = 0.0, ocmz = 0.0;

	int nPolymers = nMonomers / monomers_per_chain;
	for (int idx = 0; idx < nPolymers; idx++)
	{
		// original oligomer center of mass
		ocmx = d_olig_cm_x[idx];
		ocmy = d_olig_cm_y[idx];
		ocmz = d_olig_cm_z[idx];

		// scaled center of mass
		d_olig_cm_x[idx] *= scaleFactorX;
		d_olig_cm_y[idx] *= scaleFactorY;
		d_olig_cm_z[idx] *= scaleFactorZ;

		// differential between old center of mass and new center of mass
		dx = d_olig_cm_x[idx] - ocmx;
		dy = d_olig_cm_y[idx] - ocmy;
		dz = d_olig_cm_z[idx] - ocmz;

		// translate the oligomer to its new location
		//translateOligomer(ps, idx, dx, dy, dz);
		{
			int lb = idx * monomers_per_chain;
			int ub = lb + monomers_per_chain;

			for (int pidx = lb; pidx < ub; pidx++)
			{
				d_cm_x[pidx] += dx;
				d_cm_y[pidx] += dy;
				d_cm_z[pidx] += dz;
			}
		}
	}

	d_calculateOligomersCenterOfMasses(d_cm_x, d_cm_y, d_cm_z,
		d_olig_cm_x, d_olig_cm_y, d_olig_cm_z, monomers_per_chain, nMonomers);

}

__device__  void d_calculateOligomersCenterOfMasses(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	DATA_TYPE* d_olig_cm_x, DATA_TYPE* d_olig_cm_y, DATA_TYPE* d_olig_cm_z, int monomers_per_chain, int nMonomers)
{
	int ctr = 0;

	for (int j = 0; j < nMonomers; j = j + monomers_per_chain)
	{
		int lb = (j / monomers_per_chain) * monomers_per_chain;
		int ub = lb + monomers_per_chain;
		DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;

		for (int i = lb; i < ub; i++)
		{
			cmx += d_cm_x[i];
			cmy += d_cm_y[i];
			cmz += d_cm_z[i];
		}

		cmx = d_olig_cm_x[ctr] = cmx / monomers_per_chain;
		cmy = d_olig_cm_y[ctr] = cmy / monomers_per_chain;
		cmz = d_olig_cm_z[ctr] = cmz / monomers_per_chain;

		cmx = 0.0;
		cmy = 0.0;
		cmz = 0.0;

		ctr++;
	}
}

__device__ void warpReduce(volatile DATA_TYPE* sdata, int tid)
{
	sdata[tid] += sdata[tid + 32];
	sdata[tid] += sdata[tid + 16];
	sdata[tid] += sdata[tid + 8];
	sdata[tid] += sdata[tid + 4];
	sdata[tid] += sdata[tid + 2];
	sdata[tid] += sdata[tid + 1];
}

__global__ void d_PyPyTotalInter(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z, DATA_TYPE* d_umonmon,
	int mlb, int mub, int monomers_per_chain, int nMonomers, bool removeDouble)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	int test_chain_id = 0;
	int input_chain_id = 0;
	int exc_lb = 0;
	int exc_ub = 0;
	int t_lb = 0;
	int t_ub = 0;
	bool isInputHeadTail = false, isTestHeadTail = false;

	extern __shared__ DATA_TYPE energy[];
	energy[threadIdx.x] = 0;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE ucoul = 0.0, uexc = 0.0, udd = 0.0;
	DATA_TYPE qsqrd = d_Q_mono * d_Q_mono;
	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE sigma_6_mid = TO_POW_6(d_sigma_inter);
	DATA_TYPE sigma_9_mid = sigma_6_mid * d_sigma_inter * d_sigma_inter * d_sigma_inter;
	DATA_TYPE sigma_6_end = TO_POW_6(d_sigma_inter_end);
	DATA_TYPE sigma_9_end = sigma_6_end * d_sigma_inter_end * d_sigma_inter_end * d_sigma_inter_end;
	DATA_TYPE sigma_6 = 0.0, sigma_9 = 0.0;
	DATA_TYPE epsilon = d_epsilon_inter;
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(d_dp_mag) * DEBEYE_TO_HA(d_dp_mag);
	int tlb = 0, tub = 0;
	DATA_TYPE rij3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd);
	DATA_TYPE dampingFactor = 0.0;
	DATA_TYPE kappa_ = d_kappa;

	while (idx < mlb)
		idx += stride;

	for (; idx < mub; idx += stride)
	{
		ox = d_cm_x[idx];
		oy = d_cm_y[idx];
		oz = d_cm_z[idx];

		dpmx = d_dpm_x[idx];
		dpmy = d_dpm_y[idx];
		dpmz = d_dpm_z[idx];

		ucoul = 0.0;
		uexc = 0.0;
		udd = 0.0;


		test_chain_id = 0;
		input_chain_id = idx / monomers_per_chain;
		exc_lb = input_chain_id * monomers_per_chain;
		exc_ub = exc_lb + monomers_per_chain - 1;
		t_lb = 0;
		t_ub = 0;
		isInputHeadTail = false, isTestHeadTail = false;
		if (idx == exc_lb || idx == exc_ub)
			isInputHeadTail = true;

		for (int inner = 0; inner < nMonomers; inner++)
		{
			isTestHeadTail = false;

			if (inner == idx)
				continue;

			if (removeDouble && (inner < idx)) // do not DATA_TYPE count end monomer interactions
				continue;

			test_chain_id = inner / monomers_per_chain;

			if (input_chain_id == test_chain_id)
				continue;

			xdif = PCB_(d_x_size, (d_cm_x[inner] - ox));
			ydif = PCB_(d_y_size, (d_cm_y[inner] - oy));
			zdif = PCB_(d_z_size, (d_cm_z[inner] - oz));

			rij = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rij < d_r_cut_sqrd)
			{
				t_lb = (inner / monomers_per_chain) * monomers_per_chain;
				t_ub = t_lb + monomers_per_chain - 1;
				if (inner == t_lb || inner == t_ub)
					isTestHeadTail = true;

				if (isInputHeadTail && isTestHeadTail)
				{
					sigma_6 = sigma_6_end;
					sigma_9 = sigma_9_end;
					epsilon = d_epsilon_inter_end;
					kappa_ = d_kappa_inter_end;
				}
				else
				{
					sigma_6 = sigma_6_mid;
					sigma_9 = sigma_9_mid;
					epsilon = d_epsilon_inter;
					kappa_ = d_kappa;
				}

#if defined __USE_PARTIAL_WOLF__
				dampingFactor = erfc(kappa_ * bohr_r_cut) / bohr_r_cut;
#endif

				xdif = ANG_TO_BOHR(xdif);
				ydif = ANG_TO_BOHR(ydif);
				zdif = ANG_TO_BOHR(zdif);

				rij = sqrt(xdif * xdif + ydif * ydif + zdif * zdif);

				rij3 = TO_POW_3(rij);

				ucoul += (qsqrd * (erfc(kappa_ * rij) / rij - dampingFactor));
				uexc += 2.0 * epsilon * ((sigma_9 / (rij3 * rij3 * rij3) - 1.5 * (sigma_6 / (rij3 * rij3))));
				dij = DOT_PRODUCT(dpmx, dpmy, dpmz, d_dpm_x[inner], d_dpm_y[inner], d_dpm_z[inner]);
				dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
				djr = DOT_PRODUCT(d_dpm_x[inner], d_dpm_y[inner], d_dpm_z[inner], xdif / rij, ydif / rij, zdif / rij);

				udd += (dp_mag_sqrd * (dij - (3.0 * dir * djr) / (rij * rij))) / (rij3);
			}
		}
		energy[threadIdx.x] += ucoul + uexc + udd;
	}

	// Wait for all threads in block to finish computations
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			energy[threadIdx.x] += energy[threadIdx.x + j];
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_umonmon[blockIdx.x] = energy[0];
	}

}

__global__ void d_PyDopTotalInter(const DATA_TYPE* d_cm_x, const DATA_TYPE* d_cm_y, const DATA_TYPE* d_cm_z,
	const DATA_TYPE* d_dpm_x, const DATA_TYPE* d_dpm_y, const DATA_TYPE* d_dpm_z,
	const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z,
	DATA_TYPE* d_pydop_dop_mov, int nDopants, int nMonomers, int mlb, int mub)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ DATA_TYPE energy[];
	energy[threadIdx.x] = 0;

	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(d_Q_dop) * DEBEYE_TO_HA(d_dp_mag) * d_C;
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(d_sigmaPyDopant);
	DATA_TYPE sigma_9 = sigma_6 * d_sigmaPyDopant * d_sigmaPyDopant * d_sigmaPyDopant;
	DATA_TYPE Q = d_Q_dop * d_Q_mono;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut);
	DATA_TYPE cutoff_sqrd_ang = ANG_TO_BOHR(d_r_cut_sqrd);
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(d_kappa * bohr_r_cut) / bohr_r_cut;
#endif

	while (idx < mlb)
		idx += stride;

	for (; idx < mub; idx += stride)
	{
		cmx = d_cm_x[idx];
		cmy = d_cm_y[idx];
		cmz = d_cm_z[idx];

		dpmx = d_dpm_x[idx];
		dpmy = d_dpm_y[idx];
		dpmz = d_dpm_z[idx];
		ucoul = ucd = udisp = 0.0;

		for (int inner = 0; inner < nDopants; inner++)
		{
			r_vec[0] = PCB_(d_x_size, (cmx - d_dop_x[inner]));
			r_vec[1] = PCB_(d_y_size, (cmy - d_dop_y[inner]));
			r_vec[2] = PCB_(d_z_size, (cmz - d_dop_z[inner]));

			ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];
			if (ris < d_r_cut_sqrd)
			{
				r_vec[0] = ANG_TO_BOHR(r_vec[0]);
				r_vec[1] = ANG_TO_BOHR(r_vec[1]);
				r_vec[2] = ANG_TO_BOHR(r_vec[2]);
				ris = r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2];

				ris = sqrt(ris);
				ris3 = TO_POW_3(ris);
				ucoul += (Q * (erfc(d_kappa * ris) / ris - dampingFactor));
				ucd += c_const * DOT_PRODUCT(r_vec[0] / ris, r_vec[1] / ris, r_vec[2] / ris, dpmx, dpmy, dpmz) * (erfc(d_kappa * ris) / (ris * ris) - erfc(d_kappa * ANG_TO_BOHR(d_r_cut)) / (ANG_TO_BOHR(d_r_cut) * ANG_TO_BOHR(d_r_cut)));
				udisp += (d_A * (sigma_9 / (ris3 * ris3 * ris3)) - d_B * (sigma_6 / (ris3 * ris3)));
			}
		}
		energy[threadIdx.x] += (ucoul + c_const * ucd + udisp);

	}

	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int i = blockDim.x / 2;
	while (0 < i)
	{
		if (threadIdx.x < i)
		{
			energy[threadIdx.x] += energy[threadIdx.x + i];
		}
		__syncthreads();
		i /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_pydop_dop_mov[blockIdx.x] = energy[0];
	}

}

__global__ void d_DopDopTotalInter(const DATA_TYPE* d_dop_x, const DATA_TYPE* d_dop_y, const DATA_TYPE* d_dop_z, DATA_TYPE* d_udopdop, int size, int dlb, int dub)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	int stride = blockDim.x * gridDim.x;

	extern __shared__ DATA_TYPE energy[];
	energy[threadIdx.x] = 0;

	DATA_TYPE sigma_dop = ANG_TO_BOHR(d_sigma_dopant);
	DATA_TYPE sigma_9 = TO_POW_6(sigma_dop) * TO_POW_3(sigma_dop);
	DATA_TYPE q_sqrd = d_Q_dop * d_Q_dop;
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0, rss = 0.0;
	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	DATA_TYPE rss3 = 0.0;
	DATA_TYPE temp = 0.0;
	DATA_TYPE bohr_r_cut = ANG_TO_BOHR(d_r_cut);
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(d_kappa * bohr_r_cut) / bohr_r_cut;
#endif
	DATA_TYPE ucoul = 0.0;
	DATA_TYPE other = 0.0;

	while (idx < dlb)
		idx += stride;

	for (; idx < dub; idx += stride)
	{
		dx = d_dop_x[idx];
		dy = d_dop_y[idx];
		dz = d_dop_z[idx];

		temp = ucoul = other = 0.0;

		for (int inner = idx + 1; inner < size; inner++)
		{
			xdif = PCB_(d_x_size, (d_dop_x[inner] - dx));
			ydif = PCB_(d_y_size, (d_dop_y[inner] - dy));
			zdif = PCB_(d_z_size, (d_dop_z[inner] - dz));

			rss = xdif * xdif + ydif * ydif + zdif * zdif;

			if (rss < d_r_cut_sqrd)
			{
				rss = sqrt(rss);

				rss = ANG_TO_BOHR(rss);
				rss3 = TO_POW_3(rss);
				ucoul += (q_sqrd * ((erfc(d_kappa * rss) / rss) - dampingFactor));
				other += (d_epsilon_dopant * (sigma_9 / (rss3 * rss3 * rss3)));
			}
		}
		temp = ucoul + other;
		energy[threadIdx.x] += temp;
	}

	// Wait for all threads in block to finish computations
	__syncthreads();

	// Build summation tree over elements and
	// Sum the terms in the block
	int j = blockDim.x / 2;
	while (0 < j)
	{
		if (threadIdx.x < j)
		{
			energy[threadIdx.x] += energy[threadIdx.x + j];
		}
		__syncthreads();
		j /= 2;
	}
	__syncthreads();

	// If this is the first thread in the block,
	// then write the block total to the array
	if (threadIdx.x == 0)
	{
		d_udopdop[blockIdx.x] = energy[0];
	}
}
