    #include "polymerUtil.h"
#include "util.h"
#include "AppConfig.h"
#include "gpuHelper.cuh"
#include <ios>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include "Random.h"
#include <fstream>
#include <sstream>


using std::ios_base;
using std::setw;
using std::ofstream;
using std::stringstream;
//using std::isnan;
using std::istringstream;

DATA_TYPE dopant_mass = 0.0;
DATA_TYPE oligomer_mass = 0.0;
bool usePCB = true;
bool rhomboidPCB = false;
DATA_TYPE angleLimit = 10.0;

DATA_TYPE roationAxis[27][3] = {
	{ -1.0, -1.0, -1.0 },
	{ -1.0, -1.0, 0.0 },
	{ -1.0, -1.0, 1.0 },
	{ -1.0, 0.0, -1.0 },
	{ -1.0, 0.0, 0.0 },
	{ -1.0, 0.0, 1.0 },
	{ -1.0, 1.0, -1.0 },
	{ -1.0, 1.0, 0.0 },
	{ -1.0, 1.0, 1.0 },
	{ 0.0, -1.0, -1.0 },
	{ 0.0, -1.0, 0.0 },
	{ 0.0, -1.0, 1.0 },
	{ 0.0, 0.0, -1.0 },
	{ 0.0, 0.0, 0.0 },
	{ 0.0, 0.0, 1.0 },
	{ 0.0, 1.0, -1.0 },
	{ 0.0, 1.0, 0.0 },
	{ 0.0, 1.0, 1.0 },
	{ 1.0, -1.0, -1.0 },
	{ 1.0, -1.0, 0.0 },
	{ 1.0, -1.0, 1.0 },
	{ 1.0, 0.0, -1.0 },
	{ 1.0, 0.0, 0.0 },
	{ 1.0, 0.0, 1.0 },
	{ 1.0, 1.0, -1.0 },
	{ 1.0, 1.0, 0.0 },
	{ 1.0, 1.0, 1.0 }
};
MONOMER a;
MONOMER a_t;

MONOMER b;
MONOMER b_t;

void setAngleLimit(DATA_TYPE angleLimit_)
{
	angleLimit = angleLimit_;
}

void setRhomboidPCB(bool rhomboidPCB_)
{
	rhomboidPCB = rhomboidPCB_;
}

void setUsePCB(bool usePCB_)
{
	usePCB = usePCB_;
}

void PCB_ShearTransform(DATA_TYPE cmx, DATA_TYPE cmy, DATA_TYPE cmz, DATA_TYPE angle, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z)
{
	// transform from non orthoganal coordinates to orthoganl system to perform the pcb test
	// xprime = x + y * cos(angle)
	// yprime = y * sin(angle)

	x = x + y * cos(angle);
	y = y * sin(angle);
	z = z;
}

void PCB_Rhombus(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z)
{
	DATA_TYPE   rt3 = -(tan(DEG_TO_RAD(68.6)));
	DATA_TYPE  rrt3 = -1.0 / rt3;
	DATA_TYPE  rt32 = -rt3 / 2.0;
	DATA_TYPE rrt32 = -1.0 / rt32;

	x = x - NINT(x - rrt3 * y) - NINT(rrt32 * y) * 0.5;
	y = y - NINT(rrt32 * y) * rt32;
	//z = PCB_(sizez, z);

	//x *= sizex;
	//y *= sizey;
} 

void WrapInBox_Rhombus(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z)
{
	x = x + cos(DEG_TO_RAD(68.6)) * sizey;
	y = y + sin(DEG_TO_RAD(68.6)) * sizey;
	//z = PCB_(sizez, z);
}

void PCB(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z)
{
	if (!usePCB)
		return;

	if (rhomboidPCB)
	{
 		PCB_Rhombus(sizex, sizey, sizez, x, y, z);
	}
	else
	{
		if (x > sizex * 0.5) x = x - sizex;
		if (x <= -sizex * 0.5) x = x + sizex;

		if (y > sizey * 0.5) y = y - sizey;
		if (y <= -sizey * 0.5) y = y + sizey;

		if (z > sizez * 0.5) z = z - sizez;
		if (z <= -sizez * 0.5) z = z + sizez;

	}
}

void wrapInBox(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z)
{
	if (!usePCB)
		return;

	if (rhomboidPCB)
	{
		WrapInBox_Rhombus(sizex, sizey, sizez, x, y, z);
	}
	else
	{
		if (x < -sizex * 0.5) x = x + sizex;
		if (x >= sizex * 0.5) x = x - sizex;

		if (y < -sizey * 0.5) y = y + sizey;
		if (y >= sizey * 0.5) y = y - sizey;

		if (z < -sizez * 0.5) z = z + sizez;
		if (z >= sizez * 0.5) z = z - sizez;
	}
}

//DATA_TYPE wrapInBox(DATA_TYPE x, DATA_TYPE lb, DATA_TYPE ub)
//{
//	if (!usePCB)
//		return x;
//
//	DATA_TYPE retVal = x;
//
//	DATA_TYPE size = ub - lb;
//
//	retVal = PCB_(size, x);
//
//	return retVal;
//}

DATA_TYPE averagePolymerLength(PARTICLE_SYSTEM & ps)
{
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE polymerLen = 0.0;
	int monomers_per_polymer = ps.monomers_per_chain;

	for (int idx = 0; idx < ps.nMonomers; idx = idx + monomers_per_polymer)
	{
		xdif = ps.cm_x[idx] - ps.cm_x[idx + monomers_per_polymer - 1];
		ydif = ps.cm_y[idx] - ps.cm_y[idx + monomers_per_polymer - 1];
		zdif = ps.cm_z[idx] - ps.cm_z[idx + monomers_per_polymer - 1];

		polymerLen += sqrt(xdif * xdif + ydif * ydif + zdif * zdif);
	}

	return (polymerLen / (ps.nMonomers / (DATA_TYPE)monomers_per_polymer));
}

void calculateOrderParameters(PARTICLE_SYSTEM & ps, DATA_TYPE & vectorOrder, DATA_TYPE & orientationOrder)
{
	int monomers_per_polymer = ps.monomers_per_chain;
	int numPolymers = ps.nMonomers / monomers_per_polymer;
	DATA_TYPE * director_x = new DATA_TYPE[numPolymers];
	DATA_TYPE * director_y = new DATA_TYPE[numPolymers];
	DATA_TYPE * director_z = new DATA_TYPE[numPolymers];
	DATA_TYPE director_avg_x = 0.0;
	DATA_TYPE director_avg_y = 0.0;
	DATA_TYPE director_avg_z = 0.0;

	memset(director_x, 0, sizeof(DATA_TYPE) * numPolymers);
	memset(director_y, 0, sizeof(DATA_TYPE) * numPolymers);
	memset(director_z, 0, sizeof(DATA_TYPE) * numPolymers);

	int pidxEnd = 0;
	int i = 0;

	for (int idx = 0; idx < ps.nMonomers; idx = idx + monomers_per_polymer)
	{
		pidxEnd = (idx + monomers_per_polymer) - 1;

		director_x[i] = ps.cm_x[idx] - ps.cm_x[pidxEnd];
		director_y[i] = ps.cm_y[idx] - ps.cm_y[pidxEnd];
		director_z[i] = ps.cm_z[idx] - ps.cm_z[pidxEnd];

		director_avg_x += director_x[i];
		director_avg_y += director_y[i];
		director_avg_z += director_z[i];

		i++;
	}

	director_avg_x /= numPolymers;
	director_avg_y /= numPolymers;
	director_avg_z /= numPolymers;
	DATA_TYPE theta = 0.0;
	DATA_TYPE v = 0.0;
	for (int idx = 0; idx < numPolymers; idx++)
	{
		theta = angleBetween2Vectors(director_x[idx], director_y[idx], director_z[idx], director_avg_x, director_avg_y, director_avg_z);
		//v += 3.0 * COS_SQRD((theta)) - 1;
		v += 3.0 * (cos(theta) * cos(theta)) - 1;
	}

	vectorOrder = (0.5 * (v / (DATA_TYPE)numPolymers));

	// now calculate the orientation order parameter
	// always make sure that the vector order parameter was calculated before 
	// otherwise results will be incorrect
	DATA_TYPE alpha = 0.0;
	DATA_TYPE cos_sqrd = 0.0;
	DATA_TYPE opar = 0.0;
	int  polymer = 0, num_polymers = ps.nMonomers / monomers_per_polymer;
	i = 0;

	for (polymer = 0; polymer < num_polymers; polymer++)
	{
		cos_sqrd = 0.0;

		for (i = polymer * monomers_per_polymer; i < (polymer * monomers_per_polymer + monomers_per_polymer) - 1; i++)
		{
			alpha = (angleBetween2Vectors(director_x[polymer], director_y[polymer], director_z[polymer], ps.cm_x[i] - ps.cm_x[i + 1], ps.cm_y[i] - ps.cm_y[i + 1], ps.cm_z[i] - ps.cm_z[i + 1]));
			cos_sqrd += (cos(alpha)) * (cos(alpha));
		}
		opar += (cos_sqrd / (DATA_TYPE)(monomers_per_polymer - 1) - 1.0 / 3.0);
	}

	opar /= num_polymers;
	opar *= (3.0 / 2.0);
	orientationOrder = opar;
}

DATA_TYPE calculateRadiusOfGyration(PARTICLE_SYSTEM & ps)
{
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE pcm_x = 0.0, pcm_y = 0.0, pcm_z = 0.0;
	DATA_TYPE radiusOfGyration = 0.0, totRadiusOfGyration = 0.0;
	int monomers_per_polymer = ps.monomers_per_chain;

	for (int idx = 0; idx < ps.nMonomers; idx++)
	{
		if (idx != 0 && idx % monomers_per_polymer == 0)
		{
			pcm_x = (ps.cm_x[idx - 1] + ps.cm_x[idx - monomers_per_polymer]) / 2.0;
			pcm_y = (ps.cm_y[idx - 1] + ps.cm_y[idx - monomers_per_polymer]) / 2.0;
			pcm_z = (ps.cm_z[idx - 1] + ps.cm_z[idx - monomers_per_polymer]) / 2.0;

			for (int i = idx - monomers_per_polymer; i < idx; i++)
			{
				xdif = ps.cm_x[i] - pcm_x;
				ydif = ps.cm_y[i] - pcm_y;
				zdif = ps.cm_z[i] - pcm_z;

				radiusOfGyration += sqrt(xdif * xdif + ydif * ydif + zdif * zdif);
			}
			totRadiusOfGyration += radiusOfGyration / (DATA_TYPE)monomers_per_polymer;

			// reset the value for the next loop
			radiusOfGyration = 0.0;
		}
		else
		{
			xdif += ps.cm_x[idx];
			ydif += ps.cm_y[idx];
			zdif += ps.cm_z[idx];
		}
	}

	return (totRadiusOfGyration / (ps.nMonomers / (DATA_TYPE)monomers_per_polymer));
}

void calculatePositionalDisorder(PARTICLE_SYSTEM & ps, const AppConfig & appConfig, 
	                               DATA_TYPE & lx, DATA_TYPE & ly, DATA_TYPE & lz, DATA_TYPE & lambda,
	                               DATA_TYPE & dlx, DATA_TYPE & dly, DATA_TYPE & dlz, DATA_TYPE & dlambda)
{
	DATA_TYPE scalar = (1.0 / (float)ps.nPolymers);
	DATA_TYPE pi_4 = 4.0 * PI;
	DATA_TYPE num_x = 2.0 * appConfig.x_off;
	DATA_TYPE num_y = 2.0 * appConfig.y_off;
	DATA_TYPE num_z = 2.0 * appConfig.z_off;

	for (int idx = 0; idx < ps.nPolymers; idx++)
	{
		// determine the lamba (positional disorder value)
		lx += scalar * cos((pi_4 * ps.olig_cm_x[idx]) / num_x);
		ly += scalar * cos((pi_4 * ps.olig_cm_y[idx]) / num_y);
		lz += scalar * cos((pi_4 * ps.olig_cm_z[idx]) / num_z);
	}

	lambda = fabs((1.f / 3.f) * (lx + ly + lz));

	scalar = (1.0 / (float)ps.nDopants);
	for (int idx = 0; idx < ps.nDopants; idx++)
	{
		// determine the lamba (positional disorder value)
		dlx += scalar * cos((pi_4 * ps.dop_x[idx]) / num_x);
		dly += scalar * cos((pi_4 * ps.dop_y[idx]) / num_y);
		dlz += scalar * cos((pi_4 * ps.dop_z[idx]) / num_z);
	}

	dlambda = fabs((1.f / 3.f) * (dlx + dly + dlz));

}

void collectParticleSystemStatistics(PARTICLE_SYSTEM & ps, const AppConfig & appConfig, ofstream & systemStatsFile)
{
	DATA_TYPE vectorOrder = 0.0, orientationOrder = 0.0, gyration = 0.0, alen = 0.0;
	DATA_TYPE lx = 0.0, ly = 0.0, lz = 0.0, lambda = 0.0;
	DATA_TYPE dlx = 0.0, dly = 0.0, dlz = 0.0, dlambda = 0.0;

	if (systemStatsFile.is_open())
	{
		calculateOrderParameters(ps, vectorOrder, orientationOrder);
		gyration = calculateRadiusOfGyration(ps);
		alen = averagePolymerLength(ps);
		calculatePositionalDisorder(ps, appConfig, lx, ly, lz, lambda, dlx, dly, dlz, dlambda);

		systemStatsFile << vectorOrder << ',' << orientationOrder << ',' << gyration << ',' << alen << ',' << lx << ',' << ly << ',' << lz << ',' << lambda << ',' << dlx << ',' << dly << ',' << dlz << ',' << dlambda << endl;

		systemStatsFile.flush();
	}
}

void saveHistograme(PARTICLE_SYSTEM & ps, unsigned int numIterations, const string & fName, ios_base::openmode mode)
{
	DATA_TYPE  itr = numIterations + 1;
	int numPolymers = (ps.nMonomers / ps.monomers_per_chain);
	int lb = 0;
	int ub = 0;

	DATA_TYPE cangle = 0.0, tangle = 0.0;
	DATA_TYPE a[3];
	DATA_TYPE b[3];
	int bidx = 0;

	for (int pi = 0; pi < numPolymers; pi++)
	{
		lb = pi * ps.monomers_per_chain;
		ub = lb + ps.monomers_per_chain;
		for (int i = lb + 1; i < ub - 1; i++)
		{
			a[0] = ps.cm_x[i - 1] - ps.cm_x[i];
			a[1] = ps.cm_y[i - 1] - ps.cm_y[i];
			a[2] = ps.cm_z[i - 1] - ps.cm_z[i];
			normalize(a[0], a[1], a[2], a[0], a[1], a[2]);

			b[0] = ps.cm_x[i + 1] - ps.cm_x[i];
			b[1] = ps.cm_y[i + 1] - ps.cm_y[i];
			b[2] = ps.cm_z[i + 1] - ps.cm_z[i];
			normalize(b[0], b[1], b[2], b[0], b[1], b[2]);

			cangle = RAD_TO_DEG(angleBetween2Vectors(a[0], a[1], a[2], b[0], b[1], b[2]));
			bidx = (int)cangle;
			if (bidx < NUM_BINS)
			{
				ps.bendingAngleHistogram[bidx] += 1;
			}

			tangle = RAD_TO_DEG(angleBetween2Vectors(ps.n_x[i], ps.n_y[i], ps.n_z[i], ps.n_x[i + 1], ps.n_y[i + 1], ps.n_z[i + 1]));
			bidx = (int)tangle;
			if (bidx < NUM_BINS)
			{
				ps.torsionAngleHistogram[bidx] += 1;
			}

		}
	}

	std::ofstream fout(fName.c_str());
	fout << "Angle, Bending Angle,Torsion Angle" << endl;

	for (int i = 0; i < NUM_BINS; i++)
	{
		fout << i + 1 << ',' << ps.bendingAngleHistogram[i] / itr << ',' << ps.torsionAngleHistogram[i] / itr << endl;
	}

	fout.flush();
	fout.close();
}

PARTICLE_SYSTEM * yosephSingleChain(const AppConfig & appConfig)
{
	PARTICLE_SYSTEM * ps = NULL;

	if (appConfig.single_chain)
	{
		usePCB = false;
		ps = new PARTICLE_SYSTEM;
		memset(ps, 0, sizeof(PARTICLE_SYSTEM));

		ps->nMonomers = 12;
		ps->nDopants = 8;

		// allocate memory
	// allocate memory
		ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
		ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
		ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

		ps->cm_x = new DATA_TYPE[ps->nMonomers];
		ps->cm_y = new DATA_TYPE[ps->nMonomers];
		ps->cm_z = new DATA_TYPE[ps->nMonomers];

		ps->n_x = new DATA_TYPE[ps->nMonomers];
		ps->n_y = new DATA_TYPE[ps->nMonomers];
		ps->n_z = new DATA_TYPE[ps->nMonomers];

		ps->dpm_x = new DATA_TYPE[ps->nMonomers];
		ps->dpm_y = new DATA_TYPE[ps->nMonomers];
		ps->dpm_z = new DATA_TYPE[ps->nMonomers];

		ps->heading = new DATA_TYPE[ps->nMonomers];
		ps->pitch = new DATA_TYPE[ps->nMonomers];
		ps->bank = new DATA_TYPE[ps->nMonomers];
		ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
		ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
		memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
		memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
		ps->x_size = 2.0 * appConfig.x_off;
		ps->y_size = 2.0 * appConfig.y_off;
		ps->z_size = 2.0 * appConfig.z_off;
		ps->uc_x = 2.0 * appConfig.x_off;
		ps->uc_y = 2.0 * appConfig.y_off;
		ps->uc_z = 2.0 * appConfig.z_off;

		ps->angleLimit = appConfig.angle_limit;
		ps->monomers_per_chain = appConfig.monomers_per_chain;

		for (int i = 0; i < 12; i = i + 2)
		{
			ps->n_x[i] = 0.0;
			ps->n_y[i] = 0.0;
			ps->n_z[i] = -1.0;

			ps->dpm_x[i] = 0.0;
			ps->dpm_y[i] = -1.0;
			ps->dpm_z[i] = 0.0;

			ps->heading[i] = 0.0;
			ps->pitch[i] = 0.0;
			ps->bank[i] = 0.0;

			ps->n_x[i + 1] = 0.0;
			ps->n_y[i + 1] = 0.0;
			ps->n_z[i + 1] = 1.0;

			ps->dpm_x[i + 1] = 0.0;
			ps->dpm_y[i + 1] = 1.0;
			ps->dpm_z[i + 1] = 0.0;

			ps->heading[i + 1] = 0.0;
			ps->pitch[i + 1] = 0.0;
			ps->bank[i + 1] = 0.0;
		}

		ps->cm_x[0] = -19.7818864542; ps->cm_y[0] = 0.6266752350;    ps->cm_z[0] = 0.0000000000;
		ps->cm_x[1] = -16.1851798261;  ps->cm_y[1] = -0.6266752350;    ps->cm_z[1] = 0.0000000000;
		ps->cm_x[2] = -12.5884731981;  ps->cm_y[2] = 0.6266752350;     ps->cm_z[2] = 0.0000000000;
		ps->cm_x[3] = -8.9917665701;   ps->cm_y[3] = -0.6266752350;    ps->cm_z[3] = 0.0000000000;
		ps->cm_x[4] = -5.3950599420;   ps->cm_y[4] = 0.6266752350;     ps->cm_z[4] = 0.0000000000;
		ps->cm_x[5] = -1.7983533140;   ps->cm_y[5] = -0.6266752350;    ps->cm_z[5] = 0.0000000000;
		ps->cm_x[6] = 1.7983533140;    ps->cm_y[6] = 0.6266752350;     ps->cm_z[6] = 0.0000000000;
		ps->cm_x[7] = 5.3950599420;    ps->cm_y[7] = -0.6266752350;    ps->cm_z[7] = 0.0000000000;
		ps->cm_x[8] = 8.9917665701;    ps->cm_y[8] = 0.6266752350;     ps->cm_z[8] = 0.0000000000;
		ps->cm_x[9] = 12.5884731981;   ps->cm_y[9] = -0.6266752350;    ps->cm_z[9] = 0.0000000000;
		ps->cm_x[10] = 16.1851798261;  ps->cm_y[10] = 0.6266752350;    ps->cm_z[10] = 0.0000000000;
		ps->cm_x[11] = 19.7818864542;  ps->cm_y[11] = -0.6266752350;   ps->cm_z[11] = 0.0000000000;

		ps->dop_x = new DATA_TYPE[ps->nDopants];
		ps->dop_y = new DATA_TYPE[ps->nDopants];
		ps->dop_z = new DATA_TYPE[ps->nDopants];

		ps->dop_x[0] = 28.94062500;      ps->dop_y[0] = 0.00000000;    ps->dop_z[0] = 2.97984000;
		ps->dop_x[1] = 17.36437500;      ps->dop_y[1] = 0.00000000;    ps->dop_z[1] = 2.97984000;
		ps->dop_x[2] = 5.78812500;       ps->dop_y[2] = 0.00000000;    ps->dop_z[2] = 2.97984000;
		ps->dop_x[3] = -5.78812500;      ps->dop_y[3] = 0.00000000;    ps->dop_z[3] = 2.97984000;
		ps->dop_x[4] = -17.36437500;     ps->dop_y[4] = 0.00000000;    ps->dop_z[4] = 2.97984000;
		ps->dop_x[5] = -28.94062500;     ps->dop_y[5] = 0.00000000;    ps->dop_z[5] = 2.97984000;
		ps->dop_x[6] = 28.94062500;      ps->dop_y[6] = 0.00000000;    ps->dop_z[6] = -2.97984000;
		ps->dop_x[7] = 17.36437500;      ps->dop_y[7] = 0.00000000;    ps->dop_z[7] = -2.97984000;
		//ps->dop_x[8] = 5.78812500;       ps->dop_y[8] = 0.00000000;    ps->dop_z[8] = -2.97984000;
		//ps->dop_x[9] = -5.78812500;      ps->dop_y[9] = 0.00000000;    ps->dop_z[9] = -2.97984000;
		//ps->dop_x[10] = -17.36437500;     ps->dop_y[10] = 0.00000000;   ps->dop_z[10] = -2.97984000;
		//ps->dop_x[11] = -28.94062500;    ps->dop_y[11] = 0.00000000;   ps->dop_z[11] = -2.97984000;

		ps->z0 = new DATA_TYPE[1];
		ps->z0[0] = 0.0;

		// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
		DATA_TYPE min = MIN(ps->x_size, ps->y_size);
		min = MIN(min, ps->z_size);
		if (appConfig.r_cut < 0)
		{
			ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
			if (ps->r_cut < ps->cm_x[11] - ps->cm_x[0])
				ps->r_cut = ps->cm_x[11] - ps->cm_x[0];
		}
		else
		{
			ps->r_cut = appConfig.r_cut;
		}

		// calcualte  the oligomer's centers of mass
		calculateOligomersCenterOfMasses(*ps);
	}
	else
	{
		ps = new PARTICLE_SYSTEM;
		memset(ps, 0, sizeof(PARTICLE_SYSTEM));

		ps->nMonomers = 12;
		ps->nDopants = 4;

		// allocate memory
	// allocate memory
		ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
		ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
		ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

		ps->cm_x = new DATA_TYPE[ps->nMonomers];
		ps->cm_y = new DATA_TYPE[ps->nMonomers];
		ps->cm_z = new DATA_TYPE[ps->nMonomers];

		ps->n_x = new DATA_TYPE[ps->nMonomers];
		ps->n_y = new DATA_TYPE[ps->nMonomers];
		ps->n_z = new DATA_TYPE[ps->nMonomers];

		ps->dpm_x = new DATA_TYPE[ps->nMonomers];
		ps->dpm_y = new DATA_TYPE[ps->nMonomers];
		ps->dpm_z = new DATA_TYPE[ps->nMonomers];

		ps->heading = new DATA_TYPE[ps->nMonomers];
		ps->pitch = new DATA_TYPE[ps->nMonomers];
		ps->bank = new DATA_TYPE[ps->nMonomers];
		ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
		ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
		memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
		memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
		ps->x_size = 2.0 * appConfig.x_off;
		ps->y_size = 2.0 * appConfig.y_off;
		ps->z_size = 2.0 * appConfig.z_off;
		ps->uc_x = 2.0 * appConfig.x_off;
		ps->uc_y = 2.0 * appConfig.y_off;
		ps->uc_z = 2.0 * appConfig.z_off;

		for (int i = 0; i < 12; i = i + 2)
		{
			ps->n_x[i] = 0.0;
			ps->n_y[i] = 0.0;
			ps->n_z[i] = -1.0;

			ps->dpm_x[i] = 0.0;
			ps->dpm_y[i] = -1.0;
			ps->dpm_z[i] = 0.0;

			ps->heading[i] = 0.0;
			ps->pitch[i] = 0.0;
			ps->bank[i] = 0.0;

			ps->n_x[i + 1] = 0.0;
			ps->n_y[i + 1] = 0.0;
			ps->n_z[i + 1] = 1.0;

			ps->dpm_x[i + 1] = 0.0;
			ps->dpm_y[i + 1] = 1.0;
			ps->dpm_z[i + 1] = 0.0;

			ps->heading[i + 1] = 0.0;
			ps->pitch[i + 1] = 0.0;
			ps->bank[i + 1] = 0.0;
		}

		ps->cm_x[0] = -19.7818864542; ps->cm_y[0] = 0.6266752350;    ps->cm_z[0] = 0.0000000000;
		ps->cm_x[1] = -16.1851798261;  ps->cm_y[1] = -0.6266752350;    ps->cm_z[1] = 0.0000000000;
		ps->cm_x[2] = -12.5884731981;  ps->cm_y[2] = 0.6266752350;     ps->cm_z[2] = 0.0000000000;
		ps->cm_x[3] = -8.9917665701;   ps->cm_y[3] = -0.6266752350;    ps->cm_z[3] = 0.0000000000;
		ps->cm_x[4] = -5.3950599420;   ps->cm_y[4] = 0.6266752350;     ps->cm_z[4] = 0.0000000000;
		ps->cm_x[5] = -1.7983533140;   ps->cm_y[5] = -0.6266752350;    ps->cm_z[5] = 0.0000000000;
		ps->cm_x[6] = 1.7983533140;    ps->cm_y[6] = 0.6266752350;     ps->cm_z[6] = 0.0000000000;
		ps->cm_x[7] = 5.3950599420;    ps->cm_y[7] = -0.6266752350;    ps->cm_z[7] = 0.0000000000;
		ps->cm_x[8] = 8.9917665701;    ps->cm_y[8] = 0.6266752350;     ps->cm_z[8] = 0.0000000000;
		ps->cm_x[9] = 12.5884731981;   ps->cm_y[9] = -0.6266752350;    ps->cm_z[9] = 0.0000000000;
		ps->cm_x[10] = 16.1851798261;  ps->cm_y[10] = 0.6266752350;    ps->cm_z[10] = 0.0000000000;
		ps->cm_x[11] = 19.7818864542;  ps->cm_y[11] = -0.6266752350;   ps->cm_z[11] = 0.0000000000;

		ps->dop_x = new DATA_TYPE[ps->nDopants];
		ps->dop_y = new DATA_TYPE[ps->nDopants];
		ps->dop_z = new DATA_TYPE[ps->nDopants];

		ps->dop_x[0] = 17.36437500;      ps->dop_y[0] = 0.00000000;    ps->dop_z[0] = 2.97984000;
		ps->dop_x[1] = 5.78812500;       ps->dop_y[1] = 0.00000000;    ps->dop_z[1] = 2.97984000;
		ps->dop_x[2] = -5.78812500;      ps->dop_y[2] = 0.00000000;    ps->dop_z[2] = 2.97984000;
		ps->dop_x[3] = -17.36437500;     ps->dop_y[3] = 0.00000000;    ps->dop_z[3] = 2.97984000;

		ps->z0 = new DATA_TYPE[1];
		ps->z0[0] = 0.0;

		// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
		DATA_TYPE min = MIN(ps->x_size, ps->y_size);
		min = MIN(min, ps->z_size);
		if (appConfig.r_cut < 0)
		{
			ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
			if (ps->r_cut < ps->cm_x[11] - ps->cm_x[0])
				ps->r_cut = ps->cm_x[11] - ps->cm_x[0];
		}
		else
		{
			ps->r_cut = appConfig.r_cut;
		}

		// calcualte  the oligomer's centers of mass
		calculateOligomersCenterOfMasses(*ps);
	}
	return ps;
}

PARTICLE_SYSTEM * buildReferencedChain(const AppConfig & appConfig, bool addDopants)
{
	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	map<string, DATA_TYPE>::const_iterator itr;
	DATA_TYPE r0 = 0.0, theta_0 = 0.0;

	itr = appConfig.constants.find("r0");
	if (itr != appConfig.constants.end())
		r0 = itr->second;
	else
		cout << "throw exception, constant r0 must have a value" << endl;
	itr = appConfig.constants.find("theta_0");
	if (itr != appConfig.constants.end())
		theta_0 = itr->second;
	else
		cout << "throw exception, constant theta_0 must have a value" << endl;

	PARTICLE_SYSTEM * ps = new PARTICLE_SYSTEM;
	memset(ps, 0, sizeof(PARTICLE_SYSTEM));

	ps->monomers_per_chain = appConfig.monomers_per_chain;
	ps->nMonomers = appConfig.monomers_per_chain;
	ps->nDopants = appConfig.dopants_per_chain;
	int numPolymers = 1;
	ps->z0 = new DATA_TYPE[numPolymers];
	ps->z0[0] = 0.0;

	// allocate memory
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

	ps->cm_x = new DATA_TYPE[ps->nMonomers];
	ps->cm_y = new DATA_TYPE[ps->nMonomers];
	ps->cm_z = new DATA_TYPE[ps->nMonomers];

	ps->n_x = new DATA_TYPE[ps->nMonomers];
	ps->n_y = new DATA_TYPE[ps->nMonomers];
	ps->n_z = new DATA_TYPE[ps->nMonomers];

	ps->dpm_x = new DATA_TYPE[ps->nMonomers];
	ps->dpm_y = new DATA_TYPE[ps->nMonomers];
	ps->dpm_z = new DATA_TYPE[ps->nMonomers];

	ps->dop_x = new DATA_TYPE[ps->nDopants];
	ps->dop_y = new DATA_TYPE[ps->nDopants];
	ps->dop_z = new DATA_TYPE[ps->nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);


	ps->heading = new DATA_TYPE[ps->nMonomers];
	ps->pitch = new DATA_TYPE[ps->nMonomers];
	ps->bank = new DATA_TYPE[ps->nMonomers];
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->x_size = 2.0 * appConfig.x_off;
	ps->y_size = 2.0 * appConfig.y_off;
	ps->z_size = 2.0 * appConfig.z_off;
	ps->uc_x = 2.0 * appConfig.x_off;
	ps->uc_y = 2.0 * appConfig.y_off;
	ps->uc_z = 2.0 * appConfig.z_off;
	ps->nPolymers = 1;

	// determine offsets for the monomers
	DATA_TYPE tangle = 90.0 - (theta_0 / 2.0);
	DATA_TYPE dpm_yoffset = sin(DEG_TO_RAD(tangle)) * r0;
	DATA_TYPE cm_dist = cos(DEG_TO_RAD(tangle)) * r0;

	// build a reference chain
	for (int i = 0; i < appConfig.monomers_per_chain; i = i + 2)
	{
		//ps->cm_x[i] = i * cm_dist - oligomer_length / 2.0;
		ps->cm_x[i] = i * cm_dist;
		ps->cm_y[i] = dpm_yoffset / 2.0;
		ps->cm_z[i] = 0.0;

		ps->n_x[i] = 0.0;
		ps->n_y[i] = 0.0;
		ps->n_z[i] = -1.0;

		ps->dpm_x[i] = 0.0;
		ps->dpm_y[i] = -1.0;
		ps->dpm_z[i] = 0.0;

		ps->heading[i] = 0.0;
		ps->pitch[i] = 0.0;
		ps->bank[i] = 0.0;

		ps->cm_x[i + 1] = ps->cm_x[i] + cm_dist;
		ps->cm_y[i + 1] = -dpm_yoffset / 2.0;
		ps->cm_z[i + 1] = 0.0;

		ps->n_x[i + 1] = 0.0;
		ps->n_y[i + 1] = 0.0;
		ps->n_z[i + 1] = 1.0;

		ps->dpm_x[i + 1] = 0.0;
		ps->dpm_y[i + 1] = 1.0;
		ps->dpm_z[i + 1] = 0.0;

		ps->heading[i + 1] = 0.0;
		ps->pitch[i + 1] = 0.0;
		ps->bank[i + 1] = 0.0;
	}

	// now add dopants
	if (addDopants)
	{
#if defined __YOSEPH_DOPANT_METHOD__
		DATA_TYPE chainLength = ps->cm_x[0] - ps->cm_x[appConfig.monomers_per_chain - 1];
		DATA_TYPE offset = fabs(chainLength / (appConfig.dopants_per_chain + 1));
		DATA_TYPE initPos = ps->cm_x[0] + offset;

		for (int i = 0; i < appConfig.dopants_per_chain; i++)
		{
			ps->dop_x[i] = initPos + i * offset;
			ps->dop_y[i] = 0.0;
			ps->dop_z[i] = appConfig.d_off;
		}
#else
		//// placeholder comment
		//DATA_TYPE chainLength = fabs(ps->cm_x[0] - ps->cm_x[appConfig.monomers_per_chain - 1]);
		//DATA_TYPE g4 = chainLength / 4.0;
		//DATA_TYPE g8 = chainLength / 8.0;
		//DATA_TYPE offset = fabs(chainLength / 2.0);
		//DATA_TYPE initPos = ps->cm_x[0] + offset;
		//DATA_TYPE t = 0;

		//for (int i = 0; i < appConfig.dopants_per_chain; i++)
		//{
		//	ps->dop_x[i] = initPos - (g8 + i * g4);
		//	ps->dop_y[i] = 0.0;
		//	ps->dop_z[i] = appConfig.d_off;
		//}

		ps->dop_x[0] = ps->cm_x[1];
		ps->dop_y[0] = ps->cm_y[1];
		ps->dop_z[0] = appConfig.d_off;

		ps->dop_x[1] = ps->cm_x[4];
		ps->dop_y[1] = ps->cm_y[4];
		ps->dop_z[1] = appConfig.d_off;

		ps->dop_x[2] = ps->cm_x[7];
		ps->dop_y[2] = ps->cm_y[7];
		ps->dop_z[2] = appConfig.d_off;

		ps->dop_x[3] = ps->cm_x[10];
		ps->dop_y[3] = ps->cm_y[10];
		ps->dop_z[3] = appConfig.d_off;

#endif
	}

	// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
	DATA_TYPE min = MIN(ps->x_size, ps->y_size);
	min = MIN(min, ps->z_size);
	if (appConfig.r_cut < 0)
	{
		ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
		//if (ps->r_cut < ps->cm_x[11] - ps->cm_x[0])
		//	ps->r_cut = ps->cm_x[11] - ps->cm_x[0];
	}
	else
	{
		ps->r_cut = appConfig.r_cut;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	return ps;
}

PARTICLE_SYSTEM * buildParticleSystem(const AppConfig & appConfig, int xdim, int ydim, int zdim, bool addDopants)
{
	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	PARTICLE_SYSTEM * referenceChain = buildReferencedChain(appConfig, addDopants);
	translateOligomer(*referenceChain, 0, -referenceChain->olig_cm_x[0], -referenceChain->olig_cm_y[0], -referenceChain->olig_cm_z[0], true);
	calculateOligomersCenterOfMasses(*referenceChain);

	if (referenceChain == NULL)
		return NULL;

	int nMonomers = xdim * ydim * zdim * appConfig.chains_per_unit_cell * appConfig.monomers_per_chain;
	int nDopants = xdim * ydim * zdim * appConfig.chains_per_unit_cell  * appConfig.dopants_per_chain;

	PARTICLE_SYSTEM * ps = new PARTICLE_SYSTEM;
	ps->x_size = xdim * referenceChain->x_size;
	ps->y_size = ydim * referenceChain->y_size;
	ps->z_size = zdim * referenceChain->z_size;
	ps->uc_x = referenceChain->x_size;
	ps->uc_y = referenceChain->y_size;
	ps->uc_z = referenceChain->z_size;

	ps->angleLimit = appConfig.angle_limit;
	ps->monomers_per_chain = appConfig.monomers_per_chain;

	ps->nMonomers = nMonomers;
	ps->cm_x = new DATA_TYPE[nMonomers];
	ps->cm_y = new DATA_TYPE[nMonomers];
	ps->cm_z = new DATA_TYPE[nMonomers];
	ps->dpm_x = new DATA_TYPE[nMonomers];
	ps->dpm_y = new DATA_TYPE[nMonomers];
	ps->dpm_z = new DATA_TYPE[nMonomers];
	ps->n_x = new DATA_TYPE[nMonomers];
	ps->n_y = new DATA_TYPE[nMonomers];
	ps->n_z = new DATA_TYPE[nMonomers];
	ps->heading = new DATA_TYPE[nMonomers];
	ps->pitch = new DATA_TYPE[nMonomers];
	ps->bank = new DATA_TYPE[nMonomers];
	memset(ps->heading, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->pitch, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->bank, 0, sizeof(DATA_TYPE) * nMonomers);
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->nPolymers = ps->nMonomers / appConfig.monomers_per_chain;
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

	ps->z0 = new DATA_TYPE[ps->nPolymers];
	int polymerCtr = 0;

	if (addDopants)
		ps->nDopants = nDopants;
	else
		ps->nDopants = 0;

	ps->dop_x = new DATA_TYPE[nDopants];
	ps->dop_y = new DATA_TYPE[nDopants];
	ps->dop_z = new DATA_TYPE[nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);


	DATA_TYPE refz = 0.0, refy = 0.0, refx = 0.0;
	int midx = 0, didx = 0, pidx = 0;

	for (int z = 0; z < zdim; z++)
	{
		refz = z *  appConfig.z_off * 2.0;
		for (int y = 0; y < ydim; y++)
		{
			refy = y *  appConfig.y_off * 2.0;
			for (int x = 0; x < xdim; x++)
			{
				refx = x *  appConfig.x_off * 2.0;

				{
					for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
					{
						ps->cm_x[midx] = referenceChain->cm_x[mi];
						ps->cm_y[midx] = referenceChain->cm_y[mi];
						ps->cm_z[midx] = referenceChain->cm_z[mi];
						ps->dpm_x[midx] = referenceChain->dpm_x[mi];
						ps->dpm_y[midx] = referenceChain->dpm_y[mi];
						ps->dpm_z[midx] = referenceChain->dpm_z[mi];
						ps->n_x[midx] = referenceChain->n_x[mi];
						ps->n_y[midx] = referenceChain->n_y[mi];
						ps->n_z[midx] = referenceChain->n_z[mi];
						midx++;
					}
					translateOligomer(*ps, pidx++, refx, refy, refz);

					ps->z0[polymerCtr++] = refz;
					if (addDopants)
					{
						for (int di = 0; di < appConfig.dopants_per_chain; di++)
						{
							ps->dop_x[didx] = referenceChain->dop_x[di] + refx;
							ps->dop_y[didx] = referenceChain->dop_y[di] + refy;
							ps->dop_z[didx] = referenceChain->dop_z[di] + refz;
							didx++;
						}
					}
					for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
					{
						ps->cm_x[midx] = referenceChain->cm_x[mi];
						ps->cm_y[midx] = referenceChain->cm_y[mi];
						ps->cm_z[midx] = referenceChain->cm_z[mi];
						ps->dpm_x[midx] = referenceChain->dpm_x[mi];
						ps->dpm_y[midx] = referenceChain->dpm_y[mi];
						ps->dpm_z[midx] = referenceChain->dpm_z[mi];
						ps->n_x[midx] = referenceChain->n_x[mi];
						ps->n_y[midx] = referenceChain->n_y[mi];
						ps->n_z[midx] = referenceChain->n_z[mi];

						midx++;
					}
					translateOligomer(*ps, pidx++, refx + appConfig.x_off, refy + appConfig.y_off, refz);

					ps->z0[polymerCtr++] = refz;
					if (addDopants)
					{
						for (int di = 0; di < appConfig.dopants_per_chain; di++)
						{
							ps->dop_x[didx] = referenceChain->dop_x[di] + refx + appConfig.x_off;
							ps->dop_y[didx] = referenceChain->dop_y[di] + refy + appConfig.y_off;
							ps->dop_z[didx] = referenceChain->dop_z[di] + refz;
							didx++;
						}
					}
					for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
					{
						ps->cm_x[midx] = referenceChain->cm_x[mi];
						ps->cm_y[midx] = referenceChain->cm_y[mi];
						ps->cm_z[midx] = referenceChain->cm_z[mi];
						ps->dpm_x[midx] = referenceChain->dpm_x[mi];
						ps->dpm_y[midx] = referenceChain->dpm_y[mi];
						ps->dpm_z[midx] = referenceChain->dpm_z[mi];
						ps->n_x[midx] = referenceChain->n_x[mi];
						ps->n_y[midx] = referenceChain->n_y[mi];
						ps->n_z[midx] = referenceChain->n_z[mi];

						midx++;
					}
					translateOligomer(*ps, pidx++, refx, refy + appConfig.y_off, refz + appConfig.z_off);

					ps->z0[polymerCtr++] = refz + appConfig.z_off;
					if (addDopants)
					{
						for (int di = 0; di < appConfig.dopants_per_chain; di++)
						{
							ps->dop_x[didx] = referenceChain->dop_x[di] + refx;
							ps->dop_y[didx] = referenceChain->dop_y[di] + refy + appConfig.y_off;
							ps->dop_z[didx] = referenceChain->dop_z[di] + refz + appConfig.z_off;
							didx++;
						}
					}
					for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
					{
						ps->cm_x[midx] = referenceChain->cm_x[mi];
						ps->cm_y[midx] = referenceChain->cm_y[mi];
						ps->cm_z[midx] = referenceChain->cm_z[mi];
						ps->dpm_x[midx] = referenceChain->dpm_x[mi];
						ps->dpm_y[midx] = referenceChain->dpm_y[mi];
						ps->dpm_z[midx] = referenceChain->dpm_z[mi];
						ps->n_x[midx] = referenceChain->n_x[mi];
						ps->n_y[midx] = referenceChain->n_y[mi];
						ps->n_z[midx] = referenceChain->n_z[mi];
						midx++;
					}
					translateOligomer(*ps, pidx++, refx + appConfig.x_off, refy, refz + appConfig.z_off);

					ps->z0[polymerCtr++] = refz + appConfig.z_off;
					if (addDopants)
					{
						for (int di = 0; di < appConfig.dopants_per_chain; di++)
						{
							ps->dop_x[didx] = referenceChain->dop_x[di] + refx + appConfig.x_off;
							ps->dop_y[didx] = referenceChain->dop_y[di] + refy;
							ps->dop_z[didx] = referenceChain->dop_z[di] + refz + appConfig.z_off;
							didx++;
						}
					}
				}
			}
		}
	}

	// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
	DATA_TYPE min = MIN(ps->x_size, ps->y_size);
	min = MIN(min, ps->z_size);
	if (appConfig.r_cut < 0)
	{
		ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
		//if (ps->r_cut < ps->cm_x[11] - ps->cm_x[0])
		//	ps->r_cut = ps->cm_x[11] - ps->cm_x[0];
	}
	else
	{
		ps->r_cut = appConfig.r_cut;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);
	
	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	centerParticleSystem(*ps);

	// update  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	// delete the reference chain
	delete referenceChain;

	return ps;
}

void initReferenceMonomers()
{
	a.nAtoms = 8;
	a.name = new char[a.nAtoms];
	a.x = new DATA_TYPE[a.nAtoms];
	a.y = new DATA_TYPE[a.nAtoms];
	a.z = new DATA_TYPE[a.nAtoms];

	a.name[0] = 'N'; a.x[0] = 0.0;	          a.y[0] = 1.34414125;	 a.z[0] = 0.0;
	a.name[1] = 'C'; a.x[1] = -1.12979875;	  a.y[1] = 0.54976125;	 a.z[1] = 0.0;
	a.name[2] = 'C'; a.x[2] = -0.71337875;	  a.y[2] = -0.76491875;	 a.z[2] = 0.0;
	a.name[3] = 'C'; a.x[3] = 0.71338125;	  a.y[3] = -0.76491875;	 a.z[3] = 0.0;
	a.name[4] = 'C'; a.x[4] = 1.12980125;	  a.y[4] = 0.54976125;	 a.z[4] = 0.0;
	a.name[5] = 'H'; a.x[5] = 0.0;	          a.y[5] = 2.34634125;	 a.z[5] = 0.0;
	a.name[6] = 'H'; a.x[6] = -1.35461875;	  a.y[6] = -1.63007875;	 a.z[6] = 0.0;
	a.name[7] = 'H'; a.x[7] = 1.35461125;	  a.y[7] = -1.63008875;	 a.z[7] = 0.0;

	b.nAtoms = 8;
	b.name = new char[b.nAtoms];
	b.x = new DATA_TYPE[b.nAtoms];
	b.y = new DATA_TYPE[b.nAtoms];
	b.z = new DATA_TYPE[b.nAtoms];

	DATA_TYPE rx = 0.0, ry = 0.0, rz = 0.0;
	for (int i = 0; i < a.nAtoms; i++)
	{
		b.name[i] = a.name[i];
		rx = a.x[i];
		ry = a.y[i];
		rz = a.z[i];

		rotatePoint(DEG_TO_RAD(180.0), 0.0, 0.0, rx, ry, rz);

		b.x[i] = rx;
		b.y[i] = ry;
		b.z[i] = rz;
	}

	a_t.nAtoms = 1;
	a_t.name = new char[a_t.nAtoms];
	a_t.x = new DATA_TYPE[a_t.nAtoms];
	a_t.y = new DATA_TYPE[a_t.nAtoms];
	a_t.z = new DATA_TYPE[a_t.nAtoms];

	a_t.name[0] = 'H';
	a_t.x[0] = 2.10757;
	a_t.y[0] = 0.87989;
	a_t.z[0] = 0.0;

	b_t.nAtoms = 9;
	b_t.name = new char[b_t.nAtoms];
	b_t.x = new DATA_TYPE[b_t.nAtoms];
	b_t.y = new DATA_TYPE[b_t.nAtoms];
	b_t.z = new DATA_TYPE[b_t.nAtoms];

	b_t.name[0] = 'H';
	rx = -2.10757;
	ry = 0.87989;
	rz = 0.0;

	rotatePoint(DEG_TO_RAD(180.0), 0.0, 0.0, rx, ry, rz);

	b_t.x[0] = rx;
	b_t.y[0] = ry;
	b_t.z[0] = rz;
}

void saveParticleSystem(PARTICLE_SYSTEM & ps, const string & fName, ios_base::openmode mode)
{
	ofstream out(fName.c_str(), mode);
	DATA_TYPE nx = 0.0, ny = 0.0, nz = 0.0;
	DATA_TYPE tx = 0.0, ty = 0.0, tz = 0.0;
	int input_chain_id = 0, exc_lb = 0, exc_ub = 0;
	int monomers_per_chain = ps.monomers_per_chain;

	if (out.is_open())
	{
		out << ps.nMonomers * a.nAtoms + ps.nDopants + 2 * ps.nPolymers << endl << endl;

		for (int y = 0; y < ps.nMonomers; y++)
		{
			input_chain_id = y / monomers_per_chain;
			exc_lb = input_chain_id * monomers_per_chain;
			exc_ub = exc_lb + monomers_per_chain - 1;

			tx = ps.cm_x[y];
			ty = ps.cm_y[y];
			tz = ps.cm_z[y];
			//wrapInBox(ps.x_size, ps.y_size, ps.z_size, tx, ty, tz);

			if (y % 2 != 0)
			{
				// now translate the monomer to 0,0,0
				for (int i = 0; i < a.nAtoms; i++)
				{
					nx = a.x[i];
					ny = a.y[i];
					nz = a.z[i];

					rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

					nx += tx;
					ny += ty;
					nz += tz;

					out << a.name[i] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;

					
				}
				if (y == exc_ub)
				{
					nx = a_t.x[0];
					ny = a_t.y[0];
					nz = a_t.z[0];

					rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

					nx += tx;
					ny += ty;
					nz += tz;

					out << a_t.name[0] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
				}
			}
			else
			{
				// now translate the monomer to 0,0,0
				for (int i = 0; i < b.nAtoms; i++)
				{
					nx = b.x[i];
					ny = b.y[i];
					nz = b.z[i];

					rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

					nx += tx;
					ny += ty;
					nz += tz;

					out << b.name[i] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
				}
				if (y == exc_lb)
				{
					nx = b_t.x[0];
					ny = b_t.y[0];
					nz = b_t.z[0];

					rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

					nx += tx;
					ny += ty;
					nz += tz;

					out << b_t.name[0] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
				}
			}
		}

		for (int i = 0; i < ps.nDopants; i++)
		{
			nx = ps.dop_x[i];
			ny = ps.dop_y[i];
			nz = ps.dop_z[i];
			//wrapInBox(ps.x_size, ps.y_size, ps.z_size, nx, ny, nz);

			out << "Cl" << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
		}

		out.flush();
		out.close();
	}
}

void saveParticleSystemCM(PARTICLE_SYSTEM & ps, const string & fName, ios_base::openmode mode)
{
	// TODO: this is temporary 
	saveParticleSystem(ps, fName, mode);
	return;

	ofstream out(fName.c_str(), mode);
	DATA_TYPE nx = 0.0, ny = 0.0, nz = 0.0;
	DATA_TYPE tx = 0.0, ty = 0.0, tz = 0.0;

	if (out.is_open())
	{
		out << ps.nMonomers + ps.nDopants << endl << endl;

		for (int y = 0; y < ps.nMonomers; y++)
		{
			tx = ps.cm_x[y];
			ty = ps.cm_y[y];
			tz = ps.cm_z[y];
			wrapInBox(ps.x_size, ps.y_size, ps.z_size, tx, ty, tz);
			out << 'C' << setw(15) << tx << setw(15) << ty << setw(15) << tz << endl;
		}

		for (int i = 0; i < ps.nDopants; i++)
		{
			nx = ps.dop_x[i];
			ny = ps.dop_y[i];
			nz = ps.dop_z[i];
			wrapInBox(ps.x_size, ps.y_size, ps.z_size, tx, ty, tz);

			out << "Cl" << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
		}

		out.flush();
		out.close();
	}
}

void saveParticleSystemToBinaryFile(PARTICLE_SYSTEM &ps, const string & fName)
{
	int nMonomers = ps.nMonomers;
	int nDopants = ps.nDopants;

	ofstream out(fName.c_str(), std::ios::out | std::ios::binary);
	if (out.is_open())
	{
		out.write((char *)&nMonomers, sizeof(int));
		out.write((char *)&nDopants, sizeof(int));
		out.write((char *)&ps.r_cut, sizeof(DATA_TYPE));
		out.write((char *)&ps.density, sizeof(DATA_TYPE));
		out.write((char *)&ps.x_size, sizeof(DATA_TYPE));
		out.write((char *)&ps.y_size, sizeof(DATA_TYPE));
		out.write((char *)&ps.z_size, sizeof(DATA_TYPE));
		out.write((char *)&ps.angleLimit, sizeof(DATA_TYPE));
		out.write((char *)&ps.monomers_per_chain, sizeof(int));
		out.write((char *)&ps.uc_x, sizeof(DATA_TYPE));
		out.write((char *)&ps.uc_y, sizeof(DATA_TYPE));
		out.write((char *)&ps.uc_z, sizeof(DATA_TYPE));

		out.write((char *)ps.cm_x, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.cm_y, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.cm_z, sizeof(DATA_TYPE) * nMonomers);

		out.write((char *)ps.n_x, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.n_y, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.n_z, sizeof(DATA_TYPE) * nMonomers);

		out.write((char *)ps.dpm_x, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.dpm_y, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.dpm_z, sizeof(DATA_TYPE) * nMonomers);

		out.write((char *)ps.heading, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.pitch, sizeof(DATA_TYPE) * nMonomers);
		out.write((char *)ps.bank, sizeof(DATA_TYPE) * nMonomers);

		out.write((char *)ps.dop_x, sizeof(DATA_TYPE) * nDopants);
		out.write((char *)ps.dop_y, sizeof(DATA_TYPE) * nDopants);
		out.write((char *)ps.dop_z, sizeof(DATA_TYPE) * nDopants);

		out.write((char *)ps.z0, sizeof(DATA_TYPE) * ps.nPolymers);
	}
	out.flush();
	out.close();
}

PARTICLE_SYSTEM * loadParticleSystemRestartFile(const string& fName, AppConfig& appConfig)
{
	ifstream in(fName.c_str(), std::ios::in);
	if (!in.is_open())
	{
		cout << "Failed to load file: " << fName << endl;
		return NULL;
	}

	DATA_TYPE x_size = 0.0, y_size = 0.0, z_size = 0.0;
	DATA_TYPE r_cut = 0.0;
	oligomer_mass = appConfig.oligomer_mass;
	dopant_mass = appConfig.dopant_mass;

		string line;
	// read the header of the file, should be 2 lines consisting of the size of the box and the cutoff radius
	// size
	std::getline(in, line);
	istringstream iss(line);
	iss >> x_size >> y_size >> z_size;
	// cutoff
	std::getline(in, line);
	iss = istringstream(line);
	iss >> r_cut;
	cout << x_size << '\t' << y_size << '\t' << z_size << '\t' << r_cut << endl;
	bool isDopant = false;
	string particleType;
	vector<DATA_TYPE> cmx, cmy, cmz, h, p, b, dopx, dopy, dopz;
	DATA_TYPE tx = 0.0, ty = 0.0, tz = 0.0, th = 0.0, tp = 0.0, tb = 0.0, tdx = 0.0, tdy = 0.0, tdz = 0.0;
	
	while (std::getline(in, line))
	{
		iss = istringstream(line);
		iss >> particleType >> tx >> ty >> tz >> th >> tp >> tb;
		if (particleType == "X")  // monomer center of mass
		{
			cmx.push_back(tx);
			cmy.push_back(ty);
			cmz.push_back(tz);

			h.push_back(th);
			p.push_back(tp);
			b.push_back(tb);
		}
		
		if (particleType == "CL" || particleType == "Cl" || particleType == "cl" ||
			particleType == "F")
		{
			iss >> particleType >> tx >> ty >> tz;
			dopx.push_back(tx);
			dopy.push_back(ty);
			dopz.push_back(tz);
		}
	}

	// now create the particle system data structure
	PARTICLE_SYSTEM* ps = new PARTICLE_SYSTEM;
	ps->r_cut = r_cut;
	ps->x_size = x_size;
	ps->y_size = y_size;
	ps->z_size = z_size;
	ps->uc_x = -1;
	ps->uc_y = -1;
	ps->uc_z = -1;

	ps->angleLimit = appConfig.angle_limit;
	ps->monomers_per_chain = appConfig.monomers_per_chain;
	ps->nMonomers = cmx.size();
	ps->nDopants = dopx.size();
	ps->nPolymers = ps->nMonomers / ps->monomers_per_chain;

	ps->cm_x = new DATA_TYPE[ps->nMonomers];
	ps->cm_y = new DATA_TYPE[ps->nMonomers];
	ps->cm_z = new DATA_TYPE[ps->nMonomers];
	ps->dpm_x = new DATA_TYPE[ps->nMonomers];
	ps->dpm_y = new DATA_TYPE[ps->nMonomers];
	ps->dpm_z = new DATA_TYPE[ps->nMonomers];
	ps->n_x = new DATA_TYPE[ps->nMonomers];
	ps->n_y = new DATA_TYPE[ps->nMonomers];
	ps->n_z = new DATA_TYPE[ps->nMonomers];
	ps->heading = new DATA_TYPE[ps->nMonomers];
	ps->pitch = new DATA_TYPE[ps->nMonomers];
	ps->bank = new DATA_TYPE[ps->nMonomers];
	memset(ps->heading, 0, sizeof(DATA_TYPE) * ps->nMonomers);
	memset(ps->pitch, 0, sizeof(DATA_TYPE) * ps->nMonomers);
	memset(ps->bank, 0, sizeof(DATA_TYPE) * ps->nMonomers);
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->nPolymers = ps->nMonomers / appConfig.monomers_per_chain;
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];
	ps->z0 = new DATA_TYPE[ps->nPolymers];
	ps->dop_x = new DATA_TYPE[ps->nDopants];
	ps->dop_y = new DATA_TYPE[ps->nDopants];
	ps->dop_z = new DATA_TYPE[ps->nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);

	// now populate the dopants first 
	for (int i = 0; i < dopx.size(); i++)
	{
		ps->dop_x[i] = dopx[i];
		ps->dop_y[i] = dopy[i];
		ps->dop_z[i] = dopz[i];
	}

	// now populate the monomer
	for (int i = 0; i < cmx.size(); i++)
	{
		ps->cm_x[i] = cmx[i];
		ps->cm_y[i] = cmy[i];
		ps->cm_z[i] = cmz[i];

		ps->heading[i] = h[i];
		ps->pitch[i]   = p[i];
		ps->bank[i] = b[i];

		if (i % 2 == 0)
		{
			ps->n_x[i] = 0.0;
			ps->n_y[i] = 0.0;
			ps->n_z[i] = -1.0;
			ps->dpm_x[i] = 0.0;
			ps->dpm_y[i] = -1.0;
			ps->dpm_z[i] = 0.0;
		}
		else
		{
			ps->n_x[i] = 0.0;
			ps->n_y[i] = 0.0;
			ps->n_z[i] = 1.0;
			ps->dpm_x[i] = 0.0;
			ps->dpm_y[i] = 1.0;
			ps->dpm_z[i] = 0.0;
		}

		// rotate the monomer
		rotateMonomer(*ps, i, DEG_TO_RAD(ps->heading[i]), DEG_TO_RAD(ps->pitch[i]), DEG_TO_RAD(ps->bank[i]));
	}

	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	recenterParticleSystem(*ps, dx, dy, dz);

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	return ps;
}

PARTICLE_SYSTEM * loadParticleSystemBinaryFile(const string & fName, AppConfig & appConfig)
{
	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	PARTICLE_SYSTEM * ps = NULL;

	int nMonomers = 0, nDopants = 0;
	int monomers_per_chain = 0;
	DATA_TYPE x_size = 0.0, y_size = 0.0, z_size = 0.0;
	DATA_TYPE r_cut = 0.0;
	DATA_TYPE density = 0.0;
	DATA_TYPE angleLimit = 0.0;
	DATA_TYPE uc_x = 0.0, uc_y = 0.0, uc_z = 0.0;

	DATA_TYPE * cm_x = NULL;
	DATA_TYPE * cm_y = NULL;
	DATA_TYPE * cm_z = NULL;
	DATA_TYPE * dpm_x = NULL;
	DATA_TYPE * dpm_y = NULL;
	DATA_TYPE * dpm_z = NULL;
	DATA_TYPE * n_x = NULL;
	DATA_TYPE * n_y = NULL;
	DATA_TYPE * n_z = NULL;
	DATA_TYPE * heading = NULL;
	DATA_TYPE * pitch = NULL;
	DATA_TYPE * bank = NULL;
	DATA_TYPE * dop_x = NULL;
	DATA_TYPE * dop_y = NULL;
	DATA_TYPE * dop_z = NULL;
	DATA_TYPE * z0 = NULL;

	ifstream in(fName.c_str(), std::ios::in | std::ios::binary);
	if (in.is_open())
	{
		in.read((char *)&nMonomers, sizeof(int));
		in.read((char *)&nDopants, sizeof(int));
		in.read((char *)&r_cut, sizeof(DATA_TYPE));
		in.read((char *)&density, sizeof(DATA_TYPE));
		in.read((char *)&x_size, sizeof(DATA_TYPE));
		in.read((char *)&y_size, sizeof(DATA_TYPE));
		in.read((char *)&z_size, sizeof(DATA_TYPE));
		in.read((char *)&angleLimit, sizeof(DATA_TYPE));
		in.read((char *)&monomers_per_chain, sizeof(int));
		in.read((char *)&uc_x, sizeof(DATA_TYPE));
		in.read((char *)&uc_y, sizeof(DATA_TYPE));
		in.read((char *)&uc_z, sizeof(DATA_TYPE));

		cm_x = new DATA_TYPE[nMonomers];
		in.read((char *)cm_x, sizeof(DATA_TYPE) * nMonomers);
		cm_y = new DATA_TYPE[nMonomers];
		in.read((char *)cm_y, sizeof(DATA_TYPE) * nMonomers);
		cm_z = new DATA_TYPE[nMonomers];
		in.read((char *)cm_z, sizeof(DATA_TYPE) * nMonomers);

		n_x = new DATA_TYPE[nMonomers];
		in.read((char *)n_x, sizeof(DATA_TYPE) * nMonomers);
		n_y = new DATA_TYPE[nMonomers];
		in.read((char *)n_y, sizeof(DATA_TYPE) * nMonomers);
		n_z = new DATA_TYPE[nMonomers];
		in.read((char *)n_z, sizeof(DATA_TYPE) * nMonomers);

		dpm_x = new DATA_TYPE[nMonomers];
		in.read((char *)dpm_x, sizeof(DATA_TYPE) * nMonomers);
		dpm_y = new DATA_TYPE[nMonomers];
		in.read((char *)dpm_y, sizeof(DATA_TYPE) * nMonomers);
		dpm_z = new DATA_TYPE[nMonomers];
		in.read((char *)dpm_z, sizeof(DATA_TYPE) * nMonomers);

		heading = new DATA_TYPE[nMonomers];
		in.read((char *)heading, sizeof(DATA_TYPE) * nMonomers);
		pitch = new DATA_TYPE[nMonomers];
		in.read((char *)pitch, sizeof(DATA_TYPE) * nMonomers);
		bank = new DATA_TYPE[nMonomers];
		in.read((char *)bank, sizeof(DATA_TYPE) * nMonomers);

		dop_x = new DATA_TYPE[nDopants];
		in.read((char *)dop_x, sizeof(DATA_TYPE) * nDopants);
		dop_y = new DATA_TYPE[nDopants];
		in.read((char *)dop_y, sizeof(DATA_TYPE) * nDopants);
		dop_z = new DATA_TYPE[nDopants];
		in.read((char *)dop_z, sizeof(DATA_TYPE) * nDopants);

		z0 = new DATA_TYPE[nMonomers / monomers_per_chain];
		in.read((char *)z0, sizeof(DATA_TYPE) * (nMonomers / monomers_per_chain));
		// to support backwards compatibility with already generated *.bin files
		if (!in)
		{
			cout << "saved file does not contain z0 values, calculating average z value for all oligomers" << endl;
			memset(z0, 0, sizeof(DATA_TYPE) * (nMonomers / monomers_per_chain));
			int lb = 0, ub = 0;
			DATA_TYPE zz = 0.0;
			for (int outer = 0; outer < nMonomers / monomers_per_chain; outer++)
			{
				lb = outer * monomers_per_chain;
				ub = lb + monomers_per_chain;
				for (int i = lb; i < ub; i++)
				{
					zz += cm_z[i];
				}
				zz /= (DATA_TYPE)monomers_per_chain;
				z0[outer] = zz;
				zz = 0;
			}
		}

		in.close();

		ps = new PARTICLE_SYSTEM;
		ps->nMonomers = nMonomers;
		ps->nDopants = nDopants;
		ps->monomers_per_chain = monomers_per_chain;
		ps->x_size = x_size;
		ps->y_size = y_size;
		ps->z_size = z_size;
		ps->r_cut = r_cut;
		ps->angleLimit = angleLimit;
		ps->density = density;
		ps->uc_x = uc_x;
		ps->uc_y = uc_y;
		ps->uc_z = uc_z;

		ps->cm_x = cm_x;
		ps->cm_y = cm_y;
		ps->cm_z = cm_z;

		ps->n_x = n_x;
		ps->n_y = n_y;
		ps->n_z = n_z;

		ps->dpm_x = dpm_x;
		ps->dpm_y = dpm_y;
		ps->dpm_z = dpm_z;

		ps->heading = heading;
		ps->pitch = pitch;
		ps->bank = bank;

		ps->z0 = z0;

		ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
		ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
		memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
		memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
		ps->nPolymers = ps->nMonomers / ps->monomers_per_chain;
		ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
		ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
		ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];
		memset(ps->olig_cm_x, 0, sizeof(DATA_TYPE) * ps->nPolymers);
		memset(ps->olig_cm_y, 0, sizeof(DATA_TYPE) * ps->nPolymers);
		memset(ps->olig_cm_z, 0, sizeof(DATA_TYPE) * ps->nPolymers);

		ps->dop_x = dop_x;
		ps->dop_y = dop_y;
		ps->dop_z = dop_z;

		ps->nPolymers = ps->nMonomers / ps->monomers_per_chain;

		DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
		recenterParticleSystem(*ps, dx, dy, dz);

		// calcualte  the oligomer's centers of mass
		calculateOligomersCenterOfMasses(*ps);

		// determine if the cutoff needs to be overridden
		if (appConfig.r_cut > 0.0)
			ps->r_cut = appConfig.r_cut;

		ps->density = ((ps->nPolymers * appConfig.oligomer_mass) + ps->nDopants * appConfig.dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	}

	return ps;
}

void printParticleSystem(PARTICLE_SYSTEM & ps)
{
	cout << ps.nMonomers << endl << endl;
	for (int i = 0; i < ps.nMonomers; i++)
	{
		cout << setw(15) << ps.cm_x[i] << setw(15) << ps.cm_y[i] << setw(15) << ps.cm_z[i] << setw(15) << RAD_TO_DEG(ps.heading[i]) << setw(15) << RAD_TO_DEG(ps.pitch[i]) << setw(15) << RAD_TO_DEG(ps.bank[i]) << endl;
	}
}

void rotateMonomer(PARTICLE_SYSTEM & ps, int mIdx, DATA_TYPE magnitude)
{
	DATA_TYPE p1_x = -1.0, p1_y = 0.0, p1_z = 0.0;
	DATA_TYPE p2_x = 1.0, p2_y = 0.0, p2_z = 0.0;

	if (mIdx % 2 == 0)
	{
		ps.n_x[mIdx] = 0.0;
		ps.n_y[mIdx] = 0.0;
		ps.n_z[mIdx] = -1.0;
		ps.dpm_x[mIdx] = 0.0;
		ps.dpm_y[mIdx] = -1.0;
		ps.dpm_z[mIdx] = 0.0;
	}
	else
	{
		ps.n_x[mIdx] = 0.0;
		ps.n_y[mIdx] = 0.0;
		ps.n_z[mIdx] = 1.0;
		ps.dpm_x[mIdx] = 0.0;
		ps.dpm_y[mIdx] = 1.0;
		ps.dpm_z[mIdx] = 0.0;
	}

	randomRotationAxis(magnitude, ps.heading[mIdx], ps.pitch[mIdx], ps.bank[mIdx], ps.angleLimit);

	// build rotation matrix
	DATA_TYPE eulerAngle[3] = { ps.heading[mIdx], ps.pitch[mIdx], ps.bank[mIdx] };

	DATA_TYPE s1 = sin(eulerAngle[1]);
	DATA_TYPE c1 = cos(eulerAngle[1]);
	DATA_TYPE s2 = sin(eulerAngle[0]);
	DATA_TYPE c2 = cos(eulerAngle[0]);
	DATA_TYPE s3 = sin(eulerAngle[2]);
	DATA_TYPE c3 = cos(eulerAngle[2]);

	DATA_TYPE mtRot[3][3];
	mtRot[0][0] = c1 * c3 - s1 * s2 * s3;
	mtRot[1][0] = c3 * s1 + c1 * s2 * s3;
	mtRot[2][0] = -c2 * s3;
	mtRot[0][1] = -c2 * s1;
	mtRot[1][1] = c1 * c2;
	mtRot[2][1] = s2;
	mtRot[0][2] = c1 * s3 + c3 * s1 * s2;
	mtRot[1][2] = s1 * s3 - c1 * c3 * s2;
	mtRot[2][2] = c2 * c3;


	// rotate dipole moment
	DATA_TYPE t_x = mtRot[0][0] * ps.dpm_x[mIdx] + mtRot[0][1] * ps.dpm_y[mIdx] + mtRot[0][2] * ps.dpm_z[mIdx];
	DATA_TYPE t_y = mtRot[1][0] * ps.dpm_x[mIdx] + mtRot[1][1] * ps.dpm_y[mIdx] + mtRot[1][2] * ps.dpm_z[mIdx];
	DATA_TYPE t_z = mtRot[2][0] * ps.dpm_x[mIdx] + mtRot[2][1] * ps.dpm_y[mIdx] + mtRot[2][2] * ps.dpm_z[mIdx];
	ps.dpm_x[mIdx] = t_x;
	ps.dpm_y[mIdx] = t_y;
	ps.dpm_z[mIdx] = t_z;

	// rotate normal
	t_x = mtRot[0][0] * ps.n_x[mIdx] + mtRot[0][1] * ps.n_y[mIdx] + mtRot[0][2] * ps.n_z[mIdx];
	t_y = mtRot[1][0] * ps.n_x[mIdx] + mtRot[1][1] * ps.n_y[mIdx] + mtRot[1][2] * ps.n_z[mIdx];
	t_z = mtRot[2][0] * ps.n_x[mIdx] + mtRot[2][1] * ps.n_y[mIdx] + mtRot[2][2] * ps.n_z[mIdx];
	ps.n_x[mIdx] = t_x;
	ps.n_y[mIdx] = t_y;
	ps.n_z[mIdx] = t_z;
}

void rotateMonomer(PARTICLE_SYSTEM& ps, int mIdx, DATA_TYPE heading, DATA_TYPE pitch, DATA_TYPE bank)
{
	DATA_TYPE p1_x = -1.0, p1_y = 0.0, p1_z = 0.0;
	DATA_TYPE p2_x = 1.0, p2_y = 0.0, p2_z = 0.0;

	if (mIdx % 2 == 0)
	{
		ps.n_x[mIdx] = 0.0;
		ps.n_y[mIdx] = 0.0;
		ps.n_z[mIdx] = -1.0;
		ps.dpm_x[mIdx] = 0.0;
		ps.dpm_y[mIdx] = -1.0;
		ps.dpm_z[mIdx] = 0.0;
	}
	else
	{
		ps.n_x[mIdx] = 0.0;
		ps.n_y[mIdx] = 0.0;
		ps.n_z[mIdx] = 1.0;
		ps.dpm_x[mIdx] = 0.0;
		ps.dpm_y[mIdx] = 1.0;
		ps.dpm_z[mIdx] = 0.0;
	}

	// build rotation matrix
	DATA_TYPE x = heading;
	DATA_TYPE y = pitch;
	DATA_TYPE z = bank;

	
	DATA_TYPE rmat[3][3];

	//rmat[0][0] = cos(z) * cos(y) - sin(x) * sin(y) * sin(z);
	//rmat[0][1] = -cos(x) * sin(z);
	//rmat[0][2] = cos(z) * sin(y) + cos(y) * sin(x) * sin(z);

	//rmat[1][0] = cos(z) * sin(x) * sin(y) + cos(y) * sin(z);
	//rmat[1][1] = cos(x) * cos(z);
	//rmat[1][2] = -cos(y) * cos(z) * sin(x) + sin(y) * sin(z);

	//rmat[2][0] = -cos(x) * sin(y);
	//rmat[2][1] = sin(x);
	//rmat[2][2] = cos(x) * cos(y);

	//// rotate dipole moment
	//DATA_TYPE t_x = rmat[0][0] * ps.dpm_x[mIdx] + rmat[0][1] * ps.dpm_y[mIdx] + rmat[0][2] * ps.dpm_z[mIdx];
	//DATA_TYPE t_y = rmat[1][0] * ps.dpm_x[mIdx] + rmat[1][1] * ps.dpm_y[mIdx] + rmat[1][2] * ps.dpm_z[mIdx];
	//DATA_TYPE t_z = rmat[2][0] * ps.dpm_x[mIdx] + rmat[2][1] * ps.dpm_y[mIdx] + rmat[2][2] * ps.dpm_z[mIdx];
	//ps.dpm_x[mIdx] = t_x;
	//ps.dpm_y[mIdx] = t_y;
	//ps.dpm_z[mIdx] = t_z;

	//// rotate normal
	//t_x = rmat[0][0] * ps.n_x[mIdx] + rmat[0][1] * ps.n_y[mIdx] + rmat[0][2] * ps.n_z[mIdx];
	//t_y = rmat[1][0] * ps.n_x[mIdx] + rmat[1][1] * ps.n_y[mIdx] + rmat[1][2] * ps.n_z[mIdx];
	//t_z = rmat[2][0] * ps.n_x[mIdx] + rmat[2][1] * ps.n_y[mIdx] + rmat[2][2] * ps.n_z[mIdx];
	//ps.n_x[mIdx] = t_x;
	//ps.n_y[mIdx] = t_y;
	//ps.n_z[mIdx] = t_z;

	DATA_TYPE eulerAngle[3] = { heading, pitch, bank};

	DATA_TYPE s1 = sin(eulerAngle[1]);
	DATA_TYPE c1 = cos(eulerAngle[1]);
	DATA_TYPE s2 = sin(eulerAngle[0]);
	DATA_TYPE c2 = cos(eulerAngle[0]);
	DATA_TYPE s3 = sin(eulerAngle[2]);
	DATA_TYPE c3 = cos(eulerAngle[2]);

	DATA_TYPE mtRot[3][3];
	mtRot[0][0] = c1 * c3 - s1 * s2 * s3;
	mtRot[1][0] = c3 * s1 + c1 * s2 * s3;
	mtRot[2][0] = -c2 * s3;
	mtRot[0][1] = -c2 * s1;
	mtRot[1][1] = c1 * c2;
	mtRot[2][1] = s2;
	mtRot[0][2] = c1 * s3 + c3 * s1 * s2;
	mtRot[1][2] = s1 * s3 - c1 * c3 * s2;
	mtRot[2][2] = c2 * c3;


	// rotate dipole moment
	DATA_TYPE t_x = mtRot[0][0] * ps.dpm_x[mIdx] + mtRot[0][1] * ps.dpm_y[mIdx] + mtRot[0][2] * ps.dpm_z[mIdx];
	DATA_TYPE t_y = mtRot[1][0] * ps.dpm_x[mIdx] + mtRot[1][1] * ps.dpm_y[mIdx] + mtRot[1][2] * ps.dpm_z[mIdx];
	DATA_TYPE t_z = mtRot[2][0] * ps.dpm_x[mIdx] + mtRot[2][1] * ps.dpm_y[mIdx] + mtRot[2][2] * ps.dpm_z[mIdx];
	ps.dpm_x[mIdx] = t_x;
	ps.dpm_y[mIdx] = t_y;
	ps.dpm_z[mIdx] = t_z;

	// rotate normal
	t_x = mtRot[0][0] * ps.n_x[mIdx] + mtRot[0][1] * ps.n_y[mIdx] + mtRot[0][2] * ps.n_z[mIdx];
	t_y = mtRot[1][0] * ps.n_x[mIdx] + mtRot[1][1] * ps.n_y[mIdx] + mtRot[1][2] * ps.n_z[mIdx];
	t_z = mtRot[2][0] * ps.n_x[mIdx] + mtRot[2][1] * ps.n_y[mIdx] + mtRot[2][2] * ps.n_z[mIdx];
	ps.n_x[mIdx] = t_x;
	ps.n_y[mIdx] = t_y;
	ps.n_z[mIdx] = t_z;
}

void getNormal(int midx, DATA_TYPE x, DATA_TYPE y, DATA_TYPE z, DATA_TYPE & nx, DATA_TYPE& ny, DATA_TYPE& nz)
{
	// https://en.wikipedia.org/wiki/Euler_angles
	// from Proper Angle, Z1X2Z3 is used
    // Since Reference Normal is { 0,0,1 } or {0, 0, -1}, we don't need to have a full matrix multiplication
	// Only the Third column is taken for normal vector
	DATA_TYPE eulerAngle[3] = {x, z, y };
	DATA_TYPE normal[3] = {0.0, 0.0, 1.0};

	DATA_TYPE s1 = sin(eulerAngle[0]);
	DATA_TYPE c1 = cos(eulerAngle[0]);
	DATA_TYPE s2 = sin(eulerAngle[1]);
	DATA_TYPE c2 = cos(eulerAngle[1]);
	DATA_TYPE s3 = sin(eulerAngle[2]);
	DATA_TYPE c3 = cos(eulerAngle[2]);

	cout << s1 << '\t' << c1 << '\t';

	if (midx % 2 == 0)
	{
		normal[2] = 1.0;
	}
	else
	{
		normal[2] = -1.0;
	}
	nx = normal[0] = (c1 * s3 + c3 * s1 * s2) * normal[0];
	ny = normal[1] = (s1 * s3 - c1 * c3 * s2) * normal[1];
	nz = normal[2] = (c2 * c3) * normal[2];
}

void getDipole()
{

}

void rotateOligomer(PARTICLE_SYSTEM& ps, int idx, DATA_TYPE magnitude, bool moveDopants)
{
	int lb = idx * ps.monomers_per_chain;
	int ub = lb + ps.monomers_per_chain;
	
	for (int idx = lb; idx < ub; idx++)
	{
		rotateMonomer(ps, idx, magnitude);
	}

	//if (moveDopants)
	//{
	//	for (int idx = 0; idx < ps.nDopants; idx++)
	//	{
	//		ps.dop_x[idx] += dx;
	//		ps.dop_y[idx] += dy;
	//		ps.dop_z[idx] += dz;
	//	}
	//}

}

void reverseRotateMonomer(PARTICLE_SYSTEM & ps, int mIdx)
{
	if (mIdx % 2 == 0)
	{
		ps.n_x[mIdx] = 0.0;
		ps.n_y[mIdx] = 0.0;
		ps.n_z[mIdx] = -1.0;
		ps.dpm_x[mIdx] = 0.0;
		ps.dpm_y[mIdx] = -1.0;
		ps.dpm_z[mIdx] = 0.0;
	}
	else
	{
		ps.n_x[mIdx] = 0.0;
		ps.n_y[mIdx] = 0.0;
		ps.n_z[mIdx] = 1.0;
		ps.dpm_x[mIdx] = 0.0;
		ps.dpm_y[mIdx] = 1.0;
		ps.dpm_z[mIdx] = 0.0;
	}
}

void translateMonomer(PARTICLE_SYSTEM & ps, int idx, DATA_TYPE stepSize)
{
	DATA_TYPE z = (2.0 * Random::getInstance().randomDouble()) - 1;
	DATA_TYPE h = sqrt(1.0 - z * z);
	DATA_TYPE p = 2.0 * PI * Random::getInstance().randomDouble();

	DATA_TYPE dx = stepSize * cos(p) * h;
	DATA_TYPE dy = stepSize * sin(p) * h;
	DATA_TYPE dz = stepSize * z;

	ps.cm_x[idx] += dx;
	ps.cm_y[idx] += dy;
	ps.cm_z[idx] += dz;
}

void translateDopant(PARTICLE_SYSTEM & ps, int idx, DATA_TYPE stepSize)
{
	DATA_TYPE z = (2.0 * Random::getInstance().randomDouble()) - 1;
	DATA_TYPE h = sqrt(1.0 - z * z);
	DATA_TYPE p = 2.0 * PI * Random::getInstance().randomDouble();

	DATA_TYPE dx = stepSize * cos(p) * h;
	DATA_TYPE dy = stepSize * sin(p) * h;
	DATA_TYPE dz = stepSize * z;

	ps.dop_x[idx] += dx;
	ps.dop_y[idx] += dy;
	ps.dop_z[idx] += dz;
}

void translateOligomer(PARTICLE_SYSTEM & ps, int idx, DATA_TYPE dx, DATA_TYPE dy, DATA_TYPE dz, bool moveDopants)
{
	int lb = idx * ps.monomers_per_chain;
	int ub = lb + ps.monomers_per_chain;

	for (int idx = lb; idx < ub; idx++)
	{
		ps.cm_x[idx] += dx;
		ps.cm_y[idx] += dy;
		ps.cm_z[idx] += dz;
	}

	if (moveDopants)
	{
		for (int idx = 0; idx < ps.nDopants; idx++)
		{
			ps.dop_x[idx] += dx;
			ps.dop_y[idx] += dy;
			ps.dop_z[idx] += dz;
		}
	}
}

void calculateMove(DATA_TYPE & dx, DATA_TYPE & dy, DATA_TYPE & dz, DATA_TYPE stepSize)
{
	DATA_TYPE z = (2.0 * Random::getInstance().randomDouble()) - 1;
	DATA_TYPE h = sqrt(1.0 - z * z);
	DATA_TYPE p = 2.0 * PI * Random::getInstance().randomDouble();

	dx = stepSize * cos(p) * h;
	dy = stepSize * sin(p) * h;
	dz = stepSize * z;
}

void rotatePoint(DATA_TYPE heading, DATA_TYPE pitch, DATA_TYPE bank, DATA_TYPE & xx, DATA_TYPE & yy, DATA_TYPE & zz)
{
	// build rotation matrix
	DATA_TYPE eulerAngle[3] = { heading, pitch, bank };

	DATA_TYPE s1 = sin(eulerAngle[1]);
	DATA_TYPE c1 = cos(eulerAngle[1]);
	DATA_TYPE s2 = sin(eulerAngle[0]);
	DATA_TYPE c2 = cos(eulerAngle[0]);
	DATA_TYPE s3 = sin(eulerAngle[2]);
	DATA_TYPE c3 = cos(eulerAngle[2]);

	DATA_TYPE mtRot[3][3];
	mtRot[0][0] = c1 * c3 - s1 * s2 * s3;
	mtRot[1][0] = c3 * s1 + c1 * s2 * s3;
	mtRot[2][0] = -c2 * s3;
	mtRot[0][1] = -c2 * s1;
	mtRot[1][1] = c1 * c2;
	mtRot[2][1] = s2;
	mtRot[0][2] = c1 * s3 + c3 * s1 * s2;
	mtRot[1][2] = s1 * s3 - c1 * c3 * s2;
	mtRot[2][2] = c2 * c3;


	// rotate normal
	DATA_TYPE t_x = mtRot[0][0] * xx + mtRot[0][1] * yy + mtRot[0][2] * zz;
	DATA_TYPE t_y = mtRot[1][0] * xx + mtRot[1][1] * yy + mtRot[1][2] * zz;
	DATA_TYPE t_z = mtRot[2][0] * xx + mtRot[2][1] * yy + mtRot[2][2] * zz;

	xx = t_x;
	yy = t_y;
	zz = t_z;

}

void randomRotationAxis(DATA_TYPE magnitude, DATA_TYPE & heading, DATA_TYPE & pitch, DATA_TYPE & bank, DATA_TYPE angleLimit)
{
	int random = (int)(Random::getInstance().randomDouble() * 25.99) + 1;
	DATA_TYPE oheading = heading;
	DATA_TYPE opitch   = pitch;
	DATA_TYPE obank    = bank;

	heading += roationAxis[random][2] * magnitude;
	pitch += roationAxis[random][0] * magnitude;
	bank += roationAxis[random][1] * magnitude;

	DATA_TYPE tangleLimit = DEG_TO_RAD(angleLimit);

	if (fabs(heading) >= tangleLimit)
		heading = oheading;
	if (fabs(bank) >= tangleLimit)
		bank = obank;
	if (fabs(pitch) >= tangleLimit)
		pitch = opitch;
}

void PyPyImageInteraction(PARTICLE_SYSTEM& ps, int midx, vector<DATA_TYPE> & distances, int & dist_idx)
{
	int monomers_per_chain = ps.monomers_per_chain;

	int exc_lb = (midx / monomers_per_chain) * monomers_per_chain;
	int exc_ub = exc_lb + monomers_per_chain;
	int t_lb = 0;
	int t_ub = 0;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	int i = 0;
	int tlb = 0, tub = 0;

	DATA_TYPE* _cm_x = ps.cm_x;
	DATA_TYPE* _cm_y = ps.cm_y;
	DATA_TYPE* _cm_z = ps.cm_z;

	DATA_TYPE* _dpm_x = ps.dpm_x;
	DATA_TYPE* _dpm_y = ps.dpm_y;
	DATA_TYPE* _dpm_z = ps.dpm_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;
	int _nMonomers = ps.nMonomers;

	ox = _cm_x[midx];
	oy = _cm_y[midx];
	oz = _cm_z[midx];

	bool srcInsideBox = true;
	bool destInsideBox = true;
	if (fabs(ox) > ps.x_size / 2.0 ||
		fabs(oy) > ps.y_size / 2.0 ||
		fabs(oz) > ps.z_size / 2.0)
	{
		srcInsideBox = false;

		if (ox < -_x_size * 0.5) ox = ox + _x_size;
		if (ox >= _x_size * 0.5) ox = ox - _x_size;

		if (oy < -_y_size * 0.5) oy = oy + _y_size;
		if (oy >= _y_size * 0.5) oy = oy - _y_size;

		if (oz < -_z_size * 0.5) oz = oz + _z_size;
		if (oz >= _z_size * 0.5) oz = oz - _z_size;
	}

	for (i = exc_lb; i < exc_ub; i++)
	{
		if (i == midx)
			continue;

		if (fabs(_cm_x[i]) > ps.x_size / 2.0 ||
			fabs(_cm_y[i]) > ps.y_size / 2.0 ||
			fabs(_cm_z[i]) > ps.z_size / 2.0)
		{
			destInsideBox = false;
		}
		else
		{
			destInsideBox = true;
		}

		if (srcInsideBox && destInsideBox)
			continue;

		if (srcInsideBox && !destInsideBox)
		{
			DATA_TYPE tx = _cm_x[i];
			if (tx < -_x_size * 0.5) tx = tx + _x_size;
			if (tx >= _x_size * 0.5) tx = tx - _x_size;

			DATA_TYPE ty = _cm_y[i];
			if (ty < -_y_size * 0.5) ty = ty + _y_size;
			if (ty >= _y_size * 0.5) ty = ty - _y_size;

			DATA_TYPE tz = _cm_z[i];
			if (tz < -_z_size * 0.5) tz = tz + _z_size;
			if (tz >= _z_size * 0.5) tz = tz - _z_size;

			xdif = tx - ox;
			ydif = ty - oy;
			zdif = tz - oz;

			xdif = ANG_TO_BOHR(xdif);
			ydif = ANG_TO_BOHR(ydif);
			zdif = ANG_TO_BOHR(zdif);

			rij = MAGNITUDE(xdif, ydif, zdif);
			DATA_TYPE tcut = ANG_TO_BOHR(_r_cut);
			if (rij > tcut)
			{
				continue;
			}
			distances[dist_idx++] = rij;
		}
		else if (!srcInsideBox && destInsideBox)
		{
			DATA_TYPE tx = _cm_x[i];
			if (tx < -_x_size * 0.5) tx = tx + _x_size;
			if (tx >= _x_size * 0.5) tx = tx - _x_size;

			DATA_TYPE ty = _cm_y[i];
			if (ty < -_y_size * 0.5) ty = ty + _y_size;
			if (ty >= _y_size * 0.5) ty = ty - _y_size;

			DATA_TYPE tz = _cm_z[i];
			if (tz < -_z_size * 0.5) tz = tz + _z_size;
			if (tz >= _z_size * 0.5) tz = tz - _z_size;

			xdif = tx - ox;
			ydif = ty - oy;
			zdif = tz - oz;

			xdif = ANG_TO_BOHR(xdif);
			ydif = ANG_TO_BOHR(ydif);
			zdif = ANG_TO_BOHR(zdif);

			rij = MAGNITUDE(xdif, ydif, zdif);

			if (rij > ANG_TO_BOHR(_r_cut))
			{
				continue;
			}
			distances[dist_idx++] = rij;
		}
	}
}

void saveDistanceHistogram(PARTICLE_SYSTEM& ps, const string& fName, ios_base::openmode mode )
{
	{
		DATA_TYPE s_x = 0.0, s_y = 0.0, s_z = 0.0;
		DATA_TYPE d_x = 0.0, d_y = 0.0, d_z = 0.0;
		DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
		DATA_TYPE dist = 0.0;
		DATA_TYPE r_cut_sqrd = 1000; // ps.r_cut* ps.r_cut;
		// initialize the contains to maximum number of pairs between monomers
		// k(k-1)/2
		int k = ps.nMonomers * (ps.nMonomers - 1) / 2;

		vector<DATA_TYPE> mm_neighbors(k);
		vector<DATA_TYPE> md_neighbors(k);
		vector<DATA_TYPE> dd_neighbors(k);
		vector<DATA_TYPE> dm_neighbors(k);

		int inner_chain_id = 0;
		int outer_chain_id = 0;
		int outer = 0;
		int ctr = 0;

		for (outer = 0; outer < ps.nMonomers; outer++)
		{
			s_x = ps.cm_x[outer];
			s_y = ps.cm_y[outer];
			s_z = ps.cm_z[outer];

			inner_chain_id = 0;
			outer_chain_id = outer / ps.monomers_per_chain;

			for (int inner = 0; inner < ps.nMonomers; inner++)
			{
				inner_chain_id = inner / ps.monomers_per_chain;
				if (inner_chain_id == outer_chain_id)
					continue;

				d_x = ps.cm_x[inner];
				d_y = ps.cm_y[inner];
				d_z = ps.cm_z[inner];

				xdif = d_x - s_x;
				ydif = d_y - s_y;
				zdif = d_z - s_z;
				PCB(ps.x_size, ps.y_size, ps.z_size, xdif, ydif, zdif);

				dist = xdif * xdif + ydif * ydif + zdif * zdif;

				if (dist < r_cut_sqrd)
				{
					mm_neighbors[ctr++] = sqrt(dist);
				}
			}

			ctr = 0;
			for (int inner = 0; inner < ps.nDopants; inner++)
			{
				d_x = ps.dop_x[inner];
				d_y = ps.dop_y[inner];
				d_z = ps.dop_z[inner];

				xdif = d_x - s_x;
				ydif = d_y - s_y;
				zdif = d_z - s_z;
				PCB(ps.x_size, ps.y_size, ps.z_size, xdif, ydif, zdif);

				dist = xdif * xdif + ydif * ydif + zdif * zdif;

				if (dist < r_cut_sqrd)
				{
					md_neighbors[ctr++] = sqrt(dist);
				}
			}

			// now do the self interactions for a complete pcb condition evaluation
			PyPyImageInteraction(ps, outer, mm_neighbors, ctr);
		}

		ctr = 0;
		for (int outer = 0; outer < ps.nDopants; outer++)
		{
			s_x = ps.dop_x[outer];
			s_y = ps.dop_y[outer];
			s_z = ps.dop_z[outer];

			for (int inner = 0; inner < ps.nDopants; inner++)
			{
				if (inner == outer)
					continue;

				d_x = ps.dop_x[inner];
				d_y = ps.dop_y[inner];
				d_z = ps.dop_z[inner];

				xdif = d_x - s_x;
				ydif = d_y - s_y;
				zdif = d_z - s_z;
				PCB(ps.x_size, ps.y_size, ps.z_size, xdif, ydif, zdif);

				dist = xdif * xdif + ydif * ydif + zdif * zdif;

				if (dist < r_cut_sqrd)
				{
					dd_neighbors[ctr++] = sqrt(dist);
				}
			}

			ctr = 0;
			for (int inner = 0; inner < ps.nMonomers; inner++)
			{
				if (inner == outer)
					continue;

				d_x = ps.cm_x[inner];
				d_y = ps.cm_y[inner];
				d_z = ps.cm_z[inner];

				xdif = d_x - s_x;
				ydif = d_y - s_y;
				zdif = d_z - s_z;
				PCB(ps.x_size, ps.y_size, ps.z_size, xdif, ydif, zdif);

				dist = xdif * xdif + ydif * ydif + zdif * zdif;

				if (dist < r_cut_sqrd)
				{
					dm_neighbors[ctr++] = sqrt(dist);
				}
			}
		}

		// write out the 
		std::ofstream fout(fName.c_str());
		fout << "Monomer-Monomer, Monomer-Dopant, Dopant-Dopant, monomer-dopant" << endl;

		for (int i = 0; i < k; i++)
		{
			if (mm_neighbors[i] > 0.0)
				fout << mm_neighbors[i] << ',';

			if (md_neighbors[i] > 0.0)
				fout << md_neighbors[i] << ',';
			
			if (dd_neighbors[i] > 0.0)
				fout << dd_neighbors[i] << ',';
			
			if (dm_neighbors[i] > 0.0)
				fout << dm_neighbors[i];
			
			fout << endl;
		}

		fout.flush();
		fout.close();
	}
}

void collectGofR(PARTICLE_SYSTEM & ps, unsigned int * gofrMonomerCM, unsigned int * gofrMonomer, unsigned int * gofrDopant, unsigned int * gofrAll, unsigned int numIterations, unsigned int numBins, DATA_TYPE gofrDR)
{
	int monomers_per_polymer = ps.monomers_per_chain;
	DATA_TYPE r_cut = ps.r_cut;
	unsigned int index = 0;
	DATA_TYPE t_x = 0.0, t_y = 0.0, t_z = 0.0;
	DATA_TYPE dst_x = 0.0, dst_y = 0.0, dst_z = 0.0;
	DATA_TYPE src_x = 0.0, src_y = 0.0, src_z = 0.0;
	DATA_TYPE dist = 0.0;
	int exc_lb = 0, exc_ub = 0, idx = 0;

	for (int oIdx = 0; oIdx < ps.nPolymers; oIdx++)
	{
		src_x = ps.olig_cm_x[oIdx];
		src_y = ps.olig_cm_y[oIdx];
		src_z = ps.olig_cm_z[oIdx];

		for (idx = oIdx + 1; idx < ps.nPolymers; idx++)
		{
			t_x = ps.olig_cm_x[idx];
			t_y = ps.olig_cm_y[idx];
			t_z = ps.olig_cm_z[idx];

			dst_x = src_x - t_x;
			dst_y = src_y - t_y;
			dst_z = src_z - t_z;

			PCB(ps.x_size, ps.y_size, ps.z_size, dst_x, dst_y, dst_z);

			dist = MAGNITUDE(dst_x, dst_y, dst_z);
			if (r_cut < dist)
				continue;

			// calculate gofr
			index = (int)(dist / gofrDR);

			if (index < numBins)
			{
				gofrMonomerCM[index] += 1;
			}
		}
	}

	for (int mIdx = 0; mIdx < ps.nMonomers; mIdx++)
	{
		exc_lb = (mIdx / monomers_per_polymer) * monomers_per_polymer;
		exc_ub = exc_lb + monomers_per_polymer;
		idx = 0;

		src_x = ps.cm_x[mIdx];
		src_y = ps.cm_y[mIdx];
		src_z = ps.cm_z[mIdx];

		for (idx = mIdx + 1; idx < ps.nMonomers; idx++)
		{
			if (idx >= exc_lb && idx < exc_ub)
				continue;

			t_x = ps.cm_x[idx];
			t_y = ps.cm_y[idx];
			t_z = ps.cm_z[idx];

			dst_x = src_x - t_x;
			dst_y = src_y - t_y;
			dst_z = src_z - t_z;

			PCB(ps.x_size, ps.y_size, ps.z_size, dst_x, dst_y, dst_z);

			dist = MAGNITUDE(dst_x, dst_y, dst_z);
			if (r_cut < dist)
				continue;

			// calculate gofr
			index = (int)(dist / gofrDR);

			if (index < numBins)
			{
				gofrMonomer[index] += 1;
			}
		}
	}

	for (int dIdx = 0; dIdx < ps.nDopants; dIdx++)
	{
		src_x = ps.dop_x[dIdx];
		src_y = ps.dop_y[dIdx];
		src_z = ps.dop_z[dIdx];

		for (idx = dIdx + 1; idx < ps.nDopants; idx++)
		{
			t_x = ps.dop_x[idx];
			t_y = ps.dop_y[idx];
			t_z = ps.dop_z[idx];

			dst_x = src_x - t_x;
			dst_y = src_y - t_y;
			dst_z = src_z - t_z;

			PCB(ps.x_size, ps.y_size, ps.z_size, dst_x, dst_y, dst_z);

			dist = MAGNITUDE(dst_x, dst_y, dst_z);
			if (r_cut < dist)
				continue;

			// calculate gofr
			index = (int)(dist / gofrDR);

			if (index < numBins)
			{
				gofrDopant[index] += 1;
			}
		}
	}

	for (int mIdx = 0; mIdx < ps.nMonomers; mIdx++)
	{
		src_x = ps.cm_x[mIdx];
		src_y = ps.cm_y[mIdx];
		src_z = ps.cm_z[mIdx];

		for (idx = 0; idx < ps.nDopants; idx++)
		{
			t_x = ps.dop_x[idx];
			t_y = ps.dop_y[idx];
			t_z = ps.dop_z[idx];

			dst_x = src_x - t_x;
			dst_y = src_y - t_y;
			dst_z = src_z - t_z;

			PCB(ps.x_size, ps.y_size, ps.z_size, dst_x, dst_y, dst_z);

			dist = MAGNITUDE(dst_x, dst_y, dst_z);
			if (r_cut < dist)
				continue;

			// calculate gofr
			index = (int)(dist / gofrDR);

			if (index < numBins)
			{
				gofrAll[index] += 1;
			}
		}
	}
}

void generateGofR(PARTICLE_SYSTEM & ps, const string fname, unsigned int * gofrMonomerCM, unsigned int * gofrMonomer, unsigned int * gofrDopant, unsigned int * gofrAll, unsigned int numIterations, unsigned int numBins, DATA_TYPE gofrDR)
{
	std::ofstream ppostream(fname.c_str(), std::ios::out);
	ppostream << "Radial Distance,Monomer-Monomer,Dopant-Dopant,Monomer-Dopant, Oligomer-Oligomer" << endl;
	DATA_TYPE r = 0.0, vb = 0.0;
	DATA_TYPE gor_mm = 0.0, gor_md = 0.0, gor_dd = 0.0, gor_oo = 0.0;
	DATA_TYPE mm_pairs = ps.nMonomers * (ps.nMonomers - 1) / 2.0;
	DATA_TYPE md_pairs = ((ps.nMonomers + ps.nDopants) * ((ps.nMonomers + ps.nDopants) - 1)) / 2.0;
	DATA_TYPE dd_pairs = ps.nDopants * (ps.nDopants - 1) / 2.0;
	DATA_TYPE oo_pairs = ps.nPolymers * (ps.nPolymers - 1) / 2.0;

	DATA_TYPE vol = ps.x_size * ps.y_size * ps.z_size;
	numIterations = numIterations;

	for (unsigned int i = 0; i < numBins; i++)
	{
		r = gofrDR * i;

		gor_mm = (gofrMonomer[i]) / (DATA_TYPE)numIterations;
		gor_md = (gofrAll[i] * 2.0) / (DATA_TYPE)numIterations;
		gor_dd = (gofrDopant[i]) / (DATA_TYPE)numIterations;
		gor_oo = (gofrMonomerCM[i]) / (DATA_TYPE)numIterations;

		gor_mm = gor_mm / ((4.0 * PI * mm_pairs) / vol * (r*r*gofrDR));
		gor_md = gor_md / ((4.0 * PI * md_pairs) / vol * (r*r*gofrDR));
		gor_dd = gor_dd / ((4.0 * PI * dd_pairs) / vol * (r*r*gofrDR));
		gor_oo = gor_oo / ((4.0 * PI * oo_pairs) / vol * (r*r*gofrDR));

		if (isnan(gor_mm))
			gor_mm = 0.0;
		if (isnan(gor_md))
			gor_md = 0.0;
		if (isnan(gor_dd))
			gor_dd = 0.0;
		if (isnan(gor_oo))
			gor_oo = 0.0;

		ppostream << r << ',' << gor_mm << ',' << gor_dd << ',' << gor_md << ','<< gor_oo << endl;
	}
	ppostream.flush();
	ppostream.close();
}

DATA_TYPE pressureFromGofR(PARTICLE_SYSTEM & ps, unsigned int * gofrAll, unsigned int numIterations, unsigned int numBins, DATA_TYPE gofrDR)
{

	return 0.0;
}

void calculateOligomersCenterOfMasses(PARTICLE_SYSTEM & ps)
{
	int ctr = 0;

	for (int j = 0; j < ps.nMonomers; j = j + ps.monomers_per_chain)
	{
		int lb = (j / ps.monomers_per_chain) * ps.monomers_per_chain;
		int ub = lb + ps.monomers_per_chain;
		DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;

		for (int i = lb; i < ub; i++)
		{
			cmx += ps.cm_x[i];
			cmy += ps.cm_y[i];
			cmz += ps.cm_z[i];
		}

		cmx = ps.olig_cm_x[ctr] = cmx / (float)ps.monomers_per_chain;
		cmy = ps.olig_cm_y[ctr] = cmy / (float)ps.monomers_per_chain;
		cmz = ps.olig_cm_z[ctr] = cmz / (float)ps.monomers_per_chain;

		cmx = 0.0;
		cmy = 0.0;
		cmz = 0.0;

		ctr++;
	}
}

void calculateOligomerCenterOfMasses(PARTICLE_SYSTEM & ps, int mIdx)
{
	int oligomerId = mIdx / ps.monomers_per_chain;
	int lb = oligomerId * ps.monomers_per_chain;
	int ub = lb + 12;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;

	for (int i = lb; i < ub; i++)
	{
		cmx += ps.cm_x[i];
		cmy += ps.cm_y[i];
		cmz += ps.cm_z[i];
	}

	ps.olig_cm_x[oligomerId] = cmx / (float)ps.monomers_per_chain;
	ps.olig_cm_y[oligomerId] = cmy / (float)ps.monomers_per_chain;
	ps.olig_cm_z[oligomerId] = cmz / (float)ps.monomers_per_chain;
}

void recenterParticleSystem(PARTICLE_SYSTEM & ps, DATA_TYPE & dx, DATA_TYPE & dy, DATA_TYPE & dz)
{
	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(ps);

	DATA_TYPE minx = ps.cm_x[0], maxx = ps.cm_x[0], miny = ps.cm_y[0], maxy = ps.cm_y[0], minz = ps.cm_z[0], maxz = ps.cm_z[0];
	calculateBoundingInformation(ps, dx, dy, dz, minx, miny, minz, maxx, maxy, maxz);

	for (int ocm = 0; ocm < ps.nPolymers; ocm++)
	{
		translateOligomer(ps, ocm, -dx, -dy, -dz);
		ps.z0[ocm] += -dz;
	}
	for (int i = 0; i < ps.nDopants; i++)
	{
		ps.dop_x[i] -= dx;
		ps.dop_y[i] -= dy;
		ps.dop_z[i] -= dz;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(ps);
	calculateBoundingInformation(ps, dx, dy, dz, minx, miny, minz, maxx, maxy, maxz);
}

void centerParticleSystem(PARTICLE_SYSTEM& ps)
{
	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(ps);

	DATA_TYPE minx = ps.olig_cm_x[0], maxx = ps.olig_cm_x[0], miny = ps.olig_cm_y[0], maxy = ps.olig_cm_y[0], minz = ps.olig_cm_z[0], maxz = ps.olig_cm_z[0];
	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;

	for (int ocm = 0; ocm < ps.nPolymers; ocm++)
	{
		if (minx > ps.olig_cm_x[ocm])
			minx = ps.olig_cm_x[ocm];
		if (maxx < ps.olig_cm_x[ocm])
			maxx = ps.olig_cm_x[ocm];

		if (miny > ps.olig_cm_y[ocm])
			miny = ps.olig_cm_y[ocm];
		if (maxy < ps.olig_cm_y[ocm])
			maxy = ps.olig_cm_y[ocm];

		if (minz > ps.olig_cm_z[ocm])
			minz = ps.olig_cm_z[ocm];
		if (maxz < ps.olig_cm_z[ocm])
			maxz = ps.olig_cm_z[ocm];
	}


	for (int ocm = 0; ocm < ps.nDopants; ocm++)
	{
		if (minz > ps.dop_z[ocm])
			minz = ps.dop_z[ocm];
		if (maxz < ps.dop_z[ocm])
			maxz = ps.dop_z[ocm];
	}

	DATA_TYPE sizex = (maxx + minx), sizey = (maxy + miny), sizez = (maxz + minz);
	dx = sizex / 2.0;
	dy = sizey / 2.0;
	dz = sizez / 2.0;

	for (int ocm = 0; ocm < ps.nPolymers; ocm++)
	{
		translateOligomer(ps, ocm, -dx, -dy, -dz);
		ps.z0[ocm] += -dz;
	}
	for (int i = 0; i < ps.nDopants; i++)
	{
		ps.dop_x[i] -= dx;
		ps.dop_y[i] -= dy;
		ps.dop_z[i] -= dz;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(ps);
}

void setParticleSystemDensity(PARTICLE_SYSTEM & ps, DATA_TYPE targetDensity)
{
	DATA_TYPE scaleFactor = 1.0 / (targetDensity / ps.density);
	DATA_TYPE sfactor[] = { scaleFactor, scaleFactor, scaleFactor };

	scaleParticleSystem(ps, sfactor, true);
}

DATA_TYPE calculateDensity(PARTICLE_SYSTEM & ps, DATA_TYPE & sizex, DATA_TYPE & sizey, DATA_TYPE & sizez)
{
	DATA_TYPE minx = ps.olig_cm_x[0], maxx = ps.olig_cm_x[0], miny = ps.olig_cm_y[0], maxy = ps.olig_cm_y[0], minz = ps.olig_cm_z[0], maxz = ps.olig_cm_z[0];

	for (int ocm = 0; ocm < ps.nPolymers; ocm++)
	{
		if (minx > ps.olig_cm_x[ocm])
			minx = ps.olig_cm_x[ocm];
		if (maxx < ps.olig_cm_x[ocm])
			maxx = ps.olig_cm_x[ocm];

		if (miny > ps.olig_cm_y[ocm])
			miny = ps.olig_cm_y[ocm];
		if (maxy < ps.olig_cm_y[ocm])
			maxy = ps.olig_cm_y[ocm];

		if (minz > ps.olig_cm_z[ocm])
			minz = ps.olig_cm_z[ocm];
		if (maxz < ps.olig_cm_z[ocm])
			maxz = ps.olig_cm_z[ocm];
	}
	
	sizex = fabs(maxx - minx) + (0.5 * ps.uc_x), sizey = fabs(maxy - miny) + (0.5 * ps.uc_y), sizez = fabs(maxz - minz) + (0.5 * ps.uc_z);
	DATA_TYPE density = ps.density = ((ps.nPolymers * oligomer_mass) + ps.nDopants * dopant_mass) / (AVOGADRO_NUM * (sizex * 1e-8) * (sizey * 1e-8) * (sizez * 1e-8));

	return density;
}

DATA_TYPE * scaleSystemVolume(PARTICLE_SYSTEM& ps, bool * scaleDimension, DATA_TYPE scale, bool random, bool updateCutoff)
{
	DATA_TYPE vOld = ps.x_size * ps.y_size * ps.z_size;

	// calculate the delta in the volumne
	DATA_TYPE deltaV = log(vOld);
	DATA_TYPE sign = 1.0;
	if (random)
		sign = SIGN2(sign, Random::getInstance().randomDouble() - .5);
	DATA_TYPE deltaV1 = deltaV + sign * scale;
	DATA_TYPE vNew = exp(deltaV1);
	DATA_TYPE volumeRatio = vNew / vOld;
	DATA_TYPE* scaleFactor = new DATA_TYPE[3];
	DATA_TYPE factor = pow(volumeRatio, 1.0 / 3.0);
	

	if (scaleDimension[0])
		scaleFactor[0] = factor;
	else
		scaleFactor[0] = 1.0;

	if (scaleDimension[1])
		scaleFactor[1] = factor;
	else
		scaleFactor[1] = 1.0;

	if (scaleDimension[2])
		scaleFactor[2] = factor;
	else
		scaleFactor[2] = 1.0;


	scaleParticleSystem(ps, scaleFactor, updateCutoff);

	return 	scaleFactor;
}

void scaleParticleSystem(PARTICLE_SYSTEM& ps, DATA_TYPE * scaleFactor, bool updateCutoff)
{
	calculateOligomersCenterOfMasses(ps);

	// update the dimensions
	ps.x_size *= scaleFactor[0];
	ps.y_size *= scaleFactor[1];
	ps.z_size *= scaleFactor[2];

	DATA_TYPE min = MIN(ps.x_size, ps.y_size);
	min = MIN(min, ps.z_size);
	if (updateCutoff)
		ps.r_cut = (min / 2.0) * 0.98;

	ps.uc_x *= scaleFactor[0];
	ps.uc_y *= scaleFactor[1];
	ps.uc_z *= scaleFactor[2];

	{
		{
			int didx = 0;
			#pragma omp parallel for private(didx)
			for (didx = 0; didx < ps.nDopants; didx++)
			{
				ps.dop_x[didx] = ps.dop_x[didx] * scaleFactor[0];
				ps.dop_y[didx] = ps.dop_y[didx] * scaleFactor[1];
				ps.dop_z[didx] = ps.dop_z[didx] * scaleFactor[2];
			}
		}

		{
			DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
			DATA_TYPE ocmx = 0.0, ocmy = 0.0, ocmz = 0.0;
			int idx = 0;
			for (idx = 0; idx < ps.nPolymers; idx++)
			{
				// original oligomer center of mass
				ocmx = ps.olig_cm_x[idx];
				ocmy = ps.olig_cm_y[idx];
				ocmz = ps.olig_cm_z[idx];

				// scaled center of mass
				ps.olig_cm_x[idx] *= scaleFactor[0];
				ps.olig_cm_y[idx] *= scaleFactor[1];
				ps.olig_cm_z[idx] *= scaleFactor[2];

				// scaled z0 position
				//ps.z0[idx] *= scaleFactor;

				// differential between old center of mass and new center of mass
				dx = ps.olig_cm_x[idx] - ocmx;
				dy = ps.olig_cm_y[idx] - ocmy;
				dz = ps.olig_cm_z[idx] - ocmz;

				// translate the oligomer to its new location
				translateOligomer(ps, idx, dx, dy, dz);
			}
		}
	}
	calculateOligomersCenterOfMasses(ps);
	ps.density = ((ps.nPolymers * oligomer_mass) + ps.nDopants * dopant_mass) / (AVOGADRO_NUM * (ps.x_size * 1e-8) * (ps.y_size * 1e-8) * (ps.z_size * 1e-8));	
}

void broadCastParticleSystem(PARTICLE_SYSTEM& ps)
{
	MPI_Barrier(MPI_COMM_WORLD);

	// first broadcast positions
	MPI_Bcast(ps.cm_x, ps.nMonomers, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(ps.cm_y, ps.nMonomers, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(ps.cm_z, ps.nMonomers, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(ps.z0, ps.nPolymers, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(ps.olig_cm_x, ps.nPolymers, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(ps.olig_cm_y, ps.nPolymers, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(ps.olig_cm_z, ps.nPolymers, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	// update the dopants
	MPI_Bcast(ps.dop_x, ps.nDopants, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(ps.dop_y, ps.nDopants, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);
	MPI_Bcast(ps.dop_z, ps.nDopants, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(&ps.x_size, 1, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(&ps.y_size, 1, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(&ps.z_size, 1, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(&ps.r_cut, 1, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(&ps.uc_x, 1, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(&ps.uc_y, 1, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

	MPI_Bcast(&ps.uc_z, 1, MPI_DATA_TYPE, 0, MPI_COMM_WORLD);
	MPI_Barrier(MPI_COMM_WORLD);

}

PARTICLE_SYSTEM * buildRenneReferencedCell(const AppConfig & appConfig, bool addDopants)
{
	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	PARTICLE_SYSTEM* referenceChain = buildReferencedChain(appConfig, addDopants);

	if (referenceChain == NULL)
		return NULL;

	int nMonomers = appConfig.chains_per_unit_cell * appConfig.monomers_per_chain;
	int nDopants = appConfig.chains_per_unit_cell * appConfig.dopants_per_chain;

	PARTICLE_SYSTEM * ps = new PARTICLE_SYSTEM;
	ps->x_size = appConfig.x_off * 2.0;
	ps->y_size = appConfig.y_off * 2.0;
	ps->z_size = appConfig.z_off * 2.0;
	ps->uc_x = referenceChain->x_size;
	ps->uc_y = referenceChain->y_size;
	ps->uc_z = referenceChain->z_size;

	ps->angleLimit = appConfig.angle_limit;
	ps->monomers_per_chain = appConfig.monomers_per_chain;

	ps->nMonomers = nMonomers;
	ps->cm_x = new DATA_TYPE[nMonomers];
	ps->cm_y = new DATA_TYPE[nMonomers];
	ps->cm_z = new DATA_TYPE[nMonomers];
	ps->dpm_x = new DATA_TYPE[nMonomers];
	ps->dpm_y = new DATA_TYPE[nMonomers];
	ps->dpm_z = new DATA_TYPE[nMonomers];
	ps->n_x = new DATA_TYPE[nMonomers];
	ps->n_y = new DATA_TYPE[nMonomers];
	ps->n_z = new DATA_TYPE[nMonomers];
	ps->heading = new DATA_TYPE[nMonomers];
	ps->pitch = new DATA_TYPE[nMonomers];
	ps->bank = new DATA_TYPE[nMonomers];
	memset(ps->heading, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->pitch, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->bank, 0, sizeof(DATA_TYPE) * nMonomers);
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->nPolymers = ps->nMonomers / appConfig.monomers_per_chain;
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

	ps->z0 = new DATA_TYPE[ps->nPolymers];
	int polymerCtr = 0;

	if (addDopants)
		ps->nDopants = nDopants;
	else
		ps->nDopants = 0;

	ps->dop_x = new DATA_TYPE[nDopants];
	ps->dop_y = new DATA_TYPE[nDopants];
	ps->dop_z = new DATA_TYPE[nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);

	DATA_TYPE refz = 0.0, refy = 0.0, refx = 0.0;
	int midx = 0, didx = 0;
	DATA_TYPE xoff = cos(DEG_TO_RAD(68.6)) * 8.59;
	DATA_TYPE yoff = sin(DEG_TO_RAD(68.6)) * 8.59;

	// bottom chains
	// build a reference cell
	for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
	{
		ps->cm_x[midx] = referenceChain->cm_x[mi];
		ps->cm_y[midx] = referenceChain->cm_y[mi];
		ps->cm_z[midx] = referenceChain->cm_z[mi];
		ps->dpm_x[midx] = referenceChain->dpm_x[mi];
		ps->dpm_y[midx] = referenceChain->dpm_y[mi];
		ps->dpm_z[midx] = referenceChain->dpm_z[mi];
		ps->n_x[midx] = referenceChain->n_x[mi];
		ps->n_y[midx] = referenceChain->n_y[mi];
		ps->n_z[midx] = referenceChain->n_z[mi];
		midx++;
	}
	translateOligomer(*ps, 0, 0.0, 0.0, 0.0);
	ps->z0[0] = 0.0;

	for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
	{
		ps->cm_x[midx] = referenceChain->cm_x[mi];
		ps->cm_y[midx] = referenceChain->cm_y[mi];
		ps->cm_z[midx] = referenceChain->cm_z[mi];
		ps->dpm_x[midx] = referenceChain->dpm_x[mi];
		ps->dpm_y[midx] = referenceChain->dpm_y[mi];
		ps->dpm_z[midx] = referenceChain->dpm_z[mi];
		ps->n_x[midx] = referenceChain->n_x[mi];
		ps->n_y[midx] = referenceChain->n_y[mi];
		ps->n_z[midx] = referenceChain->n_z[mi];
		midx++;
	}
	translateOligomer(*ps, 1, -xoff, yoff, 0);
	ps->z0[1] = 0.0;

	// top chains
	// build a reference cell
	for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
	{
		ps->cm_x[midx] = referenceChain->cm_x[mi];
		ps->cm_y[midx] = referenceChain->cm_y[mi];
		ps->cm_z[midx] = referenceChain->cm_z[mi];
		ps->dpm_x[midx] = referenceChain->dpm_x[mi];
		ps->dpm_y[midx] = referenceChain->dpm_y[mi];
		ps->dpm_z[midx] = referenceChain->dpm_z[mi];
		ps->n_x[midx] = referenceChain->n_x[mi];
		ps->n_y[midx] = referenceChain->n_y[mi];
		ps->n_z[midx] = referenceChain->n_z[mi];
		midx++;
	}
	translateOligomer(*ps, 2, -0.5 * xoff, yoff * 0.5, 5.86 * 0.5);
	ps->z0[2] = appConfig.z_off;

	for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
	{
		ps->cm_x[midx] = referenceChain->cm_x[mi];
		ps->cm_y[midx] = referenceChain->cm_y[mi];
		ps->cm_z[midx] = referenceChain->cm_z[mi];
		ps->dpm_x[midx] = referenceChain->dpm_x[mi];
		ps->dpm_y[midx] = referenceChain->dpm_y[mi];
		ps->dpm_z[midx] = referenceChain->dpm_z[mi];
		ps->n_x[midx] = referenceChain->n_x[mi];
		ps->n_y[midx] = referenceChain->n_y[mi];
		ps->n_z[midx] = referenceChain->n_z[mi];
		midx++;
	}
	translateOligomer(*ps, 3, -xoff - 0.5 * xoff, 1.5*yoff, 5.86 * 0.5);
	ps->z0[3] = appConfig.z_off;

	DATA_TYPE d1_off = fabs(ps->cm_y[1] - ps->cm_y[14]) / 2.0;
	DATA_TYPE d2_off = fabs(ps->cm_y[1] - ps->cm_y[14]) / 2.0;

	// now add dopants
	if (addDopants)
	{
		// bottom dopants
		ps->dop_x[0] = ps->cm_x[1];
		ps->dop_y[0] = ps->cm_y[1] + d1_off;
		ps->dop_z[0] = 0.0;

		ps->dop_x[1] = ps->cm_x[5];
		ps->dop_y[1] = ps->cm_y[5] + d1_off;
		ps->dop_z[1] = 0.0;

		ps->dop_x[2] = ps->cm_x[9];
		ps->dop_y[2] = ps->cm_y[9] + d1_off;
		ps->dop_z[2] = 0.0;

		ps->dop_x[3] = ps->cm_x[13];
		ps->dop_y[3] = ps->cm_y[13] + d1_off;
		ps->dop_z[3] = 0.0;

		ps->dop_x[4] = ps->cm_x[17];
		ps->dop_y[4] = ps->cm_y[17] + d1_off;
		ps->dop_z[4] = 0.0;

		ps->dop_x[5] = ps->cm_x[21];
		ps->dop_y[5] = ps->cm_y[21] + d1_off;
		ps->dop_z[5] = 0.0;

		// top dopants
		ps->dop_x[6] = ps->cm_x[25];
		ps->dop_y[6] = ps->cm_y[25] + d1_off;
		ps->dop_z[6] = 5.86 * 0.5;

		ps->dop_x[7] = ps->cm_x[29];
		ps->dop_y[7] = ps->cm_y[29] + d1_off;
		ps->dop_z[7] = 5.86 * 0.5;

		ps->dop_x[8] = ps->cm_x[33];
		ps->dop_y[8] = ps->cm_y[33] + d1_off;
		ps->dop_z[8] = 5.86 * 0.5;

		ps->dop_x[9] = ps->cm_x[37];
		ps->dop_y[9] = ps->cm_y[37] + d1_off;
		ps->dop_z[9] = 5.86 * 0.5;

		ps->dop_x[10] = ps->cm_x[41];
		ps->dop_y[10] = ps->cm_y[41] + d1_off;
		ps->dop_z[10] = 5.86 * 0.5;

		ps->dop_x[11] = ps->cm_x[45];
		ps->dop_y[11] = ps->cm_y[45] + d1_off;
		ps->dop_z[11] = 5.86 * 0.5;
	}

 //   // calculate the unit cell size
	//DATA_TYPE minx = 0.0, miny = 0.0, minz = 0.0, maxx = 0.0, maxy = 0.0, maxz = 0.0;
	//minx = maxx = ps->cm_x[0];
	//miny = maxy = ps->cm_y[0];
	//minz = maxz = ps->cm_z[0];
	//for (int i = 0; i < ps->nMonomers; i++)
	//{
	//	if (minx < ps->cm_x[i])
	//		minx = ps->cm_x[i];
	//	if (miny < ps->cm_y[i])
	//		miny = ps->cm_y[i];
	//	if (minz < ps->cm_z[i])
	//		minz = ps->cm_z[i];

	//	if (maxx >ps->cm_x[i])
	//		maxx = ps->cm_x[i];
	//	if (maxy > ps->cm_y[i])
	//		maxy = ps->cm_y[i];
	//	if (maxz > ps->cm_z[i])
	//		maxz = ps->cm_z[i];
	//}

	//for (int i = 0; i < ps->nDopants; i++)
	//{
	//	if (minx < ps->dop_x[i])
	//		minx = ps->dop_x[i];
	//	if (miny < ps->dop_y[i])
	//		miny = ps->dop_y[i];
	//	if (minz < ps->dop_z[i])
	//		minz = ps->dop_z[i];

	//	if (maxx > ps->dop_x[i])
	//		maxx = ps->dop_x[i];
	//	if (maxy > ps->dop_y[i])
	//		maxy = ps->dop_y[i];
	//	if (maxz > ps->dop_z[i])
	//		maxz = ps->dop_z[i];
	//}
	//ps->x_size = fabs(maxx - minx);
	//ps->y_size = fabs(maxy - miny);
	//ps->z_size = fabs(maxz - minz);


	// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
	DATA_TYPE min = MIN(ps->x_size, ps->y_size);
	min = MIN(min, ps->z_size);
	if (appConfig.r_cut < 0)
	{
		ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
		//if (ps->r_cut < ps->cm_x[11] - ps->cm_x[0])
		//	ps->r_cut = ps->cm_x[11] - ps->cm_x[0];
	}
	else
	{
		ps->r_cut = appConfig.r_cut;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	centerParticleSystem(*ps);

	// update  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	// delete the reference chain
	delete referenceChain;

	return ps;
}

PARTICLE_SYSTEM * buildRenneParticleSystem(const AppConfig & appConfig, int xdim, int ydim, int zdim, bool addDopants)
{
	PARTICLE_SYSTEM * refCell = buildRenneReferencedCell(appConfig, addDopants);

	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	if (refCell == NULL)
		return NULL;

	int nMonomers = xdim * ydim * zdim * appConfig.chains_per_unit_cell * appConfig.monomers_per_chain;
	int nDopants = xdim * ydim * zdim * appConfig.chains_per_unit_cell  * appConfig.dopants_per_chain;

	PARTICLE_SYSTEM * ps = new PARTICLE_SYSTEM;
	ps->x_size = refCell->x_size * xdim;
	ps->y_size = refCell->y_size * ydim;
	ps->z_size = refCell->z_size * zdim;
	ps->uc_x = refCell->x_size;
	ps->uc_y = refCell->y_size;
	ps->uc_z = refCell->z_size;

	// make the cut off radius 60% of the smallest dimension
	DATA_TYPE min = MIN(ps->x_size, ps->y_size);
	min = MIN(min, ps->z_size);
	if (appConfig.r_cut < 0)
		ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
	else
		ps->r_cut = appConfig.r_cut;
	ps->angleLimit = appConfig.angle_limit;
	ps->monomers_per_chain = appConfig.monomers_per_chain;

	ps->nMonomers = nMonomers;
	ps->cm_x = new DATA_TYPE[nMonomers];
	ps->cm_y = new DATA_TYPE[nMonomers];
	ps->cm_z = new DATA_TYPE[nMonomers];
	ps->dpm_x = new DATA_TYPE[nMonomers];
	ps->dpm_y = new DATA_TYPE[nMonomers];
	ps->dpm_z = new DATA_TYPE[nMonomers];
	ps->n_x = new DATA_TYPE[nMonomers];
	ps->n_y = new DATA_TYPE[nMonomers];
	ps->n_z = new DATA_TYPE[nMonomers];
	ps->heading = new DATA_TYPE[nMonomers];
	ps->pitch = new DATA_TYPE[nMonomers];
	ps->bank = new DATA_TYPE[nMonomers];
	memset(ps->heading, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->pitch, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->bank, 0, sizeof(DATA_TYPE) * nMonomers);
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->nPolymers = ps->nMonomers / appConfig.monomers_per_chain;
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

	ps->z0 = new DATA_TYPE[ps->nPolymers];
	int polymerCtr = 0;

	if (addDopants)
		ps->nDopants = nDopants;
	else
		ps->nDopants = 0;

	ps->dop_x = new DATA_TYPE[nDopants];
	ps->dop_y = new DATA_TYPE[nDopants];
	ps->dop_z = new DATA_TYPE[nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);


	DATA_TYPE refz = 0.0, refy = 0.0, refx = 0.0;
	int midx = 0, didx = 0, pidx = 0;

	for (int z = 0; z < zdim; z++)
	{
		refz = z *  appConfig.z_off * 2.0;
		for (int y = 0; y < ydim; y++)
		{
			refy = y *  appConfig.y_off * 3.5;
			for (int x = 0; x < xdim; x++)
			{
				refx = x *  appConfig.x_off * 2.0 + 3.0 + (-y * refCell->x_size * 0.125);

				for (int mi = 0; mi < refCell->nMonomers; mi++)
				{
					ps->cm_x[midx] = refCell->cm_x[mi] + refx;
					ps->cm_y[midx] = refCell->cm_y[mi] + refy;
					ps->cm_z[midx] = refCell->cm_z[mi] + refz;
					ps->dpm_x[midx] = refCell->dpm_x[mi];
					ps->dpm_y[midx] = refCell->dpm_y[mi];
					ps->dpm_z[midx] = refCell->dpm_z[mi];
					ps->n_x[midx] = refCell->n_x[mi];
					ps->n_y[midx] = refCell->n_y[mi];
					ps->n_z[midx] = refCell->n_z[mi];
					ps->heading[midx] = refCell->heading[mi];
					ps->pitch[midx] = refCell->pitch[mi];
					ps->bank[midx] = refCell->bank[mi];

					midx++;
				}

				if (addDopants)
				{
					for (int di = 0; di < refCell->nDopants; di++)
					{
						ps->dop_x[didx] = refCell->dop_x[di] + refx;
						ps->dop_y[didx] = refCell->dop_y[di] + refy;
						ps->dop_z[didx] = refCell->dop_z[di] + refz;
						didx++;
					}
				}

			}
		}
	}

	for (int i = 0; i < ps->nPolymers; i++)
	{
		midx = i * 12;
		ps->z0[i] = ps->cm_z[midx];
	}

	//// calculate the unit cell size
	//DATA_TYPE minx = 0.0, miny = 0.0, minz = 0.0, maxx = 0.0, maxy = 0.0, maxz = 0.0;
	//minx = maxx = ps->cm_x[0];
	//miny = maxy = ps->cm_y[0];
	//minz = maxz = ps->cm_z[0];
	//for (int i = 0; i < ps->nMonomers; i++)
	//{
	//	if (minx < ps->cm_x[i])
	//		minx = ps->cm_x[i];
	//	if (miny < ps->cm_y[i])
	//		miny = ps->cm_y[i];
	//	if (minz < ps->cm_z[i])
	//		minz = ps->cm_z[i];

	//	if (maxx > ps->cm_x[i])
	//		maxx = ps->cm_x[i];
	//	if (maxy > ps->cm_y[i])
	//		maxy = ps->cm_y[i];
	//	if (maxz > ps->cm_z[i])
	//		maxz = ps->cm_z[i];
	//}

	//for (int i = 0; i < ps->nDopants; i++)
	//{
	//	if (minx < ps->dop_x[i])
	//		minx = ps->dop_x[i];
	//	if (miny < ps->dop_y[i])
	//		miny = ps->dop_y[i];
	//	if (minz < ps->dop_z[i])
	//		minz = ps->dop_z[i];

	//	if (maxx > ps->dop_x[i])
	//		maxx = ps->dop_x[i];
	//	if (maxy > ps->dop_y[i])
	//		maxy = ps->dop_y[i];
	//	if (maxz > ps->dop_z[i])
	//		maxz = ps->dop_z[i];
	//}
	//ps->x_size = fabs(maxx - minx);
	//ps->y_size = fabs(maxy - miny);
	//ps->z_size = fabs(maxz - minz);

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	centerParticleSystem(*ps);

	// update  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	return ps;
}

PARTICLE_SYSTEM * buildStackedParticleSystem(const AppConfig & appConfig, int xdim, int ydim, int zdim, bool addDopants)
{
	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	PARTICLE_SYSTEM * referenceChain = buildReferencedChain(appConfig, addDopants);

	if (referenceChain == NULL)
		return NULL;

	int nMonomers = xdim * ydim * zdim * appConfig.chains_per_unit_cell * appConfig.monomers_per_chain;
	int nDopants = xdim * ydim * zdim * appConfig.chains_per_unit_cell  * appConfig.dopants_per_chain;

	PARTICLE_SYSTEM * ps = new PARTICLE_SYSTEM;
	ps->x_size = xdim * referenceChain->x_size;
	ps->y_size = ydim * referenceChain->y_size / 2;
	ps->z_size = zdim * referenceChain->z_size / 2;
	ps->uc_x = referenceChain->x_size;
	ps->uc_y = referenceChain->y_size;
	ps->uc_z = referenceChain->z_size;

	ps->angleLimit = appConfig.angle_limit;
	ps->monomers_per_chain = appConfig.monomers_per_chain;

	ps->nMonomers = nMonomers;
	ps->cm_x = new DATA_TYPE[nMonomers];
	ps->cm_y = new DATA_TYPE[nMonomers];
	ps->cm_z = new DATA_TYPE[nMonomers];
	ps->dpm_x = new DATA_TYPE[nMonomers];
	ps->dpm_y = new DATA_TYPE[nMonomers];
	ps->dpm_z = new DATA_TYPE[nMonomers];
	ps->n_x = new DATA_TYPE[nMonomers];
	ps->n_y = new DATA_TYPE[nMonomers];
	ps->n_z = new DATA_TYPE[nMonomers];
	ps->heading = new DATA_TYPE[nMonomers];
	ps->pitch = new DATA_TYPE[nMonomers];
	ps->bank = new DATA_TYPE[nMonomers];
	memset(ps->heading, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->pitch, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->bank, 0, sizeof(DATA_TYPE) * nMonomers);
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->nPolymers = ps->nMonomers / appConfig.monomers_per_chain;
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

	ps->z0 = new DATA_TYPE[ps->nPolymers];
	int polymerCtr = 0;

	if (addDopants)
		ps->nDopants = nDopants;
	else
		ps->nDopants = 0;

	ps->dop_x = new DATA_TYPE[nDopants];
	ps->dop_y = new DATA_TYPE[nDopants];
	ps->dop_z = new DATA_TYPE[nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);


	DATA_TYPE refz = 0.0, refy = 0.0, refx = 0.0;
	int midx = 0, didx = 0;

	for (int z = 0; z < zdim; z++)
	{
		refz = z * appConfig.z_off;
		for (int y = 0; y < ydim; y++)
		{
			refy = y * appConfig.y_off;
			for (int x = 0; x < xdim; x++)
			{
				refx = x * appConfig.x_off * 2.0;

				 {
					for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
					{
						ps->cm_x[midx] = referenceChain->cm_x[mi] + refx;
						ps->cm_y[midx] = referenceChain->cm_y[mi] + refy;
						ps->cm_z[midx] = referenceChain->cm_z[mi] + refz;
						ps->dpm_x[midx] = referenceChain->dpm_x[mi];
						ps->dpm_y[midx] = referenceChain->dpm_y[mi];
						ps->dpm_z[midx] = referenceChain->dpm_z[mi];
						ps->n_x[midx] = referenceChain->n_x[mi];
						ps->n_y[midx] = referenceChain->n_y[mi];
						ps->n_z[midx] = referenceChain->n_z[mi];
						midx++;
					}
					ps->z0[polymerCtr++] = refz;
					if (addDopants)
					{
						for (int di = 0; di < appConfig.dopants_per_chain; di++)
						{
							ps->dop_x[didx] = referenceChain->dop_x[di] + refx;
							ps->dop_y[didx] = referenceChain->dop_y[di] + refy;
							ps->dop_z[didx] = referenceChain->dop_z[di] + refz;
							didx++;
						}
					}
				}
			}
		}
	}

	// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
	DATA_TYPE min = MIN(ps->x_size, ps->y_size);
	min = MIN(min, ps->z_size);
	if (appConfig.r_cut < 0)
	{
		ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
		//if (ps->r_cut < ps->cm_x[11] - ps->cm_x[0])
		//	ps->r_cut = ps->cm_x[11] - ps->cm_x[0];
	}
	else
	{
		ps->r_cut = appConfig.r_cut;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	centerParticleSystem(*ps);

	// update  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	// delete the reference chain
	delete referenceChain;

	return ps;
}

//TODO: finish this implementation
PARTICLE_SYSTEM * buildRotatedStackedParticleSystem(const AppConfig & appConfig, int xdim, int ydim, int zdim, bool addDopants)
{
	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	PARTICLE_SYSTEM * referenceChain = buildReferencedChain(appConfig, addDopants);

	if (referenceChain == NULL)
		return NULL;

	int nMonomers = xdim * ydim * zdim * appConfig.chains_per_unit_cell * appConfig.monomers_per_chain;
	int nDopants = xdim * ydim * zdim * appConfig.chains_per_unit_cell  * appConfig.dopants_per_chain;

	PARTICLE_SYSTEM * ps = new PARTICLE_SYSTEM;
	ps->x_size = xdim * referenceChain->x_size;
	ps->y_size = ydim * referenceChain->y_size / 2;
	ps->z_size = zdim * referenceChain->z_size / 2;
	ps->uc_x = referenceChain->x_size;
	ps->uc_y = referenceChain->y_size;
	ps->uc_z = referenceChain->z_size;

	ps->angleLimit = appConfig.angle_limit;
	ps->monomers_per_chain = appConfig.monomers_per_chain;

	ps->nMonomers = nMonomers;
	ps->cm_x = new DATA_TYPE[nMonomers];
	ps->cm_y = new DATA_TYPE[nMonomers];
	ps->cm_z = new DATA_TYPE[nMonomers];
	ps->dpm_x = new DATA_TYPE[nMonomers];
	ps->dpm_y = new DATA_TYPE[nMonomers];
	ps->dpm_z = new DATA_TYPE[nMonomers];
	ps->n_x = new DATA_TYPE[nMonomers];
	ps->n_y = new DATA_TYPE[nMonomers];
	ps->n_z = new DATA_TYPE[nMonomers];
	ps->heading = new DATA_TYPE[nMonomers];
	ps->pitch = new DATA_TYPE[nMonomers];
	ps->bank = new DATA_TYPE[nMonomers];
	memset(ps->heading, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->pitch, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->bank, 0, sizeof(DATA_TYPE) * nMonomers);
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->nPolymers = ps->nMonomers / appConfig.monomers_per_chain;
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

	ps->z0 = new DATA_TYPE[ps->nPolymers];
	int polymerCtr = 0;

	if (addDopants)
		ps->nDopants = nDopants;
	else
		ps->nDopants = 0;

	ps->dop_x = new DATA_TYPE[nDopants];
	ps->dop_y = new DATA_TYPE[nDopants];
	ps->dop_z = new DATA_TYPE[nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);


	DATA_TYPE refz = 0.0, refy = 0.0, refx = 0.0;
	int midx = 0, didx = 0;

	for (int z = 0; z < zdim; z++)
	{
		refz = z * appConfig.z_off;
		for (int y = 0; y < ydim; y++)
		{
			refy = y * appConfig.y_off;
			for (int x = 0; x < xdim; x++)
			{
				refx = x * appConfig.x_off * 2.0;

				{
					for (int mi = 0; mi < appConfig.monomers_per_chain; mi++)
					{
						ps->cm_x[midx] = referenceChain->cm_x[mi] + refx;
						ps->cm_y[midx] = referenceChain->cm_y[mi] + refy;
						ps->cm_z[midx] = referenceChain->cm_z[mi] + refz;
						ps->dpm_x[midx] = referenceChain->dpm_x[mi];
						ps->dpm_y[midx] = referenceChain->dpm_y[mi];
						ps->dpm_z[midx] = referenceChain->dpm_z[mi];
						ps->n_x[midx] = referenceChain->n_x[mi];
						ps->n_y[midx] = referenceChain->n_y[mi];
						ps->n_z[midx] = referenceChain->n_z[mi];
						midx++;
					}
					ps->z0[polymerCtr++] = refz;
					if (addDopants)
					{
						for (int di = 0; di < appConfig.dopants_per_chain; di++)
						{
							ps->dop_x[didx] = referenceChain->dop_x[di] + refx;
							ps->dop_y[didx] = referenceChain->dop_y[di] + refy;
							ps->dop_z[didx] = referenceChain->dop_z[di] + refz;
							didx++;
						}
					}
				}
			}
		}
	}

	// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
	DATA_TYPE min = MIN(ps->x_size, ps->y_size);
	min = MIN(min, ps->z_size);
	if (appConfig.r_cut < 0)
	{
		ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
		//if (ps->r_cut < ps->cm_x[11] - ps->cm_x[0])
		//	ps->r_cut = ps->cm_x[11] - ps->cm_x[0];
	}
	else
	{
		ps->r_cut = appConfig.r_cut;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	centerParticleSystem(*ps);

	// update  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(*ps);

	// delete the reference chain
	delete referenceChain;

	return ps;
}

DATA_TYPE * scaleSystem(PARTICLE_SYSTEM & ps, bool* scaleDimension, DATA_TYPE volumeScaleFactor, bool random, bool updateCutoff)
{
	return scaleSystemVolume(ps, scaleDimension, volumeScaleFactor, random, updateCutoff);
}

PARTICLE_SYSTEM * clone(const PARTICLE_SYSTEM & ps, const AppConfig & appConfig)
{
	PARTICLE_SYSTEM * copy = new PARTICLE_SYSTEM;
	copy->x_size = ps.x_size;
	copy->y_size = ps.y_size;
	copy->z_size = ps.z_size;
	copy->uc_x = ps.uc_x;
	copy->uc_y = ps.uc_y;
	copy->uc_z = ps.uc_z;
	copy->r_cut = ps.r_cut;
	copy->angleLimit = ps.angleLimit;
	copy->density = ps.density;

	copy->monomers_per_chain = ps.monomers_per_chain;
	copy->nPolymers = ps.nPolymers;
	copy->nMonomers = ps.nMonomers;
	copy->nDopants = ps.nDopants;

	copy->olig_cm_x = new DATA_TYPE[ps.nPolymers];
	copy->olig_cm_y = new DATA_TYPE[ps.nPolymers];
	copy->olig_cm_z = new DATA_TYPE[ps.nPolymers];
	memcpy(copy->olig_cm_x, ps.olig_cm_x, sizeof(DATA_TYPE) * ps.nPolymers);
	memcpy(copy->olig_cm_y, ps.olig_cm_y, sizeof(DATA_TYPE) * ps.nPolymers);
	memcpy(copy->olig_cm_z, ps.olig_cm_z, sizeof(DATA_TYPE) * ps.nPolymers);

	copy->cm_x = new DATA_TYPE[ps.nMonomers];
	copy->cm_y = new DATA_TYPE[ps.nMonomers];
	copy->cm_z = new DATA_TYPE[ps.nMonomers];
	memcpy(copy->cm_x, ps.cm_x, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->cm_y, ps.cm_y, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->cm_z, ps.cm_z, sizeof(DATA_TYPE) * ps.nMonomers);

	copy->n_x = new DATA_TYPE[ps.nMonomers];
	copy->n_y = new DATA_TYPE[ps.nMonomers];
	copy->n_z = new DATA_TYPE[ps.nMonomers];
	memcpy(copy->n_x, ps.n_x, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->n_y, ps.n_y, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->n_z, ps.n_z, sizeof(DATA_TYPE) * ps.nMonomers);

	copy->dpm_x = new DATA_TYPE[ps.nMonomers];
	copy->dpm_y = new DATA_TYPE[ps.nMonomers];
	copy->dpm_z = new DATA_TYPE[ps.nMonomers];
	memcpy(copy->dpm_x, ps.dpm_x, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->dpm_y, ps.dpm_y, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->dpm_z, ps.dpm_z, sizeof(DATA_TYPE) * ps.nMonomers);

	copy->heading = new DATA_TYPE[ps.nMonomers];
	copy->pitch   = new DATA_TYPE[ps.nMonomers];
	copy->bank    = new DATA_TYPE[ps.nMonomers];
	memcpy(copy->heading, ps.heading, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->pitch, ps.pitch, sizeof(DATA_TYPE) * ps.nMonomers);
	memcpy(copy->bank, ps.bank, sizeof(DATA_TYPE) * ps.nMonomers);

	copy->z0 = new DATA_TYPE[ps.nPolymers];
	memcpy(copy->z0, ps.z0, sizeof(DATA_TYPE) * ps.nPolymers);

	copy->dop_x = new DATA_TYPE[ps.nDopants];
	copy->dop_y = new DATA_TYPE[ps.nDopants];
	copy->dop_z = new DATA_TYPE[ps.nDopants];
	memcpy(copy->dop_x, ps.dop_x, sizeof(DATA_TYPE) * ps.nDopants);
	memcpy(copy->dop_y, ps.dop_y, sizeof(DATA_TYPE) * ps.nDopants);
	memcpy(copy->dop_z, ps.dop_z, sizeof(DATA_TYPE) * ps.nDopants);

	copy->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	copy->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];;
	memcpy(copy->torsionAngleHistogram, ps.torsionAngleHistogram, sizeof(DATA_TYPE) * NUM_BINS);
	memcpy(copy->bendingAngleHistogram, ps.bendingAngleHistogram, sizeof(DATA_TYPE) * NUM_BINS);

	return copy;
}

void calculateMSD(const PARTICLE_SYSTEM & ps, const PARTICLE_SYSTEM & reference, DATA_TYPE & monomer_msd, DATA_TYPE & dopant_msd, DATA_TYPE & oligomer_msd)
{
	return;

	int nMonomers = ps.nMonomers;
	int nDopants = ps.nDopants;
	int nPolymers = ps.nPolymers;
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	monomer_msd = 0.0;
	dopant_msd = 0.0;
	oligomer_msd = 0.0;

	for (int i = 0; i < nMonomers; i++)
	{
		xdif = ps.cm_x[i] - reference.cm_x[i];
		ydif = ps.cm_y[i] - reference.cm_y[i];
		zdif = ps.cm_z[i] - reference.cm_z[i];

		monomer_msd += xdif * xdif + ydif * ydif + zdif * zdif;
	}
	monomer_msd /= nMonomers;

	for (int i = 0; i < nDopants; i++)
	{
		xdif = ps.dop_x[i] - reference.dop_x[i];
		ydif = ps.dop_y[i] - reference.dop_y[i];
		zdif = ps.dop_z[i] - reference.dop_z[i];

		dopant_msd += xdif * xdif + ydif * ydif + zdif * zdif;
	}
	dopant_msd /= nDopants;

	for (int i = 0; i < nMonomers; i++)
	{
		xdif = ps.olig_cm_x[i] - reference.olig_cm_x[i];
		ydif = ps.olig_cm_y[i] - reference.olig_cm_y[i];
		zdif = ps.olig_cm_z[i] - reference.olig_cm_z[i];

		oligomer_msd += xdif * xdif + ydif * ydif + zdif * zdif;
	}
	oligomer_msd /= nPolymers;
}

void calculateMeanMonomerDopantClosestDistance(const PARTICLE_SYSTEM & ps, DATA_TYPE & avgMinDist, DATA_TYPE & minx, DATA_TYPE & maxx, DATA_TYPE & miny, DATA_TYPE & maxy, DATA_TYPE& minz, DATA_TYPE& maxz )
{
	DATA_TYPE r_cut = ps.r_cut;
	DATA_TYPE t_x = 0.0, t_y = 0.0, t_z = 0.0;
	DATA_TYPE dst_x = 0.0, dst_y = 0.0, dst_z = 0.0;
	DATA_TYPE src_x = 0.0, src_y = 0.0, src_z = 0.0;
	DATA_TYPE dist = 0.0;
	DATA_TYPE tmp = 0.0;
	//DATA_TYPE minx = 0.0, miny = 0.0, minz = 0.0;
	//DATA_TYPE maxx = 0.0, maxy = 0.0, maxz = 0.0;

	for (int idx = 0; idx < ps.nDopants; idx++)
	{
		src_x = ps.dop_x[idx];
		src_y = ps.dop_y[idx];
		src_z = ps.dop_z[idx];
		
		t_x = ps.cm_x[0];
		t_y = ps.cm_y[0];
		t_z = ps.cm_z[0];

		dst_x = src_x - t_x;
		dst_y = src_y - t_y;
		dst_z = src_z - t_z;

		PCB(ps.x_size, ps.y_size, ps.z_size, dst_x, dst_y, dst_z);

		dist = MAGNITUDE(dst_x, dst_y, dst_z);
		minx = maxx = fabs(dst_x);
		miny = maxy = fabs(dst_y);
		minz = maxz = fabs(dst_z);

		for (int mIdx = 0; mIdx < ps.nMonomers; mIdx++)
		{
			t_x = ps.cm_x[mIdx];
			t_y = ps.cm_y[mIdx];
			t_z = ps.cm_z[mIdx];

			dst_x = src_x - t_x;
			dst_y = src_y - t_y;
			dst_z = src_z - t_z;

			PCB(ps.x_size, ps.y_size, ps.z_size, dst_x, dst_y, dst_z);

			tmp = MAGNITUDE(dst_x, dst_y, dst_z);

			if (tmp < dist)
				dist = tmp;

			if (fabs(dst_x) < minx)
				minx = fabs(dst_x);
			if (fabs(dst_x) > maxx)
				maxx = fabs(dst_x);

			if (fabs(dst_y) < miny)
				miny = fabs(dst_y);
			if (fabs(dst_y) > maxy)
				maxy = fabs(dst_y);
			if (fabs(dst_z) < minz)
				minz = fabs(dst_z);
			if (fabs(dst_z) > maxz)
				maxz = fabs(dst_z);
		}
		avgMinDist += dist;
	}

	avgMinDist /= ps.nDopants;
}

PARTICLE_SYSTEM * buildParticleSystemFromReferenceUnitCell(const AppConfig& appConfig, const string& referenceCell, int xdim, int ydim, int zdim, bool addDopants)
{
	dopant_mass = appConfig.dopant_mass;
	oligomer_mass = appConfig.oligomer_mass;

	PARTICLE_SYSTEM* referenceUnitCell = loadParticleSystemBinaryFile(referenceCell, const_cast<AppConfig&>(appConfig));
	DATA_TYPE minx = 0.0, miny = 0.0, minz = 0.0, maxx = 0.0, maxy = 0.0, maxz = 0.0;
	DATA_TYPE cx = 0.0, cy = 0.0, cz = 0.0;

	calculateBoundingInformation(*referenceUnitCell, cx, cy, cz, minx, miny, minz, maxx, maxy, maxz);
	translateParticleSystem(*referenceUnitCell, -cx, -cy, -cz);
	calculateBoundingInformation(*referenceUnitCell, cx, cy, cz, minx, miny, minz, maxx, maxy, maxz);

	int nMonomers = referenceUnitCell->nMonomers * xdim * ydim * zdim;
	int nDopants = referenceUnitCell->nDopants * xdim * ydim * zdim;

	PARTICLE_SYSTEM* ps = new PARTICLE_SYSTEM;
	ps->x_size = xdim * referenceUnitCell->x_size;
	ps->y_size = ydim * referenceUnitCell->y_size;
	ps->z_size = zdim * referenceUnitCell->z_size;
	ps->uc_x = referenceUnitCell->x_size;
	ps->uc_y = referenceUnitCell->y_size;
	ps->uc_z = referenceUnitCell->z_size;

	ps->angleLimit = appConfig.angle_limit;
	ps->monomers_per_chain = appConfig.monomers_per_chain;

	ps->nMonomers = nMonomers;
	ps->cm_x = new DATA_TYPE[nMonomers];
	ps->cm_y = new DATA_TYPE[nMonomers];
	ps->cm_z = new DATA_TYPE[nMonomers];
	ps->dpm_x = new DATA_TYPE[nMonomers];
	ps->dpm_y = new DATA_TYPE[nMonomers];
	ps->dpm_z = new DATA_TYPE[nMonomers];
	ps->n_x = new DATA_TYPE[nMonomers];
	ps->n_y = new DATA_TYPE[nMonomers];
	ps->n_z = new DATA_TYPE[nMonomers];
	ps->heading = new DATA_TYPE[nMonomers];
	ps->pitch = new DATA_TYPE[nMonomers];
	ps->bank = new DATA_TYPE[nMonomers];
	memset(ps->heading, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->pitch, 0, sizeof(DATA_TYPE) * nMonomers);
	memset(ps->bank, 0, sizeof(DATA_TYPE) * nMonomers);
	ps->torsionAngleHistogram = new DATA_TYPE[NUM_BINS];
	ps->bendingAngleHistogram = new DATA_TYPE[NUM_BINS];
	memset(ps->torsionAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	memset(ps->bendingAngleHistogram, 0, sizeof(DATA_TYPE) * NUM_BINS);
	ps->nPolymers = ps->nMonomers / appConfig.monomers_per_chain;
	ps->olig_cm_x = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_y = new DATA_TYPE[ps->nPolymers];
	ps->olig_cm_z = new DATA_TYPE[ps->nPolymers];

	ps->z0 = new DATA_TYPE[ps->nPolymers];
	int polymerCtr = 0;

	if (addDopants)
		ps->nDopants = nDopants;
	else
		ps->nDopants = 0;

	ps->dop_x = new DATA_TYPE[nDopants];
	ps->dop_y = new DATA_TYPE[nDopants];
	ps->dop_z = new DATA_TYPE[nDopants];
	memset(ps->dop_x, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_y, 0, sizeof(DATA_TYPE) * ps->nDopants);
	memset(ps->dop_z, 0, sizeof(DATA_TYPE) * ps->nDopants);
	DATA_TYPE xoff = 0.0, yoff = 0.0, zoff = 0.0;
	int memoff_monomer = 0, memoff_dopant = 0;
	int monomers_per_row   = xdim * referenceUnitCell->nMonomers;
	int monomers_per_layer = ydim * monomers_per_row;
	int dopants_per_row    = xdim * referenceUnitCell->nDopants;
	int dopants_per_layer  = ydim * dopants_per_row;

	for (int iz = 0; iz < zdim; iz++)
	{
		zoff = iz * referenceUnitCell->z_size;
		for (int iy = 0; iy < ydim; iy++)
		{
			yoff = iy * referenceUnitCell->y_size;
			// now translate the system in the x direction
			for (int ix = 0; ix < xdim; ix++)
			{
				// calculate the x direction offset
				xoff = ix * referenceUnitCell->x_size;
				// translate to the new location
				translateParticleSystem(*referenceUnitCell, xoff, yoff, zoff);

				// now copy the memory
				memoff_monomer = iz * monomers_per_layer + iy * monomers_per_row + ix * referenceUnitCell->nMonomers;
				memcpy(&ps->cm_x[memoff_monomer], referenceUnitCell->cm_x, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->cm_y[memoff_monomer], referenceUnitCell->cm_y, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->cm_z[memoff_monomer], referenceUnitCell->cm_z, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);

				memcpy(&ps->n_x[memoff_monomer], referenceUnitCell->n_x, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->n_y[memoff_monomer], referenceUnitCell->n_y, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->n_z[memoff_monomer], referenceUnitCell->n_z, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);

				memcpy(&ps->dpm_x[memoff_monomer], referenceUnitCell->dpm_x, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->dpm_y[memoff_monomer], referenceUnitCell->dpm_y, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->dpm_z[memoff_monomer], referenceUnitCell->dpm_z, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);

				memcpy(&ps->heading[memoff_monomer], referenceUnitCell->heading, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->pitch[memoff_monomer], referenceUnitCell->pitch, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);
				memcpy(&ps->bank[memoff_monomer], referenceUnitCell->bank, sizeof(DATA_TYPE) * referenceUnitCell->nMonomers);

				if (addDopants)
				{
					memoff_dopant = iz * dopants_per_layer + iy * dopants_per_row + ix * referenceUnitCell->nDopants;
					memcpy(&ps->dop_x[memoff_dopant], referenceUnitCell->dop_x, sizeof(DATA_TYPE) * referenceUnitCell->nDopants);
					memcpy(&ps->dop_y[memoff_dopant], referenceUnitCell->dop_y, sizeof(DATA_TYPE) * referenceUnitCell->nDopants);
					memcpy(&ps->dop_z[memoff_dopant], referenceUnitCell->dop_z, sizeof(DATA_TYPE) * referenceUnitCell->nDopants);
				}
				// put it back to original location
				translateParticleSystem(*referenceUnitCell, -xoff, -yoff, -zoff);
			}
		}
	}
	// make the cut off radius 60% of the smallest dimension or the length of a chain (this prevents chain tearing)
	DATA_TYPE min = MIN(ps->x_size, ps->y_size);
	min = MIN(min, ps->z_size);
	if (appConfig.r_cut < 0)
	{
		ps->r_cut = (min / 2.0) * 0.98; // appConfig.r_cut;
	}
	else
	{
		ps->r_cut = appConfig.r_cut;
	}

	ps->density = ((ps->nPolymers * oligomer_mass) + ps->nDopants * dopant_mass) / (AVOGADRO_NUM * (ps->x_size * 1e-8) * (ps->y_size * 1e-8) * (ps->z_size * 1e-8));

	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	recenterParticleSystem(*ps, dx, dy, dz);

	// delete the reference chain
	delete referenceUnitCell;

	return ps;
}

void calculateBoundingInformation(PARTICLE_SYSTEM& ps, DATA_TYPE& cx, DATA_TYPE& cy, DATA_TYPE& cz, 
	                              DATA_TYPE & minx, DATA_TYPE& miny, DATA_TYPE& minz,
	                              DATA_TYPE& maxx, DATA_TYPE& maxy, DATA_TYPE& maxz)
{
	minx = ps.cm_x[0], maxx = ps.cm_x[0], miny = ps.cm_y[0], maxy = ps.cm_y[0], minz = ps.cm_z[0], maxz = ps.cm_z[0];
	DATA_TYPE tx = 0.0, ty = 0.0, tz = 0.0;

	for (int ocm = 0; ocm < ps.nMonomers; ocm++)
	{
		tx = ps.cm_x[ocm];
		ty = ps.cm_y[ocm];
		tz = ps.cm_z[ocm];

		wrapInBox(ps.x_size, ps.y_size, ps.z_size, tx, ty, tz);

		if (minx > tx)
			minx = tx;
		if (maxx < tx)
			maxx = tx;

		if (miny > ty)
			miny = ty;
		if (maxy < ty)
			maxy = ty;

		if (minz > tz)
			minz = tz;
		if (maxz < tz)
			maxz = tz;
	}

	for (int ocm = 0; ocm < ps.nDopants; ocm++)
	{
		tx = ps.dop_x[ocm];
		ty = ps.dop_y[ocm];
		tz = ps.dop_z[ocm];

		wrapInBox(ps.x_size, ps.y_size, ps.z_size, tx, ty, tz);

		if (minx > tx)
			minx = tx;
		if (maxx < tx)
			maxx = tx;

		if (miny > ty)
			miny = ty;
		if (maxy < ty)
			maxy = ty;

		if (minz > tz)
			minz = tz;
		if (maxz < tz)
			maxz = tz;
	}

	DATA_TYPE sizex = (maxx + minx), sizey = (maxy + miny), sizez = (maxz + minz);
	cx = sizex / 2.0;
	cy = sizey / 2.0;
	cz = sizez / 2.0;
}

void translateParticleSystem(PARTICLE_SYSTEM& ps, DATA_TYPE dx, DATA_TYPE dy, DATA_TYPE dz)
{
	for (int ocm = 0; ocm < ps.nPolymers; ocm++)
	{
		translateOligomer(ps, ocm, dx, dy, dz);
		ps.z0[ocm] += -dz;
	}
	for (int i = 0; i < ps.nDopants; i++)
	{
		ps.dop_x[i] += dx;
		ps.dop_y[i] += dy;
		ps.dop_z[i] += dz;
	}

	// calcualte  the oligomer's centers of mass
	calculateOligomersCenterOfMasses(ps);
}

void generateOligomerGroupings(PARTICLE_SYSTEM& ps, int groupSize)
{
	for (int outer = 0; outer < ps.nPolymers - groupSize; outer++)
	{
		for (int inner = outer + 1; inner < ps.nPolymers; inner += groupSize)
		{
			stringstream ss;
			ss << "pairs/oligomer_" << outer << "-" << inner << ".xyz";
			ofstream out(ss.str().c_str());

			int outer_lb = outer * ps.monomers_per_chain;
			int outer_ub = outer_lb + ps.monomers_per_chain;

			int inner_lb = inner * ps.monomers_per_chain;
			int inner_ub = inner_lb + ps.monomers_per_chain * groupSize;

			if (out.is_open())
			{
				out << ((groupSize + 1) * ps.monomers_per_chain) * a.nAtoms + (groupSize + 1) * (ps.nDopants / ps.nPolymers) + 2 * (groupSize + 1) << endl << endl;
				// write the first chain
				{
					DATA_TYPE nx = 0.0, ny = 0.0, nz = 0.0;
					DATA_TYPE tx = 0.0, ty = 0.0, tz = 0.0;
					int input_chain_id = 0, exc_lb = 0, exc_ub = 0;
					int monomers_per_chain = ps.monomers_per_chain;

					for (int y = outer_lb; y < outer_ub; y++)
					{
						input_chain_id = outer;
						exc_lb = outer_lb;
						exc_ub = outer_ub - 1;

						tx = ps.cm_x[y];
						ty = ps.cm_y[y];
						tz = ps.cm_z[y];
						//wrapInBox(ps.x_size, ps.y_size, ps.z_size, tx, ty, tz);

						if (y % 2 != 0)
						{
							// now translate the monomer to 0,0,0
							for (int i = 0; i < a.nAtoms; i++)
							{
								nx = a.x[i];
								ny = a.y[i];
								nz = a.z[i];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << a.name[i] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;


							}
							if (y == exc_ub)
							{
								nx = a_t.x[0];
								ny = a_t.y[0];
								nz = a_t.z[0];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << a_t.name[0] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
							}
						}
						else
						{
							// now translate the monomer to 0,0,0
							for (int i = 0; i < b.nAtoms; i++)
							{
								nx = b.x[i];
								ny = b.y[i];
								nz = b.z[i];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << b.name[i] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
							}
							if (y == exc_lb)
							{
								nx = b_t.x[0];
								ny = b_t.y[0];
								nz = b_t.z[0];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << b_t.name[0] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
							}
						}
					}

					int dlb = outer * 4;
					int dub = dlb + 4;

					for (int i = dlb; i < dub; i++)
					{
						nx = ps.dop_x[i];
						ny = ps.dop_y[i];
						nz = ps.dop_z[i];
						//wrapInBox(ps.x_size, ps.y_size, ps.z_size, nx, ny, nz);

						out << "Cl" << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
					}
				}
				// write the second group
				{
					DATA_TYPE nx = 0.0, ny = 0.0, nz = 0.0;
					DATA_TYPE tx = 0.0, ty = 0.0, tz = 0.0;
					int input_chain_id = 0, exc_lb = 0, exc_ub = 0;
					int monomers_per_chain = ps.monomers_per_chain;

					for (int y = inner_lb; y < inner_ub; y++)
					{
						input_chain_id = y / monomers_per_chain;;
						exc_lb = inner_lb;
						exc_ub = inner_ub - 1;

						tx = ps.cm_x[y];
						ty = ps.cm_y[y];
						tz = ps.cm_z[y];
						//wrapInBox(ps.x_size, ps.y_size, ps.z_size, tx, ty, tz);

						if (y % 2 != 0)
						{
							// now translate the monomer to 0,0,0
							for (int i = 0; i < a.nAtoms; i++)
							{
								nx = a.x[i];
								ny = a.y[i];
								nz = a.z[i];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << a.name[i] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
							}
							if (y == ((input_chain_id * monomers_per_chain) + monomers_per_chain - 1))  // TODO: fix this, only add hydrogen on the boundaries not end of chain, caused by recent grouping change
							{
								nx = a_t.x[0];
								ny = a_t.y[0];
								nz = a_t.z[0];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << a_t.name[0] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
							}
						}
						else
						{
							// now translate the monomer to 0,0,0
							for (int i = 0; i < b.nAtoms; i++)
							{
								nx = b.x[i];
								ny = b.y[i];
								nz = b.z[i];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << b.name[i] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
							}
							if (y == (input_chain_id * monomers_per_chain)) // TODO: fix this, only add hydrogen on the boundaries not end of chain, caused by recent grouping change
							{
								nx = b_t.x[0];
								ny = b_t.y[0];
								nz = b_t.z[0];

								rotatePoint(ps.heading[y], ps.pitch[y], ps.bank[y], nx, ny, nz);

								nx += tx;
								ny += ty;
								nz += tz;

								out << b_t.name[0] << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
							}
						}
					}

					int dlb = inner * 4;
					int dub = dlb + 4 * (groupSize + 1);

					for (int i = dlb; i < dub; i++)
					{
						nx = ps.dop_x[i];
						ny = ps.dop_y[i];
						nz = ps.dop_z[i];
						//wrapInBox(ps.x_size, ps.y_size, ps.z_size, nx, ny, nz);

						out << "Cl" << setw(15) << nx << setw(15) << ny << setw(15) << nz << endl;
					}
				}
				out.flush();
				out.close();
			}
		}
	}
}
