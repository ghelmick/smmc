#if !defined (__GPU_HELPER__)
#define       __GPU_HELPER__

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "polymerUtil.h"

DATA_TYPE getDeviceInterPartMonomer(PARTICLE_SYSTEM & ps, int midx, int didx, int mlb, int mub, 
	                                int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, 
	                                DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz, bool removeDouble = false);
DATA_TYPE getDeviceInterPartDopant(PARTICLE_SYSTEM & ps, int midx, int didx, int mlb, int mub, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz);
void cleanup();
void synchronizeGPU();
void allocateMemoryGPU(PARTICLE_SYSTEM & ps);
void copyMemoryToGPU(PARTICLE_SYSTEM & ps);
void copyMemoryFromGPU(PARTICLE_SYSTEM & ps);
void updateFullSystem(PARTICLE_SYSTEM & ps);
void updateMonomer(PARTICLE_SYSTEM & ps, int midx);
void updateDopant(PARTICLE_SYSTEM & ps, int didx);
void updateSystemSize(DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_);
void initGPUDataStructures(PARTICLE_SYSTEM & ps, int mlb, int mub, int dlb, int dub, int number_gpu_threads_mon = 256, int number_gpu_threads_dop = 64);
DATA_TYPE getDeviceDopDopPart(PARTICLE_SYSTEM & ps, int dIdx, int dlb, int dub,	DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz);
DATA_TYPE getDevicePyPyPart(PARTICLE_SYSTEM & ps, int mIdx, int mlb, int mub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz,
	                        DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz, bool removeDouble = false);
DATA_TYPE getDevicePyDopPartMonMove(PARTICLE_SYSTEM & ps, int mIdx, int dlb, int dub);
DATA_TYPE getDevicePyDopPartDopMove(PARTICLE_SYSTEM & ps, int dIdx, int mlb, int mub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz);
void deviceScaleSystem(PARTICLE_SYSTEM& ps, DATA_TYPE * scaleFactor, bool updateCutoff);
void setModelConstants(bool usePCB_, DATA_TYPE sigma_dopant_, DATA_TYPE Q_dop_, DATA_TYPE epsilon_dopant_, DATA_TYPE kappa_, DATA_TYPE kappa_inter_end_,
	                   DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_, DATA_TYPE Q_mono_,
	                   DATA_TYPE sigma_inter_, DATA_TYPE sigma_inter_end_, DATA_TYPE epsilon_inter_,
	                   DATA_TYPE epsilon_inter_end_, DATA_TYPE dp_mag_, DATA_TYPE A_, DATA_TYPE B_, DATA_TYPE C_, DATA_TYPE sigmaPyDopant_);

__global__ void d_DopDopPart(const DATA_TYPE * d_dop_x, const DATA_TYPE * d_dop_y, const DATA_TYPE * d_dop_z, DATA_TYPE * d_udopdop, 
	                         int size, int didx, int dlb, int dub, DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz);
__global__ void d_setModelConstants(bool usePCB_, DATA_TYPE sigma_dopant_, DATA_TYPE Q_dop_, DATA_TYPE epsilon_dopant_,
	                                DATA_TYPE kappa_, DATA_TYPE kappa_inter_end_, DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_,
	                                DATA_TYPE Q_mono_, DATA_TYPE sigma_inter_, DATA_TYPE sigma_inter_end_, DATA_TYPE epsilon_inter_,
	                                DATA_TYPE epsilon_inter_end_, DATA_TYPE dp_mag_, DATA_TYPE A_, DATA_TYPE B_, DATA_TYPE C_, DATA_TYPE sigmaPyDopant_);
__global__ void d_PyPyPart(const DATA_TYPE * d_cm_x, const DATA_TYPE * d_cm_y, const DATA_TYPE * d_cm_z,
	                       const DATA_TYPE * d_dpm_x, const DATA_TYPE * d_dpm_y, const DATA_TYPE * d_dpm_z, DATA_TYPE * d_umonmon,
	                       int midx, int mlb, int mub, int monomers_per_chain, int nMonomers, bool removeDouble,
	                       DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz);
__global__ void d_PyDopPartMonMove(const DATA_TYPE * d_cm_x, const DATA_TYPE * d_cm_y, const DATA_TYPE * d_cm_z,
	                             const DATA_TYPE * d_dpm_x, const DATA_TYPE * d_dpm_y, const DATA_TYPE * d_dpm_z,
	                             const DATA_TYPE * d_dop_x, const DATA_TYPE * d_dop_y, const DATA_TYPE * d_dop_z,
	                             DATA_TYPE * d_umonmon, int nDopants, int nMonomers, int midx, int dlb, int dub,
	                             DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz, DATA_TYPE ndpmx, DATA_TYPE ndpmy, DATA_TYPE ndpmz);
__global__ void d_PyDopPartDopMove(const DATA_TYPE * d_cm_x, const DATA_TYPE * d_cm_y, const DATA_TYPE * d_cm_z,
   	                             const DATA_TYPE * d_dpm_x, const DATA_TYPE * d_dpm_y, const DATA_TYPE * d_dpm_z,
	                             const DATA_TYPE * d_dop_x, const DATA_TYPE * d_dop_y, const DATA_TYPE * d_dop_z,
	                             DATA_TYPE * d_umonmon, int nDopants, int nMonomers, int didx, int mlb, int mub,
                               	 DATA_TYPE nx, DATA_TYPE ny, DATA_TYPE nz);
__global__ void d_updateMonomer(DATA_TYPE * d_cm_x, DATA_TYPE * d_cm_y, DATA_TYPE * d_cm_z,
	                            DATA_TYPE cm_x_, DATA_TYPE cm_y_, DATA_TYPE cm_z_,
	                            DATA_TYPE * d_dpm_x, DATA_TYPE * d_pm_y, DATA_TYPE * d_pm_z,
	                            DATA_TYPE dpm_x_, DATA_TYPE dpm_y_, DATA_TYPE dpm_z_,
	                            int midx);
__global__ void d_updateDopant(DATA_TYPE * d_dop_x, DATA_TYPE * d_dop_y, DATA_TYPE * d_dop_z,
 	                           DATA_TYPE dop_x_, DATA_TYPE dop_y_, DATA_TYPE dop_z_, int didx);
__device__  void d_PCB(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z);
__global__ void d_updateSystemSize(DATA_TYPE x_size_, DATA_TYPE y_size_, DATA_TYPE z_size_, DATA_TYPE r_cut_);
__global__ void d_scaleSystem(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	                          DATA_TYPE* d_dop_x, DATA_TYPE* d_dop_y, DATA_TYPE* d_dop_z,
	                          DATA_TYPE* d_olig_cm_x, DATA_TYPE* d_olig_cm_y, DATA_TYPE* d_olig_cm_z,
	                          int monomers_per_chain, int nMonomers, int nDopants,
	                          DATA_TYPE scaleFactorX, DATA_TYPE scaleFactorY, DATA_TYPE scaleFactorZ,
							  bool updateCutoff);
__device__  void d_calculateOligomersCenterOfMasses(DATA_TYPE* d_cm_x, DATA_TYPE* d_cm_y, DATA_TYPE* d_cm_z,
	DATA_TYPE* d_olig_cm_x, DATA_TYPE* d_olig_cm_y, DATA_TYPE* d_olig_cm_z, int monomers_per_chain, int nMonomers);

__device__ void warpReduce(volatile DATA_TYPE* sdata, int tid);

DATA_TYPE getDeviceTotalInter(PARTICLE_SYSTEM & ps, int mlb, int mub, int dlb, int dub, DATA_TYPE & upypy, DATA_TYPE & upydop, DATA_TYPE & udopdop, bool removeDouble = false);
__global__ void d_PyPyTotalInter(const DATA_TYPE * d_cm_x, const DATA_TYPE * d_cm_y, const DATA_TYPE * d_cm_z,
	const DATA_TYPE * d_dpm_x, const DATA_TYPE * d_dpm_y, const DATA_TYPE * d_dpm_z, DATA_TYPE * d_umonmon,
	int mlb, int mub, int monomers_per_chain, int nMonomers, bool removeDouble);
__global__ void d_PyDopTotalInter(const DATA_TYPE * d_cm_x, const DATA_TYPE * d_cm_y, const DATA_TYPE * d_cm_z,
	const DATA_TYPE * d_dpm_x, const DATA_TYPE * d_dpm_y, const DATA_TYPE * d_dpm_z,
	const DATA_TYPE * d_dop_x, const DATA_TYPE * d_dop_y, const DATA_TYPE * d_dop_z,
	DATA_TYPE * d_pydop_dop_mov, int nDopants, int nMonomers, int mlb, int mub);
__global__ void d_DopDopTotalInter(const DATA_TYPE * d_dop_x, const DATA_TYPE * d_dop_y, const DATA_TYPE * d_dop_z, DATA_TYPE * d_udopdop, int size, int dlb, int dub);

#endif