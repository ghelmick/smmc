#include "ModelPotential.h"
#include "util.h"
#include "polymerUtil.h"
#include "AppConfig.h"
#include "gpuHelper.cuh"

#include <iomanip>


ModelPotential::ModelPotential(const AppConfig& appConfig)
{
	self_interaction = appConfig.self_interaction;
	dp_mag = appConfig.dp_mag;
	intra_coul_offset = 1; // default to nearest neighbor 
	intra_kappa = 0.085;

	map<string, DATA_TYPE>::const_iterator itr;

	// Bonding Constants
	itr = appConfig.constants.find("a0");
	if (itr != appConfig.constants.end())
		a0 = itr->second;
	else
		cout << "throw exception, constant a0 must have a value" << endl;

	itr = appConfig.constants.find("De");
	if (itr != appConfig.constants.end())
		De = itr->second;
	else
		cout << "throw exception, constant De must have a value" << endl;

	itr = appConfig.constants.find("r0");
	if (itr != appConfig.constants.end())
		r0 = itr->second;
	else
		cout << "throw exception, constant r0 must have a value" << endl;
	itr = appConfig.constants.find("alpha_");
	if (itr != appConfig.constants.end())
		alpha_ = itr->second;
	else
		cout << "throw exception, constant alpha_ must have a value" << endl;

	// Bending Constants
	itr = appConfig.constants.find("k_theta");
	if (itr != appConfig.constants.end())
		k_theta = itr->second;
	else
		cout << "throw exception, constant k_theta must have a value" << endl;
	itr = appConfig.constants.find("theta_0");
	if (itr != appConfig.constants.end())
		theta_0 = itr->second;
	else
		cout << "throw exception, constant theta_0 must have a value" << endl;

	// Torsion Constants
	itr = appConfig.constants.find("k1");
	if (itr != appConfig.constants.end())
		k1 = itr->second;
	else
		cout << "throw exception, constant k1 must have a value" << endl;
	itr = appConfig.constants.find("k2");
	if (itr != appConfig.constants.end())
		k2 = itr->second;
	else
		cout << "throw exception, constant k2 must have a value" << endl;
	itr = appConfig.constants.find("gamma_0");
	if (itr != appConfig.constants.end())
		gamma_0 = itr->second;
	else
		cout << "throw exception, constant gamma_0 must have a value" << endl;

	// Anti - Coiling Constants
	itr = appConfig.constants.find("epsilon_intra");
	if (itr != appConfig.constants.end())
		epsilon_intra = itr->second;
	else
		cout << "throw exception, constant epsilon_intra must have a value" << endl;
	itr = appConfig.constants.find("sigma_intra");
	if (itr != appConfig.constants.end())
		sigma_intra = itr->second;
	else
		cout << "throw exception, constant sigma_intra must have a value" << endl;

	// Rotation Constants
	itr = appConfig.constants.find("k4");
	if (itr != appConfig.constants.end())
		k4 = itr->second;
	else
		cout << "throw exception, constant k4 must have a value" << endl;

	// Restoration Constants
	itr = appConfig.constants.find("k5");
	if (itr != appConfig.constants.end())
		k5 = itr->second;
	else
		cout << "throw exception, constant k5 must have a value" << endl;
	itr = appConfig.constants.find("k5_end");
	if (itr != appConfig.constants.end())
		k5_end = itr->second;
	else
		cout << "throw exception, constant k5 must have a value" << endl;

	// InterPotential Constants
	// UCoulomb Py - Dopant constants
	itr = appConfig.constants.find("q");
	if (itr != appConfig.constants.end())
		Q_mono = itr->second;
	else
		cout << "throw exception, constant q must have a value" << endl;
	itr = appConfig.constants.find("Q");
	if (itr != appConfig.constants.end())
		Q_dop = itr->second;
	else
		cout << "throw exception, constant Q must have a value" << endl;
	itr = appConfig.constants.find("QSqrtd");
	if (itr != appConfig.constants.end())
		QSqrtd = itr->second;
	else
		cout << "throw exception, constant QSqrtd must have a value" << endl;
	itr = appConfig.constants.find("kappa");
	if (itr != appConfig.constants.end())
		kappa = itr->second;
	else
		cout << "throw exception, constant kappa must have a value" << endl;

	// UDisp Py - Dopant
	itr = appConfig.constants.find("A");
	if (itr != appConfig.constants.end())
		A = itr->second;
	else
		cout << "throw exception, constant A must have a value" << endl;
	itr = appConfig.constants.find("B");
	if (itr != appConfig.constants.end())
		B = itr->second;
	else
		cout << "throw exception, constant B must have a value" << endl;
	itr = appConfig.constants.find("sigmaPyDopant");
	if (itr != appConfig.constants.end())
		sigmaPyDopant = itr->second;
	else
		cout << "throw exception, constant sigmaPyDopant must have a value" << endl;

	// UExcluded Py - Py
	itr = appConfig.constants.find("epsilon_inter");
	if (itr != appConfig.constants.end())
		epsilon_inter = itr->second;
	else
		cout << "throw exception, constant epsilon_inter must have a value" << endl;
	itr = appConfig.constants.find("sigma_inter");
	if (itr != appConfig.constants.end())
		sigma_inter = itr->second;
	else
		cout << "throw exception, constant sigma_inter must have a value" << endl;
	itr = appConfig.constants.find("epsilon_inter_end");
	if (itr != appConfig.constants.end())
		epsilon_inter_end = itr->second;
	else
		cout << "throw exception, constant epsilon_inter_end must have a value" << endl;
	itr = appConfig.constants.find("sigma_inter_end");
	if (itr != appConfig.constants.end())
		sigma_inter_end = itr->second;
	else
		cout << "throw exception, constant sigma_inter_end must have a value" << endl;

	// UCD Py - Dopant
	itr = appConfig.constants.find("C");
	if (itr != appConfig.constants.end())
		C = itr->second;
	else
		cout << "throw exception, constant C must have a value" << endl;

	// UDopant - Dopant
	itr = appConfig.constants.find("epsilon_dopant");
	if (itr != appConfig.constants.end())
		epsilon_dopant = itr->second;
	else
		cout << "throw exception, constant epsilon_dopant must have a value" << endl;
	itr = appConfig.constants.find("sigma_dopant");
	if (itr != appConfig.constants.end())
		sigma_dopant = itr->second;
	else
		cout << "throw exception, constant sigma_dopant must have a value" << endl;
	itr = appConfig.constants.find("sigma_dopant_9");
	if (itr != appConfig.constants.end())
		sigma_dopant_9 = itr->second;
	else
		cout << "throw exception, constant sigma_dopant_9 must have a value" << endl;

	monomers_per_chain = appConfig.monomers_per_chain;

	itr = appConfig.constants.find("intra_coul_offset");
	if (itr != appConfig.constants.end())
		intra_coul_offset = itr->second;
	else
		cout << "throw exception, constant sigma_dopant_9 must have a value" << endl;

	itr = appConfig.constants.find("intra_kappa");
	if (itr != appConfig.constants.end())
		intra_kappa = itr->second;
	else
	{
		intra_kappa = kappa;
		cout << "no value specified for the intra kappa value, default to using the same value for both inter and intra!!!" << endl;
	}

	itr = appConfig.constants.find("kappa_inter_end");
	if (itr != appConfig.constants.end())
		kappa_inter_end = itr->second;
	else
	{
		kappa_inter_end = kappa;
		cout << "no value specified for the inter kappa end value, default to using the same value for both inter and intra!!!" << endl;
	}
}

DATA_TYPE ModelPotential::TotalIntra(PARTICLE_SYSTEM& ps, ofstream& logFile, bool logResults)
{
	DATA_TYPE ubond = 0.0, ubend = 0.0, ucoul = 0.0, ucoul2 = 0.0;
	DATA_TYPE utor = 0.0, udd = 0.0, uac = 0.0;
	DATA_TYPE urot = 0.0, urest = 0.0;
	int midx = 0;
	DATA_TYPE bond = 0.0, bend = 0.0, torsion = 0.0, coulomb = 0.0, rotation = 0.0;

	//#pragma omp parallel for reduction(+:ubond, ubend, ucoul, utor, udd, uac, urot, urest) private(midx, bond, bend, torsion, coulomb, rotation)
	for (midx = 0; midx < ps.nMonomers; midx = midx + monomers_per_chain)
	{
		bond = 0.0, bend = 0.0, torsion = 0.0, coulomb = 0.0, rotation = 0.0;

		udd += UdipoleDipolePart(ps, midx);
		uac += UantiCoilingPart(ps, midx);
		urest += URestorationPart(ps, midx);

		pairwiseIntraTerms(ps, midx, bond, bend, torsion, coulomb, rotation);
		ubond += bond;
		ubend += bend;
		ucoul2 += coulomb;
		ucoul += UcoulombPart(ps, midx);
		utor += torsion;
		urot += rotation;
	}

	DATA_TYPE total = ubond + ubend + ucoul + utor + udd + uac + urot + urest;

	if (logResults)
	{
		logFile << std::setprecision(15) << ubond << ',' << std::setprecision(15) << ubend << ',' << std::setprecision(15) << utor << ',' << std::setprecision(15) << udd << ',' << std::setprecision(15) << uac << ',' << std::setprecision(15) << urest << ',' << std::setprecision(15) << urot << ',' << std::setprecision(15) << ucoul << ',';
	}
	return total;
}

DATA_TYPE ModelPotential::TotalInter(PARTICLE_SYSTEM& ps, int mlb, int mub, int dlb, int dub, DATA_TYPE& virialPressure, DATA_TYPE& upypy, DATA_TYPE& upydop, DATA_TYPE& udopdop, DATA_TYPE& ucoul, DATA_TYPE& uother, DATA_TYPE& uself_fact)
{
	upypy = 0.0;
	upydop = 0.0;
	udopdop = 0.0;
	DATA_TYPE tempDopDop = 0.0, t1 = 0.0, t2 = 0.0, t3 = 0.0, t4 = 0.0;
	DATA_TYPE mon_coul = 0.0, mon_exc = 0.0, mon_dd = 0.0;
	DATA_TYPE mon_dop_coul = 0.0, mon_dop_cd = 0.0, mon_dop_disp = 0.0;
	DATA_TYPE dop_coul = 0.0, dop_other = 0.0;
	int midx = 0, didx = 0;
	DATA_TYPE dampingShiftMonMon = 0.0;
	DATA_TYPE dampingShiftMonDop = 0.0;
	DATA_TYPE dampingShiftDopDop = 0.0;
	DATA_TYPE self_interaction_correction = 0.0;

#if defined __USE_FULL_WOLF__
	dampingShiftMonMon = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers);
	dampingShiftMonDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers + Q_dop * Q_dop * ps.nDopants);
	dampingShiftDopDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_dop * Q_dop * ps.nDopants);
#endif

#if defined USE_GPU
	DATA_TYPE inter_pe = 0.0;
//#pragma omp parallel sections
	{
		//#pragma omp section
		{
			inter_pe = getDeviceTotalInter(ps, mlb, mub, dlb, dub, upypy, upydop, udopdop, true);
		}

		//#pragma omp section
		{
			ucoul = 0.0;
			uother = 0.0;
			DATA_TYPE temp = 0.0;

			for (int oidx = 0; oidx < ps.nPolymers; oidx++)
			{
				temp = 0.0;
				for (midx = oidx * ps.monomers_per_chain; midx < oidx * ps.monomers_per_chain + ps.monomers_per_chain; midx++)
				{
					temp += PyPyPartImageInteraction(ps, midx, mlb, mub, t1, t2, t3, t4);
				}
				//cout << oidx << '\t' << temp << endl;
				self_interaction_correction += temp;
			}
			self_interaction_correction *= 0.5;
		}
	}
	uself_fact = self_interaction_correction;
	upypy += self_interaction_correction;
	return (inter_pe + self_interaction_correction - dampingShiftMonMon - dampingShiftMonDop - dampingShiftDopDop);
#else
	for (midx = 0; midx < ps.nMonomers; midx++)
	{
		upypy += PyPyPart(ps, midx, mlb, mub, virialPressure, t1, t2, t3, true);
		mon_coul += t1;
		mon_exc += t2;
		mon_dd += t3;
		upydop += PyDopPartMonMove(ps, midx, dlb, dub, virialPressure, t1, t2, t3);
		mon_dop_coul += t1;
		mon_dop_cd += t2;
		mon_dop_disp += t3;

		self_interaction_correction += PyPyPartImageInteraction(ps, midx, mlb, mub, t1, t2, t3, t4);
	}

	for (didx = 0; didx < ps.nDopants; didx++)
	{
		udopdop += DopDopPart(ps, didx, dlb, dub, virialPressure, t1, t2);
		dop_coul += t1;
		dop_other += t2;
	}
	udopdop *= 0.5;
	self_interaction_correction *= 0.5;
	uself_fact = self_interaction_correction;

	ucoul = mon_coul + mon_dop_coul + dop_coul * 0.5;
	uother = mon_exc + mon_dd + mon_dop_cd + mon_dop_disp + dop_other * 0.5;
	upypy += self_interaction_correction;

	return (upypy + upydop + udopdop - dampingShiftMonMon - dampingShiftMonDop - dampingShiftDopDop);
#endif
}

DATA_TYPE ModelPotential::TotalIntraPart(PARTICLE_SYSTEM& ps, int midx)
{
	// calculate the initial energy
	DATA_TYPE ubond = 0.0, ubend = 0.0, ucoul = 0.0, ucoul2 = 0.0;
	DATA_TYPE utor = 0.0, udd = 0.0, uac = 0.0;
	DATA_TYPE urot = 0.0, urest = 0.0;

	{
		udd = UdipoleDipolePart(ps, midx);
		uac = UantiCoilingPart(ps, midx);
		urest = URestorationPart(ps, midx);
		pairwiseIntraTerms(ps, midx, ubond, ubend, utor, ucoul2, urot);
		ucoul += UcoulombPart(ps, midx);

	}
	DATA_TYPE total = ubond + ubend + ucoul + utor + udd + uac + urot + urest;

	return total;
}

DATA_TYPE ModelPotential::TotalInterPartMonomer(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, int dlb, int dub)
{
	DATA_TYPE upypy = 0.0;
	DATA_TYPE upydop = 0.0;
	DATA_TYPE virialPressure = 0.0;
	DATA_TYPE t1 = 0.0, t2 = 0.0, t3 = 0.0; // dummy placeholder variables
	DATA_TYPE test = 0.0, t4 = 0.0;

#if defined USE_GPU
	upypy += getDevicePyPyPart(ps, midx, mlb, mub, ps.cm_x[midx], ps.cm_y[midx], ps.cm_z[midx], ps.dpm_x[midx], ps.dpm_y[midx], ps.dpm_z[midx]);
	upydop += getDevicePyDopPartMonMove(ps, midx, dlb, dub);
#else	
	upypy = PyPyPart(ps, midx, mlb, mub, virialPressure, t1, t2, t3);
	upydop = PyDopPartMonMove(ps, midx, dlb, dub, virialPressure, t1, t2, t3);
#endif	

	test += PyPyPartImageInteraction(ps, midx, mlb, mub, t1, t2, t3, t4);

	DATA_TYPE dampingShiftMonMon = 0.0;
	DATA_TYPE dampingShiftMonDop = 0.0;
#if defined __USE_FULL_WOLF__
	dampingShiftMonMon = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers);
	dampingShiftMonDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers + Q_dop * Q_dop * ps.nDopants);
#endif
	return (upypy + upydop + test - dampingShiftMonMon - dampingShiftMonDop);
}

DATA_TYPE ModelPotential::TotalInterPartDopant(PARTICLE_SYSTEM& ps, int midx, int didx, int mlb, int mub, int dlb, int dub)
{
	DATA_TYPE upydop = 0.0;
	DATA_TYPE udopdop = 0.0;
	DATA_TYPE virialPressure = 0.0;
	DATA_TYPE t1 = 0.0, t2 = 0.0; // dummy placeholder variables
#if defined USE_GPU
	upydop = getDevicePyDopPartDopMove(ps, didx, mlb, mub, ps.dop_x[didx], ps.dop_y[didx], ps.dop_z[didx]);
	udopdop = getDeviceDopDopPart(ps, didx, dlb, dub, ps.dop_x[didx], ps.dop_y[didx], ps.dop_z[didx]);
#else	
	upydop = PyDopPartDopMove(ps, didx, mlb, mub, virialPressure);
	udopdop = DopDopPart(ps, didx, dlb, dub, virialPressure, t1, t2);
#endif	

	DATA_TYPE dampingShiftMonDop = 0.0;
	DATA_TYPE dampingShiftDopDop = 0.0;
#if defined __USE_FULL_WOLF__
	dampingShiftMonDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_mono * Q_mono * ps.nMonomers + Q_dop * Q_dop * ps.nDopants);
	dampingShiftDopDop = ((erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / (2.0 * ANG_TO_BOHR(ps.r_cut))) + (kappa / sqrt(PI))) * (Q_dop * Q_dop * ps.nDopants);
#endif
	return (upydop + udopdop - dampingShiftDopDop - dampingShiftMonDop);
}

void ModelPotential::pairwiseIntraTerms(PARTICLE_SYSTEM& ps, int midx, DATA_TYPE& bond, DATA_TYPE& bend, DATA_TYPE& torsion, DATA_TYPE& coulomb, DATA_TYPE& rotation)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE ubond = 0.0, ubend = 0.0, utor = 0.0;
	DATA_TYPE dif = 0.0, e = 0.0;
	DATA_TYPE theta0 = cos(DEG_TO_RAD(theta_0));
	DATA_TYPE cangle = 0.0;
	DATA_TYPE a[3], b[3];
	DATA_TYPE gamma_0_rad = DEG_TO_RAD(gamma_0);
	DATA_TYPE cos_gamma_0 = cos(gamma_0_rad);
	DATA_TYPE cosGamma = 0.0, cosGamma0 = cos_gamma_0, sinGamma = 0.0, sinGamma0 = sin(gamma_0_rad), cosDeltaGamma = 0.0, cos2DeltaGamma = 0.0;
	DATA_TYPE qsrd = Q_mono * Q_mono;
	DATA_TYPE ucoulomb = 0.0, r = 0.0;
	DATA_TYPE urot = 0.0;
	DATA_TYPE rij[3], rji[3], rij_dot = 0.0, rji_dot = 0.0;
	DATA_TYPE cos_theta0 = cos(DEG_TO_RAD(theta_0 / 2.0));
	DATA_TYPE dei1 = 0.0, dei2 = 0.0;

	for (int i = lb; i < ub - 1; i++)
	{
		if (i - 1 < lb)
		{
			a[0] = ps.cm_x[lb] - ps.cm_x[i];
			a[1] = ps.cm_y[lb] - ps.cm_y[i];
			a[2] = ps.cm_z[lb] - ps.cm_z[i];
		}
		else
		{
			a[0] = ps.cm_x[i - 1] - ps.cm_x[i];
			a[1] = ps.cm_y[i - 1] - ps.cm_y[i];
			a[2] = ps.cm_z[i - 1] - ps.cm_z[i];
		}
		b[0] = ps.cm_x[i + 1] - ps.cm_x[i];
		b[1] = ps.cm_y[i + 1] - ps.cm_y[i];
		b[2] = ps.cm_z[i + 1] - ps.cm_z[i];
		dif = MAGNITUDE(b[0], b[1], b[2]);

		// bond term
		e = exp(-alpha_ * (dif / r0 - 1));
		ubond += ((1.0 - e) * (1.0 - e)) - 1.0;

		// torsion
		cosGamma = DOT_PRODUCT(ps.n_x[i], ps.n_y[i], ps.n_z[i], ps.n_x[i + 1], ps.n_y[i + 1], ps.n_z[i + 1]);
		if (cosGamma > 1.0)
			cosGamma = 1.0;
		if (cosGamma < -1.0)
			cosGamma = -1.0;

		sinGamma = sqrt(1.0 - cosGamma * cosGamma);
		sinGamma0 = sqrt(1.0 - cosGamma0 * cosGamma0);
		cosDeltaGamma = cosGamma * cosGamma0 + sinGamma * sinGamma0;
		cos2DeltaGamma = 2.0 * cosDeltaGamma * cosDeltaGamma - 1.0;
		utor += (k1 * (1.0 - cosDeltaGamma) + k2 * (1.0 - cos2DeltaGamma));

		// coulomb
		r = ANG_TO_BOHR(dif);
		ucoulomb += (qsrd * (erfc(intra_kappa * r) / r));

		// rotation
		rij[0] = b[0];
		rij[1] = b[1];
		rij[2] = b[2];
		normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);

		rji[0] = -rij[0];
		rji[1] = -rij[1];
		rji[2] = -rij[2];

		rij_dot = DOT_PRODUCT(ps.dpm_x[i], ps.dpm_y[i], ps.dpm_z[i], rij[0], rij[1], rij[2]);
		dei1 = rij_dot - cos_theta0;

		rji_dot = DOT_PRODUCT(ps.dpm_x[i + 1], ps.dpm_y[i + 1], ps.dpm_z[i + 1], rji[0], rji[1], rji[2]);
		dei2 = rji_dot - cos_theta0;

		urot += ((dei1 * dei1) + (dei2 * dei2));

		// bend term
		normalize(b[0], b[1], b[2], b[0], b[1], b[2]);
		normalize(a[0], a[1], a[2], a[0], a[1], a[2]);
		cangle = cos(angleBetween2Vectors(a[0], a[1], a[2], b[0], b[1], b[2]));

		if (i > lb)
			ubend += ((cangle - theta0) * (cangle - theta0));

	}
	ubond *= De;
	ubend *= k_theta;
	bond = EV_TO_HA(ubond);
	bend = EV_TO_HA(ubend);
	torsion = EV_TO_HA(utor);
	coulomb = ucoulomb;
	DATA_TYPE temp = urot * k4;
	rotation = EV_TO_HA(temp);

}

DATA_TYPE ModelPotential::UbondPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE ubond = 0.0;
	DATA_TYPE dif = 0.0;
	DATA_TYPE e = 0.0;

	for (int i = lb; i < ub - 1; i++)
	{
		dif = dist(ps.cm_x[i], ps.cm_y[i], ps.cm_z[i], ps.cm_x[i + 1], ps.cm_y[i + 1], ps.cm_z[i + 1]);
		e = exp(-alpha_ * (dif / r0 - 1));
		ubond += ((1.0 - e) * (1.0 - e)) - 1.0;
	}
	ubond *= De;

	return EV_TO_HA(ubond);
}

DATA_TYPE ModelPotential::UbendPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE ubend = 0.0;
	DATA_TYPE theta0 = cos(DEG_TO_RAD(theta_0));
	DATA_TYPE cangle = 0.0;
	DATA_TYPE a[3];
	DATA_TYPE b[3];

	for (int i = lb + 1; i < ub - 1; i++)
	{
		a[0] = ps.cm_x[i - 1] - ps.cm_x[i];
		a[1] = ps.cm_y[i - 1] - ps.cm_y[i];
		a[2] = ps.cm_z[i - 1] - ps.cm_z[i];
		normalize(a[0], a[1], a[2], a[0], a[1], a[2]);

		b[0] = ps.cm_x[i + 1] - ps.cm_x[i];
		b[1] = ps.cm_y[i + 1] - ps.cm_y[i];
		b[2] = ps.cm_z[i + 1] - ps.cm_z[i];
		normalize(b[0], b[1], b[2], b[0], b[1], b[2]);

		cangle = cos(angleBetween2Vectors(a[0], a[1], a[2], b[0], b[1], b[2]));

		ubend += ((cangle - theta0) * (cangle - theta0));
	}

	ubend *= k_theta;

	return EV_TO_HA(ubend);
}

DATA_TYPE ModelPotential::UtorsionPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE utor = 0.0;
	DATA_TYPE gamma_0_rad = DEG_TO_RAD(gamma_0);
	DATA_TYPE cos_gamma_0 = cos(gamma_0_rad);

	DATA_TYPE cosGamma = 0.0, cosGamma0 = cos_gamma_0, sinGamma = 0.0, sinGamma0 = sin(gamma_0_rad), cosDeltaGamma = 0.0, cos2DeltaGamma = 0.0;

	for (int i = lb; i < ub - 1; i++)
	{
		cosGamma = DOT_PRODUCT(ps.n_x[i], ps.n_y[i], ps.n_z[i], ps.n_x[i + 1], ps.n_y[i + 1], ps.n_z[i + 1]);
		if (cosGamma > 1.0)
			cosGamma = 1.0;
		if (cosGamma < -1.0)
			cosGamma = -1.0;

		sinGamma = sqrt(1.0 - cosGamma * cosGamma);
		sinGamma0 = sqrt(1.0 - cosGamma0 * cosGamma0);
		cosDeltaGamma = cosGamma * cosGamma0 + sinGamma * sinGamma0;
		cos2DeltaGamma = 2.0 * cosDeltaGamma * cosDeltaGamma - 1.0;
		utor += (k1 * (1.0 - cosDeltaGamma) + k2 * (1.0 - cos2DeltaGamma));

		//gamma = angleBetween2Vectors(ps.n_x[i], ps.n_y[i], ps.n_z[i], ps.n_x[i + 1], ps.n_y[i + 1], ps.n_z[i + 1]);
		//utor += (K_1 * (1.0 - cos(gamma - gamma_0_rad)) + K_2 * (1.0 - cos(2.0 * (gamma - gamma_0_rad))));
	}

	return EV_TO_HA(utor);
}

DATA_TYPE ModelPotential::UcoulombPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;
	int offset = intra_coul_offset;

	DATA_TYPE qsrd = Q_mono * Q_mono;
	DATA_TYPE ucoulomb = 0.0;
	DATA_TYPE r = 0.0;
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(intra_kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
#endif
	int i = 0;

	for (i = lb; i < ub - offset; i++)
	{
		for (int j = i + offset; j < ub; j++)
		{
			r = ANG_TO_BOHR(dist(ps.cm_x[i], ps.cm_y[i], ps.cm_z[i], ps.cm_x[j], ps.cm_y[j], ps.cm_z[j]));
			ucoulomb += qsrd * ((erfc(intra_kappa * r) / r) - dampingFactor);
		}
	}
	return ucoulomb;
}

DATA_TYPE ModelPotential::UdipoleDipolePart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;
	int i = 0, j = 0;

	DATA_TYPE udd = 0.0, dpm_dot = 0.0, mag = 0.0;
	DATA_TYPE rij[3];
	DATA_TYPE mu_i = 0.0, mu_j = 0.0;
	DATA_TYPE mu_sqrd = DEBEYE_TO_HA(dp_mag) * DEBEYE_TO_HA(dp_mag);

	for (i = lb; i < ub - 1; i++)
	{
		for (j = i + 1; j < ub; j++)
		{
			rij[0] = ANG_TO_BOHR((ps.cm_x[j] - ps.cm_x[i]));
			rij[1] = ANG_TO_BOHR((ps.cm_y[j] - ps.cm_y[i]));
			rij[2] = ANG_TO_BOHR((ps.cm_z[j] - ps.cm_z[i]));

			mag = MAGNITUDE(rij[0], rij[1], rij[2]);
			normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);
			dpm_dot = dotProduct(ps.dpm_x[i], ps.dpm_y[i], ps.dpm_z[i], ps.dpm_x[j], ps.dpm_y[j], ps.dpm_z[j]);
			mu_i = dotProduct(ps.dpm_x[i], ps.dpm_y[i], ps.dpm_z[i], rij[0], rij[1], rij[2]);
			mu_j = dotProduct(ps.dpm_x[j], ps.dpm_y[j], ps.dpm_z[j], rij[0], rij[1], rij[2]);

			udd += (mu_sqrd * ((dpm_dot - (3.0 * mu_i * mu_j) / (mag * mag)) / (mag * mag * mag)));
		}
	}

	return udd;
}

DATA_TYPE ModelPotential::UantiCoilingPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;
	int i = 0, j = 0;

	DATA_TYPE uac = 0.0;
	DATA_TYPE r = 0.0, r_6 = 0.0, r_9 = 0.0;
	DATA_TYPE sigma_6 = sigma_intra * sigma_intra * sigma_intra * sigma_intra * sigma_intra * sigma_intra;
	DATA_TYPE sigma_9 = sigma_6 * sigma_intra * sigma_intra * sigma_intra;

	for (i = lb; i < ub - 2; i++)
	{
		for (j = i + 2; j < ub; j++)
		{
			r = dist(ps.cm_x[i], ps.cm_y[i], ps.cm_z[i], ps.cm_x[j], ps.cm_y[j], ps.cm_z[j]);
			r_6 = r * r * r * r * r * r;
			r_9 = r_6 * r * r * r;

			uac += (sigma_9 / r_9 - 1.5 * sigma_6 / r_6);
		}
	}

	uac = 2 * epsilon_intra * uac;
	return EV_TO_HA(uac);
}

DATA_TYPE ModelPotential::URotationPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE urot = 0.0;
	DATA_TYPE rij[3], rji[3], rij_dot = 0.0, rji_dot = 0.0;
	DATA_TYPE cos_theta0 = cos(DEG_TO_RAD(theta_0 / 2.0));
	DATA_TYPE dei1 = 0.0, dei2 = 0.0;

	for (int i = lb; i < ub - 1; i++)
	{
		rij[0] = ps.cm_x[i + 1] - ps.cm_x[i];
		rij[1] = ps.cm_y[i + 1] - ps.cm_y[i];
		rij[2] = ps.cm_z[i + 1] - ps.cm_z[i];
		normalize(rij[0], rij[1], rij[2], rij[0], rij[1], rij[2]);

		rji[0] = -rij[0];
		rji[1] = -rij[1];
		rji[2] = -rij[2];

		rij_dot = DOT_PRODUCT(ps.dpm_x[i], ps.dpm_y[i], ps.dpm_z[i], rij[0], rij[1], rij[2]);
		dei1 = rij_dot - cos_theta0;

		rji_dot = DOT_PRODUCT(ps.dpm_x[i + 1], ps.dpm_y[i + 1], ps.dpm_z[i + 1], rji[0], rji[1], rji[2]);
		dei2 = rji_dot - cos_theta0;

		urot += ((dei1 * dei1) + (dei2 * dei2));
	}
	DATA_TYPE temp = urot * k4;
	return EV_TO_HA(temp);
}

DATA_TYPE ModelPotential::URestorationPart(PARTICLE_SYSTEM& ps, int midx)
{
	int lb = (midx / monomers_per_chain) * monomers_per_chain;
	int ub = lb + monomers_per_chain;

	DATA_TYPE urest = 0.0;
	DATA_TYPE z = 0.0, temp = 0.0;
	int polyIdx = midx / monomers_per_chain;
	DATA_TYPE z0 = ps.z0[polyIdx];

	z0 = ANG_TO_BOHR(z0);
	
	DATA_TYPE avg_z = 0.0;

	// calculate the median Z value of the chain
	for (int i = lb; i < ub; i++)
	{
		avg_z += ps.cm_z[i];
	}

	avg_z /= monomers_per_chain;

	z0 = ANG_TO_BOHR(avg_z);

	// do not lef the monomer move too far from the average Z plane
	for (int i = lb; i < ub; i++)
	{
		z = ANG_TO_BOHR(ps.cm_z[i]);
		temp = z - z0;

		if (i == lb || i == ub - 1)
			urest += (k5_end * (temp * temp));
		else
			urest += (k5 * temp * temp);
	}
	return urest;
}

DATA_TYPE ModelPotential::PyPyPart(PARTICLE_SYSTEM& ps, int midx, int mlb, int mub, DATA_TYPE& virialPressure, DATA_TYPE& coul, DATA_TYPE& exc, DATA_TYPE& dd, bool removeDouble)
{
	int test_chain_id = 0;
	int input_chain_id = midx / monomers_per_chain; 
	int exc_lb = input_chain_id * monomers_per_chain;
	int exc_ub = exc_lb + monomers_per_chain - 1;
	int t_lb = 0;
	int t_ub = 0;
	bool isInputHeadTail = false, isTestHeadTail = false;
	if (midx == exc_lb || midx == exc_ub)
		isInputHeadTail = true; 

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE ucoul = 0.0, uexc = 0.0, udd = 0.0, utot = 0.0;
	DATA_TYPE qsqrd = Q_mono * Q_mono;
	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE sigma_6_mid = TO_POW_6(sigma_inter);
	DATA_TYPE sigma_9_mid = sigma_6_mid * sigma_inter * sigma_inter * sigma_inter;
	DATA_TYPE sigma_6_end = TO_POW_6(sigma_inter_end * 1.0);
	DATA_TYPE sigma_9_end = sigma_6_end * ((sigma_inter_end * sigma_inter_end * sigma_inter_end) * 1.0);
	DATA_TYPE sigma_6 = 0.0, sigma_9 = 0.0;
	DATA_TYPE epsilon = epsilon_inter;
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(dp_mag) * DEBEYE_TO_HA(dp_mag);
	int i = 0;
	DATA_TYPE vPressure = 0.0, rij3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;

	DATA_TYPE* _cm_x = ps.cm_x;
	DATA_TYPE* _cm_y = ps.cm_y;
	DATA_TYPE* _cm_z = ps.cm_z;

	DATA_TYPE* _dpm_x = ps.dpm_x;
	DATA_TYPE* _dpm_y = ps.dpm_y;
	DATA_TYPE* _dpm_z = ps.dpm_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;
	int _nMonomers = ps.nMonomers;

	DATA_TYPE kappa_ = kappa;

	ox = _cm_x[midx];
	oy = _cm_y[midx];
	oz = _cm_z[midx];

	dpmx = _dpm_x[midx];
	dpmy = _dpm_y[midx];
	dpmz = _dpm_z[midx];

	#pragma omp parallel for reduction(+:ucoul, uexc, udd, vPressure) private(i, t_lb, t_ub, test_chain_id, isTestHeadTail, sigma_6, sigma_9, epsilon, xdif, ydif, zdif, rij, dij, dir, djr, rij3)
	for (i = mlb; i < mub; i++)
	{
		isTestHeadTail = false;

		if (i == midx)
			continue;

		if (removeDouble && (i < midx)) // do not DATA_TYPE count end monomer interactions
			continue;

		test_chain_id = i / monomers_per_chain;

		if (input_chain_id == test_chain_id)
			continue;

		xdif = PCB_(_x_size, (_cm_x[i] - ox));
		ydif = PCB_(_y_size, (_cm_y[i] - oy));
		zdif = PCB_(_z_size, (_cm_z[i] - oz));

		xdif = ANG_TO_BOHR(xdif);
		ydif = ANG_TO_BOHR(ydif);
		zdif = ANG_TO_BOHR(zdif);

		rij = MAGNITUDE(xdif, ydif, zdif);
		if (rij > ANG_TO_BOHR(_r_cut))
		{
			continue;
		}

		t_lb = (i / monomers_per_chain) * monomers_per_chain;
		t_ub = t_lb + monomers_per_chain - 1;
		if (i == t_lb || i == t_ub)
	            isTestHeadTail = true; 
	        
        if (isInputHeadTail && isTestHeadTail)
		{
			sigma_6 = sigma_6_end;
			sigma_9 = sigma_9_end;
			epsilon = epsilon_inter_end;
			kappa_ = kappa_inter_end;
		}
		else
		{
			sigma_6 = sigma_6_mid;
			sigma_9 = sigma_9_mid;
			epsilon = epsilon_inter;
			kappa_ = kappa;
		}

#if defined __USE_PARTIAL_WOLF__
		dampingFactor = erfc(kappa_ * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
#endif

		rij3 = TO_POW_3(rij);

		ucoul += (qsqrd * (erfc(kappa_ * rij) / rij - dampingFactor));

		uexc += 2.0 * epsilon * ((sigma_9 / (rij3 * rij3 * rij3) - 1.5 * (sigma_6 / (rij3 * rij3))));
		dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
		dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
		djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

		udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
	}


	utot = ucoul + uexc + udd;
	coul = ucoul;
	exc = uexc;
	dd = udd;

	return utot;
}

DATA_TYPE ModelPotential::PyDopPartMonMove(PARTICLE_SYSTEM& ps, int midx, int dlb, int dub, DATA_TYPE& virialPressure, DATA_TYPE& coul, DATA_TYPE& cd, DATA_TYPE& disp)
{
	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(Q_dop) * DEBEYE_TO_HA(dp_mag) * C;
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(sigmaPyDopant);
	DATA_TYPE sigma_9 = sigma_6 * sigmaPyDopant * sigmaPyDopant * sigmaPyDopant;
	DATA_TYPE rdist = 0.0;
	DATA_TYPE Q = Q_dop * Q_mono;
	DATA_TYPE vPressure = 0.0;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
#endif

	DATA_TYPE* _dop_x = ps.dop_x;
	DATA_TYPE* _dop_y = ps.dop_y;
	DATA_TYPE* _dop_z = ps.dop_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;

	int d_lb = dlb, d_ub = dub, dIdx = 0;

	cmx = ps.cm_x[midx];
	cmy = ps.cm_y[midx];
	cmz = ps.cm_z[midx];

	dpmx = ps.dpm_x[midx];
	dpmy = ps.dpm_y[midx];
	dpmz = ps.dpm_z[midx];

#pragma omp parallel for reduction(+:ucoul, ucd, udisp, vPressure) private(dIdx, r_vec, ris, ris3)
	for (dIdx = d_lb; dIdx < d_ub; dIdx++)
	{
		r_vec[0] = PCB_(_x_size, (cmx - _dop_x[dIdx]));
		r_vec[1] = PCB_(_y_size, (cmy - _dop_y[dIdx]));
		r_vec[2] = PCB_(_z_size, (cmz - _dop_z[dIdx]));

		r_vec[0] = ANG_TO_BOHR(r_vec[0]);
		r_vec[1] = ANG_TO_BOHR(r_vec[1]);
		r_vec[2] = ANG_TO_BOHR(r_vec[2]);

		ris = sqrt(r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2]);
		if (ris > ANG_TO_BOHR(_r_cut))
		{
			continue;
		}
		ris3 = TO_POW_3(ris);

		ucoul += Q * ((erfc(kappa * ris) / ris) - dampingFactor);

#if defined __OLD_CD_TERM__
		ucd += c_const * DOT_PRODUCT(r_vec[0] / ris, r_vec[1] / ris, r_vec[2] / ris, dpmx, dpmy, dpmz) / (ris3);
#else
		ucd += c_const * DOT_PRODUCT(r_vec[0] / ris, r_vec[1] / ris, r_vec[2] / ris, dpmx, dpmy, dpmz) * (erfc(kappa * ris) / (ris * ris) - erfc(kappa * ANG_TO_BOHR(_r_cut)) / (ANG_TO_BOHR(_r_cut) * ANG_TO_BOHR(_r_cut)));
#endif

		udisp += (A * (sigma_9 / (ris3 * ris3 * ris3)) - B * (sigma_6 / (ris3 * ris3)));
	}

	virialPressure += 0.0;
	coul = ucoul;
	cd =  ucd;
	disp = udisp;

	return (coul + cd + udisp);
}

DATA_TYPE ModelPotential::PyDopPartDopMove(PARTICLE_SYSTEM& ps, int didx, int mlb, int mub, DATA_TYPE& virialPressure)
{
	DATA_TYPE ucoul = 0.0, ucd = 0.0, udisp = 0.0;
	DATA_TYPE cmx = 0.0, cmy = 0.0, cmz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE ris = 0.0;
	DATA_TYPE c_const = fabs(Q_dop) * DEBEYE_TO_HA(dp_mag) * C;
	DATA_TYPE r_vec[3];
	DATA_TYPE sigma_6 = TO_POW_6(sigmaPyDopant);
	DATA_TYPE sigma_9 = sigma_6 * sigmaPyDopant * sigmaPyDopant * sigmaPyDopant;
	DATA_TYPE Q = Q_dop * Q_mono;
	DATA_TYPE vPressure = 0.0;
	DATA_TYPE ris3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = Q * erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
#endif

	DATA_TYPE* _cm_x = ps.cm_x;
	DATA_TYPE* _cm_y = ps.cm_y;
	DATA_TYPE* _cm_z = ps.cm_z;

	DATA_TYPE* _dpm_x = ps.dpm_x;
	DATA_TYPE* _dpm_y = ps.dpm_y;
	DATA_TYPE* _dpm_z = ps.dpm_z;

	DATA_TYPE* _dop_x = ps.dop_x;
	DATA_TYPE* _dop_y = ps.dop_y;
	DATA_TYPE* _dop_z = ps.dop_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;

	int m_lb = mlb, m_ub = mub;
	int mIdx = 0;

#pragma omp parallel for reduction(+:ucoul, ucd, udisp, vPressure) private(mIdx, cmx, cmy, cmz, dpmx, dpmy, dpmz, r_vec, ris, ris3)
	for (mIdx = m_lb; mIdx < m_ub; mIdx++)
	{
		cmx = _cm_x[mIdx];
		cmy = _cm_y[mIdx];
		cmz = _cm_z[mIdx];

		dpmx = _dpm_x[mIdx];
		dpmy = _dpm_y[mIdx];
		dpmz = _dpm_z[mIdx];

		r_vec[0] = PCB_(_x_size, (cmx - _dop_x[didx]));
		r_vec[1] = PCB_(_y_size, (cmy - _dop_y[didx]));
		r_vec[2] = PCB_(_z_size, (cmz - _dop_z[didx]));

		r_vec[0] = ANG_TO_BOHR(r_vec[0]);
		r_vec[1] = ANG_TO_BOHR(r_vec[1]);
		r_vec[2] = ANG_TO_BOHR(r_vec[2]);

		ris = sqrt(r_vec[0] * r_vec[0] + r_vec[1] * r_vec[1] + r_vec[2] * r_vec[2]);
		if (ris < ANG_TO_BOHR(_r_cut))
		{
			ris3 = TO_POW_3(ris);

			ucoul += Q * ((erfc(kappa * ris) / ris) - dampingFactor);

#if defined __OLD_CD_TERM__
			ucd += c_const * DOT_PRODUCT(r_vec[0] / ris, r_vec[1] / ris, r_vec[2] / ris, dpmx, dpmy, dpmz) / ris3;
#else
			ucd += c_const * DOT_PRODUCT(r_vec[0] / ris, r_vec[1] / ris, r_vec[2] / ris, dpmx, dpmy, dpmz) * (erfc(kappa * ris) / (ris * ris) - erfc(kappa * ANG_TO_BOHR(_r_cut)) / (ANG_TO_BOHR(_r_cut) * ANG_TO_BOHR(_r_cut)));
#endif
			udisp += (A * (sigma_9 / (ris3 * ris3 * ris3)) - B * (sigma_6 / (ris3 * ris3)));
		}
	}

	virialPressure += 0;
	return (ucoul + ucd + udisp);
}

DATA_TYPE ModelPotential::DopDopPart(PARTICLE_SYSTEM& ps, int didx, int dlb, int dub, DATA_TYPE& virialPressure, DATA_TYPE& coul, DATA_TYPE& other)
{
	DATA_TYPE sigma_dop = ANG_TO_BOHR(sigma_dopant);
	DATA_TYPE udopdop = 0.0, sigma_9 = TO_POW_6(sigma_dop) * TO_POW_3(sigma_dop);
	DATA_TYPE q_sqrd = Q_dop * Q_dop;
	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0, rss = 0.0;
	DATA_TYPE dx = 0.0, dy = 0.0, dz = 0.0;
	DATA_TYPE ucoul = 0.0, uother = 0.0, rss3 = 0.0;
	int j = 0;
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(kappa * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
#endif
	DATA_TYPE* _dop_x = ps.dop_x;
	DATA_TYPE* _dop_y = ps.dop_y;
	DATA_TYPE* _dop_z = ps.dop_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;

	int _nDopants = ps.nDopants;

	dx = _dop_x[didx];
	dy = _dop_y[didx];
	dz = _dop_z[didx];

#pragma omp parallel for reduction(+:ucoul,uother) private(j, xdif, ydif, zdif, rss, rss3)
	for (j = dlb; j < dub; j++)
	{
		if (j == didx)
			continue;

		xdif = PCB_(_x_size, (_dop_x[j] - dx));
		ydif = PCB_(_y_size, (_dop_y[j] - dy));
		zdif = PCB_(_z_size, (_dop_z[j] - dz));

		rss = MAGNITUDE(xdif, ydif, zdif);
		if (rss < _r_cut)
		{

			rss = ANG_TO_BOHR(rss);
			rss3 = TO_POW_3(rss);
			ucoul += (q_sqrd * ((erfc(kappa * rss) / rss) - dampingFactor));
			uother += (epsilon_dopant * (sigma_9 / (rss3 * rss3 * rss3)));
		}
	}

	coul = ucoul;
	other = uother;
	return (coul + other);
}

//DATA_TYPE ModelPotential::VolumeChangeEnergy(PARTICLE_SYSTEM& ps, int mlb, int mub, int dlb, int dub, int nodeId, DATA_TYPE& virialPressure)
//{
//	DATA_TYPE upypy = 0.0, upydop = 0.0, udopdop = 0.0, ucoul = 0.0, uother = 0.0;
//
//	DATA_TYPE inter = TotalInter(ps, mlb, mub, dlb, dub, virialPressure, upypy, upydop, udopdop, ucoul, uother);
//
//	DATA_TYPE intra_ucoul = 0.0;
//	if (nodeId == 0)
//	{
//		int midx = 0;
//		#pragma omp parallel for reduction(+:ucoul) private(midx)
//		for (midx = 0; midx < ps.nMonomers; midx = midx + monomers_per_chain)
//		{
//			intra_ucoul += UcoulombPart(ps, midx);
//		}
//	}
//	return (inter + intra_ucoul);
//}

DATA_TYPE ModelPotential::PyPyPartImageInteraction(PARTICLE_SYSTEM& ps, int midx, int mlb, int mub, DATA_TYPE& virialPressure, DATA_TYPE& coul, DATA_TYPE& exc, DATA_TYPE& dd)
{
	if (!self_interaction)
		return 0.0;
	// only calculate the interaction if it is within your work assignment
	if (midx < mlb || midx >= mub)
		return 0.0;

	int exc_lb = (midx / monomers_per_chain) * monomers_per_chain;
	int exc_ub = exc_lb + monomers_per_chain;
	int t_lb = 0;
	int t_ub = 0;
	DATA_TYPE kappa_ = kappa;

	DATA_TYPE xdif = 0.0, ydif = 0.0, zdif = 0.0;
	DATA_TYPE ucoul = 0.0, uexc = 0.0, udd = 0.0, utot = 0.0;
	DATA_TYPE qsqrd = Q_mono * Q_mono;
	DATA_TYPE ox = 0.0, oy = 0.0, oz = 0.0;
	DATA_TYPE dpmx = 0.0, dpmy = 0.0, dpmz = 0.0;
	DATA_TYPE rij = 0.0, dij = 0.0, dir = 0.0, djr = 0.0;
	DATA_TYPE sigma_6_mid = TO_POW_6(sigma_inter);
	DATA_TYPE sigma_9_mid = sigma_6_mid * sigma_inter * sigma_inter * sigma_inter;
	DATA_TYPE sigma_6_end = TO_POW_6(sigma_inter_end);
	DATA_TYPE sigma_9_end = sigma_6_end * sigma_inter_end * sigma_inter_end * sigma_inter_end;
	DATA_TYPE sigma_6 = 0.0, sigma_9 = 0.0;
	DATA_TYPE epsilon = epsilon_inter;
	DATA_TYPE dp_mag_sqrd = DEBEYE_TO_HA(dp_mag) * DEBEYE_TO_HA(dp_mag);
	int i = 0;
	int tlb = 0, tub = 0;
	DATA_TYPE vPressure = 0.0, rij3 = 0.0;
	DATA_TYPE dampingFactor = 0.0;
#if defined __USE_PARTIAL_WOLF__
	dampingFactor = erfc(kappa_ * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
#endif

	DATA_TYPE* _cm_x = ps.cm_x;
	DATA_TYPE* _cm_y = ps.cm_y;
	DATA_TYPE* _cm_z = ps.cm_z;

	DATA_TYPE* _dpm_x = ps.dpm_x;
	DATA_TYPE* _dpm_y = ps.dpm_y;
	DATA_TYPE* _dpm_z = ps.dpm_z;

	DATA_TYPE _x_size = ps.x_size;
	DATA_TYPE _y_size = ps.y_size;
	DATA_TYPE _z_size = ps.z_size;
	DATA_TYPE _r_cut = ps.r_cut;
	int _nMonomers = ps.nMonomers;

	ox = _cm_x[midx];
	oy = _cm_y[midx];
	oz = _cm_z[midx];

	bool srcInsideBox = true;
	bool destInsideBox = true;
	if (fabs(ox) > ps.x_size / 2.0 ||
		fabs(oy) > ps.y_size / 2.0 ||
		fabs(oz) > ps.z_size / 2.0)
	{
		srcInsideBox = false;

		if (ox < -_x_size * 0.5) ox = ox + _x_size;
		if (ox >= _x_size * 0.5) ox = ox - _x_size;

		if (oy < -_y_size * 0.5) oy = oy + _y_size;
		if (oy >= _y_size * 0.5) oy = oy - _y_size;

		if (oz < -_z_size * 0.5) oz = oz + _z_size;
		if (oz >= _z_size * 0.5) oz = oz - _z_size;
	}

	dpmx = _dpm_x[midx];
	dpmy = _dpm_y[midx];
	dpmz = _dpm_z[midx];

	for (i = exc_lb; i < exc_ub; i++)
	{
		if (i == midx)
			continue;

		if ((midx == exc_lb || midx == exc_ub - 1) &&
			(i == exc_lb || i == exc_ub))
		{
			sigma_6 = sigma_6_end;
			sigma_9 = sigma_9_end;
			epsilon = epsilon_inter_end;
			kappa_ = kappa_inter_end;
		}
		else
		{
			sigma_6 = sigma_6_mid;
			sigma_9 = sigma_9_mid;
			epsilon = epsilon_inter;
			kappa_ = kappa;
		}

		if (fabs(_cm_x[i]) > ps.x_size / 2.0 ||
			fabs(_cm_y[i]) > ps.y_size / 2.0 ||
			fabs(_cm_z[i]) > ps.z_size / 2.0)
		{
			destInsideBox = false;
		}
		else
		{
			destInsideBox = true;
		}

		if (srcInsideBox && destInsideBox)
			continue;

#if defined __USE_PARTIAL_WOLF__
		dampingFactor = erfc(kappa_ * ANG_TO_BOHR(ps.r_cut)) / ANG_TO_BOHR(ps.r_cut);
#endif

		if (srcInsideBox && !destInsideBox)
		{
			DATA_TYPE tx = _cm_x[i];
			if (tx < -_x_size * 0.5) tx = tx + _x_size;
			if (tx >= _x_size * 0.5) tx = tx - _x_size;

			DATA_TYPE ty = _cm_y[i];
			if (ty < -_y_size * 0.5) ty = ty + _y_size;
			if (ty >= _y_size * 0.5) ty = ty - _y_size;

			DATA_TYPE tz = _cm_z[i];
			if (tz < -_z_size * 0.5) tz = tz + _z_size;
			if (tz >= _z_size * 0.5) tz = tz - _z_size;

			xdif = tx - ox;
			ydif = ty - oy;
			zdif = tz - oz;

			xdif = ANG_TO_BOHR(xdif);
			ydif = ANG_TO_BOHR(ydif);
			zdif = ANG_TO_BOHR(zdif);

			rij = MAGNITUDE(xdif, ydif, zdif);
			DATA_TYPE tcut = ANG_TO_BOHR(_r_cut);
			if (rij > tcut)
			{
				continue;
			}

			rij3 = TO_POW_3(rij);

			ucoul += (qsqrd * (erfc(kappa_ * rij) / rij - dampingFactor));

			uexc += 2.0 * epsilon * ((sigma_9 / (rij3 * rij3 * rij3) - 1.5 * (sigma_6 / (rij3 * rij3))));
			dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
			dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
			djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

			udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
		}
		else if (!srcInsideBox && destInsideBox)
		{
			DATA_TYPE tx = _cm_x[i];
			if (tx < -_x_size * 0.5) tx = tx + _x_size;
			if (tx >= _x_size * 0.5) tx = tx - _x_size;

			DATA_TYPE ty = _cm_y[i];
			if (ty < -_y_size * 0.5) ty = ty + _y_size;
			if (ty >= _y_size * 0.5) ty = ty - _y_size;

			DATA_TYPE tz = _cm_z[i];
			if (tz < -_z_size * 0.5) tz = tz + _z_size;
			if (tz >= _z_size * 0.5) tz = tz - _z_size;

			xdif = tx - ox;
			ydif = ty - oy;
			zdif = tz - oz;

			xdif = ANG_TO_BOHR(xdif);
			ydif = ANG_TO_BOHR(ydif);
			zdif = ANG_TO_BOHR(zdif);

			rij = MAGNITUDE(xdif, ydif, zdif);

			if (rij > ANG_TO_BOHR(_r_cut))
			{
				continue;
			}

			rij3 = TO_POW_3(rij);

			ucoul += (qsqrd * (erfc(kappa_ * rij) / rij - dampingFactor));

			uexc += 2.0 * epsilon * ((sigma_9 / (rij3 * rij3 * rij3) - 1.5 * (sigma_6 / (rij3 * rij3))));
			dij = DOT_PRODUCT(dpmx, dpmy, dpmz, _dpm_x[i], _dpm_y[i], _dpm_z[i]);
			dir = DOT_PRODUCT(dpmx, dpmy, dpmz, xdif / rij, ydif / rij, zdif / rij);
			djr = DOT_PRODUCT(_dpm_x[i], _dpm_y[i], _dpm_z[i], xdif / rij, ydif / rij, zdif / rij);

			udd += (dp_mag_sqrd * ((dij - (3.0 * dir * djr) / (rij * rij))) / (rij3));
		}
	}


	utot = ucoul + uexc + udd;
	coul = ucoul;
	exc = uexc;
	dd = udd;

	return utot;
}
