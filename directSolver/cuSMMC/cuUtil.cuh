#if !defined (__CU_UTIL_CUH__)
#define       __CU_UTIL_CUH__

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include "util.h"


// utility functions for memory allocation and memory copying
template<typename T>
void freeMemory(T * mem)
{
	cudaFree(mem);
}

template<typename T>
cudaError_t copyToDevice(const T * src, T * dst, size_t size)
{
	size_t numbytes = size * sizeof(T);
	// Copy input vectors from host memory to GPU buffers.
	return cudaMemcpy(dst, src, numbytes, cudaMemcpyHostToDevice);
}

template<typename T>
cudaError_t copyFromDevice(const T * src, T * dst, size_t size)
{
	// Copy output vector from GPU buffer to host memory.
	return cudaMemcpy(dst, src, size * sizeof(T), cudaMemcpyDeviceToHost);
}

template<typename T>
cudaError_t zeroMemory(T * data, size_t size)
{
	return cudaMemset(data, '0', size * sizeof(T));
}

template<typename T>
cudaError_t allocateMemoryOnDevice(T ** data, size_t size)
{
	cudaError_t cudaStatus;

	cudaStatus = cudaMalloc((void**)data, size * sizeof(T));
	if (cudaStatus != cudaSuccess)
	{
		return cudaStatus;
	}

	cudaStatus = cudaMemset(*data, '0', size * sizeof(T));
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaMemcpy failed!");
	}

	return cudaStatus;
}

template<typename T>
cudaError_t allocateMemoryOnHost(T* data, size_t size, unsigned int flags)
{
	return cudaMallocHost(data, size * sizeof(T), cudaHostAllocWriteCombined | cudaHostAllocMapped);
}

inline bool initialize(unsigned short devId)
{
	bool retVal = true;
	cudaError_t cudaStatus;

	// Choose which GPU to run on, change this on a multi-GPU system.
	cudaStatus = cudaSetDevice(devId);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
		retVal = false;
	}

	return retVal;
}

inline int getDeviceId()
{
	int dev_id = -1;
	cudaError_t cudaStatus = cudaGetDevice(&dev_id);
	if (cudaStatus != cudaSuccess)
	{
		fprintf(stderr, "cudaGetDevice failed!  Device id could not be determined?");
	}
	return dev_id;
}
#endif