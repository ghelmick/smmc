#if !defined (__MODEL_POTENTIAL_H__)
#define       __MODEL_POTENTIAL_H__

#include "polymerUtil.h"
#include <fstream>

using std::ofstream;
struct AppConfig;

class ModelPotential
{
public:
	ModelPotential(const AppConfig & appConfig);

	// partial energy functions
	DATA_TYPE UbondPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE UbendPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE UtorsionPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE UdipoleDipolePart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE UcoulombPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE UantiCoilingPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE URotationPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE URestorationPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE PyPyPart(PARTICLE_SYSTEM & ps, int midx, int mlb, int mub, DATA_TYPE & virialPressure, DATA_TYPE & coul, DATA_TYPE & exc, DATA_TYPE & dd, bool removeDouble = false);
	DATA_TYPE PyDopPartMonMove(PARTICLE_SYSTEM & ps, int midx, int dlb, int dub, DATA_TYPE & virialPressure, DATA_TYPE & coul, DATA_TYPE & cd, DATA_TYPE & disp);
	DATA_TYPE PyDopPartDopMove(PARTICLE_SYSTEM & ps, int didx, int mlb, int mub, DATA_TYPE & virialPressure);
	DATA_TYPE PyPyPartImageInteraction(PARTICLE_SYSTEM& ps, int midx, int mlb, int mub, DATA_TYPE& virialPressure, DATA_TYPE& coul, DATA_TYPE& exc, DATA_TYPE& dd);

	DATA_TYPE DopDopPart(PARTICLE_SYSTEM & ps, int didx, int dlb, int dub, DATA_TYPE & virialPressure, DATA_TYPE & coul, DATA_TYPE & other);
	DATA_TYPE TotalIntra(PARTICLE_SYSTEM & ps, ofstream & logFile, bool logResults = false);
	DATA_TYPE TotalInter(PARTICLE_SYSTEM & ps, int mlb, int mub, int dlb, int dub, DATA_TYPE & virialPressure, DATA_TYPE & upypy, DATA_TYPE & upydop, DATA_TYPE & udopdop, DATA_TYPE & ucoul, DATA_TYPE & uother, DATA_TYPE& uself_fact);
	DATA_TYPE TotalIntraPart(PARTICLE_SYSTEM & ps, int midx);
	DATA_TYPE TotalInterPartMonomer(PARTICLE_SYSTEM & ps, int midx, int didx, int mlb, int mub, int dlb, int dub);
	DATA_TYPE TotalInterPartDopant(PARTICLE_SYSTEM & ps, int midx, int didx, int mlb, int mub, int dlb, int dub);
	DATA_TYPE VolumeChangeEnergy(PARTICLE_SYSTEM& ps, int mlb, int mub, int dlb, int dub, int nodeId, DATA_TYPE& virialPressure);

	// utility methods to access the common model potential constants
	int getMonomersPerChain() { return monomers_per_chain; }
	DATA_TYPE getA0() { return a0; }
	DATA_TYPE getDe() { return De; }
	DATA_TYPE getR0() { return r0; }
	DATA_TYPE getAlpha_() { return alpha_; }
	DATA_TYPE getDpMag() { return dp_mag; }
	DATA_TYPE getK_Theta() { return k_theta; }
	DATA_TYPE getTheta_0() { return theta_0; }
	DATA_TYPE getK1() { return k1; }
	DATA_TYPE getK2() { return k2; }
	DATA_TYPE getGamma_0() { return gamma_0; }
	DATA_TYPE getEpsilon_Intra() { return epsilon_intra; }
	DATA_TYPE getSigma_Intra() { return sigma_intra; }
	DATA_TYPE getK4() { return k4; }
	DATA_TYPE getK5() { return k5; }
	DATA_TYPE getK5_End() { return k5_end; }
	DATA_TYPE getQ_Mono() { return Q_mono; }
	DATA_TYPE getQ_Dop() { return Q_dop; }
	DATA_TYPE getKappa() { return kappa; }
	DATA_TYPE getA() { return A; }
	DATA_TYPE getB() { return B; }
	DATA_TYPE getC() { return C; }
	DATA_TYPE getSigmaPyDopant() { return sigmaPyDopant; }
	DATA_TYPE getEpsilon_Inter() { return epsilon_inter; }
	DATA_TYPE getSigma_Inter() { return sigma_inter; }
	DATA_TYPE getEpsilon_Inter_End() { return epsilon_inter_end; }
	DATA_TYPE getSigma_inter_End() { return sigma_inter_end; }
	DATA_TYPE getEpsilon_Dopant() { return epsilon_dopant; }
	DATA_TYPE getSigma_Dopant() { return sigma_dopant; }
	DATA_TYPE getKappaIntra() { return intra_kappa; }
	DATA_TYPE getKappaInterEnd() { return kappa_inter_end; }

private:
	void pairwiseIntraTerms(PARTICLE_SYSTEM & ps, int midx, DATA_TYPE & bond, DATA_TYPE & bend, DATA_TYPE & torsion, DATA_TYPE & coulomb, DATA_TYPE & rotation);

	int monomers_per_chain;

	DATA_TYPE a0;
	DATA_TYPE De;
	DATA_TYPE r0;
	DATA_TYPE alpha_;
	DATA_TYPE dp_mag;
	DATA_TYPE k_theta;
	DATA_TYPE theta_0;
	DATA_TYPE k1;
	DATA_TYPE k2;
	DATA_TYPE gamma_0;
	DATA_TYPE epsilon_intra;
	DATA_TYPE sigma_intra;
	DATA_TYPE k4;
	DATA_TYPE k5;
	DATA_TYPE k5_end;
	DATA_TYPE Q_mono;
	DATA_TYPE Q_dop;
	DATA_TYPE QSqrtd;
	DATA_TYPE kappa;
	DATA_TYPE A;
	DATA_TYPE B;
	DATA_TYPE C;
	DATA_TYPE sigmaPyDopant;
	DATA_TYPE epsilon_inter;
	DATA_TYPE sigma_inter;
	DATA_TYPE epsilon_inter_end;
	DATA_TYPE sigma_inter_end;
	DATA_TYPE epsilon_dopant;
	DATA_TYPE sigma_dopant;
	DATA_TYPE sigma_dopant_9;

	int intra_coul_offset;
	DATA_TYPE intra_kappa;
	DATA_TYPE kappa_inter_end;
	bool self_interaction;

};
#endif

