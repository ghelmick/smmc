#if !defined (__UTIL_H__)
#define       __UTIL_H__

#include <string>
#include <vector>
#include <ios>
#include <iostream>
#include <random>
#include <fstream>
#include <cmath>
#include <mpi.h>

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::ofstream;

#define DATA_TYPE double
#define MPI_DATA_TYPE MPI_DOUBLE

#define K_ .00008617332478
#define AVOGADRO_NUM 6.02214129e23
#define EVOLTS_PER_HARTREE 27.21138505
#define PI                 3.14159265358979323846
#define G				   9.8067
#define DEG_TO_RAD(angle)  (angle * PI / 180.0)
#define RAD_TO_DEG(radian) (radian * 180.0 / PI)
#define SIGN(a) ((a < 0.0f) ? -1.0 : 1.0)
#define SIGN2(a, b) ((b > 0.0) ? abs(a) : -abs(a))
#define SQUARE(X) X * X
#define CUBE(X) (X * X * X)
#define MAGNITUDE(X, Y, Z) sqrt(SQUARE(X) + SQUARE(Y) + SQUARE(Z))
#define DOT_PRODUCT(X1, Y1, Z1, X2, Y2, Z2) (X1*X2 + Y1*Y2 + Z1*Z2)
#define COS_SQRD(THETA) (0.5 + 0.5 * cos(2.0 *THETA))
#define NINT(a) ((a) >= 0.0 ? (int)((a)+0.5) : (int)((a)-0.5))
#define MAX(a,b) ((a > b) ? a : b)
#define MIN(a,b) ((a < b) ? a : b)
#define TO_POW_2(X) (X * X)
#define TO_POW_3(X) (X * X * X)
#define TO_POW_6(X) (X * X * X * X * X * X)
#define HA_TO_EV(X) (X * EVOLTS_PER_HARTREE)
#define EV_TO_HA(X) (X / EVOLTS_PER_HARTREE)
#define DEBEYE_TO_HA(X) (X * 0.393430307)
#define A_0 0.52917720859
#define A_0_INV 1.889726132885643067222711130708
#define KEL_PER_HAR 315775.13
#define ANG_TO_BOHR(X) (X * A_0_INV)
#define BOHR_TO_ANG(X) (X * A_0)
#define NUM_BINS 180
//#define ANGLE_LIMIT 10.0
#define ATM_TO_PA(X) (X * 101325)        // Pa
#define ATM_TO_GPA(X) (X * 0.000101325)        // GPa
#define GPA_TO_ATM(X) ( X / 0.000101325)
#define GPA_TO_ATU(X) (X / 29421.02648438959)  // Ha/Bohr^3
#define ATU_TO_GPA(X) (X * 29421.02648438959)
#define HA_PER_BOHR_TO_EV_PER_ANG(X) (X * 51.42208619083232)
#define EV_PER_ANG_TO_NEWTON(X) (X * 1.60217662e-9)
#define PCB_(SIZE, X) (X - SIZE * NINT(X / SIZE))  // only usable if system origin is the lower left corner of the box

inline void crossProduct(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1,
	                     DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2,
	                     DATA_TYPE &cx, DATA_TYPE & cy, DATA_TYPE & cz)
{
	cx = y1 * z2 - z1 * y2;
	cy = z1 * x2 - x1 * z2;
	cz = x1 * y2 - y1 * x2;
}

inline DATA_TYPE dotProduct(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1,
	DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2)
{
	return (x1 * x2 + y1 * y2 + z1 * z2);
}

inline DATA_TYPE dist(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1, DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2)
{
	DATA_TYPE xdif = x1 - x2;
	DATA_TYPE ydif = y1 - y2;
	DATA_TYPE zdif = z1 - z2;

	return sqrt(xdif * xdif + ydif * ydif + zdif * zdif);
}

inline void normalize(DATA_TYPE x, DATA_TYPE y, DATA_TYPE z, DATA_TYPE & nx, DATA_TYPE & ny, DATA_TYPE & nz)
{
	DATA_TYPE mag = MAGNITUDE(x, y, z);
	DATA_TYPE mag_inv = 1.0 / mag;
	if (mag > 0.0)
	{
		nx = x * mag_inv;
		ny = y * mag_inv;
		nz = z * mag_inv;
	}
	else
	{
		nx = 0.0;
		ny = 0.0;
		nz = 0.0;
	}
}

inline DATA_TYPE angleBetween2Vectors(DATA_TYPE x1, DATA_TYPE y1, DATA_TYPE z1,
	DATA_TYPE x2, DATA_TYPE y2, DATA_TYPE z2)
{
	if (x1 == x2 && y1 == y2 && z1 == z2)
		return 0.0;
	if (x1 == 0 && y1 == 0 && z1 == 0)
		return 0.0;
	if (x2 == 0 && y2 == 0 && z2 == 0)
		return 0.0;

	DATA_TYPE dp = dotProduct(x1, y1, z1, x2, y2, z2) / (MAGNITUDE(x1, y1, z1) * MAGNITUDE(x2, y2, z2));
	if (fabs(dp) > 1.0)
		dp = (int)dp;

	DATA_TYPE angle = acos(dp);

	return angle;
}

inline vector<vector<DATA_TYPE> > createHistogram(DATA_TYPE * values, int numValues, int numBins)
{
	if (values == NULL)
		return vector<vector<DATA_TYPE> >();

	vector<vector<DATA_TYPE> > bins(numBins);
	DATA_TYPE maxV = 0.0, minV = 0.0, dt = 0.0;
	maxV = minV = values[0];

	for (int i = 0; i < numValues; i++)
	{
		if (values[i] < minV)
			minV = values[i];
		if (values[i] > maxV)
			maxV = values[i];
	}
	dt = (ceil(maxV) - floor(minV)) / numBins;

	int bidx = 0;

	for (int i = 0; i < numValues; i++)
	{
		bidx = (int)(values[i] / dt);
		if (bidx >= numBins)
			continue;
		bins[bidx].push_back(values[i]);
	}

	return bins;
}

inline vector<vector<DATA_TYPE> > createHistogram(DATA_TYPE * values, int numValues, int numBins, float min, float max)
{
	if (values == NULL)
		return vector<vector<DATA_TYPE> >();

	vector<vector<DATA_TYPE> > bins(numBins);
	DATA_TYPE dt = 0.0;
	dt = (ceil(max) - floor(min)) / numBins;

	int bidx = 0;

	for (int i = 0; i < numValues; i++)
	{
		bidx = (int)(values[i] / dt);
		if (bidx >= numBins)
			continue;
		bins[bidx].push_back(values[i]);
	}

	return bins;
}

inline void Tokenizer(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
	// Skip delimiters at beginning.
	string::size_type lastPos = str.find_first_not_of(delimiters, 0);

	// Find first "non-delimiter".
	string::size_type pos = str.find_first_of(delimiters, lastPos);

	while (string::npos != pos || string::npos != lastPos)
	{
		// Found a token, add it to the vector.
		tokens.push_back(str.substr(lastPos, pos - lastPos));

		// Skip delimiters.  Note the "not_of"
		lastPos = str.find_first_not_of(delimiters, pos);

		// Find next "non-delimiter"
		pos = str.find_first_of(delimiters, lastPos);
	}
}

// function to find factorial of given number 
inline unsigned long factorial(unsigned long n)
{
	if (n == 0)
		return 1;
	return n * factorial(n - 1);
}
#endif
