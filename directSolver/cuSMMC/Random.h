#if !defined (__RANDOM_H__)
#define       __RANDOM_H__

#include <string>
#include <fstream>
#include <random>
#include "MyRandom.h"

using std::string;
using std::ofstream;

#if defined USE_TEST_DATA
class TestData
{
public:
	TestData() {}
	TestData(const string & testData)
	{
		inData = ifstream(testData.c_str());
		max_col = 85;
		col = max_col;
		rowId = 0;
	}

	DATA_TYPE getValue()
	{
		if (col == max_col)
		{
			col = 1;
			data = vector<DATA_TYPE>();
			readData();
			rowId++;
		}

		return data[col++];
	}

	int getColCount()
	{
		return max_col;
	}

private:
	void readData()
	{
		if (inData.is_open())
		{
			std::string line;
			if (!inData.eof())
			{
				std::getline(inData, line);
				DATA_TYPE v = 0.0;
				stringstream ss(line);
				while (ss >> v)
				{
					data.push_back(v);
				}
			}
		}
	}

	int rowId;
	int col;
	int max_col;
	vector<DATA_TYPE> data;
	ifstream inData;
};

TestData tData;

#endif

class Random
{
public:
	static Random & getInstance()
	{
		static Random instance;
		return instance;
	}

	// The copy constructor is deleted, to prevent client code from creating new
	// instances of this class by copying the instance returned by get_instance()
	Random(Random const&) = delete;

	// The move constructor is deleted, to prevent client code from moving from
	// the object returned by get_instance(), which could result in other clients
	// retrieving a reference to an object with unspecified state.
	Random(Random&&) = delete;

	~Random();

	DATA_TYPE randomDouble();

private:
	Random();

	std::mt19937 generator;
	std::uniform_real_distribution<DATA_TYPE> dis;

	MyRandom myRandom;
};

#endif

