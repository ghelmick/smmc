#if !defined (__POLYMER_UTIL_H__)
#define       __POLYMER_UTIL_H__

#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include "util.h"

using std::ios_base;
using std::ofstream;
using std::string;

struct AppConfig;

typedef struct ParticleSystem
{
	DATA_TYPE * olig_cm_x;
	DATA_TYPE * olig_cm_y;
	DATA_TYPE * olig_cm_z;

	DATA_TYPE * cm_x;
	DATA_TYPE * cm_y;
	DATA_TYPE * cm_z;

	DATA_TYPE * n_x;
	DATA_TYPE * n_y;
	DATA_TYPE * n_z;

	DATA_TYPE * dpm_x;
	DATA_TYPE * dpm_y;
	DATA_TYPE * dpm_z;

	DATA_TYPE * heading;
	DATA_TYPE * pitch;
	DATA_TYPE * bank;

	DATA_TYPE * z0;

	DATA_TYPE * dop_x;
	DATA_TYPE * dop_y;
	DATA_TYPE * dop_z;

	DATA_TYPE * torsionAngleHistogram;
	DATA_TYPE * bendingAngleHistogram;

	DATA_TYPE x_size;
	DATA_TYPE y_size;
	DATA_TYPE z_size;
	DATA_TYPE uc_x;
	DATA_TYPE uc_y;
	DATA_TYPE uc_z;
	DATA_TYPE r_cut;
	DATA_TYPE angleLimit;
	DATA_TYPE density;

	int monomers_per_chain;
	int nPolymers;
	int nMonomers;
	int nDopants;

	void cleanUp()
	{
		if (olig_cm_x)
			delete olig_cm_x;

		if (olig_cm_y)
			delete olig_cm_y;

		if (olig_cm_z)
			delete olig_cm_z;
		
		if (cm_x)
			delete cm_x;

		if (cm_y)
			delete cm_y;

		if (cm_z)
			delete cm_z;
		
		if (n_x)
			delete n_x;
		if (n_y)
			delete n_y;
		if (n_z)
			delete n_z;
		
		if (dpm_x)
			delete dpm_x;
		if (dpm_y)
			delete dpm_y;
		if (dpm_z)
			delete dpm_z;
		
		if (heading)
			delete heading;

		if (pitch)
			delete pitch;
		if (bank)
			delete bank;
		
		if (z0)
			delete z0;
		
		if (dop_x)
			delete dop_x;
		if (dop_y)
			delete dop_y;
		if (dop_z)
			delete dop_z;
	
		if (torsionAngleHistogram)
			delete torsionAngleHistogram;
		if (bendingAngleHistogram)
			delete bendingAngleHistogram;

	}

} PARTICLE_SYSTEM;

typedef struct Monomer
{
	DATA_TYPE * x;
	DATA_TYPE * y;
	DATA_TYPE * z;

	char * name;
	int nAtoms;
} MONOMER;

typedef struct SimulationResults
{
	DATA_TYPE intraMean;
	DATA_TYPE intraStdDev;
	DATA_TYPE interMean;
	DATA_TYPE interStdDev;
	DATA_TYPE totalMean;
	DATA_TYPE enthalpyMean;
	DATA_TYPE totalStdDev;
	DATA_TYPE enthalpyStdDev;
	DATA_TYPE densityMean;
	DATA_TYPE densityStdDev;

	DATA_TYPE intra;
	DATA_TYPE inter;
	DATA_TYPE total;
	DATA_TYPE enthalpy;
	DATA_TYPE density;

	DATA_TYPE monomerStepSize;
	DATA_TYPE dopantStepSize;
	DATA_TYPE volumeChangeFactor;

	SimulationResults()
	{
		intraMean = 0.0;
		intraStdDev = 0.0;
		interMean = 0.0;
		interStdDev = 0.0;
		totalMean = 0.0;
		enthalpyMean = 0.0;
		totalStdDev = 0.0;
		enthalpyStdDev = 0.0;
		densityMean = 0.0;
		densityStdDev = 0.0;

		intra = 0.0;
		inter = 0.0;
		total = 0.0;
		enthalpy = 0.0;

		monomerStepSize = 0.0;
		dopantStepSize = 0.0;
		volumeChangeFactor = 0.0;
	}

} SIMULATION_RESULTS;

void setAngleLimit(DATA_TYPE angleLimit_);
void setUsePCB(bool usePCB_ = true);
void setRhomboidPCB(bool rhomboidPCB_);
void PCB_ShearTransform(DATA_TYPE cmx, DATA_TYPE cmy, DATA_TYPE cmz, DATA_TYPE angle, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z);
void PCB_Rhombus(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z);
void PCB(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z);
void wrapInBox(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z);
void WrapInBox_Rhombus(DATA_TYPE sizex, DATA_TYPE sizey, DATA_TYPE sizez, DATA_TYPE& x, DATA_TYPE& y, DATA_TYPE& z);
//DATA_TYPE wrapInBox(DATA_TYPE x, DATA_TYPE lb, DATA_TYPE ub);
DATA_TYPE averagePolymerLength(PARTICLE_SYSTEM & ps);
void calculateOrderParameters(PARTICLE_SYSTEM & ps, DATA_TYPE & vectorOrder, DATA_TYPE & orientationOrder);
void calculatePositionalDisorder(PARTICLE_SYSTEM & ps, const AppConfig & appConfig,
	                               DATA_TYPE & lx, DATA_TYPE & ly, DATA_TYPE & lz, DATA_TYPE & lambda,
	                               DATA_TYPE & dlx, DATA_TYPE & dly, DATA_TYPE & dlz, DATA_TYPE & dlambda);
DATA_TYPE calculateRadiusOfGyration(PARTICLE_SYSTEM & ps);
void collectParticleSystemStatistics(PARTICLE_SYSTEM & ps, const AppConfig & appConfig, ofstream & systemStatsFile);
PARTICLE_SYSTEM * buildParticleSystem(const AppConfig & appConfig, int xdim = 1, int ydim = 1, int zdim = 1, bool addDopants = false);
PARTICLE_SYSTEM* buildParticleSystemFromReferenceUnitCell(const AppConfig& appConfig, const string& referenceCell, int xdim = 1, int ydim = 1, int zdim = 1, bool addDopants = true);
PARTICLE_SYSTEM * buildReferencedChain(const AppConfig & appConfig, bool addDopants = false);
PARTICLE_SYSTEM * yosephSingleChain(const AppConfig & appConfig);
void initReferenceMonomers();
void saveHistograme(PARTICLE_SYSTEM & ps, unsigned int numIterations, const string & fName, ios_base::openmode mode = ios_base::out);
void saveParticleSystem(PARTICLE_SYSTEM & ps, const string & fName, ios_base::openmode mode = ios_base::out);
void saveParticleSystemCM(PARTICLE_SYSTEM & ps, const string & fName, ios_base::openmode mode = ios_base::out);
void saveParticleSystemToBinaryFile(PARTICLE_SYSTEM &ps, const string & fName);
PARTICLE_SYSTEM * loadParticleSystemBinaryFile(const string & fName, AppConfig & appConfig);
void printParticleSystem(PARTICLE_SYSTEM & ps);
void calculateMove(DATA_TYPE & dx, DATA_TYPE & dy, DATA_TYPE & dz, DATA_TYPE stepSize);
void rotatePoint(DATA_TYPE heading, DATA_TYPE pitch, DATA_TYPE bank, DATA_TYPE & xx, DATA_TYPE & yy, DATA_TYPE & zz);
void randomRotationAxis(DATA_TYPE magnitude, DATA_TYPE & heading, DATA_TYPE & pitch, DATA_TYPE & bank, DATA_TYPE angleLimit = 20.0);
void translateOligomer(PARTICLE_SYSTEM & ps, int idx, DATA_TYPE dx, DATA_TYPE dy, DATA_TYPE dz, bool moveDopants = false);
void rotateOligomer(PARTICLE_SYSTEM& ps, int idx, DATA_TYPE magnitude, bool moveDopants);
void rotateMonomer(PARTICLE_SYSTEM & ps, int mIdx, DATA_TYPE magnitude);
void translateMonomer(PARTICLE_SYSTEM & ps, int idx, DATA_TYPE stepSize);
void translateDopant(PARTICLE_SYSTEM & ps, int idx, DATA_TYPE stepSize);
void reverseRotateMonomer(PARTICLE_SYSTEM & ps, int mIdx);
void collectGofR(PARTICLE_SYSTEM & ps, unsigned int * gofrMonomerCM, unsigned int * gofrMonomer, unsigned int * gofrDopant, unsigned int * gofrAll, unsigned int numIterations, unsigned int numBins, DATA_TYPE gofrDR);
void generateGofR(PARTICLE_SYSTEM & ps, const string fname, unsigned int * gofrMonomerCM, unsigned int * gofrMonomer, unsigned int * gofrDopant, unsigned int * gofrAll, unsigned int numIterations, unsigned int numBins, DATA_TYPE gofrDR);
void recenterParticleSystem(PARTICLE_SYSTEM & ps, DATA_TYPE & dx, DATA_TYPE & dy, DATA_TYPE & dz);
void setParticleSystemDensity(PARTICLE_SYSTEM & ps, DATA_TYPE targetDensity);
DATA_TYPE calculateDensity(PARTICLE_SYSTEM & ps, DATA_TYPE & sizex, DATA_TYPE & sizey, DATA_TYPE & sizez);
void calculateOligomersCenterOfMasses(PARTICLE_SYSTEM & ps);
void calculateOligomerCenterOfMasses(PARTICLE_SYSTEM & ps, int mIdx);
DATA_TYPE * scaleSystemVolume(PARTICLE_SYSTEM & ps, bool* scaleDimension, DATA_TYPE scale = 0.0008, bool random = true, bool updateCutoff = true);
DATA_TYPE pressureFromGofR(PARTICLE_SYSTEM & ps, unsigned int * gofrAll, unsigned int numIterations, unsigned int numBins, DATA_TYPE gofrDR);
DATA_TYPE * scaleSystem(PARTICLE_SYSTEM & ps, bool* scaleDimension, DATA_TYPE volumeScaleFactor, bool random = true, bool updateCutoff = true);
void scaleParticleSystem(PARTICLE_SYSTEM& ps, DATA_TYPE * scaleFactor, bool updateCutoff = true);
void broadCastParticleSystem(PARTICLE_SYSTEM& ps);
PARTICLE_SYSTEM * clone(const PARTICLE_SYSTEM & ps, const AppConfig & appConfig);
void calculateMSD(const PARTICLE_SYSTEM & ps, const PARTICLE_SYSTEM & reference, DATA_TYPE & monomer_msd, DATA_TYPE & dopant_msd, DATA_TYPE & oligomer_msd);

PARTICLE_SYSTEM * buildRenneReferencedCell(const AppConfig & appConfig, bool addDopants);
PARTICLE_SYSTEM * buildRenneParticleSystem(const AppConfig & appConfig, int xdim, int ydim, int zdim, bool addDopants);
PARTICLE_SYSTEM * buildStackedParticleSystem(const AppConfig & appConfig, int xdim, int ydim, int zdim, bool addDopants);
PARTICLE_SYSTEM * buildRotatedStackedParticleSystem(const AppConfig & appConfig, int xdim, int ydim, int zdim, bool addDopants);

PARTICLE_SYSTEM* loadParticleSystemRestartFile(const string& fName, AppConfig& appConfig);
void getNormal(int midx, DATA_TYPE x, DATA_TYPE y, DATA_TYPE z, DATA_TYPE& nx, DATA_TYPE& ny, DATA_TYPE& nz);
void rotateMonomer(PARTICLE_SYSTEM& ps, int mIdx, DATA_TYPE heading, DATA_TYPE pitch, DATA_TYPE bank);

void calculateMeanMonomerDopantClosestDistance(const PARTICLE_SYSTEM & ps, DATA_TYPE & avgMinDist, DATA_TYPE & minx, DATA_TYPE & maxx, DATA_TYPE & miny, DATA_TYPE & maxy, DATA_TYPE & minz, DATA_TYPE & maxz);
void calculateBoundingInformation(PARTICLE_SYSTEM& ps, DATA_TYPE& cx, DATA_TYPE& cy, DATA_TYPE& cz,
	DATA_TYPE& minx, DATA_TYPE& miny, DATA_TYPE& minz,
	DATA_TYPE& maxx, DATA_TYPE& maxy, DATA_TYPE& maxz);
void translateParticleSystem(PARTICLE_SYSTEM& ps, DATA_TYPE dx, DATA_TYPE dy, DATA_TYPE dz);

void generateOligomerGroupings(PARTICLE_SYSTEM& ps, int groupSize);
void saveDistanceHistogram(PARTICLE_SYSTEM& ps, const string& fName, ios_base::openmode mode = ios_base::out);
void PyPyImageInteraction(PARTICLE_SYSTEM& ps, int midx, vector<DATA_TYPE>& distances, int& dist_idx);
void centerParticleSystem(PARTICLE_SYSTEM& ps);

#endif

